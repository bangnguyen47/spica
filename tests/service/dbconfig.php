<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Mock configuration for unit tests.
 *
 * @category   spica
 * @package    tests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: dbconfig.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
$GLOBALS['__ds__'] = array(
  'db1' => array(
     'dbhost'     => 'localhost',
     'db'         => 'spica',
     'dbuser'     => 'root',
     'dbpassword' => '123456',
     'dbvendor'   => 'mysql',
     'dbcharset'  => 'utf8',
     'dbport'     => '3306',
     'dbsocket'   => null,
     'dbflag'     => null,
     'dbssl'      => null,
     'dbreport'   => 'all'),

  'db2' => array(
     'dbhost'     => 'localhost',
     'db'         => 'spica',
     'dbuser'     => 'root',
     'dbpassword' => '123456',
     'dbvendor'   => 'mysql',
     'dbcharset'  => 'utf8',
     'dbport'     => '3306',
     'dbsocket'   => null,
     'dbflag'     => null,
     'dbssl'      => null,
     'dbreport'   => 'all'),
);
?>