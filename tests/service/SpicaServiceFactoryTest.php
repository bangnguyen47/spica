<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().PATH_SEPARATOR.
$baseDir.PATH_SEPARATOR.
$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/service/ServiceFactory.php';
include_once 'core/test/Test.php';

/**
 * Set of test cases and assertions to ensure that SpicaServiceFactory works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage service
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 21, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaServiceFactoryTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * Base directory.
     *
     * @var string
     */
    public $baseDir;

    /**
     * (non-PHPdoc)
     * @see tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    public function setUp()
    {
        $baseDir   = dirname(dirname(dirname(__FILE__)));
        $simulator = new SpicaMockEnvironment($baseDir.'/index.php');
        $simulator->setSubdirectory('/spica/trunk');
        $simulator->setUrl('http://localhost/spica/trunk');
        $simulator->run();
    }

    /**
     * Asserts that a non-existent application service class file will lead to
     * an exception.
     *
     * @test
     * @expectedException SpicaClassNotFoundException
     */
    public function test1()
    {
        $factory = new SampleServiceFactory();
        $service = $factory->create('db1');
    }

    /**
     * Asserts that a non-existent application service class will lead to
     * an exception.
     *
     * @test
     * @expectedException SpicaClassNotFoundException
     */
    public function test2()
    {
        $factory = new Sample2ServiceFactory(dirname(__FILE__).'/sample/mysql');
        $service = $factory->create('db1');
    }

    /**
     * Asserts that an application service class will can be loaded.
     *
     * @test
     */
    public function test3()
    {
        $factory = new SampleServiceFactory(dirname(__FILE__).'/sample/mysql');
        $service = $factory->create('db1');
        $this->assertEquals(true, $service instanceof SampleService);

        // Test second load
        $service = $factory->create('db1');
        $this->assertEquals(true, $service instanceof SampleService);
    }
}

/**
 * Sample implementation of SpicaServiceFactory
 *
 * @category   spica
 * @package    tests
 * @subpackage service
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 21, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaServiceFactoryTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SampleServiceFactory extends SpicaServiceFactory
{

}

/**
 * Sample implementation of SpicaServiceFactory
 *
 * @category   spica
 * @package    tests
 * @subpackage service
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 21, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaServiceFactoryTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Sample2ServiceFactory extends SpicaServiceFactory
{

}

?>