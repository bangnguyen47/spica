<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
    get_include_path() .
    PATH_SEPARATOR . $baseDir . '/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/net/AsyncHttpClient.php';

/**
 * Set of test cases and assertions to ensure that SpicaAsyncHttpClient works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/net
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 10, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaAsyncHttpClientTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAsyncHttpClientTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests if UTF-8 encoded URL can be fetched.
     */
    public function test1()
    {
        $urls = array(
            "http://www.cnn.com/",
            "http://www.google.com/",
            "http://www.yahoo.com/"
        );

        $client = new SpicaAsyncHttpClient();

        foreach ($urls as $url)
        {
            $req = new SpicaHttpRequest();
            $req->headerRequired = true;
            $req->storeSentHeaders = true;
            $req->followLocation = true;
            $req->simulateBrowser();
            $req->setUrl($url);
            $req->setHeaders(array(
                'Content-Type: text/html; charset=UTF-8',
            ));

            $client->add($req);
        }

        $future = $client->sendRequest();
        $responses = $future->getResponses();

        foreach ($responses as $response)
        {
            /* @var $response SpicaHttpResponse */            
            $len = strlen($response->getBody());            
            $this->assertTrue($len > 1024 * 2); // 2 KBytes
            $this->assertEquals(200, $response->getHttpCode());
        }

        $client->close();
    }
}

?>