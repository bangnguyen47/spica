<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/net/HttpClient.php';

/**
 * Set of test cases and assertions to ensure that SpicaHttpClient works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/net
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaHttpClientTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaHttpClientTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests if UTF-8 encoded URL can be fetched.
     */
    public function test1()
    {
        $url = 'http://media.imuzik.com.vn//Resources/Data/MusicOnline/Singers/Images/CasiVietNam/Xu%C3%A2n_Hi%E1%BA%BFu.jpg';
        $req = new SpicaHttpRequest();
        $req->setUrl($url);
        $req->setHeaders(array(
          'Content-Type: text/html; charset=UTF-8',
        ));

        $client   = new SpicaHttpClient();
        $response = $client->sendRequest($req);
        $this->assertEquals(200, $response->getHttpCode());
    }

    /**
     * Tests if Yahoo.com (301 followed by 302) redirected URL can be fetched.
     */
    public function test2()
    {
        $url = 'http://www.yahoo.com/';
        $req = new SpicaHttpRequest();
        $req->followLocation = false;
        $req->setUserAgent('AAAAA');
        $req->setUrl($url);
        $req->setHeaders(array(
          'Content-Type: text/html; charset=UTF-8',
        ));

        $client   = new SpicaHttpClient();
        $response = $client->sendRequest($req);
        $this->assertEquals(302, $response->getHttpCode());
    }
}

?>