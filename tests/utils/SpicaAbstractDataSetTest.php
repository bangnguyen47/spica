<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/ContainerUtils.php';
include_once dirname(__FILE__).'/SpicaArrayListTest.php';

/**
 * Set of test cases and assertions to ensure that SpicaAbstractDataSet works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaAbstractDataSetTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 09, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaAbstractDataSetTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAbstractDataSetTest extends SpicaArrayListTest
{
    /**
     * SpicaAbstractDataSet fixtures.
     *
     * @var ConcreateSpicaAbstractDataSetTest
     */
    protected $_arrayList;

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    public function setUp()
    {
        $this->_arrayList = new ConcreateSpicaAbstractDataSetTest();
    }

    /**
     * Asserts that SpicaAbstractDataSet::getMessage() works as desired.
     *
     * @test
     */
    public function testGetSetMessage()
    {
        $message = 'Hello World';
        $this->_arrayList->setMessage($message);
        $this->assertEquals($message, $this->_arrayList->getMessage());

        $arrayList = new ConcreateSpicaAbstractDataSetTest();
        $this->assertEquals(null, $arrayList->getMessage());
    }

    /**
     * Asserts that SpicaAbstractDataSet::setMessageWhenEmpty() works as desired.
     *
     * @test
     */
    public function testSetMessageWhenEmpty()
    {
        $arrayList = new ConcreateSpicaAbstractDataSetTest();
        $message   = 'Hello World';
        $arrayList->setMessageWhenEmpty($message);
        $this->assertEquals($message, $arrayList->getMessage());

        $arrayList = new ConcreateSpicaAbstractDataSetTest();
        $message   = 'Hello World2';
        $arrayList->setMessageWhenEmpty($message);
        $this->assertEquals($message, $arrayList->getMessage());

        $arrayList = new ConcreateSpicaAbstractDataSetTest(array(1, 2, 3));
        $message   = 'Hello World3';
        $arrayList->setMessageWhenEmpty($message);
        $this->assertEquals(null, $arrayList->getMessage());
    }

    /**
     * Asserts that SpicaAbstractDataSet::setReadable() and
     * SpicaAbstractDataSet::isReadable() works as desired.
     *
     * @test
     */
    public function testSetIsReadable()
    {
        $this->_arrayList->setReadable(true);
        $this->assertEquals(true, $this->_arrayList->isReadable());

        $this->_arrayList->setReadable(false);
        $this->assertEquals(false, $this->_arrayList->isReadable());
    }
}

/**
 * Empty implementation of SpicaAbstractDataSet.
 *
 * @category   spica
 * @package    tests
 * @subpackage ConcreateSpicaAbstractDataSetTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 20, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaAbstractDataSetTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class ConcreateSpicaAbstractDataSetTest extends SpicaAbstractDataSet
{

}
?>