<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/FileReader.php';

/**
 * Set of test cases and assertions to ensure that SpicaFileReader works as desired.
 *
 * @category   spica
 * @package    tests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      October 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaFileReaderTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFileReaderTest extends PHPUnit_Framework_TestCase
{
    /**
     * SpicaFileReader fixtures.
     *
     * @var SpicaFileReader
     */
    protected $_reader;

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    protected function setUp()
    {
        $this->_reader = new SpicaFileReader("sample2.txt");
    }

    protected function tearDown()
    {
        $this->_reader->close();
    }

    /**
     * Tests to ensure that the reader can move the file pointer to a certain line in a file.
     *
     * @test
     */
    public function testMoveToLine0()
    {
        $this->_reader->moveToLine(0);
        $this->assertEquals($this->_reader->getCurrentLine(), 0);
        $this->assertEquals(trim($this->_reader->readCurrentLine()), '123456');
    }

    /**
     * Tests to ensure that the reader can move the file pointer to a certain line in a file.
     *
     * @test
     */
    public function testMoveToLine1()
    {
        $this->_reader->moveToLine(1);
        $this->assertEquals($this->_reader->getCurrentLine(), 1);
        $this->assertEquals(trim($this->_reader->readCurrentLine()), '123456789');
    }

    /**
     * Tests to ensure that the reader can read a portion of a file.
     *
     * @test
     */
    public function testReadBetween1()
    {
        $this->assertEquals($this->_reader->getCurrentLine(), null);

        $text = $this->_reader->readBetween(0, 1); // 2 lines
        $expected = "123456\n123456789";
        $this->assertEquals($expected, $text);
    }

    /**
     * Tests to ensure that the reader can read a portion of a file.
     *
     * @test
     */
    public function testReadBetween2()
    {
        $this->assertEquals($this->_reader->getCurrentLine(), null);

        $text = $this->_reader->readBetween(1, 3); // 3 lines
        $expected = "123456789\n123\n12345";
        $this->assertEquals($expected, $text);
    }
}

?>