<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/UUIDUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaUUIDUtilsTest works as desired.
 *
 * @category   spica
 * @package    tests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 22, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaUUIDUtilsTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaUUIDUtilsTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests SpicaUUIDUtils::createUUID()
     *
     * @test
     */
    public function test1()
    {
        $uuid = SpicaUUIDUtils::createUUID();
        echo "$uuid\n";
        $this->assertEquals(36, strlen($uuid));
        $this->assertEquals(32, strlen(str_replace('-', '', $uuid)));
        $this->assertEquals(true, SpicaUUIDUtils::isUUID($uuid));
    }

    public function test2()
    {
        /**
         * @see http://www.itu.int/ITU-T/asn1/uuid.html
         */
        $s1 = '79b98da0-658a-11df-81de-0002a5d5c51b';
        $s2 = '9976dbc0-658a-11df-97e8-0002a5d5c51b';
        $s3 = 'a6eac460-658a-11df-8d27-0002a5d5c51b';
        $this->assertTrue(SpicaUUIDUtils::isUUID($s1));
        $this->assertTrue(SpicaUUIDUtils::isUUID($s2));
        $this->assertTrue(SpicaUUIDUtils::isUUID($s3));
    }
}

?>