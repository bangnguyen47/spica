<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/ContainerUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaArrayList works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaArrayListTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 09, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaArrayListTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaArrayListTest extends PHPUnit_Framework_TestCase
{
    /**
     * SpicaArrayList fixtures.
     *
     * @var SpicaArrayList
     */
    protected $_arrayList;

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    protected function setUp()
    {
        $this->_arrayList = new SpicaArrayList();
    }

    /**
     * Asserts that a original array can be retrieved after being populated into
     * a SpicaArrayList.
     *
     * @test
     */
    public function testGetArrayCopy()
    {
        $seed = array(1, 2, 3);
        $this->_arrayList->populate($seed);
        $this->assertEquals($seed, $this->_arrayList->getArrayCopy());

        $seed = array('a' => 1, 'b' => 2, 'c' => 3);
        $this->_arrayList->populate($seed);
        $this->assertEquals($seed, $this->_arrayList->getArrayCopy());

        $seed = array(1, 'b' => 2, 3, 0 => 7);
        $this->_arrayList->populate($seed);
        $this->assertEquals($seed, $this->_arrayList->getArrayCopy());

        $this->_arrayList->populate(array());
        $this->assertEquals(array(), $this->_arrayList->getArrayCopy());
    }

    /**
     * Asserts that a value can be retrieved via get
     *
     * @test
     */
    public function testGetAfterPopulating()
    {
        $seed = array(1, 2, 3);
        $this->_arrayList->populate($seed);
        $this->assertEquals(2, $this->_arrayList->get(1));

        $seed = array('a' => 1, 'b' => 2, 'c' => 3);
        $this->_arrayList->populate($seed);
        $this->assertEquals(1, $this->_arrayList->get('a'));

        $seed = array(1, 'b' => 2, 3, 0 => 7);
        $this->_arrayList->populate($seed);
        $this->assertEquals(7, $this->_arrayList->get(0));
    }

    /**
     * Asserts that a value can be set and get with the same value
     *
     * @test
     */
    public function testSetGet()
    {
        // Empty internal array
        $this->_arrayList->populate(array());
        $this->_arrayList->set('x', 7);
        $this->assertEquals(7, $this->_arrayList->get('x'));

        $this->_arrayList->set('x', 10);
        $this->assertEquals(10, $this->_arrayList->get('x'));

        $this->_arrayList->set('x', $x = new StdClass());
        $this->assertEquals(new StdClass(), $this->_arrayList->get('x'));
        $this->assertNotSame(new StdClass(), $this->_arrayList->get('x'));
        $this->assertSame($x, $this->_arrayList->get('x'));

        $this->_arrayList->set('x', $x = array(1, 2, 3, 4, 5));
        $this->assertEquals(array(1, 2, 3, 4, 5), $this->_arrayList->get('x'));
        $this->assertSame(array(1, 2, 3, 4, 5), $this->_arrayList->get('x'));
        $this->assertSame($x, $this->_arrayList->get('x'));
    }

    /**
     * Asserts that a value can be set with a key and after set the key must exist
     *
     * @test
     */
    public function testHasKey()
    {
        // Empty internal array
        $this->_arrayList->populate(array());
        $this->_arrayList->set('x', 7);
        $this->assertEquals(true, $this->_arrayList->hasKey('x'));

        $this->_arrayList->set('x2', 7);
        $this->assertEquals(true, $this->_arrayList->hasKey('x2'));
    }

    /**
     * Asserts that index of a already set value can be retrieved.
     *
     * @test
     */
    public function testIndexOf()
    {
        $x = new StdClass();
        $this->_arrayList->populate(array());
        $this->_arrayList->set(1, 7);
        $this->_arrayList->set(2, $x);
        $this->assertEquals(2, $this->_arrayList->indexOf($x));
    }

    /**
     * Asserts that a value at a key is changed after a update.
     *
     * @test
     */
    public function testUpdate()
    {
        $seed = array('a', 'b', 'c', 'x' => 'y', 'k' => 454);
        $this->_arrayList->populate($seed);
        $updateData = array('c', 'x' => 6, 'x2' => 454);
        $this->_arrayList->update($updateData);
        $arrayAfterMerge = array('c', 'b', 'c', 'x' => 6, 'x2' => 454, 'k' => 454);
        $this->assertEquals($arrayAfterMerge, $this->_arrayList->getArrayCopy());
    }

    /**
     * Asserts that removeAll() has to make the ArrayList empty
     *
     * @test
     */
    public function testRemoveAll()
    {
        $seed = array('a', 'b', 'c', 'x' => 'y', 'k' => 454);
        $this->_arrayList->update($seed);
        $this->_arrayList->removeAll();
        $this->assertEquals(array(), $this->_arrayList->getArrayCopy());
    }

    /**
     * Asserts that a key-value entry can be removed.
     *
     * @test
     */
    public function testRemove()
    {
        $this->_arrayList->removeAll();
        $seed = array('a', 'b', 'c', 'x' => 'y', 'k' => 454);
        $this->_arrayList->update($seed);
        $this->_arrayList->remove(0);
        $this->assertEquals(array(1 => 'b', 2 => 'c', 'x' => 'y', 'k' => 454), $this->_arrayList->getArrayCopy());

        $this->_arrayList->remove('x');
        $this->assertEquals(array(1 => 'b', 2 => 'c', 'k' => 454), $this->_arrayList->getArrayCopy());

        $this->assertEquals(false, $this->_arrayList->remove('not_exist'));
    }

    /**
     * Asserts that pop() returns a value at a key if that key exists or
     * returns a default value. Key after being retrieved will be deleted.
     *
     * @test
     */
    public function testPop()
    {
        $this->_arrayList->removeAll();
        $seed = array('a', 'b', 'c', 'x' => 'y', 'k' => 454);
        $this->_arrayList->update($seed);
        $pop1 = $this->_arrayList->pop(0);
        $this->assertEquals(array(1 => 'b', 2 => 'c', 'x' => 'y', 'k' => 454), $this->_arrayList->getArrayCopy());
        $this->assertEquals($pop1, 'a');

        $pop2 = $this->_arrayList->pop('x');
        $this->assertEquals(array(1 => 'b', 2 => 'c', 'k' => 454), $this->_arrayList->getArrayCopy());
        $this->assertEquals($pop2, 'y');

        $this->assertEquals('default value', $this->_arrayList->pop('not_exist', 'default value'));
    }

    /**
     * Asserts that isEmpty() return true when there is no data items.
     *
     * @test
     */
    public function testIsEmpty()
    {
        $seed = array('a', 'b', 'c', 'x' => 'y', 'k' => 454);
        $this->_arrayList->update($seed);
        $this->_arrayList->removeAll();
        $this->assertTrue($this->_arrayList->isEmpty());

        $this->_arrayList->populate(array());
        $this->_arrayList->removeAll();
        $this->assertTrue($this->_arrayList->isEmpty());

        $this->_arrayList->populate(array(1));
        $this->assertFalse($this->_arrayList->isEmpty());

        $this->_arrayList->remove(0);
        $this->assertTrue($this->_arrayList->isEmpty());
    }

    /**
     * Asserts that getKeys() works correctly.
     *
     * @test
     */
    public function testGetKeys()
    {
        $seed = array('a', 'b', 'c', 'x' => 'y', 'k' => 454);
        $this->_arrayList->update($seed);
        $this->assertEquals(array(0, 1, 2, 'x', 'k'), $this->_arrayList->getKeys());
    }
}
?>