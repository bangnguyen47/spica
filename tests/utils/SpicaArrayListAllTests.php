<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.dirname(__FILE__).
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

include_once 'core/utils/ContainerUtils.php';

require_once 'PHPUnit/Util/Filter.php';

PHPUnit_Util_Filter::addFileToFilter(__FILE__);

require_once 'PHPUnit/Framework/TestSuite.php';

PHPUnit_Util_Filter::$filterPHPUnit = FALSE;

/**
 * Set of test suites to ensure that Spica*Validator(s) work as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaArrayListAllTests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 31, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaArrayListAllTests.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaArrayListAllTests extends PHPUnit_Framework_TestSuite
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('Spica ArrayList and the related - All Tests');

        $iter = new DirectoryIterator(dirname(__FILE__));

        foreach ($iter as $file)
        {
            if (!$file->isDot() && !$file->isDir() && false !== strpos($file->getFilename(), 'Test.php'))
            {
                $suite->addTestFile($file->getFilename());
            }
        }

        return $suite;
    }
}
?>
