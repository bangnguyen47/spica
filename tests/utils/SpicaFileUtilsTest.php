<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.dirname(__FILE__).
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/utils/FileUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaFileUtils works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaFileUtilsTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 29, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaFileUtilsTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFileUtilsTest extends PHPUnit_Framework_TestCase
{
    /**
     * Base path.
     *
     * @var string
     */
    protected $_basePath;

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    protected function setUp()
    {
        $this->_basePath = dirname(__FILE__);
    }

    /**
     * Asserts that SpicaFileUtils::deleteDirectory() works.
     *
     * @test
     */
    public function testDeleteDirectory()
    {
        $path = $this->_basePath.'/f2';
        mkdir($path, 0777, true);
        $this->assertEquals(true, file_exists($path) && is_readable($path));
        rmdir($path);
        $this->assertEquals(false, file_exists($path));

        $path = $this->_basePath.'/f3';
        mkdir($path, 0777, true);
        $this->assertEquals(true, SpicaFileUtils::deleteDirectory($path));
        $this->assertEquals(false, file_exists($path));

        $path = $this->_basePath.'/f3/f4';
        mkdir($path, 0777, true);
        $this->assertEquals(true, SpicaFileUtils::deleteDirectory(dirname($path)));
        $this->assertEquals(false, file_exists($path));
    }

    /**
     * Asserts that SpicaFileUtils::deleteDirectory() works.
     *
     * @test
     * @depends testDeleteDirectory
     * @expectedException SpicaFileDeletionException
     */
    public function testDeleteDirectory2()
    {
        $path = $this->_basePath.'/non-existent-yet';
        SpicaFileUtils::deleteDirectory($path);
    }
}
?>