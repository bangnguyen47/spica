<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/StringUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaStringUtils works as desired.
 *
 * @category   spica
 * @package    tests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 09, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaStringUtilsTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaStringUtilsTest extends PHPUnit_Framework_TestCase
{
    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    protected function setUp()
    {
        
    }

    protected function tearDown()
    {
        
    }

    /**
     * Tests SpicaStringUtils::stringToHex() and SpicaStringUtils::hexToString()
     *
     * @test
     */
    public function stringToHexTest()
    {
        // Unicode 1
        $string1 = 'Xin chào nhé';
        $hexed = SpicaStringUtils::stringToHex($string1);
        $this->assertEquals($string1, SpicaStringUtils::hexToString($hexed));

        // Unicode 2
        $string2 = 'Kiểm tra Spica framework lần thứ 2';
        $hexed = SpicaStringUtils::stringToHex($string2);
        $this->assertEquals($string2, SpicaStringUtils::hexToString($hexed));

        // Unicode 3
        $string3 = 'Cà phê số 7 é é ù ư ừ ự ú ủ mè mể mề mệ trụ vững trước cá cạ câ cẩ';
        $hexed = SpicaStringUtils::stringToHex($string3);
        $this->assertEquals($string3, SpicaStringUtils::hexToString($hexed));
    }

    /**
     * Tests SpicaStringUtils::hex2bin() and php#bin2hex()
     *
     * @depends stringToHexTest
     * @test
     */
    public function hex2binTest()
    {
        // Unicode 1
        $string1 = 'Xin chào nhé';
        $hexed = SpicaStringUtils::stringToHex($string1);
        $bin = SpicaStringUtils::hex2bin($hexed);
        $this->assertEquals($hexed, bin2hex($bin));

        // Unicode 2
        $string2 = 'Kiểm tra Spica framework lần thứ 2';
        $hexed = SpicaStringUtils::stringToHex($string2);
        $bin = SpicaStringUtils::hex2bin($hexed);
        $this->assertEquals($hexed, bin2hex($bin));

        // Unicode 3
        $string3 = 'Cà phê số 7 é é ù ư ừ ự ú ủ mè mể mề mệ trụ vững trước cá cạ câ cẩ';
        $hexed = SpicaStringUtils::stringToHex($string3);
        $bin = SpicaStringUtils::hex2bin($hexed);
        $this->assertEquals($hexed, bin2hex($bin));
    }
}

?>