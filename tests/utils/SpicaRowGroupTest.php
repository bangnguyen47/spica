<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.dirname(__FILE__).
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/ContainerUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaRowGroup works as desired.
 *
 * @category   Spica
 * @package    tests
 * @subpackage SpicaRowGroupTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRowGroupTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRowGroupTest extends PHPUnit_Framework_TestCase
{
    /**
     * Fixtures.
     *
     * @var SpicaRowGroup
     */
    protected $_arrayList;

    /**
     * Asserts that a original array can be grouped.
     *
     * @test
     */
    public function testPopulate()
    {
        $array1 = array(
          array('name' => 'Johnny', 'class' => 'x', 'id' => 5),
          array('name' => 'Minh Hoang', 'class' => 'x', 'id' => 6),
          array('name' => 'Ngoc Anh', 'class' => 'y', 'id' => 7),
          array('name' => 'Dikiole', 'class' => 'x', 'id' => 8),
          array('name' => 'Preole', 'class' => 'y', 'id' => 17),
          array('name' => 'Kashima', 'class' => 'z', 'id' => 15),
        );

        $array2 = array(
          new SpicaArrayList(array('class' => 'x', 'student'=> array(
                                             new SpicaArrayList(array('name' => 'Johnny', 'id' => 5)),
                                             new SpicaArrayList(array('name' => 'Minh Hoang', 'id' => 6)),
                                             new SpicaArrayList(array('name' => 'Dikiole', 'id' => 8)),
                                            ))),
          new SpicaArrayList(array('class' => 'y', 'student'=> array(
                                             new SpicaArrayList(array('name' => 'Ngoc Anh', 'id' => 7)),
                                             new SpicaArrayList(array('name' => 'Preole', 'id' => 17)),
                                            ))),
          new SpicaArrayList(array('class' => 'z', 'student'=> array(
                                             new SpicaArrayList(array('name' => 'Kashima', 'id' => 15)),
                                            ))),
        );

        $rowGroup  = new SpicaRowGroup($array1, 'class', 'student', array('class'), array('id', 'name'));
        $processed = $rowGroup->getArrayCopy();
        $this->assertEquals($array2[0]['class'], $processed[0]['class']);
        $this->assertEquals($array2[0]['student'], $processed[0]['student']);
        $this->assertEquals($array2[1]['class'], $processed[1]['class']);
        $this->assertEquals($array2[1]['student'], $processed[1]['student']);
        $this->assertEquals($array2[2]['class'], $processed[2]['class']);
        $this->assertEquals($array2[2]['student'], $processed[2]['student']);
        $this->assertEquals($array2, $processed);
    }
}
?>