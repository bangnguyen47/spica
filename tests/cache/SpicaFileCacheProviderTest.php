<?php

/*
 * Copyright (C) 2009  - 2010 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/utils/FileUtils.php';
include_once 'core/cache/CacheManager.php';
include_once 'core/cache/FileCacheProvider.php';

/**
 * Set of test cases and assertions to ensure that SpicaFileCacheProvider works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaFileCacheProviderTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 23, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaFileCacheProviderTest.php 1799 2010-05-04 17:44:08Z pcdinh $
 */
class SpicaFileCacheProviderTest extends PHPUnit_Framework_TestCase
{
    /**
     * SpicaFileCacheProvider fixtures.
     *
     * @var SpicaFileCacheProvider
     */
    protected $_cache;

    /**
     * Base cache file path.
     *
     * @var string
     */
    protected $_path;

    /**
     * Default options for the file cache provider.
     *
     * @var array
     */
    public $defaultOptions = array('path' => '', 'ttl' => 5, 'segment' => '', 'lock_expr_time' => 5);

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    protected function setUp()
    {
        $this->_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'fcaches';
        $option = array_merge($this->defaultOptions, array('path' => $this->_path));
        $this->_cache = new SpicaFileCacheProvider($option);
    }

    /**
     * Asserts that SpicaFileCacheProvider::getOption() works as desired.
     *
     * @test
     */
    public function testGetOption()
    {
        $option = $this->_cache->getOptions();
        $this->assertEquals(true, is_array($option));
        $this->assertEquals(5, $option['ttl']);
        $this->assertEquals($this->_path, $option['path']);
    }

    /**
     * Asserts that SpicaFileCacheProvider::set() works as desired.
     *
     * @test
     * @depends testGetOption
     */
    public function testSet()
    {
        $data = array(1, 2);
        $this->assertEquals(true, $this->_cache->set('a1', $data, 3));

        $data = array(1, 2, array(1, 2));
        $this->assertEquals(true, $this->_cache->set('a2', $data, 3));

        $this->assertEquals(true, file_exists($this->_path.DIRECTORY_SEPARATOR.SpicaCacheManager::hash('a1')));
        $this->assertEquals(true, file_exists($this->_path.DIRECTORY_SEPARATOR.SpicaCacheManager::hash('a2')));
    }

    /**
     * Asserts that SpicaFileCacheProvider::getPath() works as desired.
     *
     * @test
     * @depends testGetOption
     */
    public function testGetPath()
    {
        $data  = array(1, 2);
        $this->_cache->set('a3', $data, 3);
        $this->_cache->set('a4', $data, 3);

        $path3 = $this->_path.DIRECTORY_SEPARATOR.SpicaCacheManager::hash('a3');
        $path4 = $this->_path.DIRECTORY_SEPARATOR.SpicaCacheManager::hash('a4');

        $this->assertEquals(true, file_exists($path3));
        $this->assertEquals(true, file_exists($path4));
        $this->assertEquals(true, file_exists($this->_cache->getPath('a3')));
        $this->assertEquals(true, file_exists($this->_cache->getPath('a4')));

        $this->assertEquals($path3, $this->_cache->getPath('a3'));
        $this->assertEquals($path4, $this->_cache->getPath('a4'));
    }

    /**
     * Asserts that SpicaFileCacheProvider::isExpired() works as desired.
     *
     * @test
     * @depends testSet
     */
    public function testIsExpired()
    {
        $data = array(1, 2);
        $this->assertEquals(true, $this->_cache->set('a1', $data, 3));

        $data = array(1, 2, array(1, 2));
        $this->assertEquals(true, $this->_cache->set('a2', $data, 3));

        $this->assertEquals(false, $this->_cache->isExpired('a1'));
        sleep(6);
        $this->assertEquals(true, $this->_cache->isExpired('a1'));
        $this->assertEquals(true, $this->_cache->isExpired('a1', null));
    }

    /**
     * Asserts that SpicaFileCacheProvider::isCached() works as desired.
     *
     * @test
     */
    public function testIsCached()
    {
        $data = array(1, 2);
        $this->assertEquals(true, $this->_cache->set('a1', $data, 3));

        $data = array(1, 2, array(1, 2));
        $this->assertEquals(true, $this->_cache->set('a2', $data, 3));

        $this->assertEquals(true, $this->_cache->isCached('a1'));
        $this->assertEquals(true, $this->_cache->isCached('a2'));
        $this->assertEquals(false, $this->_cache->isCached('a10'));
        $this->assertEquals(false, $this->_cache->isCached('a11'));
    }

    /**
     * Asserts that SpicaFileCacheProvider::remove() works as desired.
     *
     * @test
     * @depends testSet
     * @depends testIsCached
     */
    public function testRemove()
    {
        $data = array(1, 2, 3, 4);
        $this->_cache->set('a3', $data, 3);
        $this->_cache->remove('a3');
        $this->assertEquals(false, $this->_cache->isCached('a3'));
    }

    /**
     * Asserts that SpicaFileCacheProvider::flush() works as desired.
     *
     * @test
     * @depends testSet
     */
    public function testFlush1()
    {
        $this->_cache->flush();
        $iter      = new DirectoryIterator($this->_cache->getBasePath());
        $fileFound = 0;
        foreach ($iter as $file)
        {
            if ($file->isDot())
            {
                continue;
            }

            $fileFound++;
        }

        $this->assertEquals(0, $fileFound);
    }

    /**
     * Asserts that SpicaFileCacheProvider::flush() works as desired.
     *
     * @test
     * @depends testFlush1
     */
    public function testFlush2()
    {
        $option = array_merge($this->defaultOptions, array('path' => $this->_path, 'segment' => 'news'));

        $cache  = new SpicaFileCacheProvider($option);
        $data = array(1, 2, 3, 4);
        $cache->set('a6', $data, 3);

        $cache->setOption('segment', 'news2');
        $data = array(1, 2, 3, 4);
        $cache->set('a6', $data, 3);
        // Move base path to root ($this->_path) (not in segment anymore)
        $cache->setOption('segment', '');
        $cache->flush();

        $fileFound = 0;
        $iter      = new DirectoryIterator($this->_cache->getBasePath());
        foreach ($iter as $file)
        {
            if ($file->isDot())
            {
                continue;
            }

            $fileFound++;
        }

        $this->assertEquals(0, $fileFound);
    }

    /**
     * Asserts that SpicaFileCacheProvider::get() works as desired.
     *
     * @test
     * @depends testIsExpired
     */
    public function testGet()
    {
        $data = array(1, 2);
        $this->_cache->set('a1', $data, 3);
        $this->assertEquals($data, $this->_cache->get('a1'));

        $data = array(1, 2, array(1, 2));
        $this->_cache->set('a2', $data, 3);
        $this->assertEquals($data, $this->_cache->get('a2'));

        // Make the cache expired
        sleep(6);
        $data = array(1, 2);
        $this->assertEquals(false, $this->_cache->get('a1'));

        $data = array(1, 2, array(1, 2));
        $this->assertEquals(false, $this->_cache->get('a2'));
    }

    /**
     * Asserts that SpicaFileCacheProvider::getBasePath() works as desired.
     *
     * @test
     * @depends testSet
     */
    public function testGetBasePath()
    {
        $path   = dirname(__FILE__).DIRECTORY_SEPARATOR.'f2';
        $option = array_merge($this->defaultOptions, array('path' => $path));
        $cache  = new SpicaFileCacheProvider($option);
        $this->assertEquals($path, $cache->getBasePath());
        SpicaFileUtils::deleteDirectory($path);

        $path   = dirname(__FILE__).DIRECTORY_SEPARATOR.'f3';
        $option = array_merge($this->defaultOptions, array('path' => $path, 'segment' => 'news'));
        $cache  = new SpicaFileCacheProvider($option);
        $this->assertEquals($path.DIRECTORY_SEPARATOR.'news', $cache->getBasePath());
        SpicaFileUtils::deleteDirectory($path);
    }

    /**
     * Asserts that SpicaFileCacheProvider::acquireLock() and isLocked() works as desired.
     *
     * @test
     */
    public function testLock()
    {
        $path   = dirname(__FILE__).DIRECTORY_SEPARATOR.'f4';
        $option = array_merge($this->defaultOptions, array('path' => $path));
        $cache  = new SpicaFileCacheProvider($option);

        // Cache lock will be expired in 3 seconds
        $cache->acquireLock('a', 3, 100);
        $this->assertEquals(true, $cache->isLocked('a'));
        $cache->removeLock('a');
        SpicaFileUtils::deleteDirectory($path);
    }

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#tearDown()
     */
    public function tearDown()
    {
        if (file_exists($this->_path))
        {
            SpicaFileUtils::deleteDirectory($this->_path);
        }
    }
}

?>