<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/utils/FileUtils.php';
include_once 'core/cache/CacheManager.php';
include_once 'core/cache/FileCacheProvider.php';

/**
 * Set of test cases and assertions to ensure that SpicaFileCacheProvider works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaFileCacheProviderLockTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaFileCacheProviderLockTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFileCacheProviderLockTest extends PHPUnit_Framework_TestCase
{
    /**
     * SpicaFileCacheProvider fixtures.
     *
     * @var SpicaFileCacheProvider
     */
    protected $_cache;

    /**
     * Base cache file path.
     *
     * @var string
     */
    protected $_path;

    /**
     * Default options for the file cache provider.
     *
     * @var array
     */
    public $defaultOptions = array('path' => '', 'ttl' => 5, 'segment' => '', 'lock_expr_time' => 5);

    /**
     * Asserts that SpicaFileCacheProvider::acquireLock() works as desired.
     *
     * @test
     */
    public function testAcquireLock()
    {
        $path   = dirname(__FILE__).DIRECTORY_SEPARATOR.'f2';
        $option = array_merge($this->defaultOptions, array('path' => $path));
        $cache  = new SpicaFileCacheProvider($option);
        // Cached value
        $value  = 'aaaaaaaa----aaaa';

        try
        {
            $data = $cache->get('a');

            if (false === $data)
            {
                // Cache lock will be expired in 3 seconds
                $cache->acquireLock('a', 10, 100);
                // Fetch data from a datasource
                $cache->set('a', $value, 30);
                $cache->releaseLock('a');
            }
        }
        catch (SpicaCacheLockException $ex)
        {
            $data = $cache->get('a');
        }
        catch (SpicaCacheNotAvailableException $ex)
        {
            // Data should be not readable
            // Error will be logged and be sent to administrators
            $this->assertEquals(false, true, 'SpicaCacheNotAvailableException is thrown. ');
        }

        $this->assertEquals($value, true, $data);
        SpicaFileUtils::deleteDirectory($path);
    }
}

?>