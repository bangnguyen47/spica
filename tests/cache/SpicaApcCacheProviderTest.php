<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/utils/FileUtils.php';
include_once 'core/cache/CacheManager.php';
include_once 'core/cache/ApcCacheProvider.php';

/**
 * Set of test cases and assertions to ensure that SpicaApcCacheProvider works as desired.
 *
 * @category   spica
 * @package    tests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 30, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaApcCacheProviderTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaApcCacheProviderTest extends PHPUnit_Framework_TestCase
{
    /**
     * SpicaApcCacheProvider fixtures.
     *
     * @var SpicaApcCacheProvider
     */
    protected $_cache;

    /**
     * Default options for the file cache provider.
     *
     * @var array
     */
    public $defaultOptions = array('path' => '', 'ttl' => 5, 'segment' => 'test', 'lock_expr_time' => 5);

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    protected function setUp()
    {
        $this->_cache = new SpicaApcCacheProvider($this->defaultOptions);
    }

    /**
     * Asserts that SpicaApcCacheProvider::getOption() works as desired.
     *
     * @test
     */
    public function testGetOption()
    {
        $option = $this->_cache->getOptions();
        $this->assertEquals(true, is_array($option));
        $this->assertEquals($this->defaultOptions['ttl'], $option['ttl']);
        $this->assertEquals($this->defaultOptions['segment'], $option['segment']);
    }

    /**
     * Asserts that SpicaApcCacheProvider::set() works as desired.
     *
     * @test
     * @depends testGetOption
     */
    public function testSet()
    {
        $data1 = array(1, 2);
        $this->assertEquals(true, $this->_cache->set('a1', $data1, 3));

        $data2 = array(1, 2, array(1, 2));
        $this->assertEquals(true, $this->_cache->set('a2', $data2, 3));

        $this->assertEquals($data1, apc_fetch($this->defaultOptions['segment'].'_a1'));
        $this->assertEquals($data2, apc_fetch($this->defaultOptions['segment'].'_a2'));
    }

    /**
     * Asserts that SpicaMemcacheCacheProvider::isCached() works as desired.
     *
     * @test
     */
    public function testIsCached()
    {
        $data = array(1, 2);
        $this->assertEquals(true, $this->_cache->set('a1', $data, 3));

        $data = array(1, 2, array(1, 2));
        $this->assertEquals(true, $this->_cache->set('a2', $data, 3));

        $this->assertEquals(true, $this->_cache->isCached('a1'));
        $this->assertEquals(true, $this->_cache->isCached('a2'));
        $this->assertEquals(false, $this->_cache->isCached('a10'));
        $this->assertEquals(false, $this->_cache->isCached('a11'));
    }

    /**
     * Asserts that SpicaApcCacheProvider::remove() works as desired.
     *
     * @test
     * @depends testSet
     * @depends testIsCached
     */
    public function testRemove()
    {
        $data = array(1, 2, 3, 4);
        $this->_cache->set('a3', $data, 3);
        $this->_cache->remove('a3');
        $this->assertEquals(false, apc_fetch($this->defaultOptions['segment'].'_a3'));
    }

    /**
     * Asserts that SpicaApcCacheProvider::flush() works as desired.
     *
     * @test
     * @depends testSet
     */
    public function testFlush1()
    {
        $data = array(1, 2, 3, 4);
        $this->_cache->set('a3', $data, 3);

        $this->_cache->flush();
        $cacheInfo = apc_cache_info();
        $this->assertEquals(true, is_array($cacheInfo['cache_list']) && empty($cacheInfo['cache_list']));
    }

    /**
     * Asserts that SpicaFileCacheProvider::isExpired() works as desired.
     * This test can never pass because this bug http://pecl.php.net/bugs/bug.php?id=13331
     *
     * @test
     * @depends testSet
     */
    public function testIsExpired()
    {
        $data1 = array(1, 2);
        $this->assertEquals(true, $this->_cache->set('a1', $data1, 3));
        $this->assertEquals(false, $this->_cache->isExpired('a1'));

        $data2 = array(1, 2, array(1, 2));
        $this->assertEquals(true, $this->_cache->set('a2', $data2, 3));
        $this->assertEquals(false, $this->_cache->isExpired('a2'));

        sleep(6);
        $this->assertEquals(true, $this->_cache->isExpired('a1'));
        $this->assertEquals(true, $this->_cache->isExpired('a2'));
    }

    /**
     * Asserts that SpicaApcCacheProvider::get() works as desired.
     * This test can never pass because this bug http://pecl.php.net/bugs/bug.php?id=13331
     *
     * @test
     */
    public function testGet()
    {
        $data1 = array(1, 2);
        $this->_cache->set('a1', $data1);
        $this->assertEquals($data1, $this->_cache->get('a1'));

        $data2 = array(1, 2, array(1, 2));
        $this->_cache->set('a2', $data2);
        $this->assertEquals($data2, $this->_cache->get('a2'));

        $this->assertEquals(false, $this->_cache->get('a1'));
        $this->assertEquals(false, $this->_cache->get('a2'));
        $this->assertEquals(false, apc_fetch($this->defaultOptions['segment'].'_a1'));
        $this->assertEquals(false, apc_fetch($this->defaultOptions['segment'].'_a2'));
    }

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#tearDown()
     */
    public function tearDown()
    {
        apc_clear_cache('user');
        apc_clear_cache();
    }
}

?>