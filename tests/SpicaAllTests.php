<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
ini_set('memory_limit', '150M');
$baseDir = dirname(dirname(__FILE__));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/tests'.
PATH_SEPARATOR.$baseDir.'/tests/validator'.
PATH_SEPARATOR.$baseDir.'/tests/crypt'.
PATH_SEPARATOR.$baseDir.'/tests/utils'.
PATH_SEPARATOR.$baseDir.'/tests/core'.
PATH_SEPARATOR.$baseDir.'/tests/cache'.
PATH_SEPARATOR.$baseDir.'/tests/view'.
PATH_SEPARATOR.$baseDir.'/tests/filter'.
PATH_SEPARATOR.$baseDir.'/tests/datasource'.
PATH_SEPARATOR.$baseDir.'/tests/cms/blockgroup'
);

// PHPUnit classes
require_once 'PHPUnit/Framework.php';

// Spica validator test suites
require_once 'SpicaValidatorAllTests.php';
// Spica cryptor test suites
require_once 'SpicaCryptAllTests.php';
// Spica Utils test suites: ArrayList, ArrayListSet, RowList
require_once 'SpicaArrayListAllTests.php';
// Spica Template test suites
require_once 'SpicaTemplateAllTests.php';
// Spica Core System test suites
require_once 'SpicaCoreAllTests.php';
// Spica Cache test suites
require_once 'SpicaCacheAllTests.php';
// Spica Filter test suites
require_once 'SpicaFilterAllTests.php';
// Spica DataSource test suites
require_once 'SpicaDataSourceAllTests.php';


// Spica CMS BlockGroup test suites
require_once 'SpicaCmsBlockGroupAllTests.php';

/**
 * Set of test suites to ensure that Spica library classes work as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaAllTests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaAllTests.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAllTests
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('Spica - All Tests');
        $suite->addTest(SpicaValidatorAllTests::suite());
        $suite->addTest(SpicaCryptAllTests::suite());
        $suite->addTest(SpicaArrayListAllTests::suite());
		$suite->addTest(SpicaFilterAllTests::suite());
		$suite->addTest(SpicaDataSourceAllTests::suite());
		$suite->addTest(SpicaCacheAllTests::suite());
		$suite->addTest(SpicaCmsBlockGroupAllTests::suite());
        $suite->addTest(SpicaTemplateAllTests::suite());
        $suite->addTest(SpicaCoreAllTests::suite());
        return $suite;
    }
}

?>