<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/crypto/Crypto.php';

/**
 * Set of test cases and assertions to ensure that SpicaCryptRailFenceCipher works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaCryptRailFenceCipherTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 31, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaCryptRailFenceCipherTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaCryptRailFenceCipherTest extends PHPUnit_Framework_TestCase
{
    /**
     * Asserts that a string can be encoded an decoded to the original format.
     *
     * @test
     */
    public function stringCanBeEncodedAndDecoded()
    {
        $string  = 'abc 90990 765 HMTL';
        $encoded = SpicaCryptRailFenceCipher::encode($string);
        $decoded = SpicaCryptRailFenceCipher::decode($encoded);
        $this->assertNotEquals($string, $encoded);
        $this->assertEquals($string, $decoded);
    }
}

?>