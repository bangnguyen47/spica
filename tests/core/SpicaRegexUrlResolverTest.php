<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/RegexUrlResolver.php';

/**
 * Set of test cases and assertions to ensure that SpicaRegexUrlResolver
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 15, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRegexUrlResolverTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRegexUrlResolverTest extends PHPUnit_Framework_TestCase
{
    /**
     * Default values to module, controller, action.
     *
     * @var array
     */
    protected $_defaults;

    /**
     * Path the execution script (with leading /). Relative to document root path.
     *
     * @var string
     */
    protected $_scriptPath;

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    public function setUp()
    {
        $this->_defaults = array(
          'default_module'     => 'home',
          'default_controller' => 'index',
          'default_action'     => 'index',
        );

        $this->_scriptPath = '/index.php';
        $_GET = array();
    }

    /**
     * Asserts that URL can be parsed into expected values.
     */
    public function test1()
    {
        $resolver = new SpicaRegexUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete', array('_action' => 'remove'));
        $resolver->addRoute('a2', 'dynamic', 'news/:id', array('_module' => 'news', '_controller' => 'item', '_action' => 'view'));
        $resolver->addRoute('a3', 'dynamic', 'home2/*', array('_module' => 'home2'));
        $resolver->resolve('http://www.sun.com/home2/index2/index3');
        $this->assertEquals(true, isset($_GET['_module']) && $_GET['_module'] === 'home2');
        $this->assertEquals(true, isset($_GET['_action']) && $_GET['_action'] === 'index');
        $this->assertEquals(true, isset($_GET['_controller']) && $_GET['_controller'] === 'index');
    }

    /**
     * Asserts that URL can be parsed into expected values.
     */
    public function test2()
    {
        $resolver = new SpicaRegexUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete', array('_action' => 'remove'));
        $resolver->addRoute('a2', 'dynamic', 'news/:id', array('_module' => 'news', '_controller' => 'item', '_action' => 'view'));
        $resolver->addRoute('a3', 'dynamic', 'home2/*', array('_module' => 'home2'));
        $resolver->resolve('http://www.sun.com/home2/');
        $this->assertEquals(true, isset($_GET['_module']) && $_GET['_module'] === 'home2');
        $this->assertEquals(true, isset($_GET['_action']) && $_GET['_action'] === 'index');
        $this->assertEquals(true, isset($_GET['_controller']) && $_GET['_controller'] === 'index');
    }

    /**
     * Asserts that URL can be parsed into expected values.
     */
    public function test3()
    {
        $resolver = new SpicaRegexUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete', array('_action' => 'remove'));
        $resolver->addRoute('a2', 'dynamic', 'news/:id', array('_module' => 'news', '_controller' => 'item', '_action' => 'view'));
        $resolver->addRoute('a3', 'dynamic', 'home2/*', array('_module' => 'home2'));
        $name = $resolver->resolve('http://www.sun.com/home2'); // no trailing slash
        $this->assertEquals(false, $name);
    }

    /**
     * Asserts that URL can be parsed into expected values.
     */
    public function test4()
    {
        $resolver = new SpicaRegexUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete', array('_action' => 'remove'));
        $resolver->addRoute('a2', 'dynamic', 'news/:id', array('_module' => 'news', '_controller' => 'item', '_action' => 'view'));
        $resolver->addRoute('a3', 'dynamic', 'home2/*', array('_module' => 'home2'));
        $name = $resolver->resolve('http://www.sun.com/news/7');
        $this->assertEquals('a2', $name);
    }

    /**
     * Asserts that URL can be parsed into expected values.
     */
    public function test5()
    {
        $resolver = new SpicaRegexUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete', array('_action' => 'remove'));
        $resolver->addRoute('a2', 'dynamic', 'news/:id', array('_module' => 'news', '_controller' => 'item', '_action' => 'view'));
        $resolver->addRoute('a3', 'dynamic', 'home2/*', array('_module' => 'home2'));
        $name = $resolver->resolve('http://www.sun.com/admin/category/edit/id/9');
        $this->assertEquals(true, false === $name);
    }

    /**
     * Asserts that URL can be parsed into expected values.
     */
    public function test6()
    {
        // Fixed URL
        $resolver = new SpicaRegexUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete', array('_action' => 'remove'));
        $name = $resolver->resolve('http://www.sun.com/admin/category/delete/id/9');
        $this->assertFalse($name);

        // Wildcard URL
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete/*', array('_action' => 'remove'));
        $name = $resolver->resolve('http://www.sun.com/admin/category/delete/id/9');
        $this->assertEquals('a1', $name);

        $name = $resolver->resolve('http://www.sun.com/admin/category/delete');
        $this->assertEquals('a1', $name);

        $this->assertEquals('remove', $_GET['_action']);
        $this->assertEquals('admin', $_GET['_module']);
        $this->assertEquals('category', $_GET['_controller']);
    }

    /**
     * Asserts that URL can be parsed into expected values.
     */
    public function test7()
    {
        $resolver = new SpicaRegexUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->addRoute('a1', 'dynamic', ':module/:controller/delete', array('_action' => 'remove'));
        $resolver->addRoute('a2', 'dynamic', 'news/:id', array('_module' => 'news', '_controller' => 'item', '_action' => 'view'));
        $resolver->addRoute('a3', 'dynamic', 'home2/*', array('_module' => 'home2'));
        $resolver->addRoute('a4', 'dynamic', ':module/:controller/:action/*', array('_action' => 'remove'));
        $name = $resolver->resolve('http://www.sun.com/admin/category/edit/id/8');
        $this->assertEquals('a4', $name);
        $this->assertEquals(true, isset($_GET['_action']));
        $this->assertEquals(true, isset($_GET['_module']));
        $this->assertEquals(true, isset($_GET['_controller']));

        $this->assertEquals('remove', $_GET['_action']);
        $this->assertEquals('admin', $_GET['_module']);
        $this->assertEquals('category', $_GET['_controller']);
        $this->assertEquals('8', $_GET['id']);
    }
}

?>