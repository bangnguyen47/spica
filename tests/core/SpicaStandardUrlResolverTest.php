<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';

/**
 * Set of test cases and assertions to ensure that SpicaStandardUrlResolver
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaStandardUrlResolverTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 15, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaStandardUrlResolverTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaStandardUrlResolverTest extends PHPUnit_Framework_TestCase
{
    /**
     * Default values to module, controller, action.
     *
     * @var array
     */
    protected $_defaults;

    /**
     * Path the execution script (with leading /). Relative to document root path.
     *
     * @var string
     */
    protected $_scriptPath;

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    public function setUp()
    {
        $this->_defaults = array(
          'default_module'     => 'home',
          'default_controller' => 'index',
          'default_action'     => 'index',
        );

        $this->_scriptPath = '/index.php';
    }

    /**
     * Asserts that URL can be parsed into expected values.
     *
     * @test
     */
    public function testRequestResolving1()
    {
        $resolver = new SpicaStandardUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->resolve('http://www.sun.com/home2/index2/index3');
        $this->assertEquals('home2', $_GET['_module']);
        $this->assertEquals('home2', SpicaRequest::get('_module'));
        $this->assertEquals('home2', SpicaRequest::getModule());

        $this->assertEquals('index2', $_GET['_controller']);
        $this->assertEquals('index2', SpicaRequest::get('_controller'));
        $this->assertEquals('index2', SpicaRequest::getController());

        $this->assertEquals('index3', $_GET['_action']);
        $this->assertEquals('index3', SpicaRequest::get('_action'));
        $this->assertEquals('index3', SpicaRequest::getAction());
    }

    /**
     * Asserts that invalid URL parts name can be resolved to default value.
     *
     * @test
     */
    public function testRequestResolving2()
    {
        $resolver = new SpicaStandardUrlResolver($this->_defaults, $this->_scriptPath);
        $resolver->resolve('http://www.sun.com/news/index%2/index3');
        $this->assertEquals('news', $_GET['_module']);
        $this->assertEquals('news', SpicaRequest::get('_module'));
        $this->assertEquals('news', SpicaRequest::getModule());

        $this->assertEquals('index', $_GET['_controller']);
        $this->assertEquals('index', SpicaRequest::get('_controller'));
        $this->assertEquals('index', SpicaRequest::getController());

        $this->assertEquals('index3', $_GET['_action']);
        $this->assertEquals('index3', SpicaRequest::get('_action'));
        $this->assertEquals('index3', SpicaRequest::getAction());

        $resolver->resolve('http://www.sun.com/new@s/index%2/index3');
        $this->assertEquals('home', $_GET['_module']);
        $this->assertEquals('home', SpicaRequest::get('_module'));
        $this->assertEquals('home', SpicaRequest::getModule());

        $this->assertEquals('index', $_GET['_controller']);
        $this->assertEquals('index', SpicaRequest::get('_controller'));
        $this->assertEquals('index', SpicaRequest::getController());

        $this->assertEquals('index3', $_GET['_action']);
        $this->assertEquals('index3', SpicaRequest::get('_action'));
        $this->assertEquals('index3', SpicaRequest::getAction());
    }

    /**
     * Asserts that URL with subdirectory can be resolved and
     * invalid URL parts can be resolved to default value.
     *
     * @test
     */
    public function testRequestResolvingWithSubDir()
    {
        $resolver = new SpicaStandardUrlResolver($this->_defaults, '/repos/sub/index.php');
        $resolver->resolve('http://www.sun.com/repos/sub/news/index%2/index3');
        $this->assertEquals('news', $_GET['_module']);
        $this->assertEquals('news', SpicaRequest::get('_module'));
        $this->assertEquals('news', SpicaRequest::getModule());

        $this->assertEquals('index', $_GET['_controller']);
        $this->assertEquals('index', SpicaRequest::get('_controller'));
        $this->assertEquals('index', SpicaRequest::getController());

        $this->assertEquals('index3', $_GET['_action']);
        $this->assertEquals('index3', SpicaRequest::get('_action'));
        $this->assertEquals('index3', SpicaRequest::getAction());

        $resolver->resolve('http://www.sun.com/repos/sub/new@s/index%2/index3');
        $this->assertEquals('home', $_GET['_module']);
        $this->assertEquals('home', SpicaRequest::get('_module'));
        $this->assertEquals('home', SpicaRequest::getModule());

        $this->assertEquals('index', $_GET['_controller']);
        $this->assertEquals('index', SpicaRequest::get('_controller'));
        $this->assertEquals('index', SpicaRequest::getController());

        $this->assertEquals('index3', $_GET['_action']);
        $this->assertEquals('index3', SpicaRequest::get('_action'));
        $this->assertEquals('index3', SpicaRequest::getAction());
    }
}
?>