<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'PHPUnit/Framework.php';
include_once 'spica/core/Base.php';
include_once 'spica/core/test/Test.php';

/**
 * Set of test cases and assertions to ensure that Spica core functions work as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaCoreFunctionsTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 15, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaCoreFunctionsTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaCoreFunctionsTest extends PHPUnit_Framework_TestCase
{
    /**
     * Asserts that spica_url_* works correctly.
     *
     * @test
     */
    public function testUrlFunctions()
    {
        $baseDir   = dirname(dirname(dirname(__FILE__)));
        $bootstrap = $baseDir.'/index.php';
        $simulator = new SpicaMockRequestSimulator($bootstrap);

        /**
         * Round 1
         */
        $simulator->setUrl('http://www.sun.com/spica/trunk/home2/index3/do4');
        $simulator->setSubdirectory('/spica/trunk');
        $content = $simulator->run();
        $this->assertEquals('www.sun.com', SpicaContext::getDomain());
        $this->assertEquals('sun.com', SpicaContext::getDomain(false));
        // www is not a subdomain
        $this->assertEquals(null, SpicaContext::getSubdomain());

        /**
         * Round 2
         */
        $simulator->setUrl('http://sub1.microsoft.com.vn/spica/trunk/home2/index3/do4');
        $simulator->setSubdirectory('/spica/trunk');
        $content = $simulator->run();
        $this->assertEquals('sub1.microsoft.com.vn', SpicaContext::getDomain());
        $this->assertEquals('microsoft.com.vn', SpicaContext::getDomain(false));
        // www is not a subdomain
        $this->assertEquals('sub1', SpicaContext::getSubdomain());
    }

    /**
     * Asserts that spica_path_* works correctly.
     *
     * @test
     */
    public function testCensorPath()
    {
        $this->assertEquals('tests/core/'.basename(__FILE__), spica_path_make_relative(__FILE__));
    }

    /**
     * Asserts that SpicaDispatcher#dispatch() works correctly when a non-existent
     * controller is requested and there is no catch-all non-route controller configured.
     *
     * @test
     * @expectedException SpicaUnresolvableRequestException
     */
    public function testNonRoutingNotConfigured()
    {
        $baseDir   = dirname(dirname(dirname(__FILE__)));
        $bootstrap = $baseDir.'/index.php';
        $simulator = new SpicaMockConfigurationSimulator($bootstrap);
        $simulator->setUrl('http://www.sun.com/spica/trunk/home2/index3/do4');
        $simulator->setSubdirectory('/spica/trunk');
        $simulator->setNonRoute(array());
        $simulator->run();
        SpicaContext::setDebug(true);
        $dispatcher = new SpicaDispatcher();
        $dispatcher->dispatch('a', 'b', 'c');
   }

    /**
     * Asserts that SpicaDispatcher#dispatch() works correctly when a non-existent
     * controller is requested and the configured non-route controller does not exist
     * in practice.
     *
     * @test
     * @expectedException SpicaInfiniteRoutingException
     */
    public function testNonRoutingConfiguredButNotExist()
    {
        $baseDir   = dirname(dirname(dirname(__FILE__)));
        $bootstrap = $baseDir.'/index.php';
        $simulator = new SpicaMockConfigurationSimulator($bootstrap);
        $simulator->setUrl('http://www.sun.com/spica/trunk/home2/index3/do4');
        $simulator->setSubdirectory('/spica/trunk');
        $simulator->setNonRoute(array('_module' => 'a', '_controller' => 'b', '_action' => 'c'));

        $content   = $simulator->run();

        try
        {
            $dispatcher = new SpicaDispatcher();
            $dispatcher->dispatch('a', 'b', 'c');
        }
        catch (Exception $e)
        {
            SpicaContext::setExceptionHandler(null);
            throw $e;
        }
    }
}

?>