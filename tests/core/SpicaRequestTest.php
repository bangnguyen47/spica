<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/test/Test.php';

/**
 * Set of test cases and assertions to ensure that Spica request handler works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaRequestTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 12, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRequestTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRequestTest extends PHPUnit_Framework_TestCase
{
    /**
     * Asserts that localhost address can be resolved correctly.
     *
     * @test
     */
    public function testRequestHandler1()
    {
        $baseDir   = dirname(dirname(dirname(__FILE__)));
        $simulator = new SpicaMockRequestSimulator($baseDir.'/index.php');

        /**
         * Round 1
         */
        $url = 'http://localhost/repos/spica/trunk';
        $simulator->setUrl($url);
        $simulator->setSubdirectory('/repos/spica/trunk');
        $content = $simulator->run();
        $this->assertEquals(SpicaContext::getDefaultController(), SpicaRequest::getController());
        $this->assertEquals(SpicaContext::getDefaultAction(), SpicaRequest::getAction());
        $this->assertEquals(SpicaContext::getDefaultModule(), SpicaRequest::getModule());

        /**
         * Round 2
         */
        $url = 'http://localhost/repos/spica/trunk/home2/index3/do4'; // Company office
        $simulator->setUrl($url);
        $simulator->setSubdirectory('/repos/spica/trunk');
        $content = $simulator->run();
        $this->assertEquals('index3', SpicaRequest::getController());
        $this->assertEquals('do4', SpicaRequest::getAction());
        $this->assertEquals('home2', SpicaRequest::getModule());
    }

    /**
     * Asserts that true Internet website address can be resolved correctly.
     *
     * @test
     */
    public function testRequestHandler2()
    {
        $baseDir   = dirname(dirname(dirname(__FILE__)));
        $simulator = new SpicaMockRequestSimulator($baseDir.'/index.php');

        /**
         * Round 1
         */
        $url = 'http://www.domain.com/repos/spica/trunk';
        $simulator->setUrl($url);
        $simulator->setSubdirectory('/repos/spica/trunk');
        $content = $simulator->run();
        $this->assertEquals(SpicaContext::getDefaultController(), SpicaRequest::getController());
        $this->assertEquals(SpicaContext::getDefaultAction(), SpicaRequest::getAction());
        $this->assertEquals(SpicaContext::getDefaultModule(), SpicaRequest::getModule());

        /**
         * Round 2
         */
        $url = 'http://www.spica.com.vn/repos/spica/trunk/home2/index3/do4';
        $simulator->setUrl($url);
        $simulator->setSubdirectory('/repos/spica/trunk');
        $content = $simulator->run();
        $this->assertEquals('index3', SpicaRequest::getController());
        $this->assertEquals('do4', SpicaRequest::getAction());
        $this->assertEquals('home2', SpicaRequest::getModule());
    }
}

?>