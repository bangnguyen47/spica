<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(__FILE__))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/redis/Redis.php';
require_once 'spica/core/datasource/redis/Scalar.php';
require_once 'spica/core/datasource/redis/List.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaRedisLPUSH, SpicaRedisLPOP ...
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRedisListTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisListTest extends PHPUnit_Framework_TestCase
{
    /**
     * Remote Redis server informaton.
     *
     * @var array
     */
    public $hostInfo = array(
      'host'      => '192.168.1.150', // Redis server host name or IP.
      'port'      => 6379,            // Redis server port (optional).
      'transport' => 'tcp://',        // Protocol to communicate with Redis (optional)
    );

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);
        $response = $conn->send(new SpicaRedisFLUSHDB());
        $response->close();
        $client->disconnect();
    }

    /**
     * Tests LPUSH, LPOP command.
     */
    public function test1()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisLPOP('list1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist1'); // successful
        $client->disconnect();
    }

    /**
     * Tests LPUSH, LPOP command.
     *
     * @depends test1
     */
    public function test2()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        // Tests the results
        $response = $conn->send(new SpicaRedisLPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist3'); // successful

        $response = $conn->send(new SpicaRedisLPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist2'); // successful

        $response = $conn->send(new SpicaRedisLPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist1'); // successful

        $client->disconnect();
    }

    /**
     * Tests RPUSH, RPOP command.
     */
    public function test3()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisRPOP('list1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist1'); // successful
        $client->disconnect();
    }

    /**
     * Tests RPUSH, RPOP command (multiple times).
     *
     * @depends test2
     */
    public function test4()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist1'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist2'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist3'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        // Tests the results
        $response = $conn->send(new SpicaRedisRPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist3'); // successful

        $response = $conn->send(new SpicaRedisRPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist2'); // successful

        $response = $conn->send(new SpicaRedisRPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist1'); // successful

        $client->disconnect();
    }

    /**
     * Tests RPUSH, LPOP command.
     *
     * @depends test4
     */
    public function test5()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist1'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist2'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist3'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        // Tests the results
        $response = $conn->send(new SpicaRedisLPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist1'); // successful

        $response = $conn->send(new SpicaRedisLPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist2'); // successful

        $response = $conn->send(new SpicaRedisLPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist3'); // successful

        $client->disconnect();
    }

    /**
     * Tests LPUSH, RPOP command.
     *
     * @depends test1
     */
    public function test6()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3'));
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        // Tests the results
        $response = $conn->send(new SpicaRedisRPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist1'); // successful

        $response = $conn->send(new SpicaRedisRPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist2'); // successful

        $response = $conn->send(new SpicaRedisRPOP('list1'));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'vlist3'); // successful

        $client->disconnect();
    }

    /**
     * Tests LLEN command.
     *
     * @depends test1
     */
    public function test7()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLLEN('list1'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertTrue($response->getInt() === 3); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist4'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist5'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist6'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLLEN('list1'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertTrue($response->getInt() === 6); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisRPOP('list1'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLLEN('list1'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertTrue($response->getInt() === 5); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisLPOP('list1'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPOP('list1'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLLEN('list1'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertTrue($response->getInt() === 3); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist10'));
        $response->close();
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist11'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLLEN('list1'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertTrue($response->getInt() === 5); // amount of items
    }

    /**
     * Tests LRANGE command.
     *
     * @depends test1
     */
    public function test8()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 0, 2));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('vlist3', 'vlist2', 'vlist1'), $response->getMultiBulk()); // amount of items

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 0, 1));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('vlist3', 'vlist2'), $response->getMultiBulk()); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisFLUSHDB());
        $response->close();
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist1'));
        $response->close();
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist2'));
        $response->close();
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 1, 2));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('vlist2', 'vlist3'), $response->getMultiBulk()); // amount of items

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 1, 1));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('vlist2'), $response->getMultiBulk()); // amount of items

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 0, 1));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('vlist1', 'vlist2'), $response->getMultiBulk()); // amount of items

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 0, 0));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('vlist1'), $response->getMultiBulk()); // amount of items

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 0, 6)); // right boundary is out of range
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::MULTIBULK, $response->getType()); // multi-bulk reply (protocol)
        $this->assertEquals(array('vlist1', 'vlist2', 'vlist3'), $response->getMultiBulk()); // amount of items

        // Assertions
        $response = $conn->send(new SpicaRedisLRANGE('list1', 3, 6)); // both boundary is out of range
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::MULTIBULK, $response->getType()); // multi-bulk reply (protocol)
        $this->assertEquals(array(), $response->getMultiBulk()); // amount of items
    }


    /**
     * Tests LINDEX command.
     *
     * @depends test1
     */
    public function test9()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLINDEX('list1', 0));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist3', $response->getBulk()); // value at the specified index

        // Assertions
        $response = $conn->send(new SpicaRedisLINDEX('list1', 1));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist2', $response->getBulk()); // value at the specified index

        // Fixture
        $response = $conn->send(new SpicaRedisFLUSHDB());
        $response->close();
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist4'));
        $response->close();
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist5'));
        $response->close();
        $response = $conn->send(new SpicaRedisRPUSH('list1', 'vlist6'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLINDEX('list1', 0));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist4', $response->getBulk()); // value at the specified index

        // Assertions (same index)
        $response = $conn->send(new SpicaRedisLINDEX('list1', 0));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist4', $response->getBulk()); // value at the specified index

        // Assertions (last index)
        $response = $conn->send(new SpicaRedisLINDEX('list1', 2));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist6', $response->getBulk()); // value at the specified index

        // Assertions (non-existent index)
        $response = $conn->send(new SpicaRedisLINDEX('list1', 100));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('', $response->getBulk()); // value at the out-of-range index (should be empty string)

        // Assertions (last index with negative number)
        $response = $conn->send(new SpicaRedisLINDEX('list1', -1));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist6', $response->getBulk()); // value at the specified index

        // Assertions (index with negative number)
        $response = $conn->send(new SpicaRedisLINDEX('list1', -2));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist5', $response->getBulk()); // value at the specified index

        // Assertions (non-existent key)
        $response = $conn->send(new SpicaRedisLINDEX('list000000', -2));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals(null, $response->getBulk()); // value at the specified index
    }

    /**
     * Tests LTRIM command.
     *
     * @depends test9
     */
    public function test10()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1')); // last element
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3')); // first element
        $response->close();
        // Fixture: Trim the list
        $response = $conn->send(new SpicaRedisLTRIM('list1', 0, 1));
        $response->close();

        // Assertions (GET out-of-range index)
        $response = $conn->send(new SpicaRedisLINDEX('list1', 2));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals(null, $response->getBulk()); // value at the specified index

        // Assertions (GET in-range index)
        $response = $conn->send(new SpicaRedisLINDEX('list1', 1));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist2', $response->getBulk()); // value at the specified index

        // Assertions (GET in-range index)
        $response = $conn->send(new SpicaRedisLINDEX('list1', 0));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist3', $response->getBulk()); // value at the specified index
    }

    /**
     * Tests LSET command.
     *
     * @depends test9
     */
    public function test11()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1')); // last element
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3')); // first element
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLSET('list1', 2, 'vlist3.3'));
        // Assert that a status reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_OK, $response->getStatus()); // status code

        // Assertions
        $response = $conn->send(new SpicaRedisLINDEX('list1', 2));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('vlist3.3', $response->getBulk()); // value at the specified index
    }

    /**
     * Tests RPOPLPUSH command.
     *
     * @depends test9
     */
    public function test12()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture (list 1 - source)
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1')); // last element
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3')); // first element
        $response->close();
        $response = $conn->send(new SpicaRedisLINDEX('list1', 2));
        $last1    = $response->getBulk();

        // Fixture (list 2 - destination)
        $response = $conn->send(new SpicaRedisLPUSH('list2', 'vlist21')); // last element
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list2', 'vlist22'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list2', 'vlist23')); // first element
        $response->close();
        $response = $conn->send(new SpicaRedisLINDEX('list1', 0));
        $first1   = $response->getBulk();

        // Assertions (the affected element will be returned and it is the last ever element in list1)
        $response = $conn->send(new SpicaRedisRPOPLPUSH('list1', 'list2'));
        // Assert that a status reply returns
        $this->assertEquals(SpicaRedisResponse::BULK, $response->getType()); // bulk code reply (protocol)
        $this->assertEquals($last1, $response->getBulk()); // status code

        // Assertions (the last element in list1 must be removed and the length of list1 is reduced)
        $response = $conn->send(new SpicaRedisLINDEX('list1', 2));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals(null, $response->getBulk()); // value at the specified index

        // Assertions (the first element in list2 should be last element that is just removed in list1)
        $response = $conn->send(new SpicaRedisLINDEX('list2', 0));
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals($last1, $response->getBulk()); // value at the specified index

        // Assertions (length is reduced 1)
        $response = $conn->send(new SpicaRedisLLEN('list1'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(2, $response->getInt()); // list length

        // Assertions (length is increased 1)
        $response = $conn->send(new SpicaRedisLLEN('list2'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(4, $response->getInt()); // list length
    }

    /**
     * Tests LREM command (existent list).
     *
     * @depends test9
     */
    public function test13()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist1'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist3'));
        $response->close();
        $response = $conn->send(new SpicaRedisLPUSH('list1', 'vlist2'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisLREM('list1', 'vlist2', 1));
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // list length

        // Assertions (length is reduced 1)
        $response = $conn->send(new SpicaRedisLLEN('list1'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(4, $response->getInt()); // list length
    }

    /**
     * Tests LREM command (non-existent list).
     *
     * @depends test13
     */
    public function test14()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Assertions
        $response = $conn->send(new SpicaRedisLREM('list111111', 1, 'vlist2'));
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt()); // list length
    }
}

?>