<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(__FILE__))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/redis/Redis.php';
require_once 'spica/core/datasource/redis/Scalar.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaRedisPING, SpicaRedisFLUSHDB
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRedisServerTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisServerTest extends PHPUnit_Framework_TestCase
{
    /**
     * Remote Redis server informaton.
     *
     * @var array
     */
    public $hostInfo = array(
      'host'      => '192.168.1.150', // Redis server host name or IP.
      'port'      => 6379,            // Redis server port (optional).
      'transport' => 'tcp://',        // Protocol to communicate with Redis (optional)
    );

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);
        $response = $conn->send(new SpicaRedisFLUSHALL());
        $response->close();
        $client->disconnect();
    }

    /**
     * Tests PING response.
     */
    public function test1()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // First time
        $response = $conn->send(new SpicaRedisPING());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_PONG); // successful

        // Second time
        $response = $conn->send(new SpicaRedisPING());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertEquals(SpicaRedisResponse::STATUS, $response->getType()); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_PONG, $response->getStatus()); // successful

        // Third time
        $response = $conn->send(new SpicaRedisPING());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_PONG); // successful

        $client->disconnect();
    }

    /**
     * Tests SELECT command.
     */
    public function test2()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // First time
        $response = $conn->send(new SpicaRedisSELECT(1));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_OK, $response->getStatus()); // successful

        // Second time
        $response = $conn->send(new SpicaRedisSELECT(2));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_OK, $response->getStatus()); // successful

        // Third time
        $response = $conn->send(new SpicaRedisSELECT(1));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_OK, $response->getStatus()); // successful
    }

    /**
     * Tests DBSIZE command.
     */
    public function test3()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k3', 'value3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt()); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k3', 'value3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt()); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k4', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k5', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k6', 'value3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(6, $response->getInt()); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisDEL('k1'));
        $response->close();
        $response = $conn->send(new SpicaRedisDEL('k2'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(4, $response->getInt()); // amount of items

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests FLUSHDB command.
     *
     * @depends test3
     */
    public function test4()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSELECT(1));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k3', 'value3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt()); // amount of items

        // Assertions (flush db 1)
        $response = $conn->send(new SpicaRedisFLUSHDB());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_OK, $response->getStatus()); // status code

        // Assertions (after the db 1 is flushed)
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt()); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // amount of items

        // Change db
        $response = $conn->send(new SpicaRedisSELECT(2));
        $response->close();
        // Fixture
        $response = $conn->send(new SpicaRedisSET('k4', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k5', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k6', 'value3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt()); // amount of items

        // Assertions (flush db 2)
        $response = $conn->send(new SpicaRedisFLUSHDB());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_OK, $response->getStatus()); // status code

        // Assertions (after the db 2 is flushed)
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt()); // amount of items

        // Back to db 1
        $response = $conn->send(new SpicaRedisSELECT(1));
        $response->close();

        // Assertions (db 2 is flushed only, all item in db 1 is kept intact)
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // amount of items

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests FLUSHALL command.
     *
     * @depends test2
     */
    public function test5()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSELECT(1));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k3', 'value3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt()); // amount of items

        // Fixture
        $response = $conn->send(new SpicaRedisSELECT(2));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(2, $response->getInt()); // amount of items

        // Back to db 1
        $response = $conn->send(new SpicaRedisSELECT(1));
        $response->close();
        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt()); // amount of items

        // Back to db 2
        $response = $conn->send(new SpicaRedisSELECT(2));
        $response->close();
        // Assertions
        $response = $conn->send(new SpicaRedisDBSIZE());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(2, $response->getInt()); // amount of items

        // Assertions (flush all db )
        $response = $conn->send(new SpicaRedisFLUSHALL());
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertEquals(SpicaRedisResponse::STATUS_OK, $response->getStatus()); // status code

        // Assertions (check db 1 - after all db is flushed)
        // Back to db 1
        $response = $conn->send(new SpicaRedisSELECT(1));
        $response->close();
        $response = $conn->send(new SpicaRedisDBSIZE());
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt()); // amount of items

        // Assertions (check db 2 - after all db is flushed)
        // Back to db 2
        $response = $conn->send(new SpicaRedisSELECT(2));
        $response->close();
        $response = $conn->send(new SpicaRedisDBSIZE());
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt()); // amount of items

        $conn->close();
        $client->disconnect();
    }
}

?>