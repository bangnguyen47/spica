<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(__FILE__))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/redis/Redis.php';
require_once 'spica/core/datasource/redis/Scalar.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaRedisGET, SpicaRedisSET
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 22, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRedisKeyValueTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisKeyValueTest extends PHPUnit_Framework_TestCase
{
    /**
     * Remote Redis server informaton.
     *
     * @var array
     */
    public $hostInfo = array(
      'host'      => '192.168.1.150', // Redis server host name or IP.
      'port'      => 6379,            // Redis server port (optional).
      'transport' => 'tcp://',        // Protocol to communicate with Redis (optional)
    );

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);
        $response = $conn->send(new SpicaRedisFLUSHDB());
        $response->close();
        $client->disconnect();
    }

    /**
     * Tests error response.
     */
    public function test1()
    {
        $conn = new SpicaRedisConnection($this->hostInfo['transport'], $this->hostInfo['host'], $this->hostInfo['port']);
        $conn->open();
        $conn->send('XXX'.SpicaRedisCommand::NL);
        // in Redis 1.2: -ERR unknown command 'XXX'
        // in Redis 1.1: -ERR unknown command
        $resp = $conn->readLine();
        $this->assertContains("-ERR unknown command", $resp);
        $conn->close();
    }

    /**
     * Tests SET command.
     */
    public function test2()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful

        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS); // status code reply (protocol)
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK); // successful
        $client->disconnect();
    }

    /**
     * Tests GET command.
     *
     * @depends test2
     */
    public function test3()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisGET('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'value1'); // bulk reply content

        $response = $conn->send(new SpicaRedisGET('k2'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === 'value2'); // bulk reply content

        // Assertions
        $command  = new SpicaRedisGET('k1000'); // this key does not exist
        $response = $conn->send($command);
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertEquals(null, $response->getBulk());

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests GET command.
     *
     * @depends test2
     */
    public function test4()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();

        // Assertions
        $command  = new SpicaRedisGET('k1');
        $response = $conn->send($command);
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertTrue($response->getBulk() === 'value1');

        // Assertions
        $command->setKey('k2');
        $response = $conn->send($command);
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertTrue($response->getBulk() === 'value2');

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests GETSET command.
     *
     * @depends test2
     */
    public function test5()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();

        // Assertions
        $command  = new SpicaRedisGET('k1');
        $response = $conn->send($command);
        $value1   = $response->getBulk();

        $response = $conn->send(new SpicaRedisGETSET('k1', 'value1.1'));
        $oldValue = $response->getBulk();
        $this->assertTrue($oldValue === $value1);

        $response = $conn->send($command);
        $this->assertTrue($response->getBulk() === 'value1.1');
        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests GET command on a non existent key.
     *
     * @depends test2
     */
    public function test6()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        $command  = new SpicaRedisGET('k1nonexistent');
        $response = $conn->send($command);
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue($response->getBulk() === null); // bulk reply content

        $client->disconnect();
    }

    /**
     * Tests DEL command.
     *
     * @depends test2
     */
    public function test7()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();

        // Assertions
        $command  = new SpicaRedisDEL('k1');
        $response = $conn->send($command);
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertTrue($response->getInt() === 1); // integer reply content

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests DEL command on a non existent key.
     *
     * @depends test7
     */
    public function test8()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        $command  = new SpicaRedisDEL('k1nonexistent');
        $response = $conn->send($command);
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 0);

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests DEL command 2 times over a provided key.
     *
     * @depends test8
     */
    public function test9()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // SET command
        $response = $conn->send(new SpicaRedisSET('k1', 'value2'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS);
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK);
        // GET command
        $response = $conn->send(new SpicaRedisGET('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertTrue($response->getBulk() === 'value2');

        // DEL command (1) - key exists
        $command  = new SpicaRedisDEL('k1');
        $response = $conn->send($command);
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType());
        $this->assertEquals(1, $response->getInt());

        // DEL command (2) - key not exist
        $response = $conn->send($command);
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType());
        $this->assertEquals(0, $response->getInt());

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value3'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k3', 'value4'));
        $response->close();

        // DEL command (3) - 3 keys at the same time
        $command->setKey(array('k1', 'k2', 'k3'));
        $response = $conn->send($command);
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType());
        $this->assertEquals(3, $response->getInt());

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests a single DECR command.
     *
     * @depends test2
     */
    public function test10()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 1;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // DECR command
        $response = $conn->send(new SpicaRedisDECR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === --$val);

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests multiple DECR commands.
     *
     * @depends test10
     */
    public function test11()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 3;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // DECR command (1)
        $response = $conn->send(new SpicaRedisDECR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === --$val);

        // DECR command (2)
        $response = $conn->send(new SpicaRedisDECR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === --$val);

        // DECR command (3)
        $response = $conn->send(new SpicaRedisDECR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === --$val);

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests a single INCR command.
     *
     * @depends test2
     */
    public function test12()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 1;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // INCR command
        $response = $conn->send(new SpicaRedisINCR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ++$val);

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests multiple INCR commands.
     *
     * @depends test12
     */
    public function test13()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 3;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // INCR command (1)
        $response = $conn->send(new SpicaRedisINCR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ++$val);

        // INCR command (2)
        $response = $conn->send(new SpicaRedisINCR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ++$val);

        // INCR command (3)
        $response = $conn->send(new SpicaRedisINCR('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ++$val);

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests a single INCRBY command.
     *
     * @depends test2
     */
    public function test14()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 1;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // INCRBY command
        $response = $conn->send(new SpicaRedisINCRBY('k1', 2));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val + 2));

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests multiple INCRBY commands.
     *
     * @depends test14
     */
    public function test15()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 3;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // INCRBY command (1)
        $response = $conn->send(new SpicaRedisINCRBY('k1', 2));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val + 2));

        // INCRBY command (2)
        $response = $conn->send(new SpicaRedisINCRBY('k1', 3));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val + 5));

        // INCRBY command (3)
        $response = $conn->send(new SpicaRedisINCRBY('k1', 4));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val + 9));

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests a single DECRBY command.
     *
     * @depends test2
     */
    public function test16()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 10;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // DECRBY command
        $response = $conn->send(new SpicaRedisDECRBY('k1', 2));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val - 2));

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests multiple DECRBY commands.
     *
     * @depends test16
     */
    public function test17()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 10;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // DECRBY command (1)
        $response = $conn->send(new SpicaRedisDECRBY('k1', 2));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val - 2));

        // DECRBY command (2)
        $response = $conn->send(new SpicaRedisDECRBY('k1', 3));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val - 5));

        // DECRBY command (3)
        $response = $conn->send(new SpicaRedisDECRBY('k1', 4));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === ($val - 9));

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests EXISTS commands.
     *
     * @depends test2
     */
    public function test18()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $val      = 1;
        $response = $conn->send(new SpicaRedisSET('k1', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // DECR command
        $response = $conn->send(new SpicaRedisEXISTS('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 1);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k2', $val));
        // It must succeed so we don't care about the response here
        $response->close();

        // DECR command
        $response = $conn->send(new SpicaRedisEXISTS('k2'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 1);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k3', $val));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k4', $val));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k5', $val));
        $response->close();

        // DECR command
        $response = $conn->send(new SpicaRedisEXISTS('k3'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 1); // existed

        $response = $conn->send(new SpicaRedisEXISTS('k4'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 1); // existed

        $response = $conn->send(new SpicaRedisEXISTS('k5'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 1); // existed

        $response = $conn->send(new SpicaRedisEXISTS('k67'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 0); // not existed

        $response = $conn->send(new SpicaRedisEXISTS('k68'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 0); // not existed

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests SETNX command.
     *
     * @depends test2
     */
    public function test19()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();

        // SETNX command executed against an existent key
        $response = $conn->send(new SpicaRedisSETNX('k1', 'value1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 0);

        // SETNX command executed against an non-existent key
        $response = $conn->send(new SpicaRedisSETNX('k100nonexistent', 'value1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 1);

        // SETNX command executed against an existent key
        $response = $conn->send(new SpicaRedisSETNX('k100nonexistent', 'value1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 0);

        $response = $conn->send(new SpicaRedisDEL('k100nonexistent'));
        $response->close();

        // SETNX command executed against an non-existent key
        $response = $conn->send(new SpicaRedisSETNX('k100nonexistent', 'value1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT);
        $this->assertTrue($response->getInt() === 1);

        $client->disconnect();
    }

    /**
     * Tests MGET command that results in a multi-bulk reply.
     *
     * Using 1 key
     */
    public function test20()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();

        $response = $conn->send(new SpicaRedisMGET('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK);
        $this->assertTrue($response->getMultiBulk() === array('value1'));

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests MGET command that results in a multi-bulk reply.
     *
     * Using many keys
     */
    public function test21()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k3', 'value3'));
        $response->close();

        $response = $conn->send(new SpicaRedisMGET(array('k1', 'k2', 'k3')));
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK);
        $this->assertTrue($response->getMultiBulk() === array('value1', 'value2', 'value3'));

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests MSET command.
     */
    public function test22()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $map = array(
          'kk1' => 'value1',
          'kk2' => 'value2',
          'kk3' => 'value3',
          'kk4' => 'value4',
          'kk5' => 'value5',
        );
        $response = $conn->send(new SpicaRedisMSET($map));
        $this->assertTrue($response->getType() === SpicaRedisResponse::STATUS);
        $this->assertTrue($response->getStatus() === SpicaRedisResponse::STATUS_OK);

        $response = $conn->send(new SpicaRedisMGET(array('kk1', 'kk2', 'kk3', 'kk4', 'kk5')));
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK);
        $this->assertEquals(array('value1', 'value2', 'value3', 'value4', 'value5'), $response->getMultiBulk());

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests MSETNX command.
     */
    public function test23()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $map = array(
          'kk1' => 'value1',
          'kk2' => 'value2',
          'kk3' => 'value3',
          'kk4' => 'value4',
          'kk5' => 'value5',
        );

        // Assertions
        $response = $conn->send(new SpicaRedisMSETNX($map));
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType());
        $this->assertEquals(SpicaRedisResponse::INT_SET, $response->getInt());

        // Assertions
        $response = $conn->send(new SpicaRedisMGET(array('kk1', 'kk2', 'kk3', 'kk4', 'kk5')));
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK);
        $this->assertEquals(array('value1', 'value2', 'value3', 'value4', 'value5'), $response->getMultiBulk());

        // Assertions
        $response = $conn->send(new SpicaRedisMSETNX($map));
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType());
        $this->assertEquals(SpicaRedisResponse::INT_NOTSET, $response->getInt());

        $conn->close();
        $client->disconnect();
    }

    /**
     * Tests close() on Redis response.
     */
    public function test24()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0);
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSET('k1', 'value1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k2', 'value2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSET('k3', 'value3'));
        $response->close();

        // Skips multi-bulk reply
        $response = $conn->send(new SpicaRedisMGET(array('k1', 'k2', 'k3')));
        $response->close();

        $response = $conn->send(new SpicaRedisGET('k2'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertEquals('value2', $response->getBulk());

        // Skips bulk reply
        $response = $conn->send(new SpicaRedisGET('k1'));
        $response->close();

        $response = $conn->send(new SpicaRedisGET('k2'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertTrue($response->getBulk() === 'value2');

        // Skips status reply
        $response = $conn->send(new SpicaRedisSET('k1', 'v1'));
        $response->close();

        $response = $conn->send(new SpicaRedisGET('k3'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertTrue($response->getBulk() === 'value3');

        // Skips status, integer reply
        $response = $conn->send(new SpicaRedisSET('k1', '1'));
        $response->close();
        $response = $conn->send(new SpicaRedisINCR('k1'));
        $response->close();

        $response = $conn->send(new SpicaRedisGET('k1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertTrue($response->getBulk() === '2');

        // Skips error reply
        $response = $conn->send(new SpicaRedisNoParamCommand('MYERRORCOMMAND')); // Not a Redis command
        $response->close();

        $response = $conn->send(new SpicaRedisGET('k3'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK);
        $this->assertTrue($response->getBulk() === 'value3');

        $client->disconnect();
    }
}

?>