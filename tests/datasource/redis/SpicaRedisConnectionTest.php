<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(__FILE__))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/redis/Redis.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaRedisConnection classes
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 22, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRedisConnectionTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisConnectionTest extends PHPUnit_Framework_TestCase
{
    /**
     * Redis server host IP.
     *
     * @var string
     */
    public $host = '192.168.1.150';

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {

    }

    /**
     * Tests TCP connection can open.
     */
    public function test1()
    {
        $tcp     = new SpicaRedisConnection('tcp://', '127.0.0.1', 80, 2);
        $success = $tcp->open();
        $this->assertTrue($success);
    }

    /**
     * Tests connection to a remote Redis server.
     */
    public function test2()
    {
        $conn = new SpicaRedisConnection('tcp://', $this->host, 6379);
        $this->assertTrue($conn->open());
        $this->assertTrue(3 === $conn->send('123'));
        $conn->close();
    }

    /**
     * Tests connection status to a remote Redis server.
     */
    public function test3()
    {
        $conn = new SpicaRedisConnection('tcp://', $this->host, 6379);
        $conn->close();
        $this->assertTrue($conn->isConnected() === false);
    }
}

?>