<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(__FILE__))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/redis/Redis.php';
require_once 'spica/core/datasource/redis/Scalar.php';
require_once 'spica/core/datasource/redis/Set.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaRedisSCARD, SpicaRedisSADD ...
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 29, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaRedisSetTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSetTest extends PHPUnit_Framework_TestCase
{
    /**
     * Remote Redis server informaton.
     *
     * @var array
     */
    public $hostInfo = array(
      'host'      => '192.168.1.150', // Redis server host name or IP.
      'port'      => 6379,            // Redis server port (optional).
      'transport' => 'tcp://',        // Protocol to communicate with Redis (optional)
    );

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);
        $response = $conn->send(new SpicaRedisFLUSHDB());
        $response->close();
        $client->disconnect();
    }

    /**
     * Tests SADD, SPOP command.
     */
    public function test1()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // element was added

        // Asserts that duplicate element can not be set
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt()); // no element was added

        $response = $conn->send(new SpicaRedisSPOP('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals('svalue1', $response->getBulk()); // because there is a single item in the set

        // Asserts that after a SPOP the element value 'svalue1' does not exist any more
        // so it is not considered to be duplicate element
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // no element was added

        // Fixture (add one more element)
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue2'));
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // element was added

        // Return and remove an element (1)
        $response = $conn->send(new SpicaRedisSPOP('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue(in_array($response->getBulk(), array('svalue1', 'svalue2'))); // an random element in the SET

        // Return and remove an element (2)
        $response = $conn->send(new SpicaRedisSPOP('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertTrue(in_array($response->getBulk(), array('svalue1', 'svalue2'))); // an random element in the SET

        // Return and remove an element from the empty set (3)
        $response = $conn->send(new SpicaRedisSPOP('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::BULK); // bulk reply (protocol)
        $this->assertEquals(null, $response->getBulk()); // a null value

        $client->disconnect();
    }

    /**
     * Tests SCARD command.
     *
     * @depends test1
     */
    public function test2()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // element was added

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue2'));
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // element was added

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue3'));
        // Assert that an integer reply returns
        $this->assertEquals(SpicaRedisResponse::INT, $response->getType()); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // element was added

        $response = $conn->send(new SpicaRedisSCARD('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a status code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt());

        // Fixture
        $response = $conn->send(new SpicaRedisSPOP('set1'));
        $response->close();

        // Asserts that amount of items is reduced
        $response = $conn->send(new SpicaRedisSCARD('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(2, $response->getInt());

        // Fixture
        $response = $conn->send(new SpicaRedisSPOP('set1'));
        $response->close();

        // Asserts that amount of items is reduced
        $response = $conn->send(new SpicaRedisSCARD('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt());

        // Fixture (last element is removed)
        $response = $conn->send(new SpicaRedisSPOP('set1'));
        $response->close();

        // Asserts that amount of items is reduced
        $response = $conn->send(new SpicaRedisSCARD('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer code reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt());

        $client->disconnect();
    }

    /**
     * Tests SMEMBERS command.
     *
     * @depends test2
     */
    public function test3()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue3'));
        $response->close();

        // Return and remove an element from the empty set (3)
        $response = $conn->send(new SpicaRedisSMEMBERS('set1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue3', 'svalue1', 'svalue2'), $response->getMultiBulk()); // 3 values

        // Asserts that amount of items is not reduced
        $response = $conn->send(new SpicaRedisSCARD('set1'));
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(3, $response->getInt());

        $client->disconnect();
    }

    /**
     * Tests SISMEMBER command.
     *
     * @depends test3
     */
    public function test4()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue3'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisSISMEMBER('set1', 'svalue2'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // member does exist

        // Assertions
        $response = $conn->send(new SpicaRedisSISMEMBER('set1', 'svalue3'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // member does exist

        // Assertions
        $response = $conn->send(new SpicaRedisSISMEMBER('set1', 'svalue1'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(1, $response->getInt()); // member does exist

        // Assertions
        $response = $conn->send(new SpicaRedisSISMEMBER('set1', 'svalue30'));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that an integer reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::INT); // integer reply (protocol)
        $this->assertEquals(0, $response->getInt()); // member does not exist

        $client->disconnect();
    }

    /**
     * Tests SINTER command.
     *
     * @depends test2
     */
    public function test5()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue3'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set2', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set2', 'svalue2'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set3', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set3', 'svalue3'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set4', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set4', 'svalue2'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisSINTER(array('set1', 'set2')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue1', 'svalue2'), $response->getMultiBulk()); // intersected values

        // Assertions
        $response = $conn->send(new SpicaRedisSINTER(array('set1', 'set3')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue3', 'svalue1'), $response->getMultiBulk()); // intersected values

        // Assertions
        $response = $conn->send(new SpicaRedisSINTER(array('set1', 'set3', 'set4')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue1'), $response->getMultiBulk()); // intersected values

        $client->disconnect();
    }

    /**
     * Tests SINTER command.
     *
     * @depends test2
     */
    public function test6()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue3'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set2', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set2', 'svalue2'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set3', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set3', 'svalue3'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set4', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set4', 'svalue2'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisSDIFF(array('set1', 'set2')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue3'), $response->getMultiBulk()); // difference values

        // Assertions
        $response = $conn->send(new SpicaRedisSDIFF(array('set1', 'set3')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue2'), $response->getMultiBulk()); // difference values

        // Assertions
        $response = $conn->send(new SpicaRedisSDIFF(array('set1', 'set3', 'set4')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array(), $response->getMultiBulk()); // difference values

        $client->disconnect();
    }

    /**
     * Tests SUNION command.
     *
     * @depends test2
     */
    public function test7()
    {
        $context  = new SpicaRedisContext($this->hostInfo, 0); // database 0
        $client   = new SpicaRedisClient();
        $conn     = $client->getConnection($context);

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue2'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue3'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set1', 'svalue4'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set2', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set2', 'svalue2'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set3', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set3', 'svalue3'));
        $response->close();

        // Fixture
        $response = $conn->send(new SpicaRedisSADD('set4', 'svalue1'));
        $response->close();
        $response = $conn->send(new SpicaRedisSADD('set4', 'svalue2'));
        $response->close();

        // Assertions
        $response = $conn->send(new SpicaRedisSUNION(array('set1', 'set2')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue3', 'svalue4', 'svalue1', 'svalue2'), $response->getMultiBulk()); // union values

        // Assertions
        $response = $conn->send(new SpicaRedisSUNION(array('set1', 'set3')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue3', 'svalue4', 'svalue1', 'svalue2'), $response->getMultiBulk()); // union values

        // Assertions
        $response = $conn->send(new SpicaRedisSUNION(array('set1', 'set3', 'set4')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue3', 'svalue4', 'svalue1', 'svalue2'), $response->getMultiBulk()); // union values

        // Assertions
        $response = $conn->send(new SpicaRedisSUNION(array('set3', 'set4')));
        $this->assertTrue($response instanceof SpicaRedisResponse);
        // Assert that a multi-bulk reply returns
        $this->assertTrue($response->getType() === SpicaRedisResponse::MULTIBULK); // multi-bulk reply (protocol)
        $this->assertEquals(array('svalue3', 'svalue1', 'svalue2'), $response->getMultiBulk()); // union values

        $client->disconnect();
    }
}

?>