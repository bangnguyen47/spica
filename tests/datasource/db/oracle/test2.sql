-- Drops and recreates table TEST1
DROP TABLE test2;

CREATE TABLE test2 (
  id number default NULL,
  username varchar2(100) default 'NULL',
  seq_number number default 1
)

/*Data for the table `test2` */

insert into test2(id,username,seq_number) values (1,'pcdinh1',1)
insert into test2(id,username,seq_number) values (2,'pcdinh2',2)
insert into test2(id,username,seq_number) values (3,'pcdinh3',3)
insert into test2(id,username,seq_number) values (4,'pcdinh4',4)
insert into test2(id,username,seq_number) values (5,'pcdinh5',5)
insert into test2(id,username,seq_number) values (6,'pcdinh6',6)
insert into test2(id,username,seq_number) values (7,'pcdinh7',7)