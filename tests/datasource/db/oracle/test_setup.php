<?php

/**
 * Populate new test data or restore test data
 *
 * @since  January 18, 2009
 * @author pcdinh
 */
function refreshTestData($config)
{
    $connection = oci_connect($config['dbuser'], $config['dbpassword'], $config['db']);

    $sql1 = "DROP TABLE test2";
    $sql2 = "CREATE TABLE test2 (
      id number default NULL,
      username varchar2(100) default 'NULL',
      seq_number number default 1
    )";

    $sql3 = "insert into pcdinh.test2(id, username, seq_number) values (:user_id, :username, :seq_number)";

    // Drop table
    // ORA-00942: table or view does not exist
    $stmt = oci_parse($connection, $sql1);

    if (is_resource($stmt))
    {
        oci_execute($stmt, OCI_DEFAULT);
        oci_free_statement($stmt);
    }

    // Create table
    $stmt = oci_parse($connection, $sql2);

    if (is_resource($stmt))
    {
        oci_execute($stmt, OCI_DEFAULT);
        oci_free_statement($stmt);
    }

    for ($i   = 1; $i <= 100; $i++)
    {
        $name = 'pcdinh'.$i;
        $stmt = oci_parse($connection, sprintf($sql3, $i, $i));

        if (is_resource($stmt))
        {
            oci_bind_by_name($stmt, ":user_id", $i, -1);
            oci_bind_by_name($stmt, ":username", $name, -1);
            oci_bind_by_name($stmt, ":seq_number", $i, -1);
            oci_execute($stmt, OCI_DEFAULT);
            oci_free_statement($stmt);
        }
    }

    oci_commit($connection);
    oci_close($connection);
}

?>