<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/db/ConnectionFactory.php';
require_once 'spica/core/datasource/db/oracle/Connection.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaOracleConnection
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource/db/oracle
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      January 02, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaOracleConnectionTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaOracleConnectionTest extends PHPUnit_Framework_TestCase
{
    /**
     * Database configuration.
     *
     * @var array
     */
    private $_config;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_config = require './dbconfig.php'; // $config is defined here
        require_once './test_setup.php'; // refreshTestData() is defined here
        refreshTestData($this->_config);
    }

    /**
     *
     * @test
     */
    public function test1()
    {
        $conn = SpicaConnectionFactory::getConnection($this->_config);
        $this->assertTrue($conn instanceof SpicaOracleConnection);
        $conn->close();
    }

    /**
     * Tests default value to readOnly mode.
     *
     * @test
     */
    public function test2()
    {
        $conn = SpicaConnectionFactory::getConnection($this->_config);
        $this->assertFalse($conn->isClosed());
        $conn->close();
        $this->assertTrue($conn->isClosed());
    }

    /**
     * Tests default value to readOnly mode.
     *
     * @test
     */
    public function test3()
    {
        $conn = SpicaConnectionFactory::getConnection($this->_config);
        $this->assertTrue(false === $conn->isReadOnly());
        $conn->close();
    }

    /**
     * Tests default value to inTransaction mode.
     *
     * @test
     */
    public function test4()
    {
        $conn = SpicaConnectionFactory::getConnection($this->_config);
        $this->assertTrue(false === $conn->isInTransaction());
        $conn->close();
    }
}

?>