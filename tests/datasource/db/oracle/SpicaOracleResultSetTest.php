<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/db/ConnectionFactory.php';
require_once 'spica/core/datasource/db/oracle/Connection.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaOracleStatement
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource/db/oracle
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      January 02, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaOracleResultSetTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaOracleResultSetTest extends PHPUnit_Framework_TestCase
{
    /**
     * Database configuration.
     *
     * @var array
     */
    private $_config;

    /**
     * Database connection.
     *
     * @var SpicaOracleConnection
     */
    private $_conn;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_config = require './dbconfig.php'; // $config is defined here
        require_once './test_setup.php'; // refreshTestData() is defined here
        refreshTestData($this->_config);
        $this->_conn = SpicaConnectionFactory::getConnection($this->_config);
    }

    protected function tearDown()
    {
        $this->_conn->close();
    }

    /**
     *
     * @test
     */
    public function test1()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT * FROM test2');
        $pstmt->execute();
        $rs    = $pstmt->getResultSet();
        $data  = $rs->getAssociativeArray();
        $rowCount = count($data);
        $this->assertTrue($rowCount === 7 && 7 === $rs->rowCount(), 'Expected: 7 rows are returned. Actual: '.$rowCount);
        $rs->close();
        $pstmt->close();
    }

    /**
     *
     * @test
     */
    public function test2()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT * FROM test2');
        $pstmt->execute();
        $rs    = $pstmt->getResultSet();
        $valid = $rs->absolute(2);
        $this->assertTrue($valid === true, 'Expected: Be able to move the cursor to the second row (2) of result set. ');

        $valid = $rs->absolute(8);
        $this->assertTrue($valid === false, 'Expected: Unable to move to eighth row (8) of result set. ');

        $valid = $rs->absolute(7);
        $this->assertTrue($valid === true, 'Expected: Be able to move to seventh row (7 and the last row) of result set. ');

        $rowNum = $rs->currentRowNumber();
        $this->assertTrue($rowNum === 7, 'Expected: The current row number must be 7. ');

        $row = $rs->currentRow();
        $this->assertTrue($row === array('id' => 7, 'username' => 'pcdinh7', 'seq_number' => 7), 'Expected: Row returned contains an id value of 7, username value of pcdinh7 and seq_number value of 7. ');

        $valid = $rs->absolute(2);
        $row   = $rs->currentRow();
        $this->assertTrue($row === array('id' => 2, 'username' => 'pcdinh2', 'seq_number' => 2), 'Expected: Be able to move the cursor to second row of the result set and row returned contains an id value of 2, username value of pcdinh2 and seq_number value of 2. ');

        $rs->close();
        $pstmt->close();
    }
}

?>