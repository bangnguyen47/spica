<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/db/ConnectionFactory.php';
require_once 'spica/core/datasource/db/oracle/Connection.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaOracleCallableStatement
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource/db/oracle
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      January 02, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaOracleCallableStatementTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaOracleCallableStatementTest extends PHPUnit_Framework_TestCase
{
    /**
     * Database configuration.
     *
     * @var array
     */
    private $_config;

    /**
     * Database connection.
     *
     * @var SpicaOracleConnection
     */
    private $_conn;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_config = require './dbconfig.php'; // $config is defined here
        require_once './test_setup.php'; // refreshTestData() is defined here
        refreshTestData($this->_config);
        $this->_conn = SpicaConnectionFactory::getConnection($this->_config);
    }

    protected function tearDown()
    {
        $this->_conn->close();
    }

    /**
     *
     * @test
     */
    public function test1()
    {
        $cstmt = $this->_conn->prepareCall('CALL p1(2, 5)');
        $this->assertTrue($cstmt instanceof SpicaOracleCallableStatement && $cstmt instanceof SpicaCallableStatement, 'SpicaOracleConnection::prepareCall() returns an object of SpicaOracleCallableStatement. ');
        $cstmt->close();
    }

    /**
     * Tests if an exception is thrown if a stored statement does not exist.
     *
     * @expectedException SpicaDatabaseException
     */
    public function test2()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP PROCEDURE IF EXISTS `test`.`p1`");
        $stmt->close();
        $this->assertTrue(true === $success1, 'Expected: SpicaOracleCallableStatement::execute() The stored procedure named p1 is created successfully. ');
        $cstmt = $this->_conn->prepareCall('CALL p1(2, 5)');
        $mrs   = $cstmt->execute();
        $stmt->close();
    }

    /**
     * Tests if an exception is thrown if a stored statement contains an error.
     *
     * Expected: no exception
     */
    public function test3()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP PROCEDURE IF EXISTS `test`.`p1`");
        $success2 = $stmt->execute("
    	CREATE PROCEDURE p1(x INT, y INT)
    	DETERMINISTIC
    	BEGIN
        	SELECT
            	x ;
        	SELECT
            	x AS first_param,
            	y AS second_param;
        	SELECT
            	x,
            	y,
           		x + y AS sum_xy,
            	x * y AS prod_xy;
        	SELECT 1 FROM dual;
    	END
    	");

        $this->assertTrue(true === $success2, 'Expected: SpicaOracleCallableStatement::execute() The stored procedure named p1 is created successfully. ');

        if (false === $success2)
        {
            throw new Exception('Fixture does not work. Test environment can not be created properly');
        }

        $cstmt = $this->_conn->prepareCall('CALL p1(2, 5)');
        $mrs   = $cstmt->execute();
        $this->assertTrue(true, "No exception is thrown.");
    }

    /**
     * @depends test3
     */
    public function test4()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP PROCEDURE IF EXISTS `test`.`p1`");
        $success2 = $stmt->execute("
    	CREATE PROCEDURE p1(x INT, y INT)
    	DETERMINISTIC
    	BEGIN
        	SELECT
            	x ;
        	SELECT
            	x AS first_param,
            	y AS second_param;
        	SELECT
            	x,
            	y,
           		x + y AS sum_xy,
            	x * y AS prod_xy;
        	SELECT 1 FROM dual;
    	END
    	");

        $cstmt   = $this->_conn->prepareCall('CALL p1(2, 5)');
        $success = $cstmt->execute();
        $this->assertTrue(true === $success);
        $mrs     = $cstmt->getResultSets();
        $this->assertTrue(is_array($mrs) && count($mrs) === 4, 'SpicaCallableStatement::execute() returns an array of 4 elements (result sets). ');

        foreach ($mrs as $rs)
        {
            $this->assertTrue($rs instanceof SpicaOracleResultSet, 'SpicaCallableStatement::execute() returns an array of 4 object of SpicaOracleResultSet. ');
        }

        $cstmt->close();
        $stmt->close();
    }

    /**
     * @depends test4
     * @expectedException SpicaDatabaseException
     */
    public function test5()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP PROCEDURE IF EXISTS `test`.`p1`");
        $success2 = $stmt->execute("
    	CREATE PROCEDURE p1(x INT, y INT)
    	DETERMINISTIC
    	BEGIN
        	SELECT
            	x ;
        	SELECT
            	x AS first_param,
            	y AS second_param;
        	SELECT
            	x,
            	y,
           		x + y AS sum_xy,
            	x * y AS prod_xy;
        	SELECT 1 FROM non_existent_; -- Intentional error
    	END
    	");

        $cstmt = $this->_conn->prepareCall('CALL p1(2, 5)');
        $cstmt->execute();
        $cstmt->close();
        $stmt->close();
    }

    /**
     * @depends test5
     */
    public function test6()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP PROCEDURE IF EXISTS `test`.`p1`");
        $success2 = $stmt->execute("
    	CREATE PROCEDURE p1(x INT, y INT)
    	DETERMINISTIC
    	BEGIN
        	SELECT
            	x ;
        	SELECT
            	x AS first_param,
            	y AS second_param;
        	SELECT
            	x,
            	y,
           		x + y AS sum_xy,
            	x * y AS prod_xy;
        	SELECT 1 FROM non_existent_; -- Intentional error
    	END
    	");

        try
        {
            $cstmt = $this->_conn->prepareCall('CALL p1(2, 5)');
            $cstmt->execute();
        }
        catch (SpicaDatabaseException $e)
        {
            $this->assertTrue(true, "SpicaDatabaseException has been thrown.");
        }

        $mrs = $cstmt->getResultSets();
        $this->assertTrue(is_array($mrs) && count($mrs) === 3);
        $cstmt->close();
        $stmt->close();
    }

    /**
     * @depends test3
     */
    public function test7()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP PROCEDURE IF EXISTS `test`.`p1`");
        $success2 = $stmt->execute("
    	CREATE PROCEDURE p1(x INT, y INT)
    	DETERMINISTIC
    	BEGIN
        	SELECT
            	x ;
        	SELECT
            	x AS first_param,
            	y AS second_param;
        	SELECT
            	x,
            	y,
           		x + y AS sum_xy,
            	x * y AS prod_xy;
        	SELECT 1 FROM dual;
    	END
    	");

        $cstmt = $this->_conn->prepareCall('CALL p1(2, 5)');
        $cstmt->execute();
        $mrs = $cstmt->getResultSets();
        $this->assertTrue(is_array($mrs) && count($mrs) === 4, 'SpicaCallableStatement::execute() returns an array of 4 elements (result sets). ');

        for ($i = 0; $i < count($mrs); $i++)
        {
            switch ($i)
            {
                case 0:
                    $this->assertTrue($mrs[0] instanceof SpicaOracleResultSet, 'An object of SpicaOracleResultSet should be returned. ');
                    $data = $mrs[0]->getAssociativeArray();
                    $this->assertTrue(isset($data[0]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]) && !isset($data[1]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]['x']) && $data[0]['x'] === "2");
                    break;

                case 1:
                    $this->assertTrue($mrs[1] instanceof SpicaOracleResultSet, 'An object of SpicaOracleResultSet should be returned. ');
                    $data = $mrs[1]->getAssociativeArray();
                    $this->assertTrue(isset($data[0]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]) && !isset($data[1]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]['first_param']) && $data[0]['first_param'] === "2");
                    $this->assertTrue(isset($data[0]['second_param']) && $data[0]['second_param'] === "5");
                    break;

                case 2:
                    $this->assertTrue($mrs[2] instanceof SpicaOracleResultSet, 'An object of SpicaOracleResultSet should be returned. ');
                    $data = $mrs[2]->getAssociativeArray();
                    $this->assertTrue(isset($data[0]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]) && !isset($data[1]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]['x']) && $data[0]['x'] === "2");
                    $this->assertTrue(isset($data[0]['y']) && $data[0]['y'] === "5");
                    $this->assertTrue(isset($data[0]['sum_xy']) && $data[0]['sum_xy'] === "7");
                    $this->assertTrue(isset($data[0]['prod_xy']) && $data[0]['prod_xy'] === "10");
                    break;

                case 3:
                    $this->assertTrue($mrs[3] instanceof SpicaOracleResultSet, 'An object of SpicaOracleResultSet should be returned. ');
                    $data = $mrs[3]->getAssociativeArray();
                    $this->assertTrue(isset($data[0]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]) && !isset($data[1]), 'First result set must produce an numeric array with 1 parent element. ');
                    $this->assertTrue(isset($data[0]['1']) && $data[0]['1'] === "1");
                    break;
            }
        }

        $cstmt->close();
        $stmt->close();
    }
}

?>