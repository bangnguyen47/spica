<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/db/ConnectionFactory.php';
require_once 'spica/core/datasource/db/oracle/Connection.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaOracleStatement
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource/db/oracle
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      January 02, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaOracleStatementTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaOracleStatementTest extends PHPUnit_Framework_TestCase
{
    /**
     * Database configuration.
     *
     * @var array
     */
    private $_config;

    /**
     * Database connection.
     *
     * @var SpicaOracleConnection
     */
    private $_conn;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_config = require './dbconfig.php'; // $config is defined here
        require_once './test_setup.php'; // refreshTestData() is defined here
        refreshTestData($this->_config);
        $this->_conn = SpicaConnectionFactory::getConnection($this->_config);
    }

    protected function tearDown()
    {
        $this->_conn->close();
    }

    /**
     *
     * @test
     */
    public function test1()
    {
        $stmt = $this->_conn->createStatement();
        $stmt->addBatch("INSERT INTO test2 VALUES (1007, 'pcdinh1007', 1)");
        // $stmt->addBatch("INSERT INTO test2 VALUES (1008-, 'pcdinh5', 1)");
        // $stmt->addBatch("SELECT * FROM X");
        $stmt->addBatch("INSERT INTO test2 VALUES (1009, 'pcdinh1009', 1)");
        $stmt->addBatch("INSERT INTO test2 VALUES (1010, 'pcdinh1010', 1)");
        $this->_conn->beginTransaction();
        $updateCounts = $stmt->executeBatch();

        for ($i = 0; $i < count($updateCounts); $i++)
        {
            $cond = isset($updateCounts[$i]['affected_rows']) && 1 === $updateCounts[$i]['affected_rows'] && isset($updateCounts[$i]['last_insert_id']) && 0 === $updateCounts[$i]['last_insert_id'];
            $this->assertTrue($cond, 'Expected: SQL command number '.($i + 1).' is executed successfully. Affected row count is 1. Last insert ID must be 0');
        }

        $newUsername = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 1007');
        $this->assertTrue($newUsername === 'pcdinh1007', 'Expected: The username which has just been just inserted is pcdinh1007 although transaction is not commited yet');
        $this->_conn->commit();
        $newUsername = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 1007');
        $this->assertTrue($newUsername === 'pcdinh1007', 'Expected: The username which has just been just inserted is pcdinh1007');
        $newUsername = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 1009');
        $this->assertTrue($newUsername === 'pcdinh1009', 'Expected: The username which has just been just inserted is pcdinh1009');
        $newUsername = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 1010');
        $this->assertTrue($newUsername === 'pcdinh1010', 'Expected: The username which has just been just inserted is pcdinh1010');
        $stmt->close();
    }

    /**
     *
     * @test
     */
    public function test2()
    {
        $stmt = $this->_conn->createStatement();
        $stmt->execute('SELECT 1');
        $rs   = $stmt->getResultSet();
        $dataSet = $rs->getAssociativeArray();
        $rs->close();
        $stmt->close();
        $this->assertTrue('1' === $dataSet[0][1], 'Expected: The array returned contains 1 row and 1 column. It contains a field named 1 and that field\'s value is "1" (a string)');
    }

    /**
     *
     * @test
     */
    public function test3()
    {
        $stmt = $this->_conn->createStatement();
        $stmt->execute('SELECT * FROM test3');
        $rs   = $stmt->getResultSet();
        $data = $rs->getAssociativeArray();
        $rs->close();
        $stmt->close();
        $this->assertTrue(array() === $data, 'Expected: empty array');
    }

    /**
     *
     * @test
     */
    public function test4()
    {
        $stmt = $this->_conn->createStatement();
        $stmt->execute('SELECT * FROM quotes');
        $rs = $stmt->getResultSet();
        $this->assertTrue(0 === $rs->rowCount(), 'Expected: Row count is 0');
        $rs->close();
        $stmt->close();
    }

    /**
     *
     * @test
     */
    public function test5()
    {
        $stmt = $this->_conn->createStatement();
        $stmt->execute('SELECT PRODUCT_ID FROM product_history_prices');
        $rs = $stmt->getResultSet();
        $this->assertTrue(9 === $rs->rowCount(), 'Expected: Row count is 9');
        $rs->close();
        $stmt->close();
    }

    /**
     *
     * @test
     */
    public function test6()
    {
        $stmt = $this->_conn->createStatement();
        $stmt->execute('SELECT * FROM product_history_prices');
        $rs = $stmt->getResultSet();
        $this->assertTrue(9 === $rs->rowCount(), 'Expected: Row count is 9');
        $rs->close();
        $stmt->close();
    }

    /**
     * Tests on SpicaOracleStatement::cancel().
     *
     * @test
     */
    public function test7()
    {
        $stmt = $this->_conn->createStatement();
        $stmt->cancel();
        $this->assertTrue(true === $stmt->cancel(), 'Expected: Long running statement will be cancelled');
        $stmt->close();
    }

    /**
     * Tests on getting connection thread ID.
     *
     * @test
     */
    public function test8()
    {
        $stmt = $this->_conn->createStatement();
        $id   = $stmt->getConnectionId();
        $this->assertTrue(is_int($id) && $id > 0, 'Expected: Connection ID is an number that is bigger than 0. Actual: '.$id);
        $stmt->close();
    }

    /**
     * Tests on delete()
     *
     * @test
     */
    public function test9()
    {
        $stmt = $this->_conn->createStatement();

        // Conditions as an array
        $deleteCond = array(
          'id' => '> 999999',
        );

        $affectedRowCount = $stmt->delete('test2', $deleteCond);
        $this->assertTrue($affectedRowCount === 0, 'Expected: No row is affected because the target ID given does not exist intentionally: 999999');

        // Conditions as an array
        $deleteCond = array(
          'id' => '= 4',
        );
        $affectedRowCount = $stmt->delete('test2', $deleteCond);
        $this->assertTrue($affectedRowCount === 1, 'Expected: One row is deleted: ID = 4');

        // Conditions as a string
        $deleteCond  = 'id = 5 AND username = \'pcdinh5\'';
        $affectedRowCount = $stmt->delete('test2', $deleteCond);
        $this->assertTrue($affectedRowCount === 1, 'Expected: One row is deleted: ID = 5');

        $rs = $stmt->fetchAll('SELECT * FROM test2 WHERE id = 5');
        $this->assertTrue(count($rs) === 0, 'Expected: One row with ID field of 5 can not be found because it is deleted. ');
        $stmt->close();
    }

    /**
     * Tests on execute().
     *
     * @test
     */
    public function test10()
    {
        $stmt    = $this->_conn->createStatement();
        $success = $stmt->execute('SELECT username FROM test2 WHERE id = 4');
        $rs      = $stmt->getResultSet();
        $this->assertTrue(is_bool($success), 'Expected: SpicaOracleStatement::execute() must return a boolean value. ');
        $this->assertTrue($success === true, 'Expected: SpicaOracleStatement::execute() must return a TRUE value because SQL command should be executed successfully. ');
        $this->assertTrue($rs instanceof SpicaOracleResultSet, 'Expected: An instance of SpicaOracleResultSet. ');
        $this->assertTrue($rs instanceof SpicaResultSet, 'Expected: An instance of SpicaResultSet. ');
        $this->assertTrue($rs->currentRowNumber() === 0, 'Expected: SpicaOracleResultSet::next() should return 0 (before first row)');

        $hasNext = $rs->next();
        $this->assertTrue(true === $hasNext, 'Expected: SpicaOracleResultSet::next() should return true because the query should return at least 1 row.');
        $this->assertTrue(1 === ($row = $rs->currentRowNumber()), 'Expected: 1 (first row). Actual: '.$row);

        $hasNext = $rs->next();
        $this->assertTrue($hasNext === false, 'Expected: false (no row can be fetched) ');
        $stmt->close();
    }

    /**
     * Tests on fetchAll()
     *
     * @test
     */
    public function test11()
    {
        $stmt = $this->_conn->createStatement();
        $rs   = $stmt->fetchAll('SELECT * FROM test2');

        $this->assertTrue(is_array($rs), 'Expected: fetchAll() should return an array. ');
        $this->assertTrue(count($rs) > 1, 'Expected: fetchAll() should return an array with more than 1 row (based on given test data). ');
        $this->assertTrue(isset($rs[0]['id']) && isset($rs[0]['username']), 'Expected: fetchAll() should return an array those each element contain fields named "id" and "username". ');
        $this->assertTrue(isset($rs[0]) && is_array($rs[0]), 'Expected: Result set array is a two dimentaional array. ');

        $stmt->close();
    }

    /**
     * Tests on fetchOne()
     *
     * @test
     */
    public function test12()
    {
        $stmt = $this->_conn->createStatement();
        $rs   = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 4');
        $this->assertTrue(!is_array($rs), 'fetchOne() can not return an array. ');
        $this->assertTrue(is_string($rs) || is_null($rs), 'fetchOne() should return a string or null value. Actual type is '.gettype($rs));
        $this->assertTrue('pcdinh4' === $rs, 'fetchOne() should return a string that is equal to "pcdinh4". ');

        $stmt->close();
    }

    /**
     * Tests on fetchPage()
     *
     * @test
     */
    public function test13()
    {
        $stmt = $this->_conn->createStatement();
        $rs   = $stmt->fetchPage('SELECT * FROM test2 ORDER BY seq_number ASC', 1, 2);
        $this->assertTrue(is_array($rs), 'Expected: An array is returned. ');
        $this->assertTrue((count($rs) === 2), 'Expected: Return array contains 2 elements. ');

        $cond = isset($rs[0]['id']) && isset($rs[0]['username']) && isset($rs[0]['seq_number']) &&
        isset($rs[1]['id']) && isset($rs[1]['username']) && isset($rs[1]['seq_number']);

        $this->assertTrue($cond, 'Expected: Each child element in return array contains 3 fields: id, username and seq_number. ');
        $this->assertTrue(isset($rs[0]['seq_number']) && $rs[0]['seq_number'] === '1', 'Expected: The value of "seq_number" field in the first row in the return array must be 1. ');
        $this->assertTrue(isset($rs[1]['seq_number']) && $rs[1]['seq_number'] === '2', 'Expected: The value of "seq_number" field in the second row in the return array must be 2. ');
        $stmt->close();
    }

    /**
     * Tests on fetchRow()
     *
     * @test
     */
    public function test14()
    {
        $stmt = $this->_conn->createStatement();
        $rs   = $stmt->fetchRow('SELECT id, username FROM test2 WHERE id = 4');

        $this->assertTrue(is_array($rs), 'Expected: Result set is an array. ');
        $this->assertTrue(isset($rs['id']) && isset($rs['username']), 'Expected: Result set array contains 2 fields named: id and username. ');
        $this->assertTrue(!(isset($rs[0]) && is_array($rs[0])), 'Expected: Result set array is not a two dimentional array. ');
        $stmt->close();
    }

    /**
     * Tests on insert()
     *
     * @test
     */
    public function test15()
    {
        $stmt = $this->_conn->createStatement();

        $insertData = array(
          'username'   => 'pcdinh8',
          'id'         => 8,
          'seq_number' => 8,
        );

        $affectedRowCount = $stmt->insert('test2', $insertData);
        $this->assertTrue($affectedRowCount === 1, 'Expected: One row is affected. ');
        $justInsertUsername = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 8');
        $this->assertTrue($justInsertUsername === 'pcdinh8', 'Expected: The username which has just been just inserted is pcdinh8');

        $stmt->close();
    }

    /**
     * Tests on replace()
     *
     * @test
     */
    public function test16()
    {
        $stmt = $this->_conn->createStatement();

        $table = 'test2';
        $data  = array(
          'username'   => 'pcdinh7',
          'seq_number' => 66666
        );
        $updated = $stmt->replace($table, $data);

        $this->assertTrue(is_int($updated), 'Expected: The return value must be an integer. Actual value: '.$updated);
        $this->assertTrue($updated === 1, 'Expected: There is one row updated/inserted (1). ');

        $table = 'test2';
        $data  = array(
          'username'   => 'pcdinh7',
          'seq_number' => 66666
        );
        $updated = $stmt->replace($table, $data);
        $this->assertTrue($updated === 1, 'Expected: There is one row updated/inserted (2). ');

        $success = $stmt->execute('DELETE FROM test2 WHERE username = \'pcdinh7\' AND seq_number = 66666');
        $success = $stmt->execute('CREATE UNIQUE INDEX test2_username_idx ON test2 (username)');

        $table = 'test2';
        $data  = array(
          'id' => 100,
          'username'   => 'pcdinh7',
          'seq_number' => 66666
        );

        $updated = $stmt->replace($table, $data);
        $this->assertTrue($updated === 2, 'Expected: There is 2 row updated/inserted (3). Actual: '.$updated);

        $stmt->close();
    }

    /**
     * Tests on update()
     *
     * @test
     */
    public function test17()
    {
        $stmt = $this->_conn->createStatement();

        $updateData  = array(
          'username' => 'pcdinh4.1',
        );

        // Conditions as an array
        $updateCond  = array(
          'id'       => '= 4',
        );

        $affectedRowCount = $stmt->update('test2', $updateData, $updateCond);
        $this->assertTrue($affectedRowCount === 1, 'Expected: One row is updated. ');
        $currentData = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 4');
        $this->assertTrue($currentData === 'pcdinh4.1', 'Expected: User who has an ID of 4 has his username updated to '.$currentData);
        $updateData  = array(
          'username' => 'pcdinh4',
        );

        // Conditions as a string
        $updateCond  = 'id = 4';
        $affectedRowCount = $stmt->update('test2', $updateData, $updateCond);
        $currentData = $stmt->fetchOne('SELECT username FROM test2 WHERE id = 4');
        $this->assertTrue($currentData === 'pcdinh4', 'Expected: User who has an ID of 4 has his username re-updated to '.$currentData);

        $stmt->close();
    }

    /**
     * Tests on explain()
     *
     * @test
     */
    public function test18()
    {
        $stmt   = $this->_conn->createStatement();
        $string = $stmt->explain('SELECT * FROM test2 WHERE id = 7');
        $this->assertTrue(is_string($string));
        $stmt->close();
    }
}

?>