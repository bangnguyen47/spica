<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/db/ConnectionFactory.php';
require_once 'spica/core/datasource/db/oracle/Connection.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaOracleMultipleQueryStatement
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource/db/oracle
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      January 02, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaOracleMultipleQueryStatementTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaOracleMultipleQueryStatementTest extends PHPUnit_Framework_TestCase
{
    /**
     * Database configuration.
     *
     * @var array
     */
    private $_config;

    /**
     * Database connection.
     *
     * @var SpicaOracleConnection
     */
    private $_conn;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_config = require './dbconfig.php'; // $config is defined here
        require_once './test_setup.php'; // refreshTestData() is defined here
        refreshTestData($this->_config);
        $this->_conn = SpicaConnectionFactory::getConnection($this->_config);
    }

    protected function tearDown()
    {
        $this->_conn->close();
    }

    /**
     *
     * @test
     */
    public function test1()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP FUNCTION IF EXISTS `test`.`is_odd`");
        $this->assertTrue($success1 === true, 'The statement "DROP FUNCTION IF EXISTS" should return true. ');

        $success2 = $stmt->execute("
        CREATE FUNCTION `test`.`is_odd` (input_number INT) RETURNS INT
        BEGIN
          DECLARE is_odd_test INT;

          IF MOD(input_number, 2) = 0 THEN
                SET is_odd_test = FALSE;
          ELSE
                SET is_odd_test = TRUE;
          END IF;

        RETURN(is_odd_test);
        END");

        $this->assertTrue($success2 === true, 'The statement should return true on the command "CREATE FUNCTION". ');
        $stmt->close();
    }

    /**
     *
     * @test
     */
    public function test2()
    {
        $stmt = $this->_conn->createMultipleQueryStatement();
        $sql = '
    	DELIMITER $$

    	DROP FUNCTION IF EXISTS `test`.`is_odd` $$
    	CREATE FUNCTION `test`.`is_odd` () RETURNS INT
    	BEGIN
        	DECLARE v_isodd INT;

        	IF MOD(input_number, 2) = 0 THEN
                SET v_isodd = FALSE;
        	ELSE
                SET v_isodd = TRUE;
        	END IF;

        	RETURN(v_isodd);
    	END $$

    	DELIMITER ;
    	';

        $stmt = $this->_conn->createMultipleQueryStatement();
        $stmt->setDelimeter('$$');
        $success = $stmt->execute($sql);
        $stmt->close();
    }

    /**
     *
     * @test
     */
    public function test3()
    {
        /* @var $stmt SpicaOracleMultipleQueryStatement */
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $success1 = $stmt->execute("DROP FUNCTION IF EXISTS `test`.`is_odd`");
        $success2 = $stmt->execute("
        CREATE FUNCTION `test`.`is_odd` (input_number INT) RETURNS INT
        BEGIN
            DECLARE is_odd_test INT;

            IF MOD(input_number, 2) = 0 THEN
                SET is_odd_test = FALSE;
            ELSE
        	    SET is_odd_test = TRUE;
            END IF;

            RETURN(is_odd_test);
        END");

        $rs = $stmt->getResultSet();
        $this->assertTrue($rs === null, 'SpicaOracleMultipleQueryStatement::getResultSet() should return null before SpicaOracleMultipleQueryStatement::getMoreResults() is called. ');

        $more = $stmt->getMoreResults();
        $this->assertTrue($more === true, 'After SpicaOracleMultipleQueryStatement::execute() on CREATE FUNCTION, the statement should contain a result set. ');

        $rs = $stmt->getResultSet();
        $this->assertTrue($rs instanceof SpicaResultSet, 'Should return an object of SpicaResultSet. ');
        $this->assertTrue($rs->isAvailable() === false, 'Not a SELECT query so there is no result set. ');

        $stmt->close();
    }

    /**
     * @depends test3
     * @test
     */
    public function test4()
    {
        $stmt     = $this->_conn->createMultipleQueryStatement();
        $stmt->execute("DROP FUNCTION IF EXISTS `test`.`is_odd`");
        $stmt->execute("
        CREATE FUNCTION `test`.`is_odd` (input_number INT) RETURNS INT
        BEGIN
            DECLARE is_odd_test INT;

            IF MOD(input_number, 2) = 0 THEN
                SET is_odd_test = FALSE;
            ELSE
        	    SET is_odd_test = TRUE;
            END IF;

            RETURN(is_odd_test);
        END");

        $success = $stmt->execute('SELECT is_odd(4)');

        $allRs = $stmt->getResultSets();
        $this->assertTrue($success === true);
        $this->assertTrue(is_array($allRs) && count($allRs) === 1, 'The total of result sets that are retrieved after the execution of SpicaOracleMultipleQueryStatement::execute() must be 1. ');

        $more = $stmt->getMoreResults();
        $this->assertTrue($more === true);

        $rs = $stmt->getResultSet();
        $this->assertTrue($rs->isAvailable() === true, "Result set should be available for SELECT");

        $data = $rs->getAssociativeArray();
        $this->assertTrue(is_array($data));

        $this->assertTrue(isset($data[0]['is_odd(4)']) && $data[0]['is_odd(4)'] == 0, 'The execution of the command SELECT is_odd(4) should return an array which contains 1 row and a field named "is_odd(4)". ');

        $stmt->close();
    }

    /**
     * Tests if invalid query can cause an exception as desired.
     *
     * @depends test3
     * @expectedException SpicaDatabaseException
     */
    public function test5()
    {
        $stmt = $this->_conn->createMultipleQueryStatement();
        $stmt->execute('SELECT call_function_existant(4)');
    }
}

?>