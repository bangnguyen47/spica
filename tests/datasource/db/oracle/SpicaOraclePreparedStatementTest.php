<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/db/ConnectionFactory.php';
require_once 'spica/core/datasource/db/oracle/Connection.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaOraclePreparedStatement
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource/db/oracle
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      January 02, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaOraclePreparedStatementTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaOraclePreparedStatementTest extends PHPUnit_Framework_TestCase
{
    /**
     * Database configuration.
     *
     * @var array
     */
    private $_config;

    /**
     * Database connection.
     *
     * @var SpicaOracleConnection
     */
    private $_conn;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_config = require './dbconfig.php'; // $config is defined here
        require_once './test_setup.php'; // refreshTestData() is defined here
        refreshTestData($this->_config);
        $this->_conn = SpicaConnectionFactory::getConnection($this->_config);
    }

    protected function tearDown()
    {
        $this->_conn->close();
    }

    /**
     * Tests on addBatch() and executeBatch()
     *
     * @test
     */
    public function test1()
    {
        $pstmt  = $this->_conn->prepareStatement('INSERT INTO test2 (id, username) VALUES (:id, :username)');

        for ($i = 11; $i < 20; $i++)
        {
            $params = array(':id' => $i, ':username' => 'pcdinh'.$i);
            $pstmt->bindParams($params);
            $pstmt->addBatch();
        }

        $updateCount = $pstmt->executeBatch();
        $count       = 0;

        foreach ($updateCount as $number => $updateInfo)
        {
            $count += $updateInfo['affected_rows'];
        }

        $this->assertTrue($count === 9, 'Expected: 9 rows are created. ');
        $pstmt->close();
    }

    /**
     * Tests on bindParams() and execute()
     *
     * @test
     */
    public function test2()
    {
        $sql    = 'INSERT INTO test2 (id, username) VALUES (:id, :name)';
        $pstmt  = $this->_conn->prepareStatement($sql);
        $params = array(
          ':id'   => array('value' => 1000, 'type' => 'int'),
          ':name' => 'pcdinh1000',
        );
        $pstmt->bindParams($params);
        $hasRs  = $pstmt->execute();

        $this->assertTrue($hasRs === false, 'Expected: SpicaOraclePreparedStatement::execute() returns false because SQL statement is an INSERT. ');

        $sql    = 'SELECT * FROM test2 WHERE id = :id AND username = :name';
        $pstmt  = $this->_conn->prepareStatement($sql);
        $params = array(
          ':id'   => array('value' => 1000, 'type' => 'int'),
          ':name' => 'pcdinh1000',
        );
        $pstmt->bindParams($params);
        $rsFound = $pstmt->execute();

        $this->assertTrue($rsFound === true, 'Expected: SpicaOraclePreparedStatement::execute() should return false when the statement is an SELECT
    which really returns a result set. ');

        $rs = $pstmt->getResultSet();
        $this->assertTrue($rs->isAvailable() === true, 'Expected: The SpicaResultSet object returns by SpicaOraclePreparedStatement::getResultSet() should be in AVAILABLE state. ');
        $this->assertTrue($rs->rowCount() === 1, 'Expected: The SpicaResultSet object returns by SpicaOraclePreparedStatement::getResultSet() contains a row. ');
        $this->assertTrue($rs->getAssociativeArray() === array( 0 => array('id' => 1000, 'username' => 'pcdinh1000', 'seq_number' => 1)),
    'Expected: The ResultSet object returns by SpicaOraclePreparedStatement::getResultSet() contains a row that includes
    the data inserted from the preceding SQL statement. ');

        $pstmt->close();
    }

    /**
     * Tests on getResultSet() and execute()
     *
     * @test
     */
    public function test3()
    {
        $sql     = 'SELECT id, username, seq_number FROM test2';
        $pstmt   = $this->_conn->prepareStatement($sql);
        $rsFound = $pstmt->execute();
        $rs      = $pstmt->getResultSet();

        $this->assertTrue($rsFound === true, 'The query should return a result set. ');
        $this->assertTrue(3 === $rs->getMetaData()->getColumnCount(), 'The result set should contains 3 fields. ');
        $pstmt->close();
    }

    /**
     * Tests on getResultSet() and execute()
     *
     * @test
     */
    public function test4()
    {
        $sql    = 'INSERT INTO test2 (id, username) VALUES (?, ?)';
        $pstmt  = $this->_conn->prepareStatement($sql);
        $params = array(
        0 => array('value' => 1000, 'type' => 'int'),
        1 => 'pcdinh1000'
        );
        $pstmt->bindValues($params);
        $rsFound = $pstmt->execute();
        $this->assertTrue($rsFound === false, 'Expected: SpicaOraclePreparedStatement::execute() should return false when the statement is an INSERT. ');

        $sql    = 'SELECT * FROM test2 WHERE id = ? AND username = ?';
        $pstmt  = $this->_conn->prepareStatement($sql);
        $params = array(
        0 => array('value' => 1000, 'type' => 'int'),
        1 => 'pcdinh1000'
        );
        $pstmt->bindValues($params);
        $rsFound = $pstmt->execute();

        $this->assertTrue($rsFound === true, 'Expected: SpicaOraclePreparedStatement::execute() should return false when the statement is an SELECT which really returns a result set. ');

        $rs = $pstmt->getResultSet();
        $this->assertTrue($rs->isAvailable() === true, 'Expected: The SpicaResultSet object returns by SpicaOraclePreparedStatement::getResultSet() should be in AVAILABLE state. ');
        $this->assertTrue($rs->rowCount() === 1, 'Expected: The SpicaResultSet object returns by SpicaOraclePreparedStatement::getResultSet() contains a row. ');
        $this->assertTrue($rs->getAssociativeArray() === array( 0 => array('id' => 1000, 'username' => 'pcdinh1000', 'seq_number' => 1)), 'Expected: The SpicaResultSet object returns by SpicaOraclePreparedStatement::getResultSet() contains a row that includes
    the data inserted from the preceding SQL statement. ');

        $pstmt->close();
    }

    /**
     * Tests on fetchAll()
     *
     * @test
     */
    public function test5()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT username AS userName FROM test2');
        $data  = $pstmt->fetchAll();
        $this->assertTrue(is_array($data) && 7 === count($data), 'Expected: 7 rows are returned. Actual: '.count($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchColumn()
     *
     * @test
     */
    public function test6()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT username AS userName FROM test2');
        $data  = $pstmt->fetchColumn();

        $this->assertTrue(is_array($data), 'Expected: A array must be returned. Actual: '.gettype($data));
        $this->assertTrue(is_array($data) && count($data) === 7, 'Expected: A array of 7 elements must be returned. Actual: '.count($data));
        $this->assertTrue(is_array($data) && count($data) === 7 && $data[6] = 'pcdinh7', 'Expected: A array of 7 elements must be returned and the value of the last element must be "pcdinh7". Actual: '.gettype($data['6']));
        $pstmt->close();
    }

    /**
     * Tests on fetchOne()
     *
     * @test
     */
    public function test7()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT username AS userName FROM test2');
        $data  = $pstmt->fetchOne();
        $this->assertTrue('pcdinh1' === $data, 'Expected: A string "pcdinh1" is returned. Actual: '.$data);
        $pstmt->close();
    }

    /**
     * Tests on fetchPage()
     *
     * @test
     */
    public function test8()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT username AS userName FROM test2');
        $data  = $pstmt->fetchPage(1, 2);
        $this->assertTrue(is_array($data) && count($data) > 0, 'Expected: A non-empty array must be returned. Actual: '.gettype($data));
        $this->assertTrue(is_array($data) && count($data) === 2, 'Expected: A array that contains 2 elements must be returned. Actual: '.count($data));
        $this->assertTrue(is_array($data) && isset($data[1]['username']), 'Expected: The second elements of the array contains a key named "username". Actual: '.gettype($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchPage()
     *
     * @test
     */
    public function test9()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchPage(2, 3);
        $this->assertTrue(is_array($data) && count($data) === 3, 'Expected: A non-empty array that contains 3 elements must be returned. Actual: '.count($data));
        $this->assertTrue(is_array($data) && isset($data[2]['username']) && isset($data[2]['id']) && $data[2]['username'] === 'pcdinh6' && $data[2]['id'] === 6, 'Expected: The last element of the array has keys named "username" and "id" and their values are "pcdinh6" and 6, respectively. ');
        $pstmt->close();
    }

    /**
     * Tests on fetchPage()
     *
     * @test
     */
    public function test10()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2 WHERE username LIKE \'%NON-EXISTANT%\'');
        $data  = $pstmt->fetchPage(1, 2);
        $this->assertTrue(is_array($data) && count($data) === 0, 'Expected: A empty array must be returned. Actual: '.count($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchPage()
     *
     * @test
     */
    public function test11()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchPage(3, 6);
        $this->assertTrue(is_array($data) && count($data) === 0, 'Expected: A empty array must be returned. Actual: '.count($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchPage()
     *
     * @test
     */
    public function test12()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchPage(3, 3);

        $this->assertTrue(is_array($data) && count($data) === 1, 'Expected: A array that contains 1 element must be returned. Actual: '.count($data));
        $this->assertTrue(is_array($data) && count($data) === 1 && $data[0]['username'] === 'pcdinh7', 'Expected: A array that contains 1 element must be returned and that element contains a key named "username" and its associated value is "pcdinh7".');
        $pstmt->close();
    }

    /**
     * Tests on fetchRange()
     *
     * @test
     */
    public function test13()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchRange(3, 3);
        $this->assertTrue(is_array($data) && count($data) === 1, 'Expected: A array that contains 1 element must be returned. Actual: '.count($data));
        $this->assertTrue(is_array($data) && count($data) === 1 && $data[0]['username'] === 'pcdinh4', 'Expected: A array that contains 1 element must be returned
    and that element contains a key named "username" and its associated value is "pcdinh4".');
        $pstmt->close();
    }

    /**
     * Tests on fetchRange()
     *
     * @test
     */
    public function test14()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchRange(1, 100);
        $this->assertTrue(is_array($data) && count($data) === 6, 'Expected: A non-empty array that contains 6 elements must be returned although a range that contains 100 elements is requested. Actual: '.count($data));
        $this->assertTrue(is_array($data) && isset($data[0]['username']) && $data[0]['username'] !== 'pcdinh1' && isset($data[1]['username']) && $data[1]['username'] !== 'pcdinh1', 'Expected: The array of data returned does not contain the first row in the test table. Offset 0 is not returned. ');
        $pstmt->close();
    }

    /**
     * Tests on fetchRange()
     *
     * @test
     */
    public function test15()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT username AS userName FROM test2');
        $data  = $pstmt->fetchRange(1, 2);
        $this->assertTrue(is_array($data) && count($data) > 0, 'Expected: A non-empty array must be returned. Actual: '.gettype($data).'. Count: '.count($data));
        $this->assertTrue(is_array($data) && count($data) === 2, 'Expected: A array that contains 2 elements must be returned. Actual: '.count($data));
        $this->assertTrue(is_array($data) && isset($data[0]['username']) && $data[0]['username'] !== 'pcdinh1' && isset($data[1]['username']) && $data[1]['username'] !== 'pcdinh1', 'Expected: The array of data returned does not contain the first row in the test table. Offset 0 is not returned. ');
        $this->assertTrue(is_array($data) && isset($data[1]['username']) && $data[1]['username'] === 'pcdinh3', 'Expected: The second elements of the array contains a key named "username" and its associated value "pcdinh3". Actual: '.count($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchRange()
     *
     * @test
     */
    public function test16()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchRange(0, 5);
        $this->assertTrue(is_array($data) && count($data) === 6, 'Expected: A non-empty array that contains 6 elements must be returned although a range that contains 6 elements is requested. Actual: '.count($data));
        $this->assertTrue(is_array($data) && isset($data[0]['username']) && $data[0]['username'] === 'pcdinh1' && isset($data[count($data) - 1]['username']) && $data[count($data) - 1]['username'] !== 'pcdinh7', 'Expected: The array of data returned does contain the first row in the test table. Offset 0 is returned. The last element in the
    returned array is not the last row in the test table. ');
        $pstmt->close();
    }

    /**
     * Tests on fetchRange()
     *
     * @test
     */
    public function test17()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2 WHERE username LIKE \'%NON-EXISTANT%\'');
        $data  = $pstmt->fetchRange(1, 2);
        $this->assertTrue(is_array($data) && count($data) === 0, 'Expected: A empty array must be returned. Actual: '.count($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchRange()
     *
     * @test
     */
    public function test18()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchRange(7, 10);
        $this->assertTrue(is_array($data) && count($data) === 0, 'Expected: A empty array must be returned. Actual: '.count($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchRow()
     *
     * @test
     */
    public function test19()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT username AS userName FROM test2');
        $data  = $pstmt->fetchRow();
        $this->assertTrue(is_array($data) && count($data) > 0, 'Expected: A non-empty array must be returned. Actual: '.gettype($data));
        $this->assertTrue(is_array($data) && count($data) === 1, 'Expected: A array that contains 1 element must be returned. Actual: '.count($data));
        $this->assertTrue(is_array($data) && isset($data['username']), 'Expected: A array that has a key named "username". Actual: '.gettype($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchRow()
     *
     * @test
     */
    public function test20()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2');
        $data  = $pstmt->fetchRow();
        $this->assertTrue(is_array($data) && count($data) > 1, 'Expected: A non-empty array must be returned. Actual: '.gettype($data));
        $this->assertTrue(is_array($data) && count($data) === 2, 'Expected: A array with 2 elements must be returned. Actual: '.gettype($data));
        $this->assertTrue(is_array($data) && isset($data['username']) && isset($data['id']), 'Expected: A array that has keys named "username" and "id". Actual: '.gettype($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchRow()
     *
     * @test
     */
    public function test21()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT id, username AS userName FROM test2 WHERE username LIKE \'%NON-EXISTANT%\'');
        $data  = $pstmt->fetchRow();
        $this->assertTrue(is_array($data) && count($data) === 0, 'Expected: A empty array must be returned. Actual: '.gettype($data));
        $pstmt->close();
    }

    /**
     * Tests on fetchRow()
     *
     * @test
     */
    public function test22()
    {
        $pstmt = $this->_conn->prepareStatement('SELECT username AS userName FROM test2');
        $data  = $pstmt->fetchAll(SpicaResultSet::IDENTIFIER_MIXEDCASE);
        $this->assertTrue(isset($data[6]) && is_array($data[6]) && 7 === count($data), 'Expected: 7 rows are returned. Actual: '.count($data));
        $this->assertTrue(isset($data[6]['userName']), 'Expected: The 7th row contains a field named. userName. ');

        $data = $pstmt->fetchAll(SpicaResultSet::IDENTIFIER_LOWERCASE);
        $this->assertTrue(isset($data[6]['username']), 'Expected: The 7th row contains a field named. username. ');

        $data = $pstmt->fetchAll(SpicaResultSet::IDENTIFIER_UPPERCASE);
        $this->assertTrue(isset($data[6]['USERNAME']), 'Expected: The 7th row contains a field named. USERNAME. ');
        $pstmt->close();
    }
}

?>