<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/library'
);

require_once 'spica/core/datasource/db/ConnectionFactory.php';
require_once 'spica/core/datasource/db/mysql/Connection.php';
require_once 'PHPUnit/Framework.php';

/**
 * Set of test cases and assertions to ensure that SpicaMySQLCommand
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core/datasource/db/mysql
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 05, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaMySQLCommandTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaMySQLCommandTest extends PHPUnit_Framework_TestCase
{

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {

    }

    /**
     *
     * @test
     */
    public function test1()
    {
        $sql = '-- Test SQL
        SELECT * FROM test2';

        $offset = SpicaMySQLCommand::skipComments($sql, 0);
        $this->assertEquals(true, 11 === $offset, 'Expected: The query <pre>'.$sql.'</pre> starts actually at character 11. ');
    }
}

?>