/*
SQLyog Enterprise - MySQL GUI v7.12 RC2
MySQL - 5.0.67-community-nt : Database - test
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test`;

/*Table structure for table `test2` */

DROP TABLE IF EXISTS `test2`;

CREATE TABLE `test2` (
  `id` int(11) default NULL,
  `username` varchar(100) default NULL,
  `seq_number` int(11) default '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `test2` */

insert  into `test2`(`id`,`username`,`seq_number`) values (1,'pcdinh1',1),(2,'pcdinh2',2),(3,'pcdinh3',3),(4,'pcdinh4',4),(5,'pcdinh5',5),(6,'pcdinh6',6),(7,'pcdinh7',7);

/*Table structure for table `test3` */

DROP TABLE IF EXISTS `test3`;

CREATE TABLE `test3` (
  `id` int(11) default NULL,
  `username` varchar(100) default NULL,
  `seq_number` int(11) default '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `quotes` */

DROP TABLE IF EXISTS `quotes`;

CREATE TABLE `quotes` (
  `quote_ticker` varchar(10) default NULL,
  `quote_volume` int(10) default NULL,
  `quote_high_price` double default NULL,
  `quote_low_price` double default NULL,
  `quote_last_trade_price` double default NULL,
  `quote_previous_trade_price` double default NULL,
  `quote_open_price` double default NULL,
  `quote_close_price` double default NULL,
  `quote_trading_date` date default NULL,
  `quote_trading_time` datetime default NULL,
  `quote_session` int(11) default '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `product_history_prices` */

DROP TABLE IF EXISTS `product_history_prices`;

CREATE TABLE `product_history_prices` (
  `product_id` int(11) default NULL,
  `effective_date` date default NULL,
  `price` int(11) default NULL,
  `quantity_available` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `product_history_prices` */

insert  into `product_history_prices`(`product_id`,`effective_date`,`price`,`quantity_available`) values (1,'2008-10-12',100,102),(2,'2008-10-10',101,800),(3,'2008-10-01',109,209),(1,'2008-10-13',102,289),(2,'2008-10-08',10,208),(1,'2008-01-12',20,200),(1,'2008-10-09',100,202),(1,'2008-10-16',108,17),(1,'2008-07-15',108,109);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
