<?php

function xml_write()
{
	$xml = new XmlWriter();
	$xml->openMemory();
	$xml->startDocument('1.0', 'UTF-8');
	$xml->startElement('root');
	      
	function write(XMLWriter $xml, $data)
	{
		foreach($data as $key => $value)
		{
			if (true === is_numeric($key))
			{
				$key = 'rows';
			}

			if (is_array($value))
			{
				$xml->startElement($key);
				write($xml, $value);
				$xml->endElement();
				continue;
			}

			$xml->writeElement($key, $value);
		}
	}

	write($xml, array(
	array(
	    'members' => array(
	array('name' => 'pcdinh', 'age' => '28.9', 'intro' => '<strong>Not available</strong>'),
	array('name' => 'pcdinh2', 'age' => '29.9', 'intro' => '<strong>Not available</strong>', 'skills' => array('java', 'php')),
	),
	     'company'        => 'WVB Inc.',
	     'establish_date' => '1985-12-03'
	     ),
	     ));

	     $xml->endElement();
	     echo '<pre>'.$xml->outputMemory(true).'</pre>';
}
?>