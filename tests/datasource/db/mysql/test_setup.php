<?php

/**
 * Populate new test data or restore test data
 *
 * @since  November 08, 2008
 * @author pcdinh
 */
function refreshTestData($config)
{
    $mysqli = new mysqli($config['dbhost'], $config['dbuser'], $config['dbpassword'], $config['db']);

    try
    {
        // Checks connection validity
        if (mysqli_connect_errno())
        {
            throw new Exception("Test data can not be setup or restored. \nConnect failed: ".mysqli_connect_error());
        }

        $mysqli->autocommit(true);

        $filePath = dirname(__FILE__).'/test2.sql';

        if (false === file_exists($filePath))
        {
            throw new Exception("Test data file can not be found. \n");
        }

        $query    = file_get_contents($filePath);
        // See: http://us.php.net/mysqli_multi_query
        $success  = $mysqli->multi_query($query);

        if (false === $success)
        {
            throw new Exception("Unable to execute SQL commands. Error: ".$mysqli->error);
        }

        // socket is still blocked. See: http://us.php.net/mysqli_multi_query
        // so I do a non-sense here to release the socket or mysqi claims that test.test2 database does not exist

        while ($mysqli->more_results())
        {
            $mysqli->next_result();
        }
    }
    catch (Exception $ex)
    {
        echo $ex->getMessage();
    }

    $mysqli->close();
}

?>