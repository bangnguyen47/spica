<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php'; // SpicaSession here
include_once 'core/session/store/Memcached.php';

/**
 * Set of test cases and assertions to ensure that SpicaSessionMemcachedStore works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core\session
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 05, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaSessionMemcachedStoreTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaSessionMemcachedStoreTest extends PHPUnit_Framework_TestCase
{
    /**
     * PHP session options.
     *
     * @var array
     */
    public $opt = array(
      'name' => 'spicatest',
      'id' => 'abcabcabc999'
    );

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        
    }

    /**
     * Asserts that SpicaSessionMemcacheStore::start() works.
     *
     * @test
     */
    public function testStartedAndClosed()
    {
        $this->assertEquals(false, SpicaSession::isStarted());
        SpicaSession::start($this->opt);
        $this->assertEquals(true, SpicaSession::isStarted());
        SpicaSession::removeAll();
        SpicaSession::commitAndClose();
        $this->assertEquals(false, SpicaSession::isStarted());
        SpicaSession::start($this->opt);
        $this->assertEquals(true, SpicaSession::isStarted());

        SpicaSession::removeAll();
        SpicaSession::commitAndClose();
        $this->assertEquals(false, SpicaSession::isStarted());
        $this->assertEquals(true, SpicaSession::isClosed());
    }

    /**
     * Asserts that SpicaSessionMemcacheStore::start() works.
     *
     * @test
     */
    public function testStartedWithNoHandler()
    {
        $this->assertEquals(false, SpicaSession::isStarted());
    }

    /**
     * Asserts that SpicaSessionMemcacheStore::start() works.
     *
     * @test
     * @depends testStartedAndClosed
     */
    public function testStartedWithHandler()
    {
        SpicaSession::setHandler(new SpicaSessionMemcachedStore());
        SpicaSession::start($this->opt);
        $this->assertEquals(true, SpicaSession::isStarted());
        $this->assertEquals($this->opt['id'], SpicaSession::getId());
        $this->assertEquals($this->opt['name'], SpicaSession::getName());
    }

    /**
     * Asserts that SpicaSession::set() works.
     *
     * @test
     */
    public function testHandlerOnSettingValue1()
    {
        SpicaSession::setHandler(new SpicaSessionMemcachedStore());
        $started = SpicaSession::start($this->opt);
        $this->assertEquals(true, $started);
        SpicaSession::set('name', "pcdinh");        
        $this->assertEquals('pcdinh', SpicaSession::get('name'));
        SpicaSession::commitAndClose();

        SpicaSession::setHandler(new SpicaSessionMemcachedStore());
        SpicaSession::restart();
        $this->assertEquals('pcdinh', SpicaSession::get('name'));
        $this->assertEquals(1, SpicaSession::getSessionCount());
        // do not write data down and close session here. tearDown() will handle it.
    }

    /**
     * Asserts that SpicaSession::set() works.
     *
     * @test
     * @depends testStartedAndClosed
      */
    public function testHandlerOnSettingValue2()
    {
        SpicaSession::setHandler(new SpicaSessionMemcachedStore());
        $started = SpicaSession::start($this->opt); 
        $this->assertEquals(false, SpicaSession::get('name'));
        SpicaSession::set('name', "pcdinh");
        $this->assertEquals('pcdinh', SpicaSession::get('name'));
        SpicaSession::commitAndClose();

        SpicaSession::setHandler(new SpicaSessionMemcachedStore());
        SpicaSession::restart();
        $this->assertEquals('pcdinh', SpicaSession::get('name'));
        SpicaSession::removeAll();
        SpicaSession::commitAndClose();

        SpicaSession::setHandler(new SpicaSessionMemcachedStore());
        SpicaSession::restart();
        $this->assertEquals(false, SpicaSession::get('name'));
    }

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#tearDown()
     */
    public function tearDown()
    {
        SpicaSession::removeAll();
        SpicaSession::commitAndClose();
    }
}

?>