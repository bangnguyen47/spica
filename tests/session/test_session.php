<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Set of test cases and assertions to ensure that SpicaSession works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage core\session
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 05, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: test_session.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
    get_include_path().
    PATH_SEPARATOR.$baseDir.'/library/spica'
);

include_once 'core/Base.php'; // SpicaSession here
include_once 'core/session/store/Memcached.php';

$opt = array(
    'name' => 'spicatest',
    'id' => 'abcabcabc999'
);

SpicaSession::start($opt);

if (!SpicaSession::isStarted())
{
    echo "Fail: Unable start <br />";
}
else
{
    echo "Pass: Session gets started <br />";
}

if (!file_exists(ini_get('session.save_path').'/sess_'.SpicaSession::getId()))
{
    echo "Fail: Session file does not exist <br />";
}
else
{
    echo "Pass: Session file does exist <br />";
}

$_SESSION = array();
SpicaSession::set('name', "pcdinh");

if (!SpicaSession::exists('name'))
{
    echo "Fail: Session data entry does not exist <br />";
}
else
{
    echo "Pass: Session data entry does exist <br />";
}

$_SESSION = array();
SpicaSession::commitAndClose();

SpicaSession::setStarted(false);
SpicaSession::start($opt);

if (!SpicaSession::exists('name'))
{
    echo "Pass: Session data entry does not exist <br />";
}
else
{
    echo "Fail: Session data entry does exist <br />";
}

SpicaSession::set('name', "pcdinh");
SpicaSession::commitAndClose();

SpicaSession::restart();
$session = file_get_contents(ini_get('session.save_path').'/sess_'.SpicaSession::getId());

if ($session === 'name|s:6:"pcdinh";')
{
    echo "Pass: Decoded session data entry does exist after the session data is saved and session is closed. <br />";
}

SpicaSession::setStarted(false);
SpicaSession::start($opt);

if ('name|s:6:"pcdinh";' === session_encode())
{
    echo "Pass: Decoded session data entry does exist <br />";
}

if (SpicaSession::get('name') !== 'pcdinh')
{
    echo "Fail: Session data entry does not exist <br />";
}
else
{
    echo "Pass: Session data entry does exist <br />";
}

SpicaSession::removeAll();
SpicaSession::restart();
$session = file_get_contents(ini_get('session.save_path').'/sess_'.SpicaSession::getId());

if ($session === '')
{
    echo "Pass: Session data file is now empty. <br />";
}

if (!SpicaSession::exists('name'))
{
    echo "Pass: Session data entry does not exist <br />";
}
else
{
    echo "Fail: Session data entry does exist <br />";
}

?>