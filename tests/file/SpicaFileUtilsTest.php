<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/utils/FileUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaFileUtils works as desired.
 *
 * @category   Spica
 * @package    tests
 * @subpackage SpicaFileUtilsTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 04, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaFileUtilsTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFileUtilsTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test path
     *
     * @test
     */
    public function testMakePath1()
    {
        $this->assertEquals("f000", SpicaFileUtils::makePath(0));
        $this->assertEquals("f001", SpicaFileUtils::makePath(1));
        $this->assertEquals("f098", SpicaFileUtils::makePath(98));
        $this->assertEquals("f099", SpicaFileUtils::makePath(99));
    }

    /**
     * Test path
     *
     * @test
     */
    public function testMakePath2()
    {
        var_dump(SpicaFileUtils::makePath(100));
        $this->assertEquals("f001/f000", SpicaFileUtils::makePath(100));
        $this->assertEquals("f001/f000", SpicaFileUtils::makePath(101));
        $this->assertEquals("f001/f098", SpicaFileUtils::makePath(198));
        $this->assertEquals("f01/f99", SpicaFileUtils::makePath(199));
    }

    /**
     * Test path
     *
     * @test
     */
    public function testMakePath3()
    {
        $this->assertEquals("f02/f00", SpicaFileUtils::makePath(200));
        $this->assertEquals("f02/f01", SpicaFileUtils::makePath(201));
        $this->assertEquals("f02/f98", SpicaFileUtils::makePath(298));
        $this->assertEquals("f02/f99", SpicaFileUtils::makePath(299));
    }

    /**
     * Test path
     *
     * @test
     */
    public function testMakePath4()
    {
        $this->assertEquals("f99/f98", SpicaFileUtils::makePath(9998));
        $this->assertEquals("f99/f99", SpicaFileUtils::makePath(9999));
        $this->assertEquals("f01/f00/f00", SpicaFileUtils::makePath(10000));
        $this->assertEquals("f01/f00/f01", SpicaFileUtils::makePath(10001));
    }
}
?>