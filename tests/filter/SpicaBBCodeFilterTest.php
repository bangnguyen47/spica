<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/filter/BaseFilter.php';

/**
 * Set of test cases and assertions to ensure that SpicaBBCodeFilter works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaBBCodeFilterTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaBBCodeFilterTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaBBCodeFilterTest extends PHPUnit_Framework_TestCase
{
    /**
     * BBCode filter.
     *
     * @var SpicaBBCodeFilter
     */
    protected $_filter;

    /**
     * (non-PHPdoc)
     * @see trunk/tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    public function setUp()
    {
        $this->_filter = new SpicaBBCodeFilter();
    }

    /**
     * Asserts that [b][/b] tags can work as desired.
     *
     * @test
     */
    public function testBTag()
    {
        $text     = '[b]123[strong></strong>[/b]';
        $filtered = $this->_filter->doFilter($text);
        $this->assertEquals('<strong>123[strong></strong></strong>', $filtered);

        $text     = '[b]123dfdsf[b][/b]';
        $filtered = $this->_filter->doFilter($text);
        $this->assertEquals('<strong>123dfdsf[b]</strong>', $filtered);

        $text     = '678 [b]123dfdsf[b][/b]';
        $filtered = $this->_filter->doFilter($text);
        $this->assertEquals('678 <strong>123dfdsf[b]</strong>', $filtered);

        $text     = '678[b-] [b]123dfdsf [b] [/b]';
        $filtered = $this->_filter->doFilter($text);
        $this->assertEquals('678[b-] <strong>123dfdsf [b] </strong>', $filtered);
    }

    /**
     * Asserts that [b][/b] tags can work as desired in stripping mode.
     *
     * @test
     */
    public function testBTagInStrippingMode()
    {
        $filter = new SpicaBBCodeFilter(2);

        $text = '[b]123[/b]';
        $this->assertEquals('123', $filter->doFilter($text));

        $text = '[b]123[i][/b]';
        $this->assertEquals('123[i]', $filter->doFilter($text));
    }

    /**
     * Asserts that [i][/i] tags can work as desired.
     *
     * @test
     */
    public function testITag()
    {
        $text = '[i]123[/i]';
        $this->assertEquals('<span style="font-style: italic;">123</span>', $this->_filter->doFilter($text));

        $text = '[ii][i]123[/i]';
        $this->assertEquals('[ii]<span style="font-style: italic;">123</span>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [i][/i] tags can work as desired in stripping mode.
     *
     * @test
     */
    public function testITagInStrippingMode()
    {
        $filter = new SpicaBBCodeFilter(2);

        $text = '[i]123[/i]';
        $this->assertEquals('123', $filter->doFilter($text));

        $text = '[i]123[i][/i]';
        $this->assertEquals('123[i]', $filter->doFilter($text));
    }

    /**
     * Asserts that [quote][/quote] tags can work as desired.
     *
     * @test
     */
    public function testQuoteTag()
    {
        $text = 'a [quote]123[/quote]';
        $this->assertEquals('a <blockquote>123</blockquote>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [size][/size] tags can work as desired.
     *
     * @test
     */
    public function testSizeTag()
    {
        $text = 'teh io [size=3]123[/size]';
        $this->assertEquals('teh io <span style="font-size: 3;">123</span>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [color][/color] tags can work as desired.
     *
     * @test
     */
    public function testColorTag()
    {
        $text = 'teh io [color=red]123[/color]';
        $this->assertEquals('teh io <span style="color: red;">123</span>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [center][/center] tags can work as desired.
     *
     * @test
     */
    public function testCenterTag()
    {
        $text = 'teh io [center]123[/center]';
        $this->assertEquals('teh io <div style="text-align: center;">123</div>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [center][/center] tags can work as desired.
     *
     * @test
     */
    public function testLiTag()
    {
        $text = 'teh io [li]123[/li]';
        $this->assertEquals('teh io <li>123</li>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [url][/url] tags can work as desired.
     *
     * @test
     */
    public function testUrlTag()
    {
        $text = 'teh io [url]http://abc.com[/url] <a>';
        $this->assertEquals('teh io <a href="http://abc.com">http://abc.com</a> <a>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [url=][/url] tags can work as desired.
     *
     * @test
     */
    public function testUrl2Tag()
    {
        $text = 'teh io [url=http://abc.com]my link[/url] <a>';
        $this->assertEquals('teh io <a href="http://abc.com">my link</a> <a>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [img][/img] tags can work as desired.
     *
     * @test
     */
    public function testImgTag()
    {
        $text = 'teh io [img]http://abc.com/a.jpg[/img] <a>';
        $this->assertEquals('teh io <img src="http://abc.com/a.jpg" alt="BBCode image" /> <a>', $this->_filter->doFilter($text));
    }

    /**
     * Asserts that [img=34x34][/img] tags can work as desired.
     *
     * @test
     */
    public function testImg2Tag()
    {
        $text = 'teh io [img=23x24]http://abc.com/a.jpg[/img] <a>';
        $this->assertEquals('teh io <img width="23" height="24" src="http://abc.com/a.jpg" alt="BBCode image" /> <a>', $this->_filter->doFilter($text));

        $text = 'teh io [img=23.4x24]http://abc.com/a.jpg[/img] <a>';
        $this->assertEquals('teh io <img width="23.4" height="24" src="http://abc.com/a.jpg" alt="BBCode image" /> <a>', $this->_filter->doFilter($text));

        $text = 'teh io [img=23fx24]http://abc.com/a.jpg[/img] <a>';
        $this->assertNotEquals('teh io <img width="23f" height="24" src="http://abc.com/a.jpg" alt="BBCode image" /> <a>', $this->_filter->doFilter($text));
    }
}

?>