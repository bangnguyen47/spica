<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(dirname(__FILE__))));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library'.
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/apps/default/blockgroup'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/test/Test.php';
include_once 'core/view/Template.php';
include_once 'DefaultFileManagerBlockGroup.php';

/**
 * Set of test cases and assertions to ensure that DefaultFileManagerBlockGroup
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage DefaultFileManagerBlockGroupTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 16, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DefaultFileManagerBlockGroupTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class DefaultFileManagerBlockGroupTest extends PHPUnit_Framework_TestCase
{
    /**
     * Base path.
     *
     * @var string
     */
    public $baseDir;

    /**
     * (non-PHPdoc)
     * @see tests/PHPUnit/PHPUnit/Framework/PHPUnit_Framework_TestCase#setUp()
     */
    public function setUp()
    {
        $baseDir   = dirname(dirname(dirname(dirname(__FILE__))));
        $simulator = new SpicaMockEnvironment($baseDir.'/index.php');
        $simulator->setSubdirectory('/spica/trunk');
        $simulator->setUrl('http://localhost/spica/trunk');
        $simulator->run();

        $this->baseDir = $baseDir;
    }

    /**
     * Asserts that default template file can be set correctly.
     *
     * @test
     */
    public function test1()
    {
        $blockGroup = new DefaultFileManagerBlockGroup('.tpl.php', $this->baseDir.'/apps/default/view/theme/default');
        $blockGroup->output();
        $this->assertStringEndsWith('defaultFileManager.tpl.php', $blockGroup->getFetchedTemplateFilePath());
        $this->assertEquals(true, file_exists($blockGroup->getFetchedTemplateFilePath()));
    }

    /**
     * Asserts that preset template file can be set correctly.
     *
     * @test
     */
    public function test2()
    {
        $mock = new DefaultFileManagerBlockGroupMock('.tpl.php', $this->baseDir.'/apps/default/view/theme/default');
        $mock->output();
        $this->assertStringEndsWith('defaultFileManager.tpl.php', $mock->getFetchedTemplateFilePath());
        $this->assertEquals(true, file_exists($mock->getFetchedTemplateFilePath()));
    }

    /**
     * Asserts that file list in DefaultFileManagerBlockGroup is generated correctly.
     *
     * @test
     */
    public function test3()
    {
        $blockGroup = new DefaultFileManagerBlockGroup('.tpl.php', $this->baseDir.'/apps/default/view/theme/default');
        $this->assertEquals(true, $blockGroup->fileList instanceof SpicaFileList);
    }

    /**
     * Asserts that file list in DefaultFileManagerBlockGroup is generated correctly.
     *
     * @test
     */
    public function test4()
    {
        $blockGroup = new DefaultFileManagerBlockGroupMock('.tpl.php', $this->baseDir.'/apps/default/view/theme/default');
        $this->assertEquals(5, $blockGroup->fileList->count());
    }

    /**
     * Asserts that HTML output is well-formed.
     *
     * @test
     */
    public function test5()
    {
        $blockGroup   = new DefaultFileManagerBlockGroup('.tpl.php', $this->baseDir.'/apps/default/view/theme/default');
        $output = $blockGroup->output();
        $this->assertEquals(true, simplexml_load_string($output) instanceof SimpleXMLElement);
    }

    /**
     * Asserts that HTML output contains some given substring.
     *
     * @test
     * @depends test5
     */
    public function test6()
    {
        $blockGroup = new DefaultFileManagerBlockGroup('.tpl.php', $this->baseDir.'/apps/default/view/theme/default');
        $output = $blockGroup->output();
        $this->assertContains('<div class="rightContent">', $output);
        $this->assertContains('<table', $output);
        $this->assertContains('</table>', $output);
        $this->assertContains('<th style="width: 200px;">Name</th>', $output);
        $this->assertContains('<th>Size</th>', $output);
        $this->assertContains('<th style="width: 200px;">Last modified</th>', $output);
    }
}

/**
 * A block group of a page represents the body content of the account registration page.
 *
 * @category   spica
 * @package    Cms
 * @subpackage DefaultFileManagerBlockGroupMock
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 16, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DefaultFileManagerBlockGroupTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class DefaultFileManagerBlockGroupMock extends SpicaPageBlockGroup
{
    /**
     * Pre-set template file name.
     *
     * @var string
     */
    protected $_templateFile = 'defaultFileManager';

    /**
     * The theme name in use
     *
     * @var string
     */
    public $themeName;

    /**
     * List of files.
     *
     * @var SpicaFileList
     */
    public $fileList;

    /**
     * Base file path.
     *
     * @var string
     */
    public $basePath;

    /**
     * Truncated base path to files that can be used to show up.
     *
     * @var string
     */
    public $publicBasePath;

    /**
     * Base path to theme specific image path.
     *
     * @var string
     */
    public $imagePath;

    /**
     * (non-PHPdoc)
     * @see app/blockgroup/DefaultFileManagerBlockGroup#initialize()
     */
    public function initialize()
    {
        include_once 'spica/core/utils/FileUtils.php';
        $this->themeName = SpicaContext::getThemeName();
        $this->fileList  = new SpicaFileList(dirname(__FILE__).'/mockfiles');
    }
}

?>