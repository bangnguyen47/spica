#!/bin/bash
# /*
#  * Copyright (C) 2009 - 2011 Pham Cong Dinh
#  *
#  * This file is part of Spica.
#  *
#  * This is free software; you can redistribute it and/or modify it
#  * under the terms of the GNU Lesser General Public License as
#  * published by the Free Software Foundation; either version 3 of
#  * the License, or (at your option) any later version.
#  *
#  * This software is distributed in the hope that it will be useful,
#  * but WITHOUT ANY WARRANTY; without even the implied warranty of
#  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  * Lesser General Public License for more details.
#  *
#  * You should have received a copy of the GNU Lesser General Public
#  * License along with this software; if not, write to the Free
#  * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
#  *
#  * $Id: run.sh 1869 2011-01-07 18:55:25Z pcdinh $
#  */
echo
echo ----------------------- 
echo @name   Spica Framework
echo @author pcdinh
echo @since  March 14, 2009
echo ----------------------- 
echo 
echo Spica Unit Testing - All Tests.
echo 
PHP_BASE_PATH=/usr/local/php52/bin
PHP_BIN="$PHP_BASE_PATH/php"
 
"$PHP_BIN" phpunit.php --log-xml ./log.xml SpicaAllTests
exit 0 # exiting without error