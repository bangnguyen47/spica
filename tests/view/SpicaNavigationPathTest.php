<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/Exception.php';
include_once 'core/view/Template.php';
include_once 'core/view/NavigationPath.php';

/**
 * Set of test cases and assertions to ensure that SpicaNavigationPath works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage template
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaNavigationPathTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaNavigationPathTest extends PHPUnit_Framework_TestCase
{
    public function test1()
    {
        $breadcrumb = new SpicaNavigationPath();
        $breadcrumb->addNode('home', 'Home page', 'home');
        $breadcrumb->addNode('home_news', 'News', 'home/news');
        $breadcrumb->addNode('home_news_categories', 'News categories', 'home/news/categories');
        $breadcrumb->addNode('home_news_category_sport', 'Sport', 'home/news/categories/sport');
        $breadcrumb->addNode('home_news_category_tech', 'Technology', 'home/news/categories/tech');
        $breadcrumb->addNode('home_news_mostread', 'Sport', 'home/news/mostread');

        $breadcrumb->setParentNode('home_news_category_tech', 'home_news_categories');
        $breadcrumb->setParentNode('home_news_category_sport', 'home_news_categories');
        $breadcrumb->setParentNode('home_news_categories', 'home_news');
        $breadcrumb->setParentNode('home_news', 'home');
        $breadcrumb->setParentNode('home_news_mostread', 'home_news');
        $path = $breadcrumb->getPathByUrl('home/news/categories/tech');
        $this->assertContains('<a href="home/news">News</a>', $path);
    }
}

?>