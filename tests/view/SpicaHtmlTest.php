<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/utils/ContainerUtils.php';
include_once 'core/view/Html.php';

/**
 * Set of test cases and assertions to ensure that SpicaHtml
 * and spica_html_* works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaHtmlTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaHtmlTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaHtmlTest extends PHPUnit_Framework_TestCase
{
    /**
     * Asserts that SpicaHtml::options() can generates HTML code for option tags.
     *
     * @test
     */
    public function testOptions()
    {
        $data = array(
        array('id' => 1, 'name' => 'x'),
        array('id' => 2, 'name' => 'y'),
        array('id' => 3, 'name' => 'z'),
        );

        $html = SpicaHtml::options(new SpicaRowList($data), 'id', 'name', 1);
        $this->assertEquals(0, strpos($html, '<option value="1" selected="selected">x</option>'));
        $this->assertEquals(true, strpos($html, '<option value="2">y</option>') > 0);
    }

    /**
     * Asserts that SpicaHtml::optionGroups() can generates HTML code for option tags.
     *
     * @test
     */
    public function testOptionGroups()
    {
        $data = array(
        array('id' => 1, 'name' => 'x', 'category' => 'Class A'),
        array('id' => 2, 'name' => 'y', 'category' => 'Class B'),
        array('id' => 3, 'name' => 'z', 'category' => 'Class A'),
        array('id' => 4, 'name' => 'n', 'category' => 'Class B'),
        array('id' => 5, 'name' => 'm', 'category' => 'Class C'),
        );

        $html = SpicaHtml::optionGroups(new SpicaRowList($data), 'category', 'id', 'name', 4);
        $html = str_replace("\n", '', $html);
        $this->assertEquals(0, strpos($html, '<optgroup label="Class A">'));
        $this->assertEquals(true, strpos($html, '<optgroup label="Class B"><option value="2">y</option>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="4" selected="selected">n</option></optgroup>') > 0);
        $this->assertEquals(true, strpos($html, '<optgroup label="Class C"><option value="5">m</option></optgroup>') > 0);
    }

    /**
     * Asserts that SpicaHtml::selectList() can generates HTML code for option select tag.
     *
     * @test
     */
    public function testSelectList()
    {
        $data = array(
        array('id' => 1, 'name' => 'x'),
        array('id' => 2, 'name' => 'y'),
        array('id' => 3, 'name' => 'z'),
        );

        $attributes = array(
          'name' => 'country',
        );

        $html = SpicaHtml::selectList(new SpicaRowList($data), 'id', 'name', 1, $attributes);
        $this->assertEquals(0, strpos($html, '<select name="country">'));
        $this->assertEquals(true, strpos($html, '</select>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="1" selected="selected">x</option>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="2">y</option>') > 0);

        $html = SpicaHtml::selectList(new SpicaRowList($data), 'id', 'name', 2, $attributes);
        $this->assertEquals(0, strpos($html, '<select name="country">'));
        $this->assertEquals(true, strpos($html, '</select>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="1">x</option>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="2" selected="selected">y</option>') > 0);
    }

    /**
     * Asserts that SpicaHtml::selectMultiple() can generates HTML code for option select tag.
     *
     * @test
     */
    public function testSelectMultiple()
    {
        $data = array(
        array('id' => 1, 'name' => 'x'),
        array('id' => 2, 'name' => 'y'),
        array('id' => 3, 'name' => 'z'),
        );

        $attributes = array(
          'name' => 'country',
        );

        $html = SpicaHtml::selectMultiple(new SpicaRowList($data), 'id', 'name', 1, $attributes);
        $this->assertEquals(0, strpos($html, '<select multiple="multiple" name="country">'));
        $this->assertEquals(true, strpos($html, '</select>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="1" selected="selected">x</option>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="2">y</option>') > 0);

        $html = SpicaHtml::selectMultiple(new SpicaRowList($data), 'id', 'name', 2, $attributes);
        $this->assertEquals(0, strpos($html, '<select multiple="multiple" name="country">'));
        $this->assertEquals(true, strpos($html, '</select>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="1">x</option>') > 0);
        $this->assertEquals(true, strpos($html, '<option value="2" selected="selected">y</option>') > 0);
    }

    /**
     * Asserts that SpicaHtml::bulletList() can generates HTML code for ul tag.
     *
     * @test
     */
    public function testBulletList()
    {
        $data = array(
        array('id' => 1, 'name' => 'x'),
        array('id' => 2, 'name' => 'y'),
        array('id' => 3, 'name' => 'z'),
        );

        $attributes = array(
          'class' => 'menu',
        );

        $html = SpicaHtml::bulletList(new SpicaRowList($data), 'name', 'id', 1, $attributes);
        $this->assertStringStartsWith('<ul class="menu">', $html);
        $this->assertStringEndsWith('</ul>', $html);
        $this->assertContains('<li class="active">x</li>', $html);
        $this->assertContains('<li>y</li>', $html);
        $this->assertContains('<li>z</li>', $html);
    }

    /**
     * Asserts that SpicaHtml::orderedList() can generates HTML code for ol tag.
     *
     * @test
     */
    public function testOrderedList()
    {
        $data = array(
        array('id' => 1, 'name' => 'x'),
        array('id' => 2, 'name' => 'y'),
        array('id' => 3, 'name' => 'z'),
        );

        $attributes = array(
          'class' => 'menu',
        );

        $html = SpicaHtml::orderedList(new SpicaRowList($data), 'name', 'id', 1, $attributes);
        $this->assertStringStartsWith('<ol class="menu">', $html);
        $this->assertStringEndsWith('</ol>', $html);
        $this->assertContains('<li class="active">x</li>', $html);
        $this->assertContains('<li>y</li>', $html);
        $this->assertContains('<li>z</li>', $html);
    }

    /**
     * Asserts that SpicaHtml::radio() and spica_html_radio() can generates
     * HTML code for radio input tag.
     *
     * @test
     */
    public function testRadio()
    {
        $html = SpicaHtml::radio('name1', 1, 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('checked="checked"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="radio"', $html);

        $html = spica_html_radio('name1', 1, 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('checked="checked"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="radio"', $html);

        $html = SpicaHtml::radio('name2', 1, 2, array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="radio"', $html);
        $this->assertContains('readonly="readonly"', $html);

        $html = spica_html_radio('name2', 1, 2, array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="radio"', $html);
        $this->assertContains('readonly="readonly"', $html);
    }

    /**
     * Asserts that SpicaHtml::checkbox() and spica_html_checkbox() can
     * generates HTML code for checkbox input tag.
     *
     * @test
     */
    public function testCheckbox()
    {
        $html = SpicaHtml::checkbox('name1', 1, 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('checked="checked"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="checkbox"', $html);

        $html = spica_html_checkbox('name1', 1, 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('checked="checked"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="checkbox"', $html);

        $html = SpicaHtml::checkbox('name2', 1, 2, array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="checkbox"', $html);
        $this->assertContains('readonly="readonly"', $html);

        $html = spica_html_checkbox('name2', 1, 2, array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="checkbox"', $html);
        $this->assertContains('readonly="readonly"', $html);
    }

    /**
     * Asserts that SpicaHtml::input() and spica_html_input() can generates
     * valid HTML code for input tag.
     *
     * @test
     */
    public function testInput()
    {
        $html = SpicaHtml::input('name1', 1, 1, array('id' => 'id2', 'type' => 'radio'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('checked="checked"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="radio"', $html);
        $this->assertContains('value="1"', $html);

        $html = spica_html_input('name1', 1, 1, array('id' => 'id2', 'type' => 'radio'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('checked="checked"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="radio"', $html);
        $this->assertContains('value="1"', $html);

        $html = SpicaHtml::input('name2', 1, 2, array('id' => 'id3', 'type' => 'radio', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="radio"', $html);
        $this->assertContains('readonly="readonly"', $html);
        $this->assertContains('value="1"', $html);

        $html = spica_html_input('name2', 1, 2, array('id' => 'id3', 'type' => 'radio', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="radio"', $html);
        $this->assertContains('readonly="readonly"', $html);
        $this->assertContains('value="1"', $html);

        $html = spica_html_input('name2', 1, 2, array('id' => 'id3', 'type' => 'text', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="text"', $html);
        $this->assertContains('readonly="readonly"', $html);
        $this->assertContains('value="1"', $html);

        $html = spica_html_input('name2', 1, 1, array('id' => 'id3', 'type' => 'text', 'readonly' => 'readonly'));
        $this->assertNotContains('checked="checked"', $html);

        $html = spica_html_input('name2', 1, 1, array('id' => 'id3', 'type' => 'checkbox', 'readonly' => 'readonly'));
        $this->assertContains('checked="checked"', $html);
    }

    /**
     * Asserts that SpicaHtml::textInput() and spica_html_input_text() can generates
     * valid HTML code for input tag.
     *
     * @test
     */
    public function testTextInput()
    {
        $html = SpicaHtml::textInput('name1', 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="text"', $html);
        $this->assertContains('value="1"', $html);

        $html = spica_html_input_text('name1', 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertContains('type="text"', $html);
        $this->assertContains('value="1"', $html);

        $html = SpicaHtml::textInput('name2', 2, array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="text"', $html);
        $this->assertContains('readonly="readonly"', $html);
        $this->assertContains('value="2"', $html);

        $html = spica_html_input_text('name2', 2, array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="text"', $html);
        $this->assertContains('readonly="readonly"', $html);
        $this->assertContains('value="2"', $html);

        $html = SpicaHtml::textInput('name2', 'Hello "World"', array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="text"', $html);
        $this->assertContains('readonly="readonly"', $html);
        $this->assertContains('value="Hello &quot;World&quot;"', $html);

        $html = spica_html_input_text('name2', 'Hello "World"', array('id' => 'id3', 'readonly' => 'readonly'));
        $this->assertStringStartsWith('<input ', $html);
        $this->assertStringEndsWith(' />', $html);
        $this->assertContains('id="id3"', $html);
        $this->assertNotContains('checked="checked"', $html);
        $this->assertContains('name="name2"', $html);
        $this->assertContains('type="text"', $html);
        $this->assertContains('readonly="readonly"', $html);
        $this->assertContains('value="Hello &quot;World&quot;"', $html);
    }

    /**
     * Asserts that SpicaHtml::textarea() and spica_html_textarea() can generates
     * valid HTML code for textarea tag.
     *
     * @test
     */
    public function testTextarea()
    {
        $html = SpicaHtml::textarea('name1', 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<textarea ', $html);
        $this->assertStringEndsWith('</textarea>', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertNotContains('type="text"', $html);
        $this->assertNotContains('value="1"', $html);
        $this->assertContains('>1</textarea>', $html);

        $html = spica_html_textarea('name1', 1, array('id' => 'id2'));
        $this->assertStringStartsWith('<textarea ', $html);
        $this->assertStringEndsWith('</textarea>', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertNotContains('type="text"', $html);
        $this->assertNotContains('value="1"', $html);
        $this->assertContains('>1</textarea>', $html);

        $html = SpicaHtml::textarea('name1', 'Hello "World"', array('id' => 'id2'));
        $this->assertStringStartsWith('<textarea ', $html);
        $this->assertStringEndsWith('</textarea>', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertNotContains('type="text"', $html);
        $this->assertNotContains('value="1"', $html);
        $this->assertContains('>Hello "World"</textarea>', $html);

        $html = spica_html_textarea('name1', 'Hello "World"', array('id' => 'id2'));
        $this->assertStringStartsWith('<textarea ', $html);
        $this->assertStringEndsWith('</textarea>', $html);
        $this->assertContains('id="id2"', $html);
        $this->assertContains('name="name1"', $html);
        $this->assertNotContains('type="text"', $html);
        $this->assertNotContains('value="1"', $html);
        $this->assertContains('>Hello "World"</textarea>', $html);
    }

    /**
     * Asserts that spica_html_escape_quotes() and spica_html_unescape_quotes()
     * can generates valid HTML code.
     *
     * @test
     */
    public function testEscapeQuotes()
    {
        $string1 = 'Hello "World"';
        $string2 = 'Hello &quot;World&quot;';
        $this->assertEquals($string2, spica_html_escape_quotes($string1));
        $this->assertEquals($string1, spica_html_unescape_quotes($string2));

        $string1 = "Hello 'World'";
        $string2 = 'Hello &#39;World&#39;';
        $this->assertEquals($string2, spica_html_escape_quotes($string1));
        $this->assertEquals($string1, spica_html_unescape_quotes($string2));

        $string1 = "Hello \'World\'";
        $string2 = 'Hello &#39;World&#39;';
        $this->assertEquals($string2, spica_html_escape_quotes($string1));
        $this->assertEquals("Hello 'World'", spica_html_unescape_quotes($string2));
    }

    /**
     * Asserts that spica_html_highlight() can generates valid HTML code.
     *
     * @test
     */
    public function testHighlight()
    {
        $string1 = 'Hello World. That is the World we love';
        $string2 = 'Hello <span class="highlight">World</span>. That is the <span class="highlight">World</span> we love';
        $this->assertEquals($string2, spica_html_highlight($string1, 'World', '<span class="highlight">', '</span>'));
    }
}

?>