<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/Base.php';
include_once 'core/utils/ContainerUtils.php';
include_once 'core/view/Template.php';

/**
 * Set of test cases and assertions to ensure that SpicaTemplate works as desired.
 *
 * @category   spica
 * @package    tests
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      August 04, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaTemplateTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaTemplateTest extends PHPUnit_Framework_TestCase
{
    public $baseDir;

    public function setUp()
    {
        $this->baseDir = str_replace('\\', '/', dirname(dirname(dirname(__FILE__))));
    }

    /**
     * Tests to ensure that Spica core widget classes can be loaded correctly.
     *
     * @test
     */
    public function testWidget1()
    {
        SpicaContext::setBasePath($this->baseDir);
        SpicaContext::setAppPath($this->baseDir.'/apps/default');
        $template = new SpicaTemplate();
        $widget   = $template->createWidget('spica.ItemSelectorWidget');
        $this->assertEquals(true, $widget instanceof ItemSelectorWidget);
        $this->assertEquals($this->baseDir.'/library/spica/core/view/ui/ItemSelectorWidget.tpl.php', $widget->getTemplatePath());
        $widget->output();
        $template->unsetOutputFilters();
        $template->unsetWidgets();
    }

    /**
     * Tests to ensure that Spica application widget classes can be loaded correctly.
     *
     * @test
     */
    public function testWidget2()
    {
        SpicaContext::setBasePath($this->baseDir);
        SpicaContext::setAppPath($this->baseDir.'/apps/default');
        $template = new SpicaTemplate();
        $widget   = $template->createWidget('DummyWidget');
        $this->assertEquals(true, $widget instanceof DummyWidget);
        $this->assertEquals($this->baseDir.'/apps/default/ui/DummyWidget.tpl.php', $widget->getTemplatePath());
        $widget->output();
        $template->unsetOutputFilters();
        $template->unsetWidgets();
    }
}

?>