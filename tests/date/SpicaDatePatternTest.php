<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/DateUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaDatePattern works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage date
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 19, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaDatePatternTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaDatePatternTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests on MMMM_DD_YYYY pattern.
     *
     * @test
     */
    public function testMMMM_DD_YYYY()
    {
        $pattern1 = SpicaDatePattern::MMMM_DD_YYYY;
        $count    = preg_match($pattern1, '2006 12 2009');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, 'Fara 12 2009');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, '!!!! 12 2009');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, 'Januaries 12 2009');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, 'January 12 2009');
        $this->assertEquals(1, $count); // matched

        $count    = preg_match($pattern1, 'January 12 2009', $matches);
        $this->assertEquals('January', $matches['month']); // matched
        $this->assertEquals('2009', $matches['yyyy']);     // matched
        $this->assertEquals('12', $matches['dd']);         // matched
    }

    /**
     * Tests on YYYYMMDD pattern.
     *
     * @test
     */
    public function testYYYYMMDD()
    {
        $pattern1 = SpicaDatePattern::YYYYMMDD;
        $count    = preg_match($pattern1, '200612234');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, 'Fara21209');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, '!!!!1209');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, 'Januaries 12 2009');
        $this->assertEquals(0, $count); // no match

        $count    = preg_match($pattern1, '2009-209');
        $this->assertEquals(0, $count); // matched

        $count    = preg_match($pattern1, '20091216', $matches);
        $this->assertEquals('2009', $matches['yyyy']); // matched
        $this->assertEquals('12', $matches['mm']);     // matched
        $this->assertEquals('16', $matches['dd']);     // matched
    }
}
?>