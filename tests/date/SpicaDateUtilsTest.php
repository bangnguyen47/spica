<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/utils/DateUtils.php';

/**
 * Set of test cases and assertions to ensure that SpicaDateUtils works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage date
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 19, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaDateUtilsTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaDateUtilsTest extends PHPUnit_Framework_TestCase
{
    public $shortMonths = array(
    1  => 'Jan',
    2  => 'Feb',
    3  => 'Mar',
    4  => 'Apr',
    5  => 'May',
    6  => 'Jun',
    7  => 'Jul',
    8  => 'Aug',
    9  => 'Sep',
    10 => 'Oct',
    11 => 'Nov',
    12 => 'Dec');

    public $months = array(
    1  => 'January',
    2  => 'February',
    3  => 'March',
    4  => 'April',
    5  => 'May',
    6  => 'June',
    7  => 'July',
    8  => 'August',
    9  => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December');

    public $days = array(
    1 => 'Monday',
    2 => 'Tuesday',
    3 => 'Wednesday',
    4 => 'Thursday',
    5 => 'Friday',
    6 => 'Saturday',
    7 => 'Sunday',
    );

    public $shortDays = array(
    1 => 'Mon',
    2 => 'Tue',
    3 => 'Wed',
    4 => 'Thur',
    5 => 'Fri',
    6 => 'Sat',
    7 => 'Sun',
    );

    /**
     * @test
     */
    public function test_spica_date_get_weekdays()
    {
        $this->assertEquals($this->days, spica_date_get_weekdays(true));
        $this->assertEquals($this->days, spica_date_get_weekdays()); // defaults
        $this->assertEquals($this->shortDays, spica_date_get_weekdays(false));
    }

    /**
     * @test
     */
    public function test_spica_date_get_months()
    {
        $this->assertEquals($this->months, spica_date_get_months(true));
        $this->assertEquals($this->months, spica_date_get_months()); // defaults
        $this->assertEquals($this->shortMonths, spica_date_get_months(false));
    }

    /**
     * @test
     */
    public function test_spica_date_num2day()
    {
        $this->assertEquals($this->days[1], spica_date_num2day(1));
        $this->assertEquals($this->days[1], spica_date_num2day("1"));
        $this->assertEquals($this->days[7], spica_date_num2day("7"));
        $this->assertEquals(false, spica_date_num2day("8"));
        $this->assertEquals(false, spica_date_num2day(0));
        $this->assertEquals(false, spica_date_num2day(20));

        $this->assertEquals($this->shortDays[1], spica_date_num2day(1, false));
        $this->assertEquals($this->shortDays[1], spica_date_num2day("1", false));
        $this->assertEquals($this->shortDays[7], spica_date_num2day("7", false));
        $this->assertEquals(false, spica_date_num2day("8", false));
        $this->assertEquals(false, spica_date_num2day(0, false));
        $this->assertEquals(false, spica_date_num2day(20, false));
    }

    /**
     * @test
     */
    public function test_spica_date_num2month()
    {
        $this->assertEquals($this->months[1], spica_date_num2month(1));
        $this->assertEquals($this->months[1], spica_date_num2month("1"));
        $this->assertEquals($this->months[12], spica_date_num2month("12"));
        $this->assertEquals(false, spica_date_num2month("13"));
        $this->assertEquals(false, spica_date_num2month("0"));
        $this->assertEquals(false, spica_date_num2month(0));

        $this->assertEquals($this->shortMonths[1], spica_date_num2month(1, false));
        $this->assertEquals($this->shortMonths[1], spica_date_num2month("1", false));
        $this->assertEquals($this->shortMonths[12], spica_date_num2month("12", false));
        $this->assertEquals(false, spica_date_num2month("13", false));
        $this->assertEquals(false, spica_date_num2month("0", false));
        $this->assertEquals(false, spica_date_num2month(0, false));

        $this->assertEquals(false, spica_date_num2month(20, false));
    }

    /**
     * @test
     */
    public function test_spica_date_day2number()
    {
        $this->assertEquals(1, spica_date_day2number('Mon'));
        $this->assertEquals(2, spica_date_day2number('Tue'));
        $this->assertEquals(3, spica_date_day2number('Wed'));
        $this->assertEquals(4, spica_date_day2number('Thur'));
        $this->assertEquals(5, spica_date_day2number('Fri'));
        $this->assertEquals(6, spica_date_day2number('Sat'));
        $this->assertEquals(7, spica_date_day2number('Sun'));

        $this->assertEquals(1, spica_date_day2number('Monday'));
        $this->assertEquals(2, spica_date_day2number('Tuesday'));
        $this->assertEquals(3, spica_date_day2number('Wednesday'));
        $this->assertEquals(4, spica_date_day2number('Thursday'));
        $this->assertEquals(5, spica_date_day2number('Friday'));
        $this->assertEquals(6, spica_date_day2number('Saturday'));
        $this->assertEquals(7, spica_date_day2number('Sunday'));
        $this->assertEquals(7, spica_date_day2number('sUnDaY'));

        $this->assertEquals(false, spica_date_day2number('xxx'));
        $this->assertEquals(false, spica_date_day2number('Sundaye'));
    }

    /**
     * @test
     */
    public function test_spica_date_month2number()
    {
        $this->assertEquals(1, spica_date_month2number('January'));
        $this->assertEquals(2, spica_date_month2number('February'));
        $this->assertEquals(3, spica_date_month2number('March'));
        $this->assertEquals(4, spica_date_month2number('April'));
        $this->assertEquals(5, spica_date_month2number('May'));
        $this->assertEquals(6, spica_date_month2number('June'));
        $this->assertEquals(7, spica_date_month2number('July'));
        $this->assertEquals(8, spica_date_month2number('August'));
        $this->assertEquals(9, spica_date_month2number('September'));
        $this->assertEquals(10, spica_date_month2number('October'));
        $this->assertEquals(11, spica_date_month2number('November'));
        $this->assertEquals(12, spica_date_month2number('December'));

        $this->assertEquals(1, spica_date_month2number('Jan'));
        $this->assertEquals(2, spica_date_month2number('Feb'));
        $this->assertEquals(3, spica_date_month2number('Mar'));
        $this->assertEquals(4, spica_date_month2number('Apr'));
        $this->assertEquals(5, spica_date_month2number('May'));
        $this->assertEquals(6, spica_date_month2number('Jun'));
        $this->assertEquals(7, spica_date_month2number('Jul'));
        $this->assertEquals(8, spica_date_month2number('Aug'));
        $this->assertEquals(9, spica_date_month2number('Sep'));
        $this->assertEquals(10, spica_date_month2number('Oct'));
        $this->assertEquals(11, spica_date_month2number('Nov'));
        $this->assertEquals(12, spica_date_month2number('Dec'));

        $this->assertEquals(12, spica_date_month2number('dec'));
        $this->assertEquals(12, spica_date_month2number('deC'));

        $this->assertEquals(false, spica_date_month2number('xxxx'));
        $this->assertEquals(false, spica_date_month2number('---'));
    }

    /**
     * @test
     */
    public function test_spica_date_diff()
    {
        $pattern = SpicaDatePattern::MMMM_DD_YYYY_US;
        $date1   = 'May 09, 2009';
        $date2   = 'May 11, 2009';
        $this->assertEquals(2, spica_date_diff($date1, $date2, $pattern));
    }
}

?>