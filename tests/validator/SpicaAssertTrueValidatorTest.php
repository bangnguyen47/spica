<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';

/**
 * Set of test cases and assertions to ensure that SpicaAssertTrueValidatorTest works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaAssertTrueValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 29, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaAssertTrueValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAssertTrueValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaAssertTrueValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide a boolean true value. ';
        $this->_validator = new SpicaAssertTrueValidator($this->_violationMessage);
    }

    /**
     * Assert that "required" property is always true.
     *
     * @test
     */
    public function requiredIsAlwaysTrue()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $this->_validator->isRequired());
    }

    /**
     * Asserts that non-boolean value must not pass the validation.
     *
     * @test
     */
    public function nonBooleanTrueValueMustNotPass()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(array()));
        $this->assertEquals(false, $this->_validator->isValid($var = 1));
        $this->assertEquals(false, $this->_validator->isValid($var = new StdClass()));
        $this->assertEquals(false, $this->_validator->isValid(''));
        $this->assertEquals(false, $this->_validator->isValid('   '));
        $this->assertEquals(false, $this->_validator->isValid(null));
        $this->assertEquals(false, $this->_validator->isValid((float) 1000.4));
        $this->assertEquals(false, $this->_validator->isValid(false));
    }

    /**
     * Asserts that boolean true value must pass the validation.
     *
     * @test
     */
    public function booleanTrueValueMustPass()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals(true, $this->_validator->isValid(true));
        $this->assertEquals(true, $this->_validator->isValid(!false));
        $this->assertEquals(true, $this->_validator->isValid((bool) 1));
        $this->assertEquals(true, $this->_validator->isValid(!((bool) 0)));
    }

    /**
     * Asserts that the tested value can be retrieved.
     *
     * @test
     */
    public function possibleToGetTestedValue()
    {
        $value = 'x';
        // Do the validation
        $this->_validator->isValid($value);
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_validator->getValue(), $value);
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}
?>