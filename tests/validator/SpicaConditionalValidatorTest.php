<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/tests/validator'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';
include_once 'core/validator/Complex.php';

/**
 * Set of test cases and assertions to ensure that SpicaConditionalValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage validator
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 02, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaConditionalValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaConditionalValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide a valid string for {:field}.';
        $_POST = array();
    }

    /**
     * Asserts that when a check on certain value in $_POST is met the validator is activated.
     *
     * @test
     */
    public function testCertainValueOnPost()
    {
        $violationMessage = 'You must provide an alphanumeric value for {:field}.';
        $validator1       = new SpicaAlphaNumericValidator($violationMessage);
        $condition1       = new SpicaTestRadioValueCondition();
        $condValidator    = new SpicaConditionalValidator();

        $condValidator->addCondition($condition1, $validator1, $this->_violationMessage);
        $this->assertEquals(true, $condValidator->isRequired());

        // Condition is not met
        $this->assertEquals(false, $condValidator->isValid(1));
        $this->assertEquals(false, $condValidator->isValid('a'));

        // Condition is met
        $_POST['radio_button'] = '5';
        $this->assertEquals(true, $condValidator->isValid(1));
        $this->assertEquals(true, $condValidator->isValid('a'));

        // Condition is met but validation can not pass
        $this->assertEquals(false, $condValidator->isValid('a-'));
        $this->assertEquals(false, $condValidator->isValid(' abc'));
    }
}

/**
 * Validator condition to check if user check on a radio button that contains a
 * value of 5.
 *
 * @category   spica
 * @package    tests
 * @subpackage validator
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 02, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaConditionalValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaTestRadioValueCondition extends SpicaValidatorCondition
{
    /**
     * (non-PHPdoc)
     * @see library/spica/core/validator/SpicaValidatorCondition#doCheck()
     */
    public function doCheck($value = null)
    {
        if (isset($_POST['radio_button']) && $_POST['radio_button'] == 5)
        {
            return true;
        }

        return false;
    }

    /**
     * A callback method that enables sub-classes intercept the value checking
     * before it is being checked.
     * Sub-classes may override this method and perform custom assertions and
     * prevent the checking by throwing an {@link InvalidArgumentException};
     * @param mixed $value
     */
    protected function beforeObjectChecked($value = null)
    {
        // no-op
    }
}
?>