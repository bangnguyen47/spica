<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
require_once 'PHPUnit/Framework/TestCase.php';
include_once 'core/validator/Common.php';

/**
 * Set of test cases and assertions to ensure that SpicaMinLengthStringValidator
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaMinLengthStringValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 31, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaMinLengthStringValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaMinLengthStringValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaMinLengthStringValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * String length.
     *
     * @var int
     */
    protected $_stringLen = 2;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'The string length can not less than than 2.';
        $this->_validator = new SpicaMinLengthStringValidator($this->_stringLen, $this->_violationMessage, 1, false, true);
    }

    /**
     * Assert that "required" property is always true.
     *
     * @test
     */
    public function requiredIsAlwaysTrue()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $this->_validator->isRequired());
    }

    /**
     * Asserts that null value can not pass the validation when "required" property
     * is true.
     *
     * @test
     */
    public function nullValueCanNotPassWhenRequiredIsTrue()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(null));
        $this->assertEquals(false, $this->_validator->isValid($var = null));
    }

    /**
     * Asserts that empty string can not pass the validation when "required" property
     * is true.
     *
     * @test
     */
    public function emptyStringCanNotPassWhenRequiredIsTrue()
    {
        $this->assertEquals(false, $this->_validator->isValid(''));
        $this->assertEquals(false, $this->_validator->isValid('   '));
    }

    /**
     * Asserts that null value or empty string can not pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function emptyStringOrNullCanPassWhenRequiredIsFalse()
    {
        // Creates a validator and assume that when empty data is provided it always
        // returns true
        $validator = new SpicaMinLengthStringValidator(10, $this->_violationMessage, 1, false, false);
        // Assert that empty value does pass the validation.
        $this->assertEquals(true, $validator->isValid(null));
        $this->assertEquals(true, $validator->isValid($var = null));
        $this->assertEquals(true, $validator->isValid($var = ''));
    }

    /**
     * Asserts that a string with all whitespaces must not pass the validation
     * when $required property is set to true.
     *
     * @test
     */
    public function stringWithAllWhitespacesCanNotPassWhenRequiredIsTrue()
    {
        // required is true
        $this->assertEquals(false, $this->_validator->isValid('      '));
    }

    /**
     * Asserts that a string with all whitespaces must pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function stringWithAllWhitespacesCanNotPassWhenRequiredIsFalse()
    {
        // required is false
        $validator = new SpicaMinLengthStringValidator($this->_stringLen, $this->_violationMessage, 1, false, false);
        $this->assertEquals(true, $validator->isValid('      '));
    }

    /**
     * Asserts that boolean value can not pass the validation.
     *
     * @test
     */
    public function booleanValueCanNotPass()
    {
        // Required is set to be true
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(true));
        $this->assertEquals(false, $this->_validator->isValid(false));

        // Required is set to be false
        $validator = new SpicaMinLengthStringValidator($this->_stringLen, $this->_violationMessage, 1, false, false);
        $this->assertEquals(false, $validator->isValid(true));
        $this->assertEquals(false, $validator->isValid(false));
    }

    /**
     * Asserts that empty array can not pass the validation.
     *
     * @test
     */
    public function nonScalarEmptyValueCanNotPass()
    {
        // Required is set to be true
        $var = array();
        $this->assertEquals(false, $this->_validator->isValid($var));

        // Required is set to be false
        $validator = new SpicaMinLengthStringValidator($this->_stringLen, $this->_violationMessage, 1, false, false);
        $this->assertEquals(false, $validator->isValid($var));
    }

    /**
     * Asserts that a integer or number (not string) can not pass
     *
     * @test
     */
    public function intergerCanNotPassTheTest()
    {
        $validator = new SpicaMinLengthStringValidator(10, $this->_violationMessage, 1, false, false);
        // Not a string
        $this->assertEquals(false, $validator->isValid(0));
        $this->assertEquals(false, $validator->isValid(0000));
        $this->assertEquals(false, $validator->isValid(10000));
        $this->assertEquals(false, $validator->isValid(9087));
    }

    /**
     * Asserts that a string with valid string length must pass the validation.
     *
     * @test
     */
    public function validStringLengthMustPass()
    {
        // Min: 2
        $string1 = 'abc';   // length = 3.
        $this->assertEquals(true, $this->_validator->isValid($string1));
        $string1 = '12345'; // length = 5.
        $this->assertEquals(true, $this->_validator->isValid($string1));
        // length: 4
        $this->assertEquals(true, $this->_validator->isValid($var = '0000'));

        $validator = new SpicaMinLengthStringValidator(10, $this->_violationMessage, 1, false, false);
        $string1 = '-123456789abcd'; // length = 14. Min: 10
        $this->assertEquals(true, $validator->isValid($string1));
    }

    /**
     * Asserts that a string with invalid string length must pass the validation.
     *
     * @test
     */
    public function invalidStringLengthMustNotPass()
    {
        $validator = new SpicaMinLengthStringValidator(10, $this->_violationMessage, 1, false, false);
        $string1   = '-124'; // length = 4. Min: 10
        $this->assertEquals(false, $validator->isValid($string1));
        $this->assertEquals(false, $validator->isValid($var = '0'));
        $this->assertEquals(false, $validator->isValid($var = 'a1 01 90'));
    }

    /**
     * Asserts that the tested value can be retrieved.
     *
     * @test
     */
    public function possibleToGetTestedValue()
    {
        $value = 'x';
        // Do the validation
        $this->_validator->isValid($value);
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_validator->getValue(), $value);
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}
?>