<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/tests/validator'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Pattern.php';

/**
 * Set of test cases and assertions to ensure that SpicaPatternValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaPatternValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 22, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaPatternValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaPatternValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaPatternValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Pattern to validate against.
     *
     * @var string
     */
    protected $_pattern;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide a valid string for {:field}.';
        $this->_pattern   = '/[.*]+/'; // non empty string
        $this->_validator = new SpicaPatternValidator($this->_pattern, $this->_violationMessage);
    }

    /**
     * Asserts that null strings can not pass the validation.
     *
     * @test
     */
    public function testNullCanNotPass()
    {
        $this->assertEquals(false, $this->_validator->isValid(null));
    }

    /**
     * Asserts that non scalar values can not pass the validation.
     *
     * @test
     */
    public function testNonScalarValuesCanNotPass()
    {
        $this->assertEquals(false, $this->_validator->isValid(array()));
        $this->assertEquals(false, $this->_validator->isValid(new StdClass()));
    }

    /**
     * Asserts that empty strings.
     *
     * @test
     */
    public function testEmptyString()
    {
        $this->assertEquals((bool)preg_match($this->_pattern, ''), $this->_validator->isValid(''));
    }

    /**
     * Asserts that non empty string must pass the validation.
     *
     * @test
     */
    public function testNonEmptyString()
    {
        $this->assertEquals((bool)preg_match($this->_pattern, '   '), $this->_validator->isValid('   '));
        $this->assertEquals((bool)preg_match($this->_pattern, '.***'), $this->_validator->isValid('.***'));
    }

    /**
     * Asserts that alpha string must pass the validation.
     *
     * @test
     */
    public function testAlphabeticStringMustPass()
    {
        $pattern   = '/([a-z])+/'; // non empty string
        $validator = new SpicaPatternValidator($pattern, $this->_violationMessage);
        $this->assertEquals(true, $validator->isValid('a'));
        $this->assertEquals(true, $validator->isValid('bca'));
        $this->assertEquals(true, $validator->isValid('bcaupoeerer'));
    }

    /**
     * Asserts that non-alpha string must pass the validation.
     *
     * @test
     */
    public function testNonAlphabeticStringMustNotPass()
    {
        $pattern   = '/^([a-z]+)$/'; // non empty string
        $validator = new SpicaPatternValidator($pattern, $this->_violationMessage);
        $this->assertEquals(false, $validator->isValid('a '));
        $this->assertEquals(false, $validator->isValid('bc a'));
        $this->assertEquals(false, $validator->isValid('bcaup5oeerer'));
    }

    /**
     * Asserts that complicated pattern can be validated.
     *
     * @test
     */
    public function testComplicatedPattern()
    {
        // Password must be at least 4 characters, no more than 12 characters,
        // and must include at least one upper case letter,
        // one lower case letter, one numeric digit and on special symbol among (# $ % @).
        $pattern   = '#^(?=.*[\#$%@])(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$#';
        $validator = new SpicaPatternValidator($pattern, $this->_violationMessage);
        $this->assertEquals(false, $validator->isValid('a '));
        $this->assertEquals(false, $validator->isValid('bc a'));
        $this->assertEquals(false, $validator->isValid('bcaup5oeerer'));
        $this->assertEquals(true, $validator->isValid('a123T@'));
        $this->assertEquals(false, $validator->isValid('1123T@'));
        $this->assertEquals(false, $validator->isValid('11A3T@'));
        $this->assertEquals(false, $validator->isValid('11A3Tx'));

        $validator = new SpicaPatternValidator('#^(\p{L}+)$#u', 'Unicode letters are accepted only. ');
        $this->assertEquals(true, $validator->isValid('a'));
        $this->assertEquals(true, $validator->isValid('am'));
        $this->assertEquals(true, $validator->isValid('aư'));
        $this->assertEquals(false, $validator->isValid('aư '));
        $this->assertEquals(false, $validator->isValid('-aư'));

        $pattern = '#^(?=.*[\p{L}]+)([\p{L}\p{Pd}\p{N}\p{Pi}\p{Pf}\p{Zs}]{1,50})$#u';
        $validator = new SpicaPatternValidator($pattern, 'The full name contains an illegal character. Letters, numbers, single quotes and dashes are allowed only. ');
        $this->assertEquals(true, $validator->isValid('a '));
        $this->assertEquals(false, $validator->isValid('??'));
        $this->assertEquals(false, $validator->isValid('?a'));
        $this->assertEquals((bool)preg_match($pattern, '-a'), $validator->isValid('-a'), $validator->getViolationMessage());
        $this->assertEquals(true, $validator->isValid('a12 tyu'));
    }
}

?>