<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';

/**
 * Set of test cases and assertions to ensure that SpicaCompareTwoFieldsValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage validator
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 02, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaCompareTwoFieldsValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaCompareTwoFieldsValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaCompareTwoFieldsValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * First field name.
     *
     * @var string
     */
    protected $_firstField = 'a';

    /**
     * Second field name.
     *
     * @var string
     */
    protected $_secondField = 'b';

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $operator = SpicaOperator::EQUAL;
        $this->_violationMessage = 'You must provide a value for {:field}.';
        $this->_validator = new SpicaCompareTwoFieldsValidator($this->_firstField, $this->_secondField, $this->_violationMessage, $operator, false);
    }

    /**
     * Asserts that if the two field have the same value, the validation must pass.
     *
     * @test
     */
    public function twoFieldsSameValueMustPass()
    {
        $_POST[$this->_firstField]  = 'm1';
        $_POST[$this->_secondField] = 'm1';
        $this->assertEquals(true, $this->_validator->isValid());

        $_POST[$this->_firstField]  = array(1, 3);
        $_POST[$this->_secondField] = array(1, 3);
        $this->assertEquals(true, $this->_validator->isValid());

        $_POST[$this->_firstField]  = '--';
        $_POST[$this->_secondField] = '--';
        $this->assertEquals(true, $this->_validator->isValid());

        $_POST[$this->_firstField]  = "  =";
        $_POST[$this->_secondField] = "  =";
        $this->assertEquals(true, $this->_validator->isValid());

        $_POST[$this->_firstField]  = "  \n \0 y\0";
        $_POST[$this->_secondField] = "  \n \0 y\0";
        $this->assertEquals(true, $this->_validator->isValid());
    }

    /**
     * Asserts that when the two field has empty value but required rule is not set,
     * the validation must pass.
     *
     * @test
     */
    public function testEmptyValueMustPassWhenRequiredIsFalse()
    {
        $_POST[$this->_firstField]  = '';
        $_POST[$this->_secondField] = '';
        $this->assertEquals(true, $this->_validator->isValid());

        $_POST[$this->_firstField]  = array();
        $_POST[$this->_secondField] = array();
        $this->assertEquals(true, $this->_validator->isValid());

        $_POST[$this->_firstField]  = "  ";
        $_POST[$this->_secondField] = "  ";
        $this->assertEquals(true, $this->_validator->isValid());

        $_POST[$this->_firstField]  = "  \n";
        $_POST[$this->_secondField] = "  \n";
        $this->assertEquals(true, $this->_validator->isValid());
    }

    /**
     * Asserts that when the two field has empty value and required rule is set,
     * the validation must not pass.
     *
     * @test
     */
    public function testEmptyValueMustNotPassWhenRequiredIsTrue()
    {
        $operator  = SpicaOperator::EQUAL;
        $validator = new SpicaCompareTwoFieldsValidator($this->_firstField, $this->_secondField, $this->_violationMessage, $operator, true);
        $_POST[$this->_firstField]  = '';
        $_POST[$this->_secondField] = '';
        $this->assertEquals(false, $validator->isValid());

        $_POST[$this->_firstField]  = array();
        $_POST[$this->_secondField] = array();
        $this->assertEquals(false, $validator->isValid());

        $_POST[$this->_firstField]  = "  ";
        $_POST[$this->_secondField] = "  ";
        $this->assertEquals(false, $validator->isValid());

        $_POST[$this->_firstField]  = "  \n";
        $_POST[$this->_secondField] = "  \n";
        $this->assertEquals(false, $validator->isValid());
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}
?>