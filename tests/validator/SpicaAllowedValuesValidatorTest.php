<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';

/**
 * Set of test cases and assertions to ensure that SpicaAllowedValuesValidatorTest
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaAllowedValuesValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 19, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaAllowedValuesValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAllowedValuesValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaAllowedValuesValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var strng
     */
    protected $_violationMessage;

    /**
     * Allowed values.
     *
     * @var array
     */
    protected $_allowedValues = array(1, 2, 3);

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide an alphanumeric value for {:field}.';
        $this->_validator = new SpicaAllowedValuesValidator($this->_allowedValues, $this->_violationMessage);
    }

    /**
     * Assert that "required" property is always true if $required argument is not set
     * at object instantiation.
     *
     * @test
     */
    public function requiredIsAlwaysTrueByDefault()
    {
        $this->assertEquals(true, $this->_validator->isRequired());
    }

    /**
     * Assert that "required" property can be reset at object instantiation.
     *
     * @test
     */
    public function requiredCanBeReset()
    {
        $validator = new SpicaAllowedValuesValidator(array(), $this->_violationMessage, false);
        $this->assertEquals(false, $validator->isRequired());

        $validator = new SpicaAllowedValuesValidator(array(), $this->_violationMessage, true);
        $this->assertEquals(true, $validator->isRequired());
    }

    /**
     * Asserts that null value or empty string can not pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function emptyStringOrNullCanPassWhenRequiredIsFalse()
    {
        $validator = new SpicaAllowedValuesValidator(array(), $this->_violationMessage, false);
        $this->assertEquals(true, $validator->isValid(null));
        $this->assertEquals(true, $validator->isValid($var = null));
        $this->assertEquals(true, $validator->isValid($var = ''));
    }

    /**
     * Asserts that a string with all whitespaces must pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function stringWithAllWhitespacesCanPassWhenRequiredIsFalse()
    {
        $validator = new SpicaAllowedValuesValidator(array(), $this->_violationMessage, false);
        $this->assertEquals(true, $validator->isValid('      '));
    }

    /**
     * Asserts that a set of values that are not in allowed values can not
     * pass the validation.
     *
     * @test
     */
    public function invalidValuesCanNotPass()
    {
        $validator = new SpicaAllowedValuesValidator(array(1, 2), $this->_violationMessage, false);
        $this->assertEquals(false, $validator->isValid(5));
        $this->assertEquals(false, $validator->isValid("5"));
        $this->assertEquals(false, $validator->isValid(10));
        $this->assertEquals(false, $validator->isValid(1000000000000005550000444040400440404040404088568586));
        $this->assertEquals(false, $validator->isValid(array('2', '3')));
        $this->assertEquals(false, $validator->isValid(array('1', '4')));
    }

    /**
     * Asserts that a set of values that are in allowed values must pass the validation.
     *
     * @test
     */
    public function validValuesMustPass()
    {
        $validator = new SpicaAllowedValuesValidator(array(1, 2), $this->_violationMessage, false);
        $this->assertEquals(true, $validator->isValid(1));
        $this->assertEquals(true, $validator->isValid(2));
        $this->assertEquals(true, $validator->isValid(array(1, 2)));
        $this->assertEquals(true, $validator->isValid(array(1, 2, 1, 2)));
        $this->assertEquals(true, $validator->isValid(array(1, 1, 1)));
    }

    /**
     * Asserts that the tested value can be retrieved.
     *
     * @test
     */
    public function possibleToGetTestedValue()
    {
        $value = 'x';
        $this->_validator->isValid($value);
        $this->assertEquals($this->_validator->getValue(), $value);
    }

    /**
     * Asserts that the allowed values can be retrieved.
     *
     * @test
     */
    public function possibleToGetAllowedValue()
    {
        $this->assertEquals($this->_validator->getAllowedValues(), $this->_allowedValues);
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}
?>