<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/tests/validator'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Finance.php';
include_once 'SpicaOptionalStringPatternValidatorTest.php';

/**
 * Set of test cases and assertions to ensure that SpicaCreditCardNumberValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaCreditCardNumberValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaCreditCardNumberValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaCreditCardNumberValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaCreditCardNumberValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide a valid credit card number for {:field}.';
    }

    /**
     * Asserts that a string with valid MasterCard credit card number must pass the validation.
     *
     * @test
     * @see  http://support.microsoft.com/kb/258255 Sample Credit Numbers for Testing Credit Card Functionality
     */
    public function validMasterCardNumberMustPass()
    {
        $validator = new SpicaCreditCardNumberValidator($this->_violationMessage, SpicaCreditCardNumberValidator::MASTER);
        $this->assertEquals(true, $validator->isValid('5500000000000004')); // 5500 0000 0000 0004
        $this->assertEquals(true, $validator->isValid('5111111111111118')); // 5111-1111-1111-1118
        $this->assertEquals(true, $validator->isValid('5500000000000004')); // 5500 0000 0000 0004
    }

    /**
     * Asserts that a string with valid Visa credit card number must pass the validation.
     *
     * @test
     * @see  http://support.microsoft.com/kb/258255 Sample Credit Numbers for Testing Credit Card Functionality
     */
    public function validVisaNumberMustPass()
    {
        $validator = new SpicaCreditCardNumberValidator($this->_violationMessage, SpicaCreditCardNumberValidator::VISA);
        $this->assertEquals(true, $validator->isValid('4111111111111111')); // 4111-1111-1111-1111
    }

    /**
     * Asserts that a string with valid American Express credit card number must pass the validation.
     *
     * @test
     * @see  http://support.microsoft.com/kb/258255 Sample Credit Numbers for Testing Credit Card Functionality
     * @see  http://twitter.com/pcdinh/status/1762166860
     */
    public function validAmericanExpressNumberMustPass()
    {
        $validator = new SpicaCreditCardNumberValidator($this->_violationMessage, SpicaCreditCardNumberValidator::AMERICAN);
        $this->assertEquals(true, $validator->isValid('340000000000009')); // 3400 0000 0000 009
    }

    /**
     * Asserts that a string with valid Discover credit card number must pass the validation.
     *
     * @test
     * @see  http://support.microsoft.com/kb/258255 Sample Credit Numbers for Testing Credit Card Functionality
     */
    public function validDiscoverNumberMustPass()
    {
        $validator = new SpicaCreditCardNumberValidator($this->_violationMessage, SpicaCreditCardNumberValidator::DISCOVER);
        $this->assertEquals(true, $validator->isValid('6111111111111116')); // 6111-1111-1111-1116
    }

    /**
     * Asserts that a string with valid Enroute credit card number must pass the validation.
     *
     * @test
     */
    public function validEnRouteNumberMustPass()
    {
        $validator = new SpicaCreditCardNumberValidator($this->_violationMessage, SpicaCreditCardNumberValidator::ENROUTE);
        // Unable to pass Luhn algorithm
        $this->assertEquals(false, $validator->isValid('201400000000009')); // 2014 0000 0000 009
    }
}

?>