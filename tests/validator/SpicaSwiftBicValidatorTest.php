<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/tests/validator'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Finance.php';

/**
 * Set of test cases and assertions to ensure that SpicaSwiftBicValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaSwiftBicValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      July 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaSwiftBicValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaSwiftBicValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaSwiftBicValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide a valid string for {:field}.';
        $this->_validator = new SpicaSwiftBicValidator($this->_violationMessage);
    }

    /**
     * Asserts that valid SWIFT-BIC code can be validated.
     *
     * @test
     */
    public function test1()
    {
        // SWIFT-BIC Natwest Offshore Bank - GB: RBOSGGSX
        $this->assertEquals(true, $this->_validator->isValid(("RBOSGGSX")));

        // SWIFT-BIC Banque et Caisse d'Epargne de l'Etat - Luxemburg: BCEELULL
        $this->assertEquals(true, $this->_validator->isValid(("BCEELULL")));

        // SWIFT-BIC Deutschen Bundesbank - Germany: MARKDEFF
        $this->assertEquals(true, $this->_validator->isValid(("MARKDEFF")));
    }

    /**
     * Asserts that invalid SWIFT-BIC code can not be validated.
     *
     * @test
     */
    public function test2()
    {
        $this->assertEquals(false, $this->_validator->isValid(("CB1EL2LLFFF")));
        $this->assertEquals(false, $this->_validator->isValid((" E31DCLLFXX")));
    }
}
?>