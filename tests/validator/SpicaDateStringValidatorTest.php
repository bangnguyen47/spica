<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/DateTime.php';

/**
 * Set of test cases and assertions to ensure that SpicaDateStringValidator
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage validator
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 18, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaDateStringValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaDateStringValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaDateStringValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var strng
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide a valid date format value for {:field}.';
        $this->_validator = new SpicaDateStringValidator($this->_violationMessage, null, true);
    }

    /**
     * Assert that "required" property is always true if $required argument is not set
     * at object instantiation.
     *
     * @test
     */
    public function requiredIsAlwaysTrueByDefault()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $this->_validator->isRequired());
    }

    /**
     * Assert that "required" property can be reset at object instantiation.
     *
     * @test
     */
    public function requiredCanBeReset()
    {
        $validator = new SpicaDateStringValidator($this->_violationMessage, null, false);
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $validator->isRequired());

        $validator = new SpicaDateStringValidator($this->_violationMessage, null, true);
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $validator->isRequired());
    }

    /**
     * Asserts that null value or empty string can not pass the validation when $required property
     * is set to true.
     *
     * @test
     */
    public function emptyStringOrNullCanNotPassWhenRequiredIsTrue()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(null));
        $this->assertEquals(false, $this->_validator->isValid($var = null));
        $this->assertEquals(false, $this->_validator->isValid(''));
        $this->assertEquals(false, $this->_validator->isValid('      '));
    }

    /**
     * Asserts that null value or empty string can not pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function emptyStringOrNullCanPassWhenRequiredIsFalse()
    {
        // Creates a validator and assume that when empty data is provided it always
        // returns true
        $validator = new SpicaDateStringValidator($this->_violationMessage, null, false);
        // Assert that empty value does pass the validation.
        $this->assertEquals(true, $validator->isValid(null));
        $this->assertEquals(true, $validator->isValid($var = null));
        $this->assertEquals(true, $validator->isValid($var = ''));
    }

    /**
     * Asserts that a string with all whitespaces must not pass the validation
     * when $required property is set to true.
     *
     * @test
     */
    public function stringWithAllWhitespacesCanNotPassWhenRequiredIsTrue()
    {
        // required is true
        $this->assertEquals(false, $this->_validator->isValid('      '));
    }

    /**
     * Asserts that a string with all whitespaces must pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function stringWithAllWhitespacesCanNotPassWhenRequiredIsFalse()
    {
        // required is false
        $validator = new SpicaDateStringValidator($this->_violationMessage, null, false);
        $this->assertEquals(true, $validator->isValid('      '));
    }

    /**
     * Asserts that boolean value can not pass the validation.
     *
     * @test
     */
    public function booleanValueCanNotPass()
    {
        // Required is set to be true
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(true));
        $this->assertEquals(false, $this->_validator->isValid(false));

        // Required is set to be false
        $validator = new SpicaDateStringValidator($this->_violationMessage, null, false);
        $this->assertEquals(false, $validator->isValid(true));
        $this->assertEquals(false, $validator->isValid(false));
    }

    /**
     * Asserts that empty array can not pass the validation.
     *
     * @test
     */
    public function nonScalarEmptyValueCanNotPass()
    {
        // Required is set to be true
        $var = array();
        $this->assertEquals(false, $this->_validator->isValid($var));

        // Required is set to be false
        $validator = new SpicaDateStringValidator($this->_violationMessage, null, false);
        $this->assertEquals(false, $validator->isValid($var));
    }

    /**
     * Asserts that zero value can pass the validation when $required property
     * is set to false.
     *
     * @test
     */
    public function zeroIsNotConsideredEmptyWhenRequiredIsFalse()
    {
        $validator = new SpicaDateStringValidator($this->_violationMessage, null, false);
        // Not alphabetic characters but zero-based digits or numeric string
        $this->assertEquals(false, $validator->isValid(0));
        $this->assertEquals(false, $validator->isValid(0000));
    }

    /**
     * Asserts that a non-string (boolean, array, object) must not pass the validation.
     *
     * @test
     */
    public function nonStringMustNotPass()
    {
        // Boolean values has been tested separately
        $this->assertEquals(false, $this->_validator->isValid(array()));
        $this->assertEquals(false, $this->_validator->isValid(new StdClass()));
    }

    /**
     * Asserts that invalid date strings must not pass.
     *
     * @test
     */
    public function invalidDateStringMustNotPass1()
    {
        // Boolean values has been tested separately
        $this->assertEquals(false, $this->_validator->isValid('00-00-00'));
        $this->assertEquals(false, $this->_validator->isValid('2009-40-12'));
        $this->assertEquals(false, $this->_validator->isValid('1000-13-32'));
        $this->assertEquals(false, $this->_validator->isValid('1000133466(2'));
        $this->assertEquals(false, $this->_validator->isValid('%%%(2'));
    }

    /**
     * Asserts that invalid date strings must not pass.
     *
     * @test
     */
    public function invalidDateStringMustNotPass2()
    {
        $validator = new SpicaDateStringValidator($this->_violationMessage, 'YYYY/MM/DD HH:MI:SS', false);
        $this->assertEquals(false, $validator->isValid('2008/01/02'));
        $this->assertEquals(false, $validator->isValid('2008/03/02 25:10:10'));
    }

    /**
     * Asserts that valid date strings must pass.
     *
     * @test
     */
    public function validDateStringMustPass1()
    {
        $this->assertEquals(true, $this->_validator->isValid('2008-01-02'));
        $this->assertEquals(true, $this->_validator->isValid('2009-10-12'));
        $this->assertEquals(true, $this->_validator->isValid('2009-12-31'));
    }

    /**
     * Asserts that valid date strings must pass.
     *
     * @test
     */
    public function validDateStringMustPass2()
    {
        $validator = new SpicaDateStringValidator($this->_violationMessage, 'YYYY/MM/DD HH:MI:SS', false);
        $this->assertEquals(true, $validator->isValid('2008/01/02 12:09:19'));
        $this->assertEquals(true, $validator->isValid('2009/10/12 03:09:59'));
        $this->assertEquals(true, $validator->isValid('2002/12/31 11:31:04'));
    }

    /**
     * Asserts that the tested value can be retrieved.
     *
     * @test
     */
    public function possibleToGetTestedValue()
    {
        $value = 'x';
        // Do the validation
        $this->_validator->isValid($value);
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_validator->getValue(), $value);
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}
?>