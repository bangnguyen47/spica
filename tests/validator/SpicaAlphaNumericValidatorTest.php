<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';

/**
 * Set of test cases and assertions to ensure that SpicaAlphaNumericValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaAlphaNumericValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 29, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaAlphaNumericValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAlphaNumericValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaAlphaNumericValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var strng
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide an alphanumeric value for {:field}.';
        $this->_validator = new SpicaAlphaNumericValidator($this->_violationMessage);
    }

    /**
     * Assert that "required" property is always true if $required argument is not set
     * at object instantiation.
     *
     * @test
     */
    public function requiredIsAlwaysTrueByDefault()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $this->_validator->isRequired());
    }

    /**
     * Assert that "required" property can be reset at object instantiation.
     *
     * @test
     */
    public function requiredCanBeReset()
    {
        $validator = new SpicaAlphaNumericValidator($this->_violationMessage, false);
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $validator->isRequired());

        $validator = new SpicaAlphaNumericValidator($this->_violationMessage, true);
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $validator->isRequired());
    }

    /**
     * Asserts that null value or empty string can not pass the validation when $required property
     * is set to true.
     *
     * @test
     */
    public function emptyStringOrNullCanNotPassWhenRequiredIsTrue()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(null));
        $this->assertEquals(false, $this->_validator->isValid($var = null));
        $this->assertEquals(false, $this->_validator->isValid(''));
        $this->assertEquals(false, $this->_validator->isValid('      '));
    }

    /**
     * Asserts that null value or empty string can not pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function emptyStringOrNullCanPassWhenRequiredIsFalse()
    {
        // Creates a validator and assume that when empty data is provided it always
        // returns true
        $validator = new SpicaAlphaNumericValidator($this->_violationMessage, false);
        // Assert that empty value does pass the validation.
        $this->assertEquals(true, $validator->isValid(null));
        $this->assertEquals(true, $validator->isValid($var = null));
        $this->assertEquals(true, $validator->isValid($var = ''));
    }

    /**
     * Asserts that a string with all whitespaces must not pass the validation
     * when $required property is set to true.
     *
     * @test
     */
    public function stringWithAllWhitespacesCanNotPassWhenRequiredIsTrue()
    {
        // required is true
        $this->assertEquals(false, $this->_validator->isValid('      '));
    }

    /**
     * Asserts that a string with all whitespaces must pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function stringWithAllWhitespacesMustPassWhenRequiredIsFalse()
    {
        // required is false
        $validator = new SpicaAlphaNumericValidator($this->_violationMessage, false);
        $this->assertEquals(true, $validator->isValid('      '));
    }

    /**
     * Asserts that boolean value can not pass the validation.
     *
     * @test
     */
    public function booleanValueCanNotPass()
    {
        // Required is set to be true
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(true));
        $this->assertEquals(false, $this->_validator->isValid(false));

        // Required is set to be false
        $validator = new SpicaAlphaNumericValidator($this->_violationMessage, false);
        $this->assertEquals(false, $validator->isValid(true));
        $this->assertEquals(false, $validator->isValid(false));
    }

    /**
     * Asserts that empty array can not pass the validation.
     *
     * @test
     */
    public function nonScalarEmptyValueCanNotPass()
    {
        // Required is set to be true
        $var = array();
        $this->assertEquals(false, $this->_validator->isValid($var));

        // Required is set to be false
        $validator = new SpicaAlphaNumericValidator($this->_violationMessage, false);
        $this->assertEquals(false, $validator->isValid($var));
    }

    /**
     * Asserts that a float number (with decimal separator like . or ,) must not
     * pass the validation.
     *
     * @test
     */
    public function floatNumberStringCanNotPass()
    {
        // Numeric string but contains commas: ,
        $this->assertEquals(false, $this->_validator->isValid('1,023,209'));
        // float
        $this->assertEquals(false, $this->_validator->isValid(1023444.99));
        $this->assertEquals(false, $this->_validator->isValid('1,023,707.99'));
        $this->assertEquals(false, $this->_validator->isValid($var = '1.2'));
        $this->assertEquals(false, $this->_validator->isValid(5/2));
    }

    /**
     * Asserts that an integer must pass the validation.
     *
     * @test
     */
    public function intergerCanPassTheTest()
    {
        $this->assertEquals(true, $this->_validator->isValid(1023209));
        // Single digit
        $this->assertEquals(true, $this->_validator->isValid(1));

        $validator = new SpicaAlphaNumericValidator($this->_violationMessage, false);
        $this->assertEquals(true, $validator->isValid(0));
        $this->assertEquals(true, $validator->isValid(0000));
    }

    /**
     * Asserts that a non-string (boolean, array, object) must not pass the validation.
     *
     * @test
     */
    public function nonStringOrNumberMustNotPass()
    {
        // Boolean values has been tested separately
        $this->assertEquals(false, $this->_validator->isValid(array()));
        $this->assertEquals(false, $this->_validator->isValid(new StdClass()));
    }

    /**
     * Asserts that a string with all letters and digits must pass the validation.
     *
     * @test
     */
    public function lettersAndDigitsMustPass()
    {
        // Single letter
        $this->assertEquals(true, $this->_validator->isValid('w'));
        $this->assertEquals(true, $this->_validator->isValid('alpha'));
        $this->assertEquals(true, $this->_validator->isValid('picture'));
        $this->assertEquals(true, $this->_validator->isValid('hundred'));
        $this->assertEquals(true, $this->_validator->isValid('millions'));
        $this->assertEquals(true, $this->_validator->isValid('mil24545ju90lions'));
        $this->assertEquals(true, $this->_validator->isValid('1334i8607il24545ju90lions'));
        $this->assertEquals(true, $this->_validator->isValid($var = '0'));
        $this->assertEquals(true, $this->_validator->isValid($var = '0000'));
    }

    /**
     * Asserts that a string with special characters must not pass the validation.
     *
     * @test
     */
    public function stringContainsSpecialCharactersMustNotPass()
    {
        $this->assertEquals(false, $this->_validator->isValid('_ff*h'));
        $this->assertEquals(false, $this->_validator->isValid('1-1'));
        $this->assertEquals(false, $this->_validator->isValid('World\'VestBase'));
        $this->assertEquals(false, $this->_validator->isValid('summer96^8'));
        $this->assertEquals(false, $this->_validator->isValid(','));
        // Contains white space
        $this->assertEquals(false, $this->_validator->isValid('hello world'));
        // Vietnamese Unicode
        $this->assertEquals(false, $this->_validator->isValid('chào'));
    }

    /**
     * Asserts that the tested value can be retrieved.
     *
     * @test
     */
    public function possibleToGetTestedValue()
    {
        $value = 'x';
        // Do the validation
        $this->_validator->isValid($value);
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_validator->getValue(), $value);
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}
?>