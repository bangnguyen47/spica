<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';

/**
 * Set of test cases and assertions to ensure that SpicaDuplicateEntryValidator
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage validator
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 23, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaDuplicateEntryValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaDuplicateEntryValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaDuplicateEntryValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'Your input must not include duplicate value.';
        $this->_validator = new SpicaDuplicateEntryValidator($this->_violationMessage, true);
    }

    /**
     * Asserts that "required" property is always true if $required argument is not set
     * at object instantiation.
     *
     * @test
     */
    public function requiredIsAlwaysTrueByDefault()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $this->_validator->isRequired());
    }

    /**
     * Asserts that "required" property can be reset at object instantiation.
     *
     * @test
     */
    public function requiredCanBeReset()
    {
        $validator = new SpicaDuplicateEntryValidator($this->_violationMessage, false);
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $validator->isRequired());

        $validator = new SpicaDuplicateEntryValidator($this->_violationMessage, true);
        // Assert that null value does not pass the validation.
        $this->assertEquals(true, $validator->isRequired());
    }

    /**
     * Asserts that null value or empty string can not pass the validation when $required property
     * is set to true.
     *
     * @test
     */
    public function nullCanNotPassWhenRequiredIsTrue()
    {
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(null));
        $this->assertEquals(false, $this->_validator->isValid($var = null));
    }

    /**
     * Asserts that null value or empty string can not pass the validation
     * when $required property is set to false.
     *
     * @test
     */
    public function nullCanPassWhenRequiredIsFalse()
    {
        // Creates a validator and assume that when empty data is provided it always returns true
        $validator = new SpicaDuplicateEntryValidator($this->_violationMessage, false);
        // Assert that empty value does pass the validation.
        $this->assertEquals(true, $validator->isValid(null));
        $this->assertEquals(true, $validator->isValid($var = null));
    }

    /**
     * Asserts that empty array can not pass the validation.
     *
     * @test
     */
    public function nonArrayValueCanNotPass()
    {
        // Required is set to be true
        // Assert that null value does not pass the validation.
        $this->assertEquals(false, $this->_validator->isValid(true));
        $this->assertEquals(false, $this->_validator->isValid(false));

        // Required is set to be false
        $validator = new SpicaDuplicateEntryValidator($this->_violationMessage, false);
        $this->assertEquals(false, $validator->isValid(true));
        $this->assertEquals(false, $validator->isValid(false));

        $validator = new SpicaDuplicateEntryValidator($this->_violationMessage, false);
        $this->assertEquals(false, $validator->isValid(new StdClass()));
        $this->assertEquals(false, $validator->isValid(new ArrayObject()));
        $this->assertEquals(false, $validator->isValid(1));
        $this->assertEquals(false, $validator->isValid("hello"));
    }

    /**
     * Asserts that zero value can pass the validation when $required property
     * is set to false.
     *
     * @test
     */
    public function zeroIsNotConsideredEmptyWhenRequiredIsFalse()
    {
        $validator = new SpicaDuplicateEntryValidator($this->_violationMessage, false);
        // Not alphabetic characters but zero-based digits or numeric string
        $this->assertEquals(false, $validator->isValid(0));
        $this->assertEquals(false, $validator->isValid(0000));
    }

    /**
     * Asserts that duplicate value can be detected.
     *
     * @test
     */
    public function testDuplicate()
    {
        $this->assertEquals(false, $this->_validator->isValid(array(1, 2, 3, 5, 6, 7, 1)));
        $this->assertEquals(false, $this->_validator->isValid(array(1, 1, 3, 5, 6, 7, 8)));
        $this->assertEquals(false, $this->_validator->isValid(array('a' => 1, 'b' => "1")));
    }

    /**
     * Asserts that non duplicate values can pass.
     *
     * @test
     */
    public function testNonDuplicate()
    {
        $this->assertEquals(true, $this->_validator->isValid(array(1, 2, 3, 5, 6, 7)));
        $this->assertEquals(true, $this->_validator->isValid(array(1, 3, 5, 6, 7, 8, 100)));
        $this->assertEquals(true, $this->_validator->isValid(array('a' => 1, 'b' => "2")));
    }

    /**
     * Asserts that the tested value can be retrieved.
     *
     * @test
     */
    public function possibleToGetTestedValue()
    {
        $value = 'x';
        // Do the validation
        $this->_validator->isValid($value);
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_validator->getValue(), $value);
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}

?>