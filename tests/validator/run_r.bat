@echo off
REM /*
REM  * Copyright (C) 2009 - 2011 Pham Cong Dinh
REM  *
REM  * This file is part of Spica.
REM  *
REM  * This is free software; you can redistribute it and/or modify it
REM  * under the terms of the GNU Lesser General Public License as
REM  * published by the Free Software Foundation; either version 3 of
REM  * the License, or (at your option) any later version.
REM  *
REM  * This software is distributed in the hope that it will be useful,
REM  * but WITHOUT ANY WARRANTY; without even the implied warranty of
REM  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
REM  * Lesser General Public License for more details.
REM  *
REM  * You should have received a copy of the GNU Lesser General Public
REM  * License along with this software; if not, write to the Free
REM  * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
REM  * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
REM  */
CLS
ECHO.
ECHO ----------------------- 
ECHO @name   Spica Framework
ECHO @author pcdinh
ECHO @since  March 14, 2009
ECHO ----------------------- 
ECHO. 
ECHO Run a specific test suite class with PHPUnit.
ECHO. 
SET /p input=Please provide test file/class name: 
ECHO. 
:RUN_IT
ECHO Prepare to run test cases named %input%
ECHO -----------------------------------------------------------------
ECHO. 
php phpunit.php %input%
SET /p c=Continue (Y,N)?
CLS
IF "%c%" == "" (
  GOTO RUN_IT
) ELSE IF "%c%"=="N" (
  GOTO END
) ELSE (
  GOTO RUN_IT
)  
::END
