<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'.
PATH_SEPARATOR.$baseDir.'/tests/validator'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';
include_once 'SpicaOptionalStringPatternValidatorTest.php';

/**
 * Set of test cases and assertions to ensure that SpicaUrlPatternValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaUrlPatternValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 22, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaUrlPatternValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaUrlPatternValidatorTest extends SpicaOptionalStringPatternValidatorTest
{
    /**
     * Validator.
     *
     * @var SpicaUrlPatternValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'You must provide a valid URL for {:field}.';
        $this->_validator = new SpicaUrlPatternValidator($this->_violationMessage);
    }

    /**
     * Asserts that a string with valid URL format must pass the validation.
     *
     * @test
     */
    public function validURLFormatMustPass()
    {
        $this->assertEquals(true, $this->_validator->isValid('yahoo.com'));
        $this->assertEquals(true, $this->_validator->isValid('nhavanvietnam.com'));
        $this->assertEquals(true, $this->_validator->isValid('danhnhadatviet.com.vn'));
        $this->assertEquals(true, $this->_validator->isValid('sun.com'));
        $this->assertEquals(true, $this->_validator->isValid('mtv.tv'));
        $this->assertEquals(true, $this->_validator->isValid('phimtruyen.us'));
        $this->assertEquals(true, $this->_validator->isValid('mICrOsOfT.cOm'));
        $this->assertEquals(true, $this->_validator->isValid('911.com'));
        $this->assertEquals(true, $this->_validator->isValid('x-nhavanvietnam.com'));
        $this->assertEquals(true, $this->_validator->isValid('http://y-kien.net'));
        $this->assertEquals(true, $this->_validator->isValid('www.ykien.net'));
        $this->assertEquals(true, $this->_validator->isValid('sub.rus.it'));
        $this->assertEquals(true, $this->_validator->isValid('67-rus.rus.it'));
        $this->assertEquals(true, $this->_validator->isValid('h.truyen.beta.forum.it'));
        $this->assertEquals(true, $this->_validator->isValid('_ht.beta.forum.it'));
        // $this->assertEquals(true, $this->_validator->isValid('http://user:password@host.com'));
    }

    /**
     * Asserts that a string with invalid URI format must pass the validation.
     *
     * @test
     */
    public function invalidURIFormatMustNotPass()
    {
        $this->assertEquals(false, $this->_validator->isValid('yahoo.com-'));
        $this->assertEquals(false, $this->_validator->isValid('nhavanvietnam.-com'));
        $this->assertEquals(false, $this->_validator->isValid('danhnhadatviet!-com.vn'));
        $this->assertEquals(false, $this->_validator->isValid('sun.^com'));
        $this->assertEquals(false, $this->_validator->isValid('mtv.&tv'));
        $this->assertEquals(false, $this->_validator->isValid('phimtruyen.#us'));
        $this->assertEquals(false, $this->_validator->isValid('mICrOsOfT.cOm%'));
        $this->assertEquals(false, $this->_validator->isValid('911#.com'));
        $this->assertEquals(false, $this->_validator->isValid('x*nhavanvietnam.com'));
        $this->assertEquals(false, $this->_validator->isValid('http://@y-kien.net'));
        $this->assertEquals(false, $this->_validator->isValid('www.y$kien.net'));
        $this->assertEquals(false, $this->_validator->isValid('www.-ykien.net'));
        $this->assertEquals(false, $this->_validator->isValid('www.ykien-.net'));
        $this->assertEquals(false, $this->_validator->isValid('sub.*rus.it'));
        $this->assertEquals(false, $this->_validator->isValid('67-rus.rus(.it'));
        $this->assertEquals(false, $this->_validator->isValid('h.-huyen-.beta).forum.it'));
    }
}
?>