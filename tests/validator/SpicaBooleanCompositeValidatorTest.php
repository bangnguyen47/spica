<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
include_once 'core/validator/Common.php';
include_once 'core/validator/Complex.php';

/**
 * Set of test cases and assertions to ensure that
 * SpicaBooleanCompositeValidator works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaBooleanCompositeValidatorTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaBooleanCompositeValidatorTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaBooleanCompositeValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Validator.
     *
     * @var SpicaBooleanCompositeValidator
     */
    protected $_validator;

    /**
     * Violation message.
     *
     * @var string
     */
    protected $_violationMessage;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp()
    {
        $this->_violationMessage = 'Your input is not valid. ';
        $this->_validator        = new SpicaBooleanCompositeValidator($this->_violationMessage);
    }

    /**
     * Asserts that FALSE || FALSE || TRUE expression must pass the validation.
     *
     * @test
     */
    public function checkFalseOrFalseOrTrueMustPass()
    {
        $validator         = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'You must provide an alphanumeric value for {:field}.';
        $alphaNumbericVtor = new SpicaAlphaNumericValidator($violationMessage);
        $violationMessage  = 'You must provide an alphabetic value for {:field}.';
        $alphaVtor         = new SpicaAlphaValidator($violationMessage);
        $violationMessage  = 'You must provide an true boolean value for {:field}.';
        $trueVtor          = new SpicaAssertTrueValidator($violationMessage);

        $validator->add($alphaNumbericVtor);
        $validator->OR_($alphaVtor);
        $validator->OR_($trueVtor);

        $this->assertEquals(true, $validator->isValid(true));
    }

    /**
     * Asserts that TRUE || FALSE || FALSE expression must pass the validation.
     *
     * @test
     */
    public function checkTrueOrFalseOrFalseMustPass()
    {
        $validator         = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'You must provide an alphanumeric value for {:field}.';
        $alphaNumbericVtor = new SpicaAlphaNumericValidator($violationMessage);
        $violationMessage  = 'You must provide an alphabetic value for {:field}.';
        $alphaVtor         = new SpicaAlphaValidator($violationMessage);
        $violationMessage  = 'You must provide an true boolean value for {:field}.';
        $trueVtor          = new SpicaAssertTrueValidator($violationMessage);

        $validator->add($trueVtor)->OR_($alphaVtor)->OR_($alphaNumbericVtor);

        $this->assertEquals(true, $validator->isValid(true));
    }

    /**
     * Asserts that FALSE || TRUE || FALSE expression must pass the validation.
     *
     * @test
     */
    public function checkFalseOrTrueOrFalseMustPass()
    {
        $validator         = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'You must provide an alphanumeric value for {:field}.';
        $alphaNumbericVtor = new SpicaAlphaNumericValidator($violationMessage);
        $violationMessage  = 'You must provide an alphabetic value for {:field}.';
        $alphaVtor         = new SpicaAlphaValidator($violationMessage);
        $violationMessage  = 'You must provide an true boolean value for {:field}.';
        $trueVtor          = new SpicaAssertTrueValidator($violationMessage);

        $validator->add($alphaVtor)->OR_($trueVtor)->OR_($alphaNumbericVtor);

        $this->assertEquals(true, $validator->isValid(true));
    }

    /**
     * Asserts that FALSE || FALSE || FALSE expression must not pass the validation.
     *
     * @test
     */
    public function checkEmptyValueWhenRequiredIsTrueMustNotPass()
    {
        $validator         = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'You must provide an alphanumeric value for {:field}.';
        $alphaNumbericVtor = new SpicaAlphaNumericValidator($violationMessage);
        $violationMessage  = 'You must provide an alphabetic value for {:field}.';
        $alphaVtor         = new SpicaAlphaValidator($violationMessage);
        $violationMessage  = 'You must provide an true boolean value for {:field}.';
        $trueVtor          = new SpicaAssertTrueValidator($violationMessage);

        $validator->add($alphaVtor)->OR_($trueVtor)->OR_($alphaNumbericVtor);

        // false || false || false
        $this->assertEquals(false, $validator->isValid(''));

        // false || false || false
        $this->assertEquals(false, $validator->isValid(null));

        // false || false || false
        $this->assertEquals(false, $validator->isValid('    '));
    }

    /**
     * Asserts that FALSE XOR TRUE XOR FALSE expression must pass the validation.
     *
     * @test
     */
    public function checkXORexpression()
    {
        $validator         = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'You must provide an alphanumeric value for {:field}.';
        $alphaNumbericVtor = new SpicaAlphaNumericValidator($violationMessage);
        $violationMessage  = 'You must provide an alphabetic value for {:field}.';
        $alphaVtor         = new SpicaAlphaValidator($violationMessage);
        $violationMessage  = 'You must provide an true boolean value for {:field}.';
        $trueVtor          = new SpicaAssertTrueValidator($violationMessage);

        $validator->add($alphaVtor)->XOR_($alphaNumbericVtor)->XOR_($trueVtor);

        // (true ^ true ^ false)
        $rs = (true XOR true XOR false); // false
        $this->assertEquals(false, $validator->isValid('aaa'));

        // (false ^ true ^ false)
        $rs = (false XOR true XOR false); // true
        $this->assertEquals(true, $validator->isValid('a1'));

        // (false ^ false ^ false)
        $rs = (false XOR false XOR false); // false
        $this->assertEquals(false, $validator->isValid('a1_'));

        // Another combination
        $validator = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $validator->add($alphaVtor)->XOR_($alphaNumbericVtor)->AND_($trueVtor);

        // (true ^ true && false)
        $rs = (true XOR true && false); // false
        $this->assertEquals($rs, $validator->isValid('aaa'));

        // (false ^ true && false)
        $rs = (false XOR true && false); // false
        $this->assertEquals(false, $validator->isValid('a1'));

        // (false ^ false && false)
        $rs = (false XOR false && false); // false
        $this->assertEquals(false, $validator->isValid('a1_'));

        // Another combination
        $validator = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $validator->add($alphaVtor)->XOR_($alphaNumbericVtor)->OR_($trueVtor);

        // (true ^ true || false)
        $rs = (true XOR true || false); // false
        $this->assertEquals(false, $validator->isValid('aaa'));

        // (false ^ true || false)
        $rs = (false XOR true || false); // true
        $this->assertEquals(true, $validator->isValid('a1'));

        // (false ^ false || false)
        $rs = (false XOR false || false); // false
        $this->assertEquals(false, $validator->isValid('a1_'));

        // Another combination
        $validator = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $validator->add($alphaVtor)->OR_($alphaNumbericVtor)->XOR_($trueVtor);

        // (true || true ^ false)
        $rs = (true || true XOR false); // true
        $this->assertEquals(true, $validator->isValid('aaa'));

        // (false || true ^ false)
        $rs = (false || true XOR false); // true
        $this->assertEquals(true, $validator->isValid('a1'));

        // (false || false ^ false)
        $rs = (false || false XOR false); // false
        $this->assertEquals(false, $validator->isValid('a1_'));
    }

    /**
     * Asserts that FALSE XOR TRUE XOR FALSE expression must pass the validation.
     *
     * @test
     */
    public function checkNOTexpression()
    {
        $validator         = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'You must provide an alphanumeric value for {:field}.';
        $alphaNumbericVtor = new SpicaAlphaNumericValidator($violationMessage);
        $violationMessage  = 'You must provide an alphabetic value for {:field}.';
        $alphaVtor         = new SpicaAlphaValidator($violationMessage);
        $violationMessage  = 'You must provide an true boolean value for {:field}.';
        $trueVtor          = new SpicaAssertTrueValidator($violationMessage);

        $validator->add($alphaVtor)->AND_NOT($alphaNumbericVtor)->AND_NOT($trueVtor);

        // (true && !true && !false)
        $rs = (true && !true && !false); // false
        $this->assertEquals(false, $validator->isValid('aaa'));

        // (false && !true && !false)
        $rs = (false && !true && !false); // false
        $this->assertEquals(false, $validator->isValid('a1'));

        // (false && !false && !false)
        $rs = (false || false XOR !false); // false
        $this->assertEquals(false, $validator->isValid('a1_'));

        // Another combination
        $validator = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $validator->add($alphaVtor)->XOR_NOT($alphaNumbericVtor)->AND_NOT($trueVtor);

        // (true XOR !true && !false)
        $rs = (true XOR !true && !false); // true
        $this->assertEquals(true, $validator->isValid('aaa'));

        // (false XOR !true && !false)
        $rs = (false XOR !true && !false); // false
        $this->assertEquals(false, $validator->isValid('a1'));

        // (false XOR !false && !false)
        $rs = (false XOR false && !false); // true
        $this->assertEquals(true, $validator->isValid('a1_'));
    }

    /**
     * Asserts that the SpicaBooleanCompositeValidator can be nested.
     *
     * ((A || B AND NOT C) && D) || E
     * |                |      |
     * |-------A1-------|      |
     * |------------A2---------|
     *
     * @test
     */
    public function testNestedComplicatedBooleanExpression()
    {
        $violationMessage  = '';
        // Bootstrap
        // A
        $alphaVtor         = new SpicaAlphaValidator($violationMessage);
        $violationMessage  = 'You must provide an alphabetic string for {:field}.';
        // B
        $alphaNumbericVtor = new SpicaAlphaNumericValidator($violationMessage);
        $violationMessage  = 'You must provide an alpha numeric string for {:field}.';
        // E
        $violationMessage  = 'You must provide a valid email for {:field}.';
        $emailVtor         = new SpicaEmailPatternValidator($violationMessage);
        // C
        $violationMessage  = 'Your string length can not greater than 20.';
        $maxLengthVtor     = new SpicaMaxLengthStringValidator(20, $violationMessage, 1);
        // D
        $violationMessage  = 'Your string length can not less than 10.';
        $minLengthVtor     = new SpicaMinLengthStringValidator(100, $violationMessage, 1);

        // Build most inner expression: (A || B AND NOT C) = A1
        $innerValidator    = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'Valid string must be alpha or alpha numeric
        but the length must be longer than 20.';
        $innerValidator->add($alphaVtor)->OR_($alphaNumbericVtor)->AND_NOT($maxLengthVtor);

        // A2: A1 && D
        $innerValidator2   = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'Valid string must be alpha or alpha numeric
        but the length must be longer than 20 but must be less than 100.';
        $innerValidator2->add($innerValidator)->AND_($minLengthVtor);

        // A3: A2 || E
        $innerValidator3   = new SpicaBooleanCompositeValidator($this->_violationMessage);
        $violationMessage  = 'Valid string must be alpha or alpha numeric
        but the length must be longer than 20 but must be less than 100 or just be an email.';
        $innerValidator3->add($innerValidator2)->OR_($emailVtor);

        // ((A || B AND NOT C) && D) || E
        // ((false || false AND NOT true) && false) || true
        $rs = ((false || false AND !true) && false) || true; // true
        $this->assertEquals(true, $innerValidator3->isValid('aaa@email.com'));
    }

    /**
     * Asserts that the tested value can be retrieved.
     *
     * @test
     */
    public function possibleToGetTestedValue()
    {
        $value = 'x';
        // Do the validation
        $this->_validator->isValid($value);
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_validator->getValue(), $value);
    }

    /**
     * Asserts that violation message can be retrieved.
     *
     * @test
     */
    public function possibleToGetViolationMessage()
    {
        // Assert that not null value does pass the validation.
        $this->assertEquals($this->_violationMessage, $this->_validator->getViolationMessage());
    }
}
?>