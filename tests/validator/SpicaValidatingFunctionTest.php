<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
$baseDir = dirname(dirname(dirname(__FILE__)));

set_include_path(
get_include_path().
PATH_SEPARATOR.$baseDir.'/library/spica'
);

require_once 'PHPUnit/Framework.php';
require_once 'PHPUnit/Framework/TestCase.php';
include_once 'core/validator/DateTime.php';

/**
 * Set of test cases and assertions to ensure that all spica_valid_* functions
 * works as desired.
 *
 * @category   spica
 * @package    tests
 * @subpackage SpicaValidatingFunctionTest
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 31, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaValidatingFunctionTest.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaValidatingFunctionTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test spica_valid_empty_string().
     *
     * @test
     */
    public function stringEmptyTest()
    {
        // String with all white spaces.
        $empty = spica_valid_empty_string('   ', false);
        $this->assertEquals(true, $empty);
        $empty = spica_valid_empty_string('', false);
        $this->assertEquals(true, $empty);
        $empty = spica_valid_empty_string(null, false);
        $this->assertEquals(true, $empty);
    }

    /**
     * Test spica_valid_empty_string().
     *
     * @test
     */
    public function testInterger()
    {
        $this->assertEquals(true, spica_valid_integer(1));
        $this->assertEquals(true, spica_valid_integer(2));
        $this->assertEquals(true, spica_valid_integer(100));
        $this->assertEquals(true, spica_valid_integer(1883465468));
        $this->assertEquals(true, spica_valid_integer(0));
        $this->assertEquals(true, spica_valid_integer(-2));
        $this->assertEquals(false, spica_valid_integer(1.2));
        $this->assertEquals(false, spica_valid_integer(9.89));
        $this->assertEquals(true, spica_valid_integer("1"));
        $this->assertEquals(true, spica_valid_integer("01"));
        $this->assertEquals(false, spica_valid_integer("01.1"));
    }

    /**
     * Test spica_valid_time().
     *
     * @test
     */
    public function testTimeValueCombination()
    {
        $this->assertEquals(true, spica_valid_time(12, 12, 12));
        $this->assertEquals(true, spica_valid_time("12", 3, 60));
        $this->assertEquals(true, spica_valid_time(13, 09, 04));
        $this->assertEquals(true, spica_valid_time("13", 12, 12));
        $this->assertEquals(true, spica_valid_time("24", 7, 6));
        $this->assertEquals(false, spica_valid_time("25", 12, 12));
        $this->assertEquals(false, spica_valid_time("23", "61", 03));
        $this->assertEquals(false, spica_valid_time("02", "100", 03));
        $this->assertEquals(true, spica_valid_time("02", 59, 11));
        $this->assertEquals(true, spica_valid_time("02", "59", 11));
        $this->assertEquals(true, spica_valid_time("02", "59", "11"));
    }

    /**
     * Test spica_valid_date_format().
     *
     * @test
     */
    public function testDateFormat()
    {
        $this->assertEquals(true, spica_valid_date_format("20051209", "YYYYMMDD", true));
        $this->assertEquals(true, spica_valid_date_format("20051220", "YYYYMMDD", true));
        $this->assertEquals(true, spica_valid_date_format("20060427", "YYYYMMDD", true));
        $this->assertEquals(false, spica_valid_date_format("20051232", "YYYYMMDD", true));
        $this->assertEquals(false, spica_valid_date_format("20051320", "YYYYMMDD", true));
        $this->assertEquals(false, spica_valid_date_format("200513", "YYYYMMDD", true));
        $this->assertEquals(false, spica_valid_date_format("2005", "YYYYMMDD", true));

        $this->assertEquals(true, spica_valid_date_format("2005/12/09", "YYYY/MM/DD", true));
        $this->assertEquals(true, spica_valid_date_format("2005/12/20", "YYYY/MM/DD", true));
        $this->assertEquals(true, spica_valid_date_format("2006/04/27", "YYYY/MM/DD", true));
        $this->assertEquals(false, spica_valid_date_format("2005/12/32", "YYYY/MM/DD", true));
        $this->assertEquals(false, spica_valid_date_format("2005/13/20", "YYYY/MM/DD", true));
        $this->assertEquals(false, spica_valid_date_format("2005/13", "YYYY/MM/DD", true));
        $this->assertEquals(false, spica_valid_date_format("2005", "YYYY/MM/DD", true));

        $this->assertEquals(false, spica_valid_date_format("20050809", null, true)); // checked against default value YYYY-MM-DD
    }

    /**
     * Test spica_valid_date_format().
     *
     * @test
     */
    public function testDateFormat2()
    {
        $this->assertEquals(false, spica_valid_date_format("    d", null, true));
        $this->assertEquals(false, spica_valid_date_format("    ", null, true));
    }
}

?>
