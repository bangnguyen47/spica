/**
 * Spica Framework
 * 
 * @author pcdinh
 * @since June 15, 2009
 * @namespace spica
 */
if (typeof spica == "undefined" || !spica) {
	var spica = {};
}

/**
 * Static methods to handle cookies.
 * 
 * @namespace spica.cookie
 */
spica.cookie = {};

/**
 * Sets a cookie.
 */
spica.cookie.set = function(name, value, expires, path, domain, secure) {
	var today = new Date();
	today.setTime(today.getTime());

	var expiresDate;
	if (expires) {
		if (expires > 0) {
			expires = expires * 1000 * 60 * 60 * 24;
		} else {
			expiresDate = new Date(0);
		}
	}
	if (!expiresDate) {
		expiresDate = new Date(today.getTime() + (expires));
	}

	document.cookie = name + "=" + escape(value)
			+ ((expires) ? ";expires=" + expiresDate.toGMTString() : "")
			+ ((path) ? ";path=" + path : ";path=/")
			+ ((domain) ? ";domain=" + domain : "")
			+ ((secure) ? ";secure" : "");
};
/**
 * Gets a cookie value.
 */
spica.cookie.get = function(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1, c.length);
		}
		if (c.indexOf(nameEQ) == 0) {
			return unescape(c.substring(nameEQ.length, c.length));
		}
	}
	return null;
};
/**
 * Removes a cookie.
 */
spica.cookie.remove = function(name, path, domain) {
	spica.cookie.set(name, "", -1, path, domain);
};

/**
 * Static methods to handle URL params.
 * 
 * @namespace spica.params
 */
spica.params = {};

/**
 * Gets query parameter value
 */
spica.params.get = function(variable) {
    var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
	    var pair = vars[i].split("=");
	    if (pair[0] == variable) {
	        return pair[1];
	    }
	}
	
	return null;
};

/**
 * Static methods to deal with runtime environment.
 * 
 * @namespace spica.Utils
 */
spica.Utils = {
	os : 'unknown',	
	preloadImage : function(image) {
	    (new Image()).src = image;
	},
	
	getOS : function() {		
	    if (navigator.userAgent.indexOf('Linux') != -1) { 
	    	os = "linux"; 
	    } else if (navigator.userAgent.indexOf('Win') != -1) { 
	    	os = "windows"; 
	    } else if (navigator.userAgent.indexOf('Mac') != -1) { 
	    	os = "macintosh"; 
	    } 
	    
	    return os;
	},
	
	date : {
	    // Takes the format of "Jan 15, 2007 15:45:00 GMT" and converts
	    // it to a relative time
	    // Ruby strftime: %b %d, %Y %H:%M:%S GMT
	    timeAgoWithParsing: function(from) {
		    var date = new Date;
			date.setTime(Date.parse(from));
			return this.timeAgo(date);
		},
			  
		timeAgo: function(from) {
			return this.distanceOfTime(new Date, from);
		},
			 
		distanceOfTime: function(to, from) {
			var inSec = ((to - from) / 1000);
			var inMin = (inSec / 60).floor();
			 
			if (inMin == 0) { 
				return 'less than a minute ago'; 
			}
			if (inMin == 1) { 
				return 'a minute ago'; 
			}
			if (inMin < 45) { 
				return inMin + ' minutes ago'; 
			}
			if (inMin < 90) { 
				return 'about 1 hour ago'; 
			}
			if (inMin < 1440) { 
				return 'about ' + (inMin / 60).floor() + ' hours ago'; 
			}
			if (inMin < 2880) { 
				return '1 day ago'; 
			}
			if (inMin < 43200) { 
				return (inMin / 1440).floor() + ' days ago'; 
			}
			if (inMin < 86400) { 
				return 'about 1 month ago'; 
			}
			if (inMin < 525960) { 
				return (inMin / 43200).floor() + ' months ago'; 
			}
			if (inMin < 1051199) { 
				return 'about 1 year ago'; 
			}
			 
			return 'over ' + (inMin / 525960).floor() + ' years ago';
		}
	}
};

spica.Text = {
	/**
	 * Get selected text on the Window or textarea
	 */	
	getSelection : function() {
	    if (window.getSelection) {
           return window.getSelection();
        } else if (document.getSelection) {
           return document.getSelection();
        } else if (document.selection) {
           return document.selection.createRange().text;
        }
	    
	    return null;    
    }	
};

/**
 * Static methods to handle HTML.
 * 
 * @namespace spica.html
 */
spica.html = {};

/**
 * Generates an unordered/ordered HTML list.
 */
spica.html.toList = function(dataArray, ordered) {
	if (true === ordered) {
		return '<ol><li>' + dataArray.join('</li><li>') + '</li></ol>';
	}
	
    return '<ul><li>' + dataArray.join('</li><li>') + '</li></ul>'; 
};

/**
 * Generates a table.
 */
spica.html.toTable = function(body, head, colgroup, foot) {
	var table     = '';
	var tableBody = [];
	var length = body.length;
	
	if (0 === length) {
	    return '';	
	}
		
	if (head instanceof Array) {
		head = '<thead><tr><th>' + head.join('</th><th>') + '</th></tr></thead>';
	} else {
	    var head = '';	
	}
	
	if (foot instanceof Array) {
		foot = '<tfoot><tr><td>' + foot.join('</td><td>') + '</td></tr></tfoot>';
	} else {
		var foot = '';	
	}	
	
	for (var i = 0; i < length; i++) {
		tableBody[i] = '<tr><td>' + body.join('</td><td>') + '</td></tr>';    	
	}
    
    return '<table>' + head + foot + '<tbody>' + tableBody.join('') + '</tbody></table>';  
};
