================================================================
Spica - a PHP MVC-pull framework and content management platform
================================================================
This is a guide to install PHPUnit to run Spica test cases.

Download PHPUnit 3.4 from http://pear.phpunit.de/get/ to /tests
and unzip it into PHPUnit. Now we have got a directory structure 
as the following:

+ tests\PHPUnit\PHPUnit
+ tests\PHPUnit\phpunit.bat
+ tests\PHPUnit\phpunit.php
+ tests\PHPUnit\PHPUnit\Framework.php


Getting in touch with the developers
====================================

The best way to communicate with the developers is the Google Group
on http://groups.google.com/group/spicaproject. 

The wiki and bug tracker is available on the `Spica development website`.
If you encounter bugs or if you want to suggest a new feature be sure to
file a new ticket there.

Other resources:

- Spica development website: http://code.google.com/p/spica
- Spica developer group:     http://groups.google.com/group/spicaproject
- Main repository:           http://spica.googlecode.com/svn/trunk/
- Commit logs:               http://twitter.com/spicacmsdev 
- Announcements:             http://twitter.com/spicacms
- Lead developer micro blog: http://twitter.com/pcdinh


Pham Cong Dinh (a.k.a pcdinh)
