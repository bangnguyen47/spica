<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Provides a set of methods to work with numbers.
 *
 * namespace spica\core\utils\NumberUtils
 *
 * @category   spica
 * @package    core
 * @subpackage utils
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      August 19, 2009
 * @version    $Id: NumberUtils.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaNumberUtils
{
    /**
     * Returns a random 12-digit hex number.
     *
     * @return string
     **/
    public static function nonce()
    {
        return sprintf('%06x', rand(0, 16776960)) . sprintf('%06x', rand(0, 16776960));
    }

    public static function probability($chance, $outOf = 100)
    {
        $random = mt_rand(1, $outOf);
        return $random <= $chance;
    }

    public static function randomFloat($min, $max)
    {
        return ($min + lcg_value()*(abs($max - $min)));
    }

    public static function suffix($i)
    {
        if ($i == 0)
        {
            return '';
        }
        
        $i %= 100;
        
        if ((10 < $i) && ($i < 14))
        {
            return 'th';
        }

        $i %= 10;
        
        switch ($i)
        {
            case 1:
                return 'st';
                
            case 2:
                return 'nd';
                
            case 3:
                return 'rd';
        }        
        
        return 'th';
    }
}

?>