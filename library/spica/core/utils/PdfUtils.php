<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * General file manipulation utilities.
 *
 * @category   spica
 * @package    core
 * @subpackage utils
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      July 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: PdfUtils.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaPdfUtils
{
    /**
     * Creates a preview of PDF document's first page.
     *
     * @param  string $filePath Path to PDF file
     * @param  string $format png, jpeg, gif
     * @return Imagick
     */
    public static function createPreview($filePath, $format = 'png')
    {
        // Read page 1
        $im = new Imagick($filePath.'[0]');
        // Convert to png
        $im->setImageFormat($format);
        echo $im;
    }

    /**
     * Saves an image into a new PDF file.
     * 
     * @param string $imagePath
     * @param string $pdfPath
     */
    public static function createPdf($imagePath, $pdfPath)
    {
        $im = new Imagick($imagePath);
        $im->setImageFormat('pdf');
        $im->writeImage($pdfPath);
    }
}

?>