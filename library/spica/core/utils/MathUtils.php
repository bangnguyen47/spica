<?php

/**
 * $datablock = file_get_contents($filename);
 $be = new bigEndian($datablock);
 $firstword = $be->getWord() //bytes 0,1
 $char = $be->getChar() //byte 2
 $doubleword = $be->getDoubleWord() //bytes 3,4,5,6
 */
class SpicaBigEndian
{
    private $data; //pointer to data
    private $ptr; //current position in data

    public function __construct(&$data)
    {
        $this->data = & $data;
        $this->ptr = 0;
    }

    public function getChar()
    {
        $r = ord($this->data[$this->ptr]);
        $this->ptr ++;
        return $r;
    }

    public function getWord()
    {
        $r = (ord($this->data[$this->ptr]) << 8) | ord($this->data[$this->ptr+1]);
        $this->ptr += 2;
        return $r;
    }

    public function getDoubleWord()
    {
        $r = (ord($this->data[$this->ptr]) << 24) | (ord($this->data[$this->ptr+1]) << 16) | (ord($this->data[$this->ptr+2]) << 8) | ord($this->data[$this->ptr+3]);
        $this->ptr += 4;
        return $r;
    }
}

define("BIGINT_DIVIDER", 0x7fffffff + 1);

/**
 * http://www.lfsforum.net/showthread.php?p=1378095
 */
class SpicaBigInt
{
    /**
     * puts the upper 32 bits into $upper and the lower 32 bits into $lower.
     * Afterwards, the result in converted to a float using bigInt2float(). 
     * @param <type> $upper
     * @param <type> $lower
     * @param <type> $value
     */
    function split2Int(&$upper, &$lower, $value)
    {
        $lower = intval($value % BIGINT_DIVIDER);
        $upper = intval(($value - $lower) / BIGINT_DIVIDER);
    }

    function bigInt2float($upper, $lower)
    {
        return $upper * BIGINT_DIVIDER + $lower;
    }
}

?>