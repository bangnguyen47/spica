<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
include_once 'library/spica/core/datasource/DatabaseManager.php';

/**
 * Abstraction of the real world database-backed service classes for Spica applications.
 * The SpicaDatabaseService is used for managing the persistence aspect of applications.
 *
 * namespace spica\core\service\DatabaseService
 *
 * @category   spica
 * @package    core
 * @subpackage service
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 06, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DatabaseService.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
abstract class SpicaDatabaseService /* implements SpicaGenericService */
{
    /**
     * Database connection name.
     *
     * @var string
     */
    public $dbAlias = 'db1';

    /**
     * Data source.
     *
     * @var SpicaConnection
     */
    public $ds;

    /**
     * Constructs an object of <code>SpicaDatabaseService</code>
     *
     * @param string $alias
     */
    public function __construct($alias = null)
    {
        $this->init($alias);
    }

    /**
     * Initializes a database connection manager.
     *
     * @param string $alias Configured alias name for a specific database connection settings
     */
    public function init($alias = null)
    {
        if (null !== $alias)
        {
            $this->dbAlias = $alias;
        }

        $this->ds = SpicaDatabaseManager::getConnection($this->dbAlias);
    }

    /**
     * Destroys this object.
     */
    public function __destruct()
    {

    }
}

?>