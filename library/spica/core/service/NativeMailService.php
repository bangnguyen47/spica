<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Base class that gives support for native PHP's mail() function.
 *
 * namespace spica\core\service\NativeMailService
 *
 * @category   spica
 * @package    core
 * @subpackage service
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      September 07, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: NativeMailService.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaNativeMailService
{
    /**
     * Sends a mail to a receiver using native PHP's mail() function.
     *
     * @see    spica\core\utils\String#encodeUtf8MailHeader
     * @param  string $to An email address
     * @param  string $body Content of the mail
     * @param  string $subject
     * @param  string $fromAddress An email address
     * @param  string $fromName
     * @param  string $bcc An email address
     * @param  string $returnPath An email address
     * @return bool
     */
    public function send($to, $body, $subject, $fromAddress, $fromName, $bcc = null, $returnPath = null)
    {
        include_once 'library/spica/core/utils/BaseUtils.php';

        $headers  = '';
        $headers .= 'From: ' . SpicaStringUtils::encodeUtf8MailHeader($fromName) . ' <' . $fromAddress . '>' . PHP_EOL;
        $headers .= 'Sender: ' . SpicaStringUtils::encodeUtf8MailHeader($fromName) . ' <' . $fromAddress . '>' . PHP_EOL;

        if (!empty($bcc))
        {
            $headers .= 'Bcc: ' . $bcc . PHP_EOL;
        }

        if (empty($bcc))
        {
            $returnPath .= $fromAddress;
        }

        $headers .= 'Reply-To: ' . $fromAddress . PHP_EOL;
        $headers .= 'Return-Path: <' . $fromAddress . '>' . PHP_EOL;
        $headers .= 'MIME-Version: 1.0' . PHP_EOL;
        $headers .= 'Message-ID: <' . md5(uniqid(time())) . '@' . SpicaStringUtils::encodeUtf8MailHeader($fromName) . '>' . PHP_EOL;
        $headers .= 'Date: ' . date('r') . PHP_EOL;
        $headers .= 'Content-Type: text/plain; charset=UTF-8' . PHP_EOL; // format=flowed
        $headers .= 'Content-Transfer-Encoding: 8bit' . PHP_EOL; // 7bit
        $headers .= 'X-Priority: 3' . PHP_EOL;
        $headers .= 'X-MSMail-Priority: Normal' . PHP_EOL;
        $headers .= 'X-Mailer: : PHP v' . phpversion() . PHP_EOL;
        $headers .= 'X-MimeOLE: spica' . PHP_EOL;

        return mail($to, SpicaStringUtils::encodeUtf8MailHeader($subject), preg_replace("#(?<!\r)\n#s", "\n", $body), $headers);
    }
}

?>