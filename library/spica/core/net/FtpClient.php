<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
// namespace spica\core\net;

/**
 * This class provides set of methods that supports FTP operations.
 *
 * @category   spica
 * @package    core
 * @subpackage net
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 19, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: FtpClient.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFtpClient
{
    /**
     * Curl handle.
     *
     * @var resource of type curl
     */
    private $_session;

    /**
     * FTP username.
     * 
     * @var string
     */
    private $_user;

    /**
     * FTP host.
     *
     * @var string
     */
    private $_host;

    /**
     * FTP password.
     * 
     * @var string
     */
    private $_pass;

    /**
     * Constructs an object of <code>SpicaFtpClient</code>.
     * 
     * @param string $host
     * @param string $user
     * @param string $password
     */
    public function __construct($host = null, $user = null, $password = null)
    {
        $this->_session = curl_init();
        
        if (null !== $host)
        {
            $this->_host = $host;
        }

        if (null !== $user)
        {
            $this->_user = $user;
        }

        if (null != $password)
        {
            $this->_pass = $password;
        }
    }

    /**
     * Uploads a file through FTP.
     *
     * @param string $localPath Local file path
     * @param string $remoteDir Remote directory path
     * @param string $host E.x: ftp.domain.com
     * @param string $ftpUsername
     * @param string $ftpPassword
     * @return SpicaFtpClientResponse
     */
    public function moveTo($localPath, $remoteDir, $host, $ftpUsername, $ftpPassword)
    {
        $remoteUrl = 'ftp://' . $ftpUsername . ':' . $ftpPassword . '@' . $host . '/' .$remoteDir . '/' . basename($localPath);
        $handle = fopen($localPath, 'r');

        curl_setopt($this->_session, CURLOPT_UPLOAD, 1);
        curl_setopt($this->_session, CURLOPT_INFILE, $handle);
        curl_setopt($this->_session, CURLOPT_INFILESIZE, filesize($localPath));
        curl_setopt($this->_session, CURLOPT_URL, $remoteUrl);

        curl_exec($this->_session);
        fclose($handle);
        return new SpicaFtpClientResponse($this->_session);
    }

    /**
     * Uploads a file through FTP using instance-wide FTP access configuration.
     *
     * @param string $localPath Local file path
     * @param string $remoteDir Remote directory path
     * @param string $host E.x: ftp.domain.com
     * @param string $ftpUsername
     * @param string $ftpPassword
     * @return SpicaFtpClientResponse
     */
    public function move($localPath, $remoteDir)
    {      
        $remoteUrl = 'ftp://' . $this->_user . ':' . $this->_pass . '@' . $this->_host . '/' .$remoteDir . '/' . basename($localPath);
        $handle = fopen($localPath, 'r');

        curl_setopt($this->_session, CURLOPT_UPLOAD, 1);
        curl_setopt($this->_session, CURLOPT_INFILE, $handle);
        curl_setopt($this->_session, CURLOPT_INFILESIZE, filesize($localPath));
        curl_setopt($this->_session, CURLOPT_URL, $remoteUrl);

        curl_exec($this->_session);
        fclose($handle);
        return new SpicaFtpClientResponse($this->_session);
    }

    /**
     * Closes FTP session.
     */
    public function close()
    {
        curl_close($this->_session);
    }
}

/**
 * This class provides set of methods that supports FTP operations.
 *
 * @category   spica
 * @package    core
 * @subpackage net
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      March 19, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: FtpClient.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFtpClientResponse
{
    /**
     * @see #getCode()
     */
    const SUCCESS = 0;
    /**
     * Curl executed session.
     *
     * @var resource of type (curl)
     */
    protected $_session;

    /**
     * Constructs an object of <code>SpicaFtpClientResponse</code>.
     *
     * @param resource $session Resource of type (curl) which represents Curl executed session handle
     */
    public function __construct($handle)
    {
        $this->_session = $handle;
    }

    /**
     * Gets response code.
     *
     * @return int 0 means successful
     */
    public function getCode()
    {
        return curl_errno($this->_session);
    }
}

?>