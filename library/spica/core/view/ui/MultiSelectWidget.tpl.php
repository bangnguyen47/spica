        <table border="0">
          <tr>
            <td>
            <?php if (isset($this->leftSelectTitle)): ?>
            <label for="leftTitle"><?php echo $this->leftSelectTitle ?></label><br />
            <?php endif; ?>
            <select name="<?php echo $this->leftSelectName ?>" id="select_left" multiple="multiple" size="15">
            <?php echo spica_html_options($this->leftList, $this->selected); ?>
            </select>
            </td>

            <td valign="middle" align="center">
              <input type="button" id="options_left" value="&lt;-" /><br /><br />
              <input type="button" id="options_right" value="-&gt;" /><br /><br />
              <input type="button" id="options_left_all" value="&lt;&lt;--" /><br /><br />
              <input type="button" id="options_right_all" value="--&gt;&gt;" /><br /><br />
            </td>

            <td>
            <?php if (isset($this->rightSelectTitle)): ?>
            <label for="rightTitle"><?php echo $this->rightSelectTitle ?></label><br />
            <?php endif; ?>

            <select name="<?php echo $this->rightSelectName; ?>" id="select_right" multiple="multiple" size="15">
            <?php echo spica_html_options($this->rightList, $this->selected); ?>
            </select>
            </td>
          </tr>
        </table>
        <?php echo $this->_registerClientScript(); ?>
        <script type="text/javascript"><!--
        $(function() {
          $("#select_left").multiSelect("#select_right", {trigger: "#options_right"});
          $("#select_right").multiSelect("#select_left", {trigger: "#options_left"});
          $("#select_left").multiSelect("#select_right", {allTrigger:"#options_right_all"});
          $("#select_right").multiSelect("#select_left", {allTrigger:"#options_left_all"});
        });
        // --></script>