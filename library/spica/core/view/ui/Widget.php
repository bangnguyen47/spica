<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

// namespace spica\core\view\widget;

/**
 * A SpicaWidget is a software module which is used to render
 * part of a GUI that allows the user to interface with the web application.
 *
 * Widgets display information and invite the user to act in a number of ways.
 * Typical widgets include buttons, dialog boxes, pop-up windows, pull-down menus,
 * icons, scroll bars, resizable window edges, progress indicators, selection boxes,
 * windows, tear-off menus, menu bars, toggle switches and forms.
 *
 * A common example of a widget that most of us run across almost every day are
 * those Google advertisements. A widget can be anything from a voting poll to
 * a weather forecast to a list of current headlines to a crossword puzzle.
 * You can utilize them in your blog to provide an interactive experience for
 * your readers, or you can place them on your personalized start page to get at
 * information you want to see on a regular basis.
 *
 * @category   spica
 * @package    core
 * @subpackage view
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 01, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Widget.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
abstract class SpicaWidget implements SpicaTemplateRenderableAsBlock
{
    /**
     * List of CSS files.
     *
     * @var array
     */
    protected $_css = array();

    /**
     * List of JavaScript files.
     *
     * @var array
     */
    protected $_script = array();

    /**
     * HTTP path to CSS base directory.
     *
     * @var string
     */
    protected $_cssPath = 'public/widget';

    /**
     * HTTP path to JavaScript base directory.
     *
     * @var string
     */
    protected $_scriptPath = 'public/widget';

    /**
     * Absolute path to template file.
     *
     * @var string
     */
    protected $_templateFile;

    /**
     * Constructs an object of <code>SpicaWidget</code>.
     *
     * @param string $templatePath Absolute template file path
     */
    public function __construct($templatePath)
    {
        $this->_templateFile = $templatePath;
        $this->initialize();
    }

    /**
     * Adds a CSS file for use in this widget.
     *
     * @param  string|array $name
     * @return SpicaWidget
     */
    public function css($name)
    {
        $this->_css += (array) $name;
        return $this;
    }

    /**
     * Adds a JavaScript file for use in this widget.
     *
     * @param  string|array $name
     * @return SpicaWidget
     */
    public function script($name)
    {
        $this->_script += (array) $name;
        return $this;
    }

    /**
     * Gets CSS directory.
     *
     * @return string
     */
    public function getScriptPath()
    {
        return $this->_scriptPath;
    }

    /**
     * Gets JavaScript directory.
     *
     * @return string
     */
    public function getCssPath()
    {
        return $this->_cssPath;
    }

    /**
     * Gets JavaScript directory.
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->_templateFile;
    }

    /**
     * A callback method that enables sub-classes intercept the widget building
     * process before its template content is retured to caller.
     * Sub-classes may override this method and perform custom processing.
     *
     * This method is executed before {SpicaWidget#output()}
     * @see SpicaWidget::__construct()
     */
    public function initialize()
    {
        throw new BadMethodCallException('The method named initialize() must be overridden in '.get_class($this).'. ');
    }

    /**
     * Evaluates and returns static content output to the caller
     * but not show the content up to the browser.
     *
     * @see    SpicaTemplate::fetchWidget()
     * @param  array $params Template variable parameters
     * @return string
     */
    public function output($params = array())
    {
        ob_start();
        include $this->_templateFile;
        return ob_get_clean();
    }
}

?>