<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * The DatePickerWidget widget implements a form field for entering a date, which opens a JavaScript driven date selector.
 *
 * namespace spica\core\view\ui\DatePickerWidget;
 *
 * @see        http://jqueryui.com/demos/datepicker/show-week.html
 * 
 * @category   spica
 * @package    core
 * @subpackage view\ui
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 29, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DatePickerWidget.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class DatePickerWidget extends SpicaWidget
{
    /**
     * Callback method on constructing <code>SpicaDatePickerWidget</code>
     */
    public function initialize()
    {
        $theme = SpicaContext::getTheme();
        $injector = SpicaHtmlInjector::factory();
        $injector->addEndJsLink('public/spica/js/jquery-1.4.2.min.js');
        $injector->addEndJsLink('public/widget/shared/jquery-ui-1.8.1/ui/jquery.ui.core.min.js');
        $injector->addEndJsLink('public/widget/shared/jquery-ui-1.8.1/ui/jquery.ui.widget.min.js');
        $injector->addEndJsLink('public/widget/shared/jquery-ui-1.8.1/ui/jquery.ui.datepicker.min.js');
        $injector->addEndJs('
        jQuery(function() {
		    jQuery("#datepicker").datepicker({showWeek: true, firstDay: 1});
	    });');
        $injector->addCssLink('public/widget/shared/jquery-ui-1.8.1/ui/themes/base/jquery-ui.css');
        $injector->addCssLink('public/widget/shared/jquery-ui-1.8.1/ui/themes/base/jquery-ui.css');
        $injector->addCssLink('public/widget/shared/jquery-ui-1.8.1/ui/themes/base/jquery.ui.base.css');
        $injector->addCssLink('public/widget/shared/jquery-ui-1.8.1/ui/themes/base/jquery.ui.datepicker.css');
        $theme->addPageOutputFilter($injector);
    }
}

?>