<?php

/*
 * Copyright (C) 2006-2009 Pham Cong Dinh
 *
 * This file is part of Pone.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Compiled MySQL statement
 */
require_once 'library/spica/core/datasource/db/oracle/PreparedStatement.php';

/**
 * Callable interface
 */
require_once 'library/spica/core/datasource/db/CallableStatement.php';

/**
 * This class provides services to execute SQL stored procedures
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\db\oracle
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      October 19, 2008
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CallableStatement.php 1603 2009-12-26 19:55:19Z pcdinh $
 */
class SpicaOracleCallableStatement extends SpicaOraclePreparedStatement
{
	/**
	 * The SQL query
	 *
	 * @var string
	 */
	protected $_lastQuery;

	protected $_outParameters = array();

	/**
	 * Class constructor
	 *
	 * @param $dbConn SpicaOracleConnection
	 * @param string  $sql
	 */
	public function __construct($dbConn, $sql)
	{
		if (true === $dbConn->isClosed())
		{
			throw new SpicaDatabaseException('The connection to database server is closed. ', null, null, null);
		}

		$this->_dbConn    = $dbConn;
		$this->_lastQuery = $sql;
	}

	/**
	 * Enter description here...
	 *
	 * @param string $sql A stored procedure call, something like this 'CALL Proc(%s, %s, %s)'
	 */
	public function execute()
	{
		$sql      = vsprintf($query, $this->_params);
		$conn     = $this->_dbConn->getNativeConnection();
		// @see http://bugs.php.net/bug.php?id=30645
		$success  = mysqli_real_query($conn, $sql);

		if (false === $success)
		{
			throw new SpicaDatabaseException('Unable to execute the stored procedure. ', mysqli_error($conn), mysqli_sqlstate($conn), mysqli_errno($conn));
		}

		// Returns a buffered result object or FALSE if an error occurred or the SP does not return anything (for example INSERT statements)
		// If mysqli_field_count() returns a non-zero value, the statement should have produced a non-empty result set
		$result   = mysqli_store_result($conn);

		if (mysqli_errno($conn) > 0)
		{
			throw new SpicaDatabaseException('Unable to retrieve buffered result after executing the stored procedure. ', mysqli_error($conn), mysqli_sqlstate($conn), mysqli_errno($conn));
		}

		$resultSet = array();

		do
		{
			while ($row = mysqli_fetch_assoc($result))
			{
				$resultSet[] = $row;
			}

			mysqli_free_result($result);

		} while (mysqli_next_result($link));

		if ($this->_callingStoredFunction)
		{
			return $resultSet;
		}

		// Functions can't return results
		return false;
	}

	/**
	 * Enter description here...
	 *
	 * @return array An array of integers
	 */
	public function executeBatch()
	{
		if ($this->hasOutputParams)
		{
			throw new SpicaDatabaseException("Can't call executeBatch() on CallableStatement with OUTPUT parameters", SQLError.SQL_STATE_ILLEGAL_ARGUMENT);
		}

		return parent::executeBatch();
	}

	/**
	 *
	 */
	public function registerOutParameter($name)
	{
		$this->_outParameters[] = $name;
	}

	/**
	 * Issues a second query to retrieve all output parameters.
	 *
	 * @throws SQLException if an error occurs
	 */
	private function retrieveOutParams()
	{
		// Cac bien lay t SELECT @@TEN_BIEN
	}
}

?>