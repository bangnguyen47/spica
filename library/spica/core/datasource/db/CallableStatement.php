<?php

/*
 * Copyright (C) 2008-2009 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * The interface used to execute SQL stored procedures.
 *
 * namespace spica\core\datasource\db\CallableStatement
 *
 * <p>
 * A <code>SpicaCallableStatement</code> can return one
 * {@link SpicaResultSet} object or multiple
 * <code>SpicaResultSet</code> objects. Multiple
 * <code>SpicaResultSet</code> objects are handled using operations
 * inherited from {@link SpicaStatement}.
 * <p>
 * For maximum portability, a call's <code>SpicaResultSet</code> objects and
 * update counts should be processed prior to getting the values of output parameters.
 * <p>
 *
 * @see SpicaConnection#prepareCall
 * @see SpicaResultSet
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\db
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      October 18, 2008
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CallableStatement.php 1505 2009-12-21 18:42:59Z pcdinh $
 */
interface SpicaCallableStatement
{
    /**
     * Gets result sets returned by SpicaCallableStatement#execute()
     *
     * @return array null or array of SpicaResultSet
     */
    public function getResultSets();
}

?>