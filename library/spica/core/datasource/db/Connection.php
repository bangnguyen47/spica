<?php

/*
 * Copyright (C) 2008-2009 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * A connection (session) with a specific database
 *
 * Within the context of a Connection, SQL statements are executed and results are returned
 *
 * A Connection's database is able to provide information describing its tables,
 * its supported SQL grammar, its stored procedures, the capabilities of this connection,
 * and so on
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\db
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      October 18, 2008
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Connection.php 1463 2009-12-09 18:12:03Z pcdinh $
 */
interface SpicaConnection
{
    // Constants that define transaction isolation levels.
    // [We don't have any code using these yet, so there's no need
    // to initialize these values at this point.]
    // const TRANSACTION_NONE = 0;
    // const TRANSACTION_READ_UNCOMMITTED = 1;
    // const TRANSACTION_READ_COMMITTED = 2;
    // const TRANSACTION_REPEATABLE_READ = 3;
    // const TRANSACTION_SERIALIZABLE = 4;

    /**
     * Gets the PHP native resource for the database connection/link
     *
     * @return resource
     */
    public function getNativeConnection();

    /**
     * Gets database server client charset endcoding
     *
     * @return string
     */
    public function getClientCharset();

    /**
     * Tests to see if the connection is in Read Only Mode
     *
     * Note that we cannot really put the database in read only mode,
     * but we pretend we can by returning the value of the readOnly flag
     *
     * @return true if the connection is read only. Default to <code>false</code>
     */
    public function isReadOnly();

    /**
     * Puts this connection in read-only mode as a hint to enable database optimizations
     *
     * Note: This method cannot be called while in the middle of a transaction
     *
     * @param bool $readOnly
     */
    public function setReadOnly($readOnly);

    /**
     * Gets the SQL identifier case mode
     *
     * Value can be: SpicaResultSet::IDENTIFIER_LOWERCASE
     *               SpicaResultSet::IDENTIFIER_UPPERCASE
     *               SpicaResultSet::IDENTIFIER_MIXEDCASE
     * @return int
     */
    public function getSQLIdentifierCase();

    /**
     * Sets the SQL identifier case mode.
     *
     * @param int $case
     *        + SpicaResultSet::IDENTIFIER_LOWERCASE
     *        + SpicaResultSet::IDENTIFIER_UPPERCASE
     *        + SpicaResultSet::IDENTIFIER_MIXEDCASE
     */
    public function setSQLIdentifierCase($case);

    /**
     * Escapes special characters in a string for use in a SQL statement
     *
     * This method prepends backslashes to the following characters: \x00, \n, \r, \, ', " and \x1a
     *
     * @param  string $variable
     * @return string
     */
    public function quote($variable);

    /**
     * Registers a SpicaStatement instance as open.
     *
     * @param SpicaStatement $stmt the Statement instance to remove
     */
    public function registerStatement($stmt);

    /**
     * Removes the given statement from the list of open statements
     *
     * @param SpicaStatement $stmt the Statement instance to remove
     */
    public function unregisterStatement($stmt);

    /**
     * Prepares a query for multiple execution with execute().
     *
     * With some database backends, this is emulated.
     * prepare() requires a generic query as string like
     * "INSERT INTO numbers VALUES(?,?,?)". The ? are placeholders.
     *
     * IMPORTANT:  All occurrences of the placeholder (?) will be assumed
     * to be a parameter.  Therefore be sure not to have ? anywhere else in
     * the query.
     *
     * So, ... DO NOT MIX WILDCARDS WITH ALREADY-PREPARED QUERIES
     *
     * INCORRECT:
     *     SELECT * FROM mytable WHERE id = ? AND title = 'Where are you?' and body LIKE ?
     *
     * CORRECT:
     *    SELECT * FROM mytable WHERE id = ? AND title = ? and body LIKE ?
     *
     * @throws SpicaDatabaseException if a database access error occurs
     * @see    SpicaPreparedStatement::execute()
     * @param  string $sql The query to prepare.
     * @return SpicaPreparedStatement
     */
    public function prepareStatement($sql);

    /**
     * Creates a new empty SpicaStatement
     *
     * @param  int $resultSetType Result set type which defaults to TYPE_FORWARD_ONLY
     * @param  int $identifierCase Field name case sensitiveness defaults to IDENTIFIER_LOWERCASE
     * @return SpicaStatement
     */
    public function createStatement($identifierCase = SpicaResultSet::IDENTIFIER_LOWERCASE);

    /**
     * Creates a new empty Statement for multiple queries
     *
     * @param  int $resultSetType Result set type which defaults to TYPE_FORWARD_ONLY
     * @param  int $identifierCase Field name case sensitiveness defaults to IDENTIFIER_LOWERCASE
     * @return SpicaMultipleQueryStatement
     */
    public function createMultipleQueryStatement($identifierCase = null);

    /**
     * Creates a SpicaCallableStatement object for calling database stored procedures.
     *
     * @param  string $sql
     * @return SpicaCallableStatement
     */
    public function prepareCall($sql);

    /**
     * Frees the db resources
     */
    public function close();

    /**
     * Returns true if connection is closed
     *
     * @return bool
     */
    public function isClosed();

    /**
     * Gets auto-commit status.
     *
     * @return bool
     */
    public function getAutoCommit();

    /**
     * Enables/disables automatic commits.
     *
     * Pushes SQLWarning onto $warnings stack if the autocommit value is being changed mid-transaction
     *
     * This function is overridden by driver classes so that they can perform the
     * necessary begin/end transaction SQL
     *
     * If auto-commit is being set to TRUE, then the current transaction will be committed immediately
     *
     * @param  bool $bit New value for auto commit.
     */
    public function setAutoCommit($autoCommit);

    /**
     * Attempts to change the transaction isolation level for this Connection object to the one given
     *
     * @param int $isolationLevel
     */
    public function setTransactionIsolation($isolationLevel);

    /**
     * Begins a transaction (if supported).
     */
    public function beginTransaction();

    /**
     * Commits statements in a transaction.
     */
    public function commit();

    /**
     * Rollbacks changes in a transaction.
     */
    public function rollback();

    /**
     * Explicitly release the given savepoint.
     *
     * Note that most transaction managers will automatically release savepoints at transaction completion.
     * Implementations should fail as silently as possible if proper resource cleanup
     * will still happen at transaction completion.
     *
     * @param string $savepoint the savepoint to release
     */
    public function releaseSavepoint($savepoint);

    /**
     * Tests if the current connection is in a transaction
     *
     * @return bool
     */
    public function isInTransaction();
}

?>