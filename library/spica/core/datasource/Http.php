<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * This class provides methods to create and manage HTTP connections to retrieve
 * data from remote server.
 *
 * @category   spica
 * @package    core
 * @subpackage datasource
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 21, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Http.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaHttpDataSource
{
    /**
     * Sets of HTTP connections.
     *
     * @var array
     */
    protected static $_con;

    /**
     * Gets HTTP datasource configuration by an alias name.
     *
     * @throws InvalidArgumentException if alias name can not be found.
     * @param  string $alias
     * @param  bool   $new Creates new one or reuse already created one
     * @return Pone_Database_Connection
     */
    public static function getConnection($alias, $new = false)
    {

    }

    /**
     * Gets HTTP datasource configuration by a set of configuration entries.
     * This method works the same as SpicaDatabaseManager#getConnection()
     *
     * @throws InvalidArgumentException if $config is not an array
     * @param  array $config
     * @param  bool  $new Creates new one or reuse already created one
     * @return object
     */
    public static function createConnection($config, $new = false)
    {

    }

    /**
     * Looks up the configured HTTP datasource alias name by specifying its content.
     *
     * @param  array $config
     * @return string|null
     */
    public static function getAlias($config)
    {

    }
}

?>