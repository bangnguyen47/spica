<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Represents Redis's SADD command which adds a string value to the <code>SET</code> stored at
 * the provided key.
 *
 * This command results in an integer reply:
 *
 * + 1 if the new element was added
 * + 0 if the element was already a member of the set
 *
 * namespace spica\core\datasource\redis\cmd\SADD;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 29, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSADD implements SpicaRedisCommand
{
    /**
     * The provided key.
     *
     * @var string
     */
    protected $_key;

    /**
     * The provided value.
     *
     * @var string
     */
    protected $_val;

    /**
     * The provided value length.
     *
     * @var int
     */
    protected $_len;

    /**
     * Constructs an object of <code>SpicaRedisSADD</code> to add a string
     * value to the <code>SET</code> stored at the provided key.
     *
     * @throws InvalidArgumentException
     * @param  string $key   The set name
     * @param  string $value The string value
     */
    public function __construct($key, $value)
    {
        if (true === empty($value))
        {
            throw new InvalidArgumentException('Parameter $value can not be empty.');
        }

        $this->_key = $key;
        $this->_val = $value;
        $this->_len = strlen($value);
    }

    /**
     * Sets key.
     *
     * @param string $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Sets value.
     *
     * @throws InvalidArgumentException
     * @param string $val The string value
     */
    public function setValue($val)
    {
        if (true === empty($val))
        {
            throw new InvalidArgumentException('Parameter $value can not be empty.');
        }

        $this->_val = $val;
        $this->_len = strlen($value);
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SADD '.$this->_key.' '.$this->_len.SpicaRedisCommand::NL.$this->_val.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's SPOP command which returns and removes a random string value from
 * the <code>SET</code> stored at the provided key.
 *
 * namespace spica\core\datasource\redis\cmd\SPOP;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 29, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSPOP implements SpicaRedisCommand
{
    /**
     * The provided key.
     *
     * @var string
     */
    protected $_key;

    /**
     * Constructs an object of <code>SpicaRedisSPOP</code> to add a string
     * value to the <code>SET</code> stored at the provided key.
     *
     * @param string $key The set name
     */
    public function __construct($key)
    {
        $this->_key = $key;
    }

    /**
     * Sets key.
     *
     * @param string $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SPOP '.$this->_key.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's SCARD command which returns the number of elements in
 * the <code>SET</code> stored at the provided key.
 *
 * namespace spica\core\datasource\redis\cmd\SCARD;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 29, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSCARD extends SpicaRedisSPOP
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SCARD '.$this->_key.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's SMEMBERS command which returns all the members in
 * the <code>SET</code> stored at the provided key.
 *
 * namespace spica\core\datasource\redis\cmd\SMEMBERS;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSMEMBERS extends SpicaRedisSPOP
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SMEMBERS '.$this->_key.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's SISMEMBER command which tests if the specified value is a member of
 * the <code>SET</code> stored at the provided key.
 *
 * namespace spica\core\datasource\redis\cmd\SISMEMBER;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSISMEMBER extends SpicaRedisSADD
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SISMEMBER '.$this->_key.' '.$this->_len.SpicaRedisCommand::NL.$this->_val.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's SINTER command which returns the intersection between the
 * the <code>SET</code>s stored at the provided keys.
 *
 * namespace spica\core\datasource\redis\cmd\SINTER;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSINTER implements SpicaRedisCommand
{
    /**
     * The provided key.
     *
     * @var string
     */
    protected $_key;

    /**
     * Constructs an object of <code>SpicaRedisSINTER</code> to return the intersection between the
     * the <code>SET</code>s stored at the provided keys.
     *
     * @param array $keys The set names
     */
    public function __construct($keys)
    {
        $this->_key = $keys;
    }

    /**
     * Sets keys.
     *
     * @param string $keys The set names
     */
    public function setKey($keys)
    {
        $this->_key = $keys;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SINTER '.implode(' ', $this->_key).SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's SDIFF command which returns the difference between the
 * the <code>SET</code>s stored at the provided keys.
 *
 * namespace spica\core\datasource\redis\cmd\SDIFF;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSDIFF extends SpicaRedisSINTER
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SDIFF '.implode(' ', $this->_key).SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's SUNION command which the union between the the <code>SET</code>s stored at the provided keys.
 *
 * namespace spica\core\datasource\redis\cmd\SUNION;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 30, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Set.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSUNION extends SpicaRedisSINTER
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SUNION '.implode(' ', $this->_key).SpicaRedisCommand::NL;
    }
}

?>