<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Represents Redis's GET command.
 *
 * namespace spica\core\datasource\redis\cmd\GET;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 22, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisGET implements SpicaRedisCommand
{
    /**
     * The provided key.
     *
     * @var string
     */
    protected $_key;

    /**
     * Constructs an object of <code>SpicaRedisGET</code> to fetches the value at the provided key.
     *
     * @param string $key
     */
    public function __construct($key)
    {
        $this->_key = $key;
    }

    /**
     * Sets key.
     *
     * @param string $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'GET '.$this->_key.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's SET command.
 *
 * namespace spica\core\datasource\redis\cmd\SET;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 22, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSET implements SpicaRedisCommand
{
    /**
     * The provided key.
     *
     * @var string
     */
    protected $_key;

    /**
     * The provided value.
     *
     * @var string
     */
    protected $_val;

    /**
     * The provided value length.
     *
     * @var int
     */
    protected $_len;

    /**
     * Constructs an object of <code>SpicaRedisSET</code> to fetches the value at the provided key.
     *
     * @param string $key
     * @param string $value
     */
    public function __construct($key, $value)
    {
        $this->_key = $key;
        $this->_val = $value;
        $this->_len = strlen($value);
    }

    /**
     * Sets key.
     *
     * @param string $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Sets value.
     *
     * @param string $val
     */
    public function setValue($val)
    {
        $this->_val = $val;
        $this->_len = strlen($value);
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SET '.$this->_key.' '.$this->_len.SpicaRedisCommand::NL.$this->_val.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's MGET command.
 *
 * namespace spica\core\datasource\redis\cmd\MGET;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisMGET implements SpicaRedisCommand
{
    /**
     * The provided keys.
     *
     * @var array
     */
    protected $_keys;

    /**
     * Constructs an object of <code>SpicaRedisMGET</code> to fetches the values at the provided keys.
     *
     * @param array $keys
     */
    public function __construct($keys)
    {
        if (false === is_array($keys))
        {
            $this->_keys = (array) $keys;
        }
        else
        {
            $this->_keys = $keys;
        }
    }

    /**
     * Sets keys.
     *
     * @param array $keys
     */
    public function setKeys($keys)
    {
        $this->_keys = $keys;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'MGET '.implode(' ', $this->_keys).SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's MSET command.
 *
 * namespace spica\core\datasource\redis\cmd\MSET;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisMSET implements SpicaRedisCommand
{
    /**
     * The provided key set.
     *
     * @var array
     */
    protected $_keys;

    /**
     * The provided key set.
     *
     * @var array
     */
    protected $_values;

    /**
     * Redis command.
     *
     * @var string
     */
    private $_command;

    /**
     * Constructs an object of <code>SpicaRedisMSET</code> to set values at several provided keys.
     *
     * @param array $sets The mapping between key and value
     */
    public function __construct($sets)
    {
        $this->_createCommand($sets);
    }

    /**
     * Sets key-value sets.
     *
     * @param array $sets
     */
    public function setKeyValueSets($sets)
    {
        $this->_createCommand($sets);
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return $this->_command;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }

    /**
     * Creates MSET command.
     *
     * @param array  $sets
     * @param string $cmd
     */
    protected function _createCommand(&$sets, $cmd = 'MSET')
    {
        $keys   = array_keys($sets);
        $values = array_values($sets);
        $cmd   .= ' ';

        for ($i = 0, $len = count($keys) - 1; $i < $len; $i++)
        {
            $cmd .= $keys[$i].' '.$values[$i].' ';
        }

        $this->_command = $cmd.$keys[$i].' '.strlen($values[$i]).SpicaRedisCommand::NL.$values[$i].SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's MSETNX command which set provided keys with the respective
 * values but unlike MSET, MSETNX will not perform any operation at all even
 * if just a single key already exists.
 *
 * namespace spica\core\datasource\redis\cmd\MSETNX;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisMSETNX extends SpicaRedisMSET
{
    /**
     * Creates MSETNX command.
     *
     * @param array  $sets
     * @param string $cmd
     */
    protected function _createCommand(&$sets, $cmd = 'MSETNX')
    {
        parent::_createCommand($sets, $cmd);
    }
}

/**
 * Represents Redis's SETNX command.
 *
 * This command results in an integer reply
 *
 * + 1 if the key was set
 * + 0 if the key was not set
 *
 * namespace spica\core\datasource\redis\cmd\SETNX;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSETNX extends SpicaRedisSET
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SETNX '.$this->_key.' '.$this->_len.SpicaRedisCommand::NL.$this->_val.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's GETSET command that sets a string at the provided key
 * and returns the old value of the key.
 *
 * namespace spica\core\datasource\redis\cmd\GETSET;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 23, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisGETSET extends SpicaRedisSET
{
    /**
     * Constructs an object of <code>SpicaRedisGETSET</code>.
     *
     * @param string $key
     * @param string $value
     * @param int    $length Value string length which defaults to null
     */
    public function __construct($key, $value, $length = null)
    {
        $this->_key = $key;
        $this->_val = $value;

        if (null === $length)
        {
            $this->_len = strlen($value);
        }
        else
        {
            $this->_len = $length;
        }
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'GETSET '.$this->_key.' '.$this->_len.SpicaRedisCommand::NL.$this->_val.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's DEL command.
 *
 * namespace spica\core\datasource\redis\cmd\DEL;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 23, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisDEL implements SpicaRedisCommand
{
    /**
     * The provided key or keys.
     *
     * @var string|array
     */
    protected $_key;

    /**
     * Constructs an object of <code>SpicaRedisDEL</code>.
     *
     * @param string|array $key Key name or names
     */
    public function __construct($key)
    {
        $this->_key = $key;
    }

    /**
     * Sets key or keys.
     *
     * @param string|array $key Key name or names
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        if (true === is_string($this->_key))
        {
            return 'DEL '.$this->_key.SpicaRedisCommand::NL;
        }

        return 'DEL '.implode(' ', $this->_key).SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's EXISTS command that help check a key for existence.
 *
 * This command will result in an integer reply (:0 or :1)
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisEXISTS extends SpicaRedisGET
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'EXISTS '.$this->_key.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's INCRBY command that
 * helps increment the integer value of the provided key by integer.
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisINCRBY implements SpicaRedisCommand
{
    /**
     * The provided key.
     *
     * @var string
     */
    protected $_key;

    /**
     * The step to increase.
     *
     * @var string
     */
    protected $_step;

    /**
     * Constructs an object of <code>SpicaRedisINCRBY</code>.
     *
     * @param string $key
     * @param int    $step
     */
    public function __construct($key, $step)
    {
        $this->_key  = $key;
        $this->_step = $step;
    }

    /**
     * Sets key.
     *
     * @param string $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Sets incremental step.
     *
     * @param int $step
     */
    public function setStep($step)
    {
        $this->_step = $step;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'INCRBY '.$this->_key.' '.$this->_step.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's DECRBY command that
 * helps decrement the integer value of the provided key by integer.
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisDECRBY extends SpicaRedisINCRBY
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'DECRBY '.$this->_key.' '.$this->_step.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's INCR command that
 * helps increment the integer value of the provided key by 1.
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisINCR extends SpicaRedisGET
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'INCR '.$this->_key.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's DECR command that
 * helps decrement the integer value of the provided key by 1.
 *
 * This command results in an integer reply, that indicates the new value of key
 * after the decrement.
 *
 * namespace spica\core\datasource\redis\cmd\DECR;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisDECR extends SpicaRedisGET
{
    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'DECR '.$this->_key.SpicaRedisCommand::NL;
    }
}

/**
 * Represents Redis's FLUSHDB command.
 *
 * namespace spica\core\datasource\redis\cmd\FLUSHDB;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisFLUSHDB extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisFLUSHDB</code> which helps send <code>FLUSHDB</code> command
     * to remove all the keys from the current databases.
     */
    public function __construct()
    {
        $this->_cmd = 'FLUSHDB';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command FLUSHDB can not be changed.');
    }
}

/**
 * Represents Redis's FLUSHALL command.
 *
 * namespace spica\core\datasource\redis\cmd\FLUSHALL;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisFLUSHALL extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisFLUSHALL</code> which helps send <code>FLUSHALL</code> command
     * to remove all the keys from all the databases.
     */
    public function __construct()
    {
        $this->_cmd = 'FLUSHALL';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command FLUSHALL can not be changed.');
    }
}

/**
 * Represents Redis's SELECT command.
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSELECT implements SpicaRedisCommand
{
    /**
     * The provided database index.
     *
     * @var int
     */
    protected $_db;

    /**
     * Constructs an object of <code>SpicaRedisSELECT</code>.
     *
     * @param int $db Database index
     */
    public function __construct($db)
    {
        $this->_db = $db;
    }

    /**
     * Sets database index.
     *
     * @param string $db
     */
    public function setDatabase($db)
    {
        $this->_db = $db;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'SELECT '.$this->_db.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return true;
    }
}

/**
 * Represents Redis's DBSIZE command that returns number of keys in the
 * currently selected database.
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisDBSIZE implements SpicaRedisCommand
{
    /**
     * Constructs an object of <code>SpicaRedisDBSIZE</code>.
     */
    public function __construct() {}

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'DBSIZE'.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return false;
    }
}

/**
 * Represents Redis's PING command.
 *
 * namespace spica\core\datasource\redis\cmd\PING;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisPING extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisPING</code>.
     */
    public function __construct()
    {
        $this->_cmd = 'PING';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command PING can not be changed.');
    }
}

/**
 * Represents Redis's EXPIRE command.
 *
 * namespace spica\core\datasource\redis\cmd\EXPIRE;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Scalar.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisEXPIRE implements SpicaRedisCommand
{
    /**
     * The provided key.
     *
     * @var string
     */
    protected $_key;

    /**
     * The provided value.
     *
     * @var string
     */
    protected $_second;

    /**
     * Constructs an object of <code>SpicaRedisEXPIRE</code> that delete a key after its timeout is reached.
     *
     * @param $key    The provided key
     * @param $second The second that the key is expired after that
     */
    public function __construct($key, $second)
    {
        $this->_key    = $key;
        $this->_second = $second;
    }

    /**
     * Sets key.
     *
     * @param $key    The provided key
     * @param $second The second that the key is expired after that
     */
    public function setKey($key, $second)
    {
        $this->_key    = $key;
        $this->_second = $second;
    }

    /**
     * Gets the command string.
     *
     * @return string
     */
    public function toString()
    {
        return 'EXPIRE '.$this->_key.' '.$this->_second.SpicaRedisCommand::NL;
    }

    /**
     * Is this command subject to hashing?
     *
     * @return bool
     */
    public function supportsHash()
    {
        return false;
    }
}

?>