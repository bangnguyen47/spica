<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Represents Redis's INFO command that provides information and statistics about the server.
 *
 * namespace spica\core\datasource\redis\cmd\INFO;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Control.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisINFO extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisINFO</code>.
     */
    public function __construct()
    {
        $this->_cmd = 'INFO';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command INFO can not be changed.');
    }
}

/**
 * Represents Redis's MONITOR command that dumps all the received requests in real time.
 *
 * namespace spica\core\datasource\redis\cmd\MONITOR;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Control.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisMONITOR extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisMONITOR</code>.
     */
    public function __construct()
    {
        $this->_cmd = 'MONITOR';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command MONITOR can not be changed.');
    }
}

/**
 * Represents Redis's SLAVEOF command that changes the replication settings.
 *
 * namespace spica\core\datasource\redis\cmd\SLAVEOF;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Control.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisSLAVEOF extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisSLAVEOF</code>.
     */
    public function __construct()
    {
        $this->_cmd = 'SLAVEOF';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command SLAVEOF can not be changed.');
    }
}

/**
 * Represents Redis's QUIT command that changes the replication settings.
 *
 * namespace spica\core\datasource\redis\cmd\QUIT;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Control.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisQUIT extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisQUIT</code>.
     */
    public function __construct()
    {
        $this->_cmd = 'QUIT';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command QUIT can not be changed.');
    }
}

/**
 * Represents Redis's AUTH command that changes the replication settings.
 *
 * namespace spica\core\datasource\redis\cmd\AUTH;
 *
 * @category   spica
 * @package    core
 * @subpackage datasource\redis\cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Control.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaRedisAUTH extends SpicaRedisNoParamCommand
{
    /**
     * Constructs an object of <code>SpicaRedisAUTH</code>.
     */
    public function __construct()
    {
        $this->_cmd = 'AUTH';
    }

    /**
     * @throws Exception
     */
    public function setCommand($cmd)
    {
        throw new Exception('The command AUTH can not be changed.');
    }
}

?>