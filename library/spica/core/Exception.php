<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Spica core exception classes.
 */
// namespace spica\core\exception;

/**
 * Exception is thrown when theme settings are not defined in the current application context.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.2
 * @since      March 16, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */

/**
 * @see SpicaContext::getTheme()
 */
class SpicaUnConfiguredThemeException extends SpicaException {}

/**
 * Exception is thrown when the full qualified class path that is resolved to serve
 * the request can not be found.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      March 15, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaUnresolvableRequestException extends SpicaException {}

/**
 * Exception is thrown when the controller class that is resolved to serve
 * the request can not be found.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      March 15, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaNotFoundControllerException extends SpicaException {}

/**
 * Exception is thrown when the class cannot be found in the file with same name.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 21, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaClassNotFoundException extends SpicaException {}

/**
 * Exception is thrown when route path for the alternative route can not be found.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      March 15, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaInfiniteRoutingException extends SpicaException {}

/**
 * Exception is thrown when a data object (article, post, record, page ...) cannot be found.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 05, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaNotFoundException extends SpicaException {}

/**
 * Signals that an I/O exception of some sort has occurred.
 * This class is the general class of exceptions produced by failed or
 * interrupted I/O operations.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 31, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaIOException extends SpicaException {}

/**
 * Exception is thrown when a file can not be found.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.2
 * @since      March 16, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFileNotFoundException extends SpicaIOException {}

/**
 * Thrown if invalid parameters are passed to a Spica component, usually a Controller.
 *
 * @category   spica
 * @package    core
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaInvalidParameterException extends SpicaException {}

?>