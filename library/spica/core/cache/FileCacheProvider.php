<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
// namespace spica\core\cache\provider;

/**
 * Provider for caching to disk. When using this provider, you need to specify
 * the cache directory using SpicaFileCacheConfiguration::path.
 *
 * @category   spica
 * @package    core
 * @subpackage cache\provider
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: FileCacheProvider.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaFileCacheProvider implements SpicaCacheProvider
{
    /**
     * Cache option
     *
     * @var array
     */
    protected $_option;

    /**
     * Constructs an object of <code>SpicaFileCacheProvider</code>
     *
     * @param array $option
     */
    public function __construct($option)
    {
        $this->_option = array_merge(array('path' => ''), $option);
    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/cache/SpicaCacheProvider#getOptions()
     */
    public function getOptions()
    {
        return $this->_option;
    }

    /**
     * Sets option value.
     *
     * @param string $name
     * @param mixed $val
     */
    public function setOption($name, $val)
    {
        $this->_option[$name] = $val;
    }

    /**
     * Reads a file from cache. If file is expired, it will be deleted.
     *
     * @throws RuntimeException If file cannot be opened (e.g. insufficient access rights or non-existent file).
     * @param  string $key a unique key identifying the cached value
     * @param  bool   $lockAware
     * @param  int    $waitTime Maximum time to wait until lock is released
     * @return string|false  the value stored in cache, false if the value is not in the cache or expired.
     */
    public function get($key, $lockAware = false, $waitTime = 10)
    {
        $fileName = $this->getPath($key);

        if (false === is_readable($fileName))
        {
            return false;
        }

        $file = new SplFileObject($fileName, 'r');

        // Read on write
        if (0 === $file->getSize())
        {
            usleep(10);
        }

        $data = unserialize($file->fgets());

        if (false === $data)
        {
            // Corrupted cache file
            if (false === $this->isLocked($key))
            {
                unlink($fileName);
            }

            return false;
        }

        // checks if the data was expired
        if (time() > $data[0])
        {
            $file = null;
            return false;
        }

        $file = null;
        return $data[1];
    }

    /**
     * Stores a value identified by a key in cache.
     *
     * @param  string $key  the key identifying the value to be cached
     * @param  string $data the value to be cached
     * @param  int    $ttl  the number of seconds in which the cached value will expire.
     *                0 means never expire.
     * @return bool
     */
    public function set($key, $data, $ttl = null)
    {
        try
        {
            $file = new SplFileObject($this->getPath($key), 'a+');

            switch ($ttl)
            {
                case null:
                    $ttl = time() + $this->_option['ttl'];
                    break;

                case 0:
                    $ttl = 0;
                    break;

                default:
                    $ttl = time() + $ttl;
                    break;
            }

            // Because pointer is put at the end
            $file->ftruncate(0);
            $file->fwrite(serialize(array($ttl, $data)));
            $file = null;
        }
        catch (Exception $e)
        {
            $file = null;
            trigger_error($e->getMessage(), E_USER_ERROR);
            return false;
        }

        return true;
    }

    /**
     * Stores a value identified by a key into cache if the cache does not
     * contain this key or that key is expired.
     *
     * @param  string $key the key identifying the value to be cached
     * @param  string $data the value to be cached
     * @param  int    $ttl the number of seconds in which the cached value will expire.
     *                0 means never expire.
     * @return bool   true if the value is successfully stored into cache, false otherwise
     */
    public function add($key, $data, $ttl = null)
    {
        if (false !== $this->get($key))
        {
            return true;
        }

        try
        {
            $file = new SplFileObject($this->getPath($key), 'a+');

            switch ($ttl)
            {
                case null:
                    $data = serialize(array(time() + $this->_option['ttl'], $data));
                    break;

                case 0:
                    $data = serialize(array(0, $data));
                    break;

                default:
                    $data = serialize(array(time() + $ttl, $data));
                    break;
            }

            $file->fwrite($data);
            $file = null;
        }
        catch (Exception $e)
        {
            // Due to the lack of SplFileObject::close()
            $file = null;
            trigger_error($e->getMessage(), E_USER_ERROR);
            return false;
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/cache/SpicaCacheProvider#remove()
     */
    public function remove($key)
    {
        return unlink($this->getPath($key));
    }

    /**
     * Removes expired data entries.
     * All the cache files in SpicaFileCacheConfiguration::path will be checked
     * for deletion.
     *
     * @return bool
     */
    public function removeExpired()
    {
        // FIXME
        throw new BadMethodCallException('This method is not implemeted yet. ');
    }

    /**
     * Deletes all values from cache.
     * Be careful of performing this operation if the cache is shared by multiple applications.
     *
     * @return bool
     */
    public function flush()
    {
        $iter   = new RecursiveDirectoryIterator($this->getBasePath());
        $failed = 0;

        foreach (new RecursiveIteratorIterator($iter, RecursiveIteratorIterator::CHILD_FIRST) as $file)
        {
            if (true === $file->isDir())
            {
                if (false === rmdir($file->getPathname()))
                {
                    $failed++;
                }
            }
            else
            {
                if (false === unlink($file->getPathname()))
                {
                    $failed++;
                }
            }
        }

        return !$failed;
    }

    /**
     * Checks if a value identified by a key is expired.
     *
     * @throws RuntimeException if file cannot be opened (e.g. insufficient access rights).
     * @param  string $key the key identifying the value to be cached
     * @return bool
     */
    public function isExpired($key)
    {
        $file = new SplFileObject($this->getPath($key), 'r');
        $data = unserialize($file->fgets());
        $file = null;

        // E_NOTICE is issued
        if (false === $data)
        {
            return true;
        }

        return (time() > $data[0]);
    }

    /**
     * Checks if a value identified by a key is cached. In fact the file that contains
     * value is checked for existence but the data is not validated.
     *
     * @param  string $key the key identifying the value to be cached
     * @return bool
     */
    public function isCached($key)
    {
        return file_exists($this->getPath($key));
    }

    /**
     * Acquires a lock on the given $key.
     * If we fail to get the lock -- this means another process is doing it so
     * we wait (block) for a few microseconds while we wait for the cache to
     * be filled or the lock to timeout.
     *
     * If you get a false from this call, you _must_ populate the cache ASAP or
     * indicate that you won't by calling SpicaFileCacheProvider::releaseLock().
     *
     * This technique forces serialisation and so helps deal with thundering
     * herd scenarios where a lot of clients ask the for the same idempotent
     * (and costly) operation.
     *
     * The implementation is based on suggestions in this message
     *
     * @throws SpicaCacheLockException Whenever the lock to a cache entry specified
     *         by a key cannot be acquired because it is acquired by another process
     *         or the data is updated and lock is released so this process should
     *         come back to get the data instead of attempting to acquire lock.
     *         If not thrown, then the lock was acquired.
     * @see    http://marc.theaimsgroup.com/?l=git&m=116562052506776&w=2
     * @param  string $key
     * @param  int $ttl seconds The maximum time that lock can exist
     * @param  int $waitTime Microseconds for attempting to acquire lock.
     *             If $waitTime is 0 then this method returns false immediately
     *             when lock can not be acquired.
     * @return bool
     */
    public function acquireLock($key, $ttl, $waitTime = 1000)
    {
        $path     = $this->getPath($key).'.lock';
        $acquired = $this->_acquireLock($path, $ttl);

        if (true  === $acquired)
        {
            return true;
        }

        if (false === $acquired && 0 === $waitTime)
        {
            // Cache lock can not be acquired - no further attempt anymore
            throw new SpicaCacheLockException();
        }

        $counter  = 0;
        $sleep    = round($waitTime/10);

        while (false === $this->_acquireLock($path, $ttl))
        {
            if ($counter++ > 9)
            {
                break; // no further attempt anymore
            }

            // Wait for next check
            usleep($sleep);
        }

        // Cache lock can not be acquired
        throw new SpicaCacheLockException();
    }

    /**
     * Acquires a lock on the given $path.
     *
     * @throws SpicaCacheLockException when the process or thread executes this method
     *         finds that data is updated so lock acquiring is not needed.
     * @param  string $path
     * @param  int    $ttl seconds The maximum time that lock entry can exist
     * @return bool
     */
    protected function _acquireLock($path, $ttl)
    {
        try
        {
            $existed = file_exists($path);
            $file    = new SplFileObject($path, 'a+');
        }
        catch (Exception $e)
        {
            $file = null;
            trigger_error($e->getMessage(), E_USER_ERROR);
            return false;
        }

        // Another process has just acquired that lock
        if (0 === ($size = $file->getSize()) && true === $existed)
        {
            return false;
        }

        $data = unserialize($file->fgets());
        // Another process is updating on the lock
        // or lock data is corrupted
        // or lock is not expired.
        if (($size > 0 && false === $data) || (isset($data[0]) && $data[1] !== -1 && time() <= $data[0]))
        {
            $file = null;
            return false;
        }

        if ($data[1] === -1 && time() <= $data[0])
        {
            $file = null;
            throw new SpicaCacheLockException('Lock has been released by another process and cache data is updated.');
        }

        // It is the first time lock is acquired
        // or data is expired (buggy clients can cause it)
        // We must prevent a buggy client to reserve the right for update forever
        $data[1] = getmypid(); // PHP process ID that acquires lock
        $data[0] = (0 === $ttl) ? 0 : time() + $ttl;
        $file->ftruncate(0);
        $file->fwrite(serialize($data));
        $file = null;

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/cache/SpicaCacheProvider#releaseLock()
     */
    public function releaseLock($key)
    {
        try
        {
            $file = $this->getPath($key);
            $path = $file.'.lock';

            // Buggy client causes it
            if (true === file_exists($file))
            {
                return unlink($path);
            }

            $file    = new SplFileObject($path, 'a+');
            $data    = unserialize($file->fgets());
            $data[0] = $this->_option['lock_expr_time'];
            $data[1] = -1;
            $file->ftruncate(0);
            $file->fwrite(serialize($data));
            $file    = null;
        }
        catch (Exception $e)
        {
            $file = null;
            trigger_error($e->getMessage(), E_USER_ERROR);
            return false;
        }

        return true;
    }

    /**
     * Removes lock for a provided key.
     *
     * @return bool
     */
    public function removeLock($key)
    {
        $file = $this->getPath($key).'.lock';

        if (false === is_file($file))
        {
            return true;
        }

        $fp = fopen($file, 'a');
        
        if (false !== $fp)
        {
            fclose($fp);
            unlink($file);
            return true;
        }

        return false;
    }

    /**
     * Checks if the key is locked for updates.
     *
     * @param  string $key
     * @return bool
     */
    public function isLocked($key)
    {
        try
        {
            $path = $this->getPath($key).'.lock';

            if (false === ($existed = file_exists($path)))
            {
                return false;
            }

            $file = new SplFileObject($path, 'r');

            // Another process has just acquired that lock
            if (0 === ($size = $file->getSize()) && true === $existed)
            {
                return true;
            }

            $data = unserialize($file->fgets());
            $file = null;

            if (false !== $data && time() <= $data[0])
            {
                return true;
            }
        }
        catch (Exception $e)
        {
            $file = null;
        }

        return false;
    }

    /**
     * Creates and checks the full filename.
     *
     * @param  string $key Data key
     * @return string The final filename with its full path
     */
    public function getPath($key)
    {
        return $this->getBasePath().DIRECTORY_SEPARATOR.SpicaCacheManager::hash($key);
    }

    /**
     * Gets base directory path to cache files. If that path does not exist,
     * it will be created and chmod-ed 777.
     *
     * @return string
     */
    public function getBasePath()
    {
        if (!empty($this->_option['segment']))
        {
            $path = $this->_option['path'].DIRECTORY_SEPARATOR.$this->_option['segment'];
        }
        else
        {
            $path = $this->_option['path'];
        }

        if (false === file_exists($path))
        {
            mkdir($path, 0777, true);
        }

        return $path;
    }
}

?>