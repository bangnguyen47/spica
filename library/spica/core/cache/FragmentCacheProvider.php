<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

// namespace spica\core\cache\provider;

class SpicaFragmentCacheProvider implements SpicaCacheProvider
{
    /**
     * Stack of names (for nested fragment support)
     *
     * @var array
     */
    public static $stack = array();

    /**
     * The event to parse the html for fragments needs to run only once
     * and only if a fragment needs to be cached
     *
     * @var bool
     */
    public static $addedEvent = FALSE;

    /**
     *
     * @var array
     */
    public static $fragments = array();

    /**
     * Creates an object of <code>SpicaCacheManager</code>.
     *
     * @return SpicaCacheSupport
     */
    public static function factory($type)
    {

    }

    /**
     * Begins recording a new page fragment if it isn't already cached.
     *
     * @param  string $name page fragment name
     * @param  int    $duration cache duration
     * @return bool   whether the fragment is in Cache
     */
    public static function start($name = 'default', $duration = NULL)
    {
        $cache = SpicaCache::getInstance();
        $old   = $cache->get($name);

        if ($old)
        {
            // load cached fragment
            echo $old;
            return false;
        }
        else
        {
            // start 'recording' the page fragment
            echo '<!-- begin fcache '.$name.' -->';
            // add to stack for stop() method to pop (nested support)
            self::$stack[] = array(
              'name' => $name,
              'duration' => $duration,
            );

            if (!self::$addedEvent)
            {
                Spica::publishEvent('system.display', array('fcache', 'store'));
                self::$addedEvent = true;
            }

            return true;
        }
    }

    /**
     * Stops recording the latest fragment.
     */
    public static function stop()
    {
        // pop the last started fragment (nested support)
        $frag = array_pop(self::$stack);
        if ($frag)
        {
            // stop 'recording' the fragment
            echo '<!-- end fcache '.$frag['name'].' -->';
            // add to the array of total fragments that need to be added to Cache
            self::$fragments[] = $frag;
        }
    }

    /**
     * Parses the output for any fragment blocks and caches them.
     */
    public static function store()
    {
        $cache = SpicaCache::getInstance();
        $html  = $cache->get();

        foreach (self::$fragments as $fragment)
        {
            $pattern = '#\<\!-- begin fcache '.preg_quote($fragment['name']).' --\>(.+)\<\!-- end fcache '.preg_quote($fragment['name']).' -->#sm';
            preg_match($pattern, $html, $matches);
            $cache->set($fragment['name'], $matches[1], null, $fragment['duration']);
        }
    }

    /**
     * Removes expired data entries.
     *
     * @return bool
     */
    public function removeExpired()
    {
        // FIXME
        throw new BadMethodCallException('This method is not implemeted yet. ');
    }
}

?>