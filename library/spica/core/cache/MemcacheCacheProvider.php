<?php

/*
 * Copyright (C) 2008 - 2010 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
// namespace spica\core\cache\provider;

/**
 * Provider for caching to memory using Memcached and Memcache PHP extension.
 * {@link http://www.danga.com/memcached/ Memcached}.
 *
 * SpicaMemcacheCacheProvider can be configured with a list of memcache servers by settings
 * its {@link setServers servers} property. By default, SpicaMemcacheCacheProvider assumes
 * there is a memcache server running on localhost at port 11211.
 *
 * See {@link SpicaCacheProvider} manual for common cache operations that are supported
 * by SpicaMemcacheCacheProvider.
 *
 * Note, there is no security measure to protected data in memcache.
 * All data in memcache can be accessed by any process running in the system.
 *
 * See {@link http://www.php.net/manual/en/function.memcache-addserver.php}
 * for more details.
 *
 * Ported from Pone_Cache_MemCache
 *
 * @category   spica
 * @package    core
 * @subpackage cache\provider
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 09, 2008
 * @copyright  Copyright (c) 2006-2009 Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: MemcacheCacheProvider.php 1783 2010-04-30 20:29:30Z pcdinh $
 */
class SpicaMemcacheCacheProvider implements SpicaCacheProvider
{
    /**
     * Memcache instance
     *
     * @var Memcache
     */
    private $_cache;

    /**
     * List of server options
     *
     * @var array
     */
    private $_options;

    /**
     * Constructs an object of <code>SpicaMemcacheCacheProvider</code>
     *
     * @param array $options An array of array of Memcache options
     */
    public function __construct($options)
    {
        $cache = $this->_getMemCache();

        foreach ($options as $option)
        {
            $option = array_merge(
            array(
              // Server hostname or IP address
              'host' => 'localhost',
              // Memcached port
              'port' => 11211,
              // Whether to use a persistent connection
              'persistent' => true,
              // Probability of using this server among all servers
              'weight' => 1,
              // Value in seconds which will be used for connecting to the server
              'timeout' => 15,
              // How often a failed server will be retried (in seconds)
              'retry_interval' => 15,
              // If the server should be flagged as online upon a failure
              'status' => true,
              // If Memcached compresses the data before saving it into memory
              'compression' => false
            ), $option);

            $cache->addServer($option['host'], $option['port'], $option['persistent'], $option['weight'], $option['timeout'], $option['status']);
            $this->_options[] = $option;
        }
    }

    /**
     * Gets Memcache cache provider option.
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Sets option value.
     *
     * @param string $name
     * @param mixed $val
     */
    public function setOption($name, $val)
    {
        $this->_option[$name] = $val;
    }

    /**
     * Instantiates an object of Memcache
     *
     * @return Memcache
     */
    protected function _getMemCache()
    {
        if ($this->_cache !== null)
        {
            return $this->_cache;
        }

        $this->_cache = new Memcache();
        return $this->_cache;
    }

    /**
     * Retrieves a value from cache with a specified key.
     *
     * @param  string $key a unique key identifying the cached value
     * @param  bool   $lockAware
     * @param  int    $waitTime Maximum time to wait until lock is released
     * @return string the value stored in cache, false if the value is not in the cache or expired.
     */
    public function get($key, $lockAware = false, $waitTime = 10)
    {
        // FIXME
        if (!empty($this->_options[0]['segment']))
        {
            $key = $this->_options[0]['segment'].'_'.$key;
        }

        return $this->_cache->get($key);
    }

    /**
     * Stores a value identified by a key in cache.
     * This is the implementation of the method declared in the parent class.
     *
     * @param string $key the key identifying the value to be cached
     * @param string $data the value to be cached
     * @param int    $ttl the number of seconds in which the cached value will expire.
     *               0 means never expire.
     * @return bool true if the value is successfully stored into cache, false otherwise
     */
    public function set($key, $data, $ttl = 0)
    {
        if ($ttl > 0)
        {
            $ttl += time();
        }
        else
        {
            $ttl = $this->_options[0]['ttl'];
        }

        if (!empty($this->_options[0]['segment']))
        {
            $key = $this->_options[0]['segment'].'_'.$key;
        }

        $compression = $this->_options[0]['compression'] ? MEMCACHE_COMPRESSED : 0;
        return $this->_cache->set($key, $data, $compression, $ttl);
    }

    /**
     * Stores a value identified by a key into cache if the cache does not contain this key.

     * @param string $key the key identifying the value to be cached
     * @param string $data the value to be cached
     * @param int    $ttl the number of seconds in which the cached value will expire.
     *               0 means never expire.
     * @return bool  true if the value is successfully stored into cache, false otherwise
     */
    public function add($key, $data, $ttl = 0)
    {
        if ($ttl > 0)
        {
            $ttl += time();
        }
        else
        {
            $ttl = 0;
        }

        if (!empty($this->_options[0]['segment']))
        {
            $key = $this->_options[0]['segment'].'_'.$key;
        }

        return $this->_cache->add($key, $data, 0, $ttl);
    }

    /**
     * Removes a value with the specified key from cache
     *
     * @param string $key the key of the value to be deleted
     * @return bool if no error happens during deletion
     */
    public function remove($key)
    {
        if (!empty($this->_options[0]['segment']))
        {
            $key = $this->_options[0]['segment'].'_'.$key;
        }

        return $this->_cache->delete($key);
    }

    /**
     * Removes expired data entries.
     *
     * @return bool
     */
    public function removeExpired()
    {
        // FIXME
        throw new BadMethodCallException('This method is not implemeted yet. ');
    }

    /**
     * Deletes all values from cache.
     *
     * Be careful of performing this operation if the cache is shared by multiple applications.
     */
    public function flush()
    {
        return $this->_cache->flush();
    }

    /**
     * Acquires lock for updating data.
     *
     * @param string $key
     * @param int $ttl
     * @param int $waitTime
     */
    public function acquireLock($key, $ttl, $waitTime = 1000)
    {
        // FIXME
    }

    /**
     * Releases lock after updating data.
     *
     * @param string $cacheKey
     */
    public function releaseLock($cacheKey)
    {
        // FIXME
    }

    /**
     * Does a cache key exist?
     *
     * @param string $key
     * @return bool
     */
    public function isCached($key)
    {
        if (!empty($this->_options[0]['segment']))
        {
            $key = $this->_options[0]['segment'].'_'.$key;
        }

        return (false !== $this->_cache->get($key)) ? true : false;
    }

    /**
     * Is a cache key expired?
     *
     * @param string $key
     * @return bool
     */
    public function isExpired($key)
    {
        if (!empty($this->_options[0]['segment']))
        {
            $key = $this->_options[0]['segment'].'_'.$key;
        }

        return (false !== $this->_cache->get($key)) ? false : true;
    }
}

?>