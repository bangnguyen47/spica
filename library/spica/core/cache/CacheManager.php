<?php

/*
 * Copyright (C) 2008 - 2010 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

// namespace spica\core\cache;

/**
 * A container for <code>SpicaCacheProvider</code>(s) that maintain all aspects
 * of their lifecycle.
 *
 * A <code>SpicaCacheManager</code> holds references to <code>SpicaCacheProvider</code>
 * objects and manages their creation and lifecycle.
 *
 * namespace spica\core\cache\CacheManager
 *
 * @category   spica
 * @package    core
 * @subpackage cache
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CacheManager.php 1833 2010-05-09 05:52:28Z pcdinh $
 */
class SpicaCacheManager
{
    /**
     * Indicates APC cache provider.
     *
     * @var string
     */
    const APC = 'apc';

    /**
     * Indicates Memcache cache provider.
     *
     * @var string
     */
    const MEMCACHE = 'memcache';

    /**
     * Indicates XCache cache provider.
     *
     * @var string
     */
    const XCACHE = 'xcache';

    /**
     * Indicates EAccelerator cache provider.
     *
     * @var string
     */
    const EA = 'eaccelerator';

    /**
     * Indicates file-based cache provider.
     *
     * @var string
     */
    const FILE = 'file';

    /**
     * Default common options for cache provider.
     *
     * @var array
     */
    private static $_options = array(
      // Partition name of the cache entries.
      'segment' => '',
      // Time in seconds, after this time cache entry is expired.
      'ttl' => 300,
      // Maximum time that lock entry is allowed to exist
      'lock_expr_time' => 5
    );

    /**
     * Prevents construction of an object of <code>SpicaCacheManager</code>
     */
    protected function __construct() {}

    /**
     * Constructs an object of <code>SpicaCacheProvider</code>
     *
     * @throws InvalidArgumentException
     * @param  string $provider Provider name
     * @param  array $option
     * @return SpicaCacheProvider
     */
    public static function factory($provider, $option)
    {
        $option = array_merge(self::$_options, (array) $option);

        switch ($provider)
        {
            case self::FILE:
                include_once 'library/spica/core/cache/FileCacheProvider.php';
                return new SpicaFileCacheProvider($option);

            case self::MEMCACHE:
                include_once 'library/spica/core/cache/MemcacheCacheProvider.php';
                return new SpicaMemcacheCacheProvider(array($option));

            case self::APC:
                include_once 'library/spica/core/cache/ApcCacheProvider.php';
                return new SpicaApcCacheProvider($option);

            case self::XCACHE:
                include_once 'library/spica/core/cache/ApcCacheProvider.php';
                return new SpicaXCacheCacheProvider($option);

            case self::EA:
                include_once 'library/spica/core/cache/EAcceleratorCacheProvider.php';
                return new SpicaEAcceleratorCacheProvider($option);

            default:
                throw new InvalidArgumentException('The cache provider "'.$provider.'" is not supported yet. ');
        }
    }

    /**
     * Calculates the portable cyclic redundancy checksum polynomial of a string (32-bit/64-bit).
     *
     * @author mail at tristansmis dot nl This code is in public domain
     * @param  string $string
     * @return string
     */
    public static function hash($string)
    {
        $crc = abs(crc32($string));

        // Smaller than 2 147 483 648
        if ($crc & 0x80000000)
        {
            $crc ^= 0xffffffff;
            $crc += 1;
        }

        return $crc;
    }
}

/**
 * A <code>SpicaCacheProvider</code> defines an API for storing and later
 * retrieving data (strings, arrays, serializable objects) based upon key values.
 *
 * To create a custom cache provider, create a class that implements SpicaCacheProvider.
 *
 * namespace spica\core\cache\CacheProvider
 *
 * @category   spica
 * @package    core
 * @subpackage cache
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CacheManager.php 1833 2010-05-09 05:52:28Z pcdinh $
 */
interface SpicaCacheProvider
{
    /**
     * Checks if a value identified by a key is expired.
     *
     * @throws RuntimeException if file cannot be opened (e.g. insufficient access rights).
     * @param  string $key  the key identifying the value to be cached
     * @return bool
     */
    public function isExpired($key);

    /**
     * Checks if a value identified by a key is cached.
     *
     * @param  string $key the key identifying the value to be cached
     * @return bool
     */
    public function isCached($key);

    /**
     * Stores the value with a key into cache
     *
     * @param  string $key Cache key
     * @param  string $data
     * @param  int    $ttl The duration that cache entry will not expire,
     *                     default to null. 0 means never expired
     * @return bool
     */
    public function set($key, $data, $ttl = null);

    /**
     * Retrieves the value with a key (if any) from cache
     *
     * @throws SpicaDeadlockException
     * @param  string $cacheKey
     * @param  bool   $lockAware
     * @param  int    $waitTime Maximum seconds that this method waits utils
     *                lock entry is released to make data entry is ready to retrieve
     * @return false|string|object
     */
    public function get($cacheKey, $lockAware = false, $waitTime = 10);

    /**
     * Stores the value only if cache does not have this key
     *
     * @param  string $key Cache key
     * @param  string $data
     * @param  int    $ttl The duration that cache entry will not expire,
     *                     default to null. 0 means never expired
     * @return bool
     */
    public function add($key, $data, $ttl = null);

    /**
     * Acquires a lock on the given $key.
     * If we fail to get the lock -- this means another process is doing it so
     * we wait (block) for a few microseconds while we wait for the cache to
     * be filled or the lock to timeout.
     *
     * If you get a false from this call, you _must_ populate the cache ASAP or
     * indicate that you won't by calling SpicaFileCacheProvider::releaseLock().
     *
     * This technique forces serialisation and so helps deal with thundering
     * herd scenarios where a lot of clients ask the for the same idempotent
     * (and costly) operation.
     *
     * The implementation is based on suggestions in this message
     *
     * @throws SpicaCacheLockException Whenever the lock to a cache entry specified
     *         by a key cannot be acquired because it is acquired by another process
     *         or the data is updated and lock is released so this process should
     *         come back to get the data instead of attempting to acquire lock.
     *         If not thrown, then the lock was acquired.
     * @see    http://marc.theaimsgroup.com/?l=git&m=116562052506776&w=2
     * @param  string $key Cache key
     * @param  int $ttl seconds The maximum time that lock can exist
     * @param  int $waitTime Microseconds for attempting to acquire lock.
     *             If $waitTime is 0 then this method returns false immediately
     *             when lock can not be acquired.
     * @return bool
     */
    public function acquireLock($key, $ttl, $waitTime = 1000);

    /**
     * Releases a lock on the given $key.
     *
     * @param string $key Cache key
     */
    public function releaseLock($key);

    /**
     * Removes the value with the specified key from cache.
     *
     * @param  string $key Cache key
     * @return bool
     */
    public function remove($ey);

    /**
     * Removes expired data entries.
     *
     * @return bool
     */
    public function removeExpired();

    /**
     * Removes all values from cache.
     */
    public function flush();

    /**
     * Gets configured options.
     *
     * @return array
     */
    public function getOptions();
}

/**
 * Used when a lock could not be acquired (a process attempts to get
 * a lock)/released (a process waits for a lock release) within the timeout,
 * or when a deadlock was detected or an upgrade failed.
 *
 * Ported from Pone_DeadlockException.
 *
 * @category   spica
 * @package    core
 * @subpackage cache
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      December 08, 2008
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CacheManager.php 1833 2010-05-09 05:52:28Z pcdinh $
 */
class SpicaDeadlockException extends Exception {}

/**
 * Indicates that the cache data has not been initialized or is temporarily unavailable.
 *
 * @category   spica
 * @package    core
 * @subpackage cache
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CacheManager.php 1833 2010-05-09 05:52:28Z pcdinh $
 */
class SpicaCacheNotAvailableException extends Exception {}

/**
 * Used to indicate that the cache lock could not be acquired.
 *
 * @category   spica
 * @package    core
 * @subpackage cache
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CacheManager.php 1833 2010-05-09 05:52:28Z pcdinh $
 */
class SpicaCacheLockException extends Exception {}

?>