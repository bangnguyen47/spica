<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
// namespace spica\core\cache\provider;

/**
 * Provider for caching to shared memory using eAaccelerator. eAccelerator is a
 * free open-source PHP accelerator, optimizer, and dynamic content cache.
 * {@link http://eaccelerator.net/}
 *
 * @category   spica
 * @package    core
 * @subpackage cache\provider
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 27, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: EAcceleratorCacheProvider.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaEAcceleratorCacheProvider implements SpicaCacheProvider
{
    /**
     * Cache option
     *
     * @var array
     */
    protected $_option;

    /**
     * Constructs an object of <code>EAcceleratorCacheProvider</code>
     *
     * @param array $option
     */
    public function __construct($option)
    {
        $this->_option = $option;
    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/cache/SpicaCacheProvider#getOptions()
     */
    public function getOptions()
    {
        return $this->_option;
    }

    /**
     * Sets option value.
     *
     * @param string $name
     * @param mixed $val
     */
    public function setOption($name, $val)
    {
        $this->_option[$name] = $val;
    }

    /**
     * Reads a file from cache. If file is expired, it will be deleted.
     *
     * @throws RuntimeException If file cannot be opened (e.g. insufficient access rights).
     * @param  string $key a unique key identifying the cached value
     * @param  bool   $lockAware
     * @param  int    $waitTime Maximum time to wait until lock is released
     * @return string|false  the value stored in cache, false if the value is not in the cache or expired.
     */
    public function get($key, $lockAware = false, $waitTime = 10)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        $rs = eaccelerator_get($key);

        if (is_array($rs))
        {
            return $rs[0];
        }

        return false;
    }

    /**
     * Stores a value identified by a key in cache.
     *
     * @param  string $key  the key identifying the value to be cached
     * @param  string $data the value to be cached
     * @param  int    $ttl  the number of seconds in which the cached value will expire.
     *                0 means never expire.
     * @return bool
     */
    public function set($key, $data, $ttl = null)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        switch ($ttl)
        {
            case null:
                $ttl = time() + $this->_option['ttl'];
                break;

            case 0:
                $ttl = 100000000;
                break;

            default:
                $data = time() + $ttl;
                break;
        }

        return eaccelerator_put($key, array($data, time()), $ttl);
    }

    /**
     * Stores a value identified by a key into cache if the cache does not
     * contain this key or that key is expired.
     *
     * @param  string $key the key identifying the value to be cached
     * @param  string $data the value to be cached
     * @param  int    $ttl the number of seconds in which the cached value will expire.
     *                0 means never expire.
     * @return bool   true if the value is successfully stored into cache, false otherwise
     */
    public function add($key, $data, $ttl = null)
    {

    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/cache/SpicaCacheProvider#remove()
     */
    public function remove($key)
    {
        return eaccelerator_rm($key);
    }

    /**
     * Deletes all values from cache.
     * Be careful of performing this operation if the cache is shared by multiple applications.
     *
     * @return bool
     */
    public function flush()
    {
        $entries = eaccelerator_list_keys();

        if (!is_array($entries))
        {
            return true;
        }

        $rs = true;

        foreach ($entries as $entry)
        {
            $rs |= eaccelerator_rm($entry['name']);
        }

        return $rs;
    }

    /**
     * Checks if a value identified by a key is expired.
     *
     * @throws RuntimeException if file cannot be opened (e.g. insufficient access rights).
     * @param  string $key the key identifying the value to be cached
     * @return bool
     */
    public function isExpired($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }
    }

    /**
     * Checks if a value identified by a key is cached. In fact the file that contains
     * value is checked for existence but the data is not validated.
     *
     * @param  string $key the key identifying the value to be cached
     * @return bool
     */
    public function isCached($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }
    }

    /**
     * Acquires a lock on the given $key.
     * If we fail to get the lock -- this means another process is doing it so
     * we wait (block) for a few microseconds while we wait for the cache to
     * be filled or the lock to timeout.
     *
     * If you get a false from this call, you _must_ populate the cache ASAP or
     * indicate that you won't by calling SpicaFileCacheProvider::releaseLock().
     *
     * This technique forces serialisation and so helps deal with thundering
     * herd scenarios where a lot of clients ask the for the same idempotent
     * (and costly) operation.
     *
     * The implementation is based on suggestions in this message
     * @see    http://marc.theaimsgroup.com/?l=git&m=116562052506776&w=2
     * @param  string $key
     * @param  int $waitTime Time for usleep() microseconds
     *             If $waitTime is null then this method returns false immediately
     *             when lock entry can not be acquired
     * @param  int $ttl seconds The maximum time that lock entry can exist
     * @return bool
     */
    public function acquireLock($key, $waitTime = 0, $ttl)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }
    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/cache/SpicaCacheProvider#releaseLock()
     */
    public function releaseLock($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        return eaccelerator_rm($key.'_lock');
    }
}

?>