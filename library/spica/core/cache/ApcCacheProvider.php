<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
// namespace spica\core\cache\provider;

/**
 * An object of SpicaApcCacheProvider provides caching facility that is backed by APC,
 * the built-in local bytecode/data cache in PHP 6
 *
 * To use this application component, the APC PHP extension must be loaded and
 * the php.ini file must have the following:
 * <pre>
 * apc.cache_by_default=0 (files are only cached if matched by a positive filter)
 * </pre>
 *
 * Ported from Pone_Cache_Apc.
 *
 * @category   spica
 * @package    core
 * @subpackage cache\provider
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      May 09, 2008
 * @copyright  Copyright (c) 2006-2009 Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: ApcCacheProvider.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaApcCacheProvider implements SpicaCacheProvider
{
    /**
     * Cache options
     *
     * @var array [segment, ttl, lock_expr_time]
     */
    protected $_option;

    /**
     * Constructs an object of <code>SpicaApcCacheProvider</code>
     *
     * @param array $options [segment, ttl, lock_expr_time]
     */
    public function __construct($options)
    {
        $this->_option = $options;
    }

    /**
     * Gets APC cache provider option.
     *
     * @return array [segment, timeout, lock_expr_time]
     */
    public function getOptions()
    {
        return $this->_option;
    }

    /**
     * Sets option value.
     *
     * @param string $name
     * @param mixed $val
     */
    public function setOption($name, $val)
    {
        $this->_option[$name] = $val;
    }

    /**
     * Stores a value identified by a key into cache if the cache does not
     * contain this key.
     *
     * @param  string $key the key identifying the value to be cached
     * @param  string $data the value to be cached
     * @param  int    $ttl the number of seconds in which the cached value will expire. 0 means never expire.
     * @return bool   true if the value is successfully stored into cache, false otherwise
     */
    public function add($key, $data, $ttl = 0)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        if (null === $ttl)
        {
            $ttl = $this->_option['ttl'];
        }

        return apc_add($key, $data, $ttl);
    }

    /**
     * Stores a value identified by a key in cache.
     *
     * @param  string $key  the key identifying the value to be cached
     * @param  string $data the value to be cached
     * @param  int    $ttl the number of seconds in which the cached value will expire. 0 means never expire.
     * @return bool
     */
    public function set($key, $data, $ttl = 0)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        if (null === $ttl)
        {
            $ttl = $this->_option['ttl'];
        }

        // @see Be warned for APC timebomb http://t3.dotgnu.info/blog/php/user-cache-timebomb
        // @see http://pecl.php.net/bugs/bug.php?id=16843
        // If some pages stop loading due to APC lock, add apc.write_lock = 1 into php.ini
        return apc_store($key, $data, $ttl);
    }

    /**
     * Retrieves a value from cache with a specified key.
     *
     * Developer needs to acquireLock() first then get('key', true) (get with lock aware)
     *
     * @param  string $key a unique key identifying the cached value
     * @param  bool   $lockAware
     * @param  int    $waitTime Maximum time to wait until lock is released
     * @return string the value stored in cache, false if the value is not in the cache or expired.
     */
    public function get($key, $lockAware = false, $waitTime = 10)
    {
        if (!empty($this->_option['segment']))
        {
            $key  = $this->_option['segment'].'_'.$key;
        }

        if (false  === $lockAware)
        {
            $value = apc_fetch($key);
            return (null === $value) ? false : $value;
        }

        $value = apc_fetch($key);

        if (false !== $value && null !== $value)
        {
            return $value;
        }

        // Cache does not exist or be expired
        try
        {
            $time    = time();
            $lockKey = $key.'_lock';
            $expire  = $this->_option['lock_expr_time'];

            // FIXME What if another process removes this key
            // after updating data sucessfully
            $lockTime = apc_fetch($lockKey);

            // Lock entry already existed
            if (false !== $lockTime)
            {
                // Lock entry is not expired
                if ($time - (int) $lockTime <= $expire)
                {
                    throw new Exception();
                }

                // Acquires new lock
                apc_delete($lockKey);

                // try to acquire lock to reserve the right to update data
                // one of processes is lucky
                // FIXME What if another process removes this key
                // after updating data sucessfully
                if (false === apc_add($lockKey, $time, $expire))
                {
                    throw new Exception();
                }

                return false;
            }

            // Lock entry does not exist
            // Try to acquire lock to reserve the right to update data
            // FIXME What if another process removes this key
            // after updating data sucessfully
            if (false === apc_add($lockKey, $time, $expire))
            {
                throw new Exception();
            }

            return false;
        }
        catch (Exception $ex)
        {
            // Can not acquire lock because one of the processes acquired it
            $value = apc_fetch($key);
            return (null === $value) ? false: $value;
        }
    }

    /**
     * Removes a value with the specified key from cache
     *
     * @param  string $key the key of the value to be deleted
     * @return bool if no error happens during deletion
     */
    public function remove($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        return apc_delete($key);
    }

    /**
     * Removes expired data entries.
     *
     * @return bool
     */
    public function removeExpired()
    {
        // FIXME
        throw new BadMethodCallException('This method is not implemeted yet. ');
    }

    /**
     * Deletes all values from cache.
     * Be careful of performing this operation if the cache is shared by multiple applications.
     *
     * @return bool
     */
    public function flush()
    {
        return apc_clear_cache('user');
    }

    /**
     * Acquires a lock on the given $key.
     * If we fail to get the lock -- this means another process is doing it so
     * we wait (block) for a few microseconds while we wait for the cache to
     * be filled or the lock to timeout
     *
     * If you get a false from this call, you _must_ populate the cache ASAP or
     * indicate that you won't by calling SpicaApcCacheProvider::releaseLock().
     *
     * This technique forces serialisation and so helps deal with thundering
     * herd scenarios where a lot of clients ask the for the same idempotent
     * (and costly) operation
     *
     * The implementation is based on suggestions in this message
     * @see    http://marc.theaimsgroup.com/?l=git&m=116562052506776&w=2
     * @throws SpicaCacheLockException Whenever the lock to a cache entry specified
     *         by a key cannot be acquired because it is acquired by another process
     *         or the data is updated and lock is released so this process should
     *         come back to get the data instead of attempting to acquire lock.
     *         If not thrown, then the lock was acquired.
     * @param  string $key
     * @param  int $ttl seconds The maximum time that lock entry can exist
     * @param  int $waitTime Time for usleep() microseconds
     *             If $waitTime is null then this method returns false immediately
     *             when lock entry can not be acquired
     * @return bool
     */
    public function acquireLock($key, $ttl, $waitTime = 1000)
    {
        if (!empty($this->_option['segment']))
        {
            $key  = $this->_option['segment'].'_'.$key;
        }

        $key = $key.'_lock';
        // apc_add() does not replace and returns true on success.
        // $ttl is obeyed by APC expiry.
        $acquired = apc_add($key, time(), $ttl);

        if (true  === $acquired)
        {
            return true;
        }

        if (false === $acquired && 0 === $waitTime)
        {
            // Cache lock can not be acquired - no further attempt anymore
            throw new SpicaCacheLockException();
        }

        $counter = 0;
        $sleep   = round($waitTime/10);

        while (false === apc_add($key, time(), $ttl))
        {
            if ($counter > 9)
            {
                return false;
            }

            // Wait for next check
            usleep($sleep);

            // Attempts every 10 loops
            if (time() - (int) apc_fetch($key) > $ttl)
            {
                // Release expired lock and place own lock
                if (false === apc_store($key, time(), $ttl))
                {
                    return true;
                }

                break;
            }

            $counter++;
        }

        // Cache lock can not be acquired
        throw new SpicaCacheLockException();
    }

    /**
     * Releases the exclusive lock obtained by SpicaApcCacheProvider::acquireLock()
     *
     * @param  string $key
     * @return bool
     */
    public function releaseLock($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        return apc_delete($key.'_lock');
    }

    /**
     * Removes the soft lock obtained by SpicaApcCacheProvider::acquireLock()
     *
     * @param  string $key
     * @return bool
     */
    public function removeLock($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        return apc_delete($key.'_lock');
    }

    /**
     * Gets APC stats.
     */
    public function getStats()
    {
        $info = apc_cache_info("user");

        foreach ($info['cache_list'] as $details)
        {
            printf("Cache entry %-10s requires %d bytes\n", $details['info'], $details['mem_size']);
        }
    }

    /**
     * Does a cache key  exist?
     *
     * @param  string $key
     * @return bool
     */
    public function isCached($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        return (false !== apc_fetch($key)) ? true : false;
    }

    /**
     * Is a cache key expired?
     *
     * @param  string $key
     * @return bool
     */
    public function isExpired($key)
    {
        if (!empty($this->_option['segment']))
        {
            $key = $this->_option['segment'].'_'.$key;
        }

        return (false !== apc_fetch($key)) ? false : true;
    }
}

?>