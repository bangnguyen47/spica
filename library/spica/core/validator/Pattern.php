<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
// namespace spica\core\validator\pattern;

include_once 'library/spica/core/validator/Common.php';

/**
 * A validator that validates against a string pattern. This validator does not
 * check optional value before validating.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.2
 * @since      March 22, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaPatternValidator extends SpicaValidator
{
    /**
     * String pattern that is used to validate against.
     *
     * @var string
     */
    protected $_pattern;

    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $pattern The string pattern to use
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($pattern, $violationMessage, $required = true)
    {
        $this->_pattern          = $pattern;
        $this->_violationMessage = $violationMessage;
        $this->_required         = (bool) $required;
    }

    /**
     * Gets the string pattern to validate against.
     *
     * @return string
     */
    public function getPattern()
    {
        return $this->_pattern;
    }

    /**
     * (non-PHPdoc)
     * @see library/spica/core/validator/SpicaValidator#isValid()
     */
    public function isValid($value = null)
    {
        $this->_value = $value;

        if (false === is_string($value))
        {
            return false;
        }

        // No data is provided but this value is optional (not mandatory)
        // Whitespace is allowed to be valid character
        if (true === spica_valid_empty_string($value, true))
        {
            return (false === $this->_required);
        }

        return (bool) preg_match($this->_pattern, $value);
    }
}

/**
 * A validator that validates against a optional string pattern. If an user
 * provides a empty string this validator will check if the value can be optional
 * or not. Therefore, please do not use this validator when you are working on
 * a pattern that matches with a null value or an empty string
 * (including whitespace strings).
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaOptionalStringPatternValidator extends SpicaPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $pattern The string pattern to use
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($pattern, $violationMessage, $required = true)
    {
        $this->_pattern          = $pattern;
        $this->_violationMessage = $violationMessage;
        $this->_required         = (bool) $required;
    }

    /**
     * (non-PHPdoc)
     * @see library/spica/core/validator/SpicaValidator#isValid()
     */
    public function isValid($value = null)
    {
        $this->_value = $value;

        if (true === spica_valid_empty_string($value))
        {
            // No data is provided but this value is not mandatory
            return (false === $this->_required);
        }

        if (false === is_string($value))
        {
            return false;
        }

        return (bool) preg_match($this->_pattern, $value);
    }
}

/**
 * A validator that validates against a email pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaEmailPatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage = 'Invalid format for e-mail address', $required = true)
    {
        parent::__construct(SpicaValidationPattern::EMAIL, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a URL pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaUrlPatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Indicates that scheme part (http/https) is required.
     *
     * @var bool
     */
    protected $_requireScheme = false;

    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $requireScheme Determines whether scheme part is required, fefaults to false
     * @param bool   $required Determines whether this value is required, defaults to true
     */
    public function __construct($violationMessage, $requireScheme = false, $required = true)
    {
        if (false    === $requireScheme)
        {
            $pattern = SpicaValidationPattern::URL_OP_SCHEME;
        }
        else
        {
            $pattern = SpicaValidationPattern::URL;
        }

        $this->_requireScheme = $requireScheme;
        parent::__construct($pattern, $violationMessage, $required);
    }

    /**
     * Tests if scheme part in the string is required.
     *
     * @return bool
     */
    public function isSchemeRequired()
    {
        return $this->_requireScheme;
    }
}

/**
 * A validator that validates against a SSN (social security number) pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaSsnPatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::SSN, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a Canadian postal code pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaCanadianPostalCodePatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::POSTALCODE_CA, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a Australian postal code pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      July 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAustralianPostalCodePatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::POSTALCODE_AUS, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a UK postal code pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      July 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaUKPostalCodePatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::POSTALCODE_UK, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a U.S zip code pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAmericanZipCodePatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::ZIPCODE_US, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a U.S telephone number (without extension) pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAmericanTelephonePatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::PHONE_US, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a U.S telephone number (with extension) pattern.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAmericanTelephoneWithExtensionPatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::PHONE_US_EXT, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a string whose characters are
 * UTF-8 alphabetical characters, numbers, underscores and dashes only.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaUtf8IdentifierAndDashPatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::ALPHANUM_DASH_UTF8, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a string whose characters are
 * ASCII alphabetical characters, numbers, underscores and dashes only.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaAsciiIdentifierAndDashPatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     * @param bool   $required Determines whether this value is required. Defaults to true
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::ALPHANUM_DASH_ASCII, $violationMessage, $required);
    }
}

/**
 * A validator that validates against a string contains only letters, numbers,
 * whitespace, dashes, periods, and underscores only.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaSafeTextPatternValidator extends SpicaOptionalStringPatternValidator
{
    /**
     * Creates a validator with the provided violation message.
     *
     * @param string $violationMessage violation message to use if the validation fails
     */
    public function __construct($violationMessage, $required = true)
    {
        parent::__construct(SpicaValidationPattern::SAFE_TEXT, $violationMessage, $required);
    }
}

/**
 * Provides set of common patterns that can be used in validator classes and functions.
 *
 * @category   spica
 * @package    core
 * @subpackage validator\pattern
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Pattern.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaValidationPattern
{
    /**
     * Email pattern.
     *
     * @var string
     */
    const EMAIL = '/^[A-z0-9\._-]+@[A-z0-9][A-z0-9-]*(\.[A-z0-9_-]+)*\.([A-z]{2,6})$/';

    /**
     * URI pattern.
     *
     * @var string
     */
    const URI = '#^([a-z\-\+]+://)?([a-z0-9]+:[a-z0-9]+\@){0,1}([a-z0-9.]*[a-z0-9-]+\.[a-z]+){1}(:[0-9]{1,5}){0,1}(.*)#i';

    /**
     * URL pattern including http, https, ftp, ftps.
     *
     * [http|https|ftp|ftps]://(domain or IP)(:port)/(anything here)
     *
     * @var string
     */
    const URL = '#^(https?|ftps?)://(([a-z0-9-]+\.)+[a-z]{2,6}|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(:[0-9]+)?(/?|/\S+)$#ix';

    /**
     * URL pattern with scheme part (http, https, ftp, ftps) is optional.
     *
     * [http|https|ftp|ftps]://(user:(password?)@)?(subdomain)(domain or IP)(:port)/(anything here)
     *
     * A domain can not start and/or end with a dash
     * Not start with - :  (?!-)
     * Not end with - : (?<!\-)
     *
     * @var string
     */
    const URL_OP_SCHEME = '#^((https?|ftps?)://)?([0-9a-z\-\_]{1}([0-9a-z\-_]+\.))?(((?!-)[a-z0-9-]+(?<!\-)\.)+[a-z]{2,6}|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(:[0-9]+)?(/?|/\S+)$#ix';

    /**
     * Regular expression that can be used to validate URL patterns (experimental).
     *
     * @var string
     */
    const URL2 = '/(?#WebOrIP)((?#protocol)((http|https):\/\/)?(?#subDomain)(([a-zA-Z0-9]+\.(?#domain)[a-zA-Z0-9\-]+(?#TLD)(\.[a-zA-Z]+){1,2})|(?#IPAddress)((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])))+(?#Port)(:[1-9][0-9]*)?)+(?#Path)((\/((?#dirOrFileName)[a-zA-Z0-9_\-\%\~\+]+)?)*)?(?#extension)(\.([a-zA-Z0-9_]+))?(?#parameters)(\?([a-zA-Z0-9_\-]+\=[a-z-A-Z0-9_\-\%\~\+]+)?(?#additionalParameters)(\&([a-zA-Z0-9_\-]+\=[a-z-A-Z0-9_\-\%\~\+]+)?)*)?/';

    /**
     * Matches any valid RFC-standard email address RFC2822 and RFC1035
     */
    const EMAIL_RFC2822 = '/^([A-Za-z0-9\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\}\|\~]+|"([\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|\\[\x0-\x7F])*")(\.([A-Za-z0-9\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\}\|\~]+|"([\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|\\[\x0-\x7F])*"))*@([A-Za-z0-9]([A-Za-z0-9\-]*[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9\-]*[A-Za-z0-9])?)*|(1[0-9]{0,2}|2([0-4][0-9]?|5[0-4]?|[6-9])?|[3-9][0-9]?)(\.(0|1[0-9]{0,2}|2([0-4][0-9]?|5[0-5]?|[6-9])?|[3-9][0-9]?)){2}\.(1[0-9]{0,2}|2([0-4][0-9]?|5[0-4]?|[6-9])?|[3-9][0-9]?))$/';

    /**
     * U.S. telephone number in the format (###) ###-####, where the area code
     * is optional, the parentheses around the area code are optional and
     * could be replaced with a dash, and there is optional spacing between
     * the number groups
     *
     * @var string
     */
    const PHONE_US = '/^(\(\s*\d{3}\s*\)|(\d{3}\s*-?))?\s*\d{3}\s*-?\s*\d{4}$/';

    /**
     * The same to PHONE_US, except allow for an optional one- to five-digit
     * extension specified with an "x", "ext", or "ext." and optional spacing
     *
     * @var string
     */
    const PHONE_US_EXT = '/^(\(\s*\d{3}\s*\)|(\d{3}\s*-?))?\s*\d{3}\s*-?\s*\d{4}\s*((x|ext|ext\.)\s*\d{1,5})?$/';

    /**
     * A string of a U.S. currency starting with a $ and followed by any number
     * with at most two optional decimal digits.
     *
     * @var string
     */
    const CURRENCY_US = '/^\$\d(\.\d{1,2})?$/';

    /**
     * A five-digit U.S. Zip Code with an optional dash and four-digit extension.
     *
     * @var string
     */
    const ZIPCODE_US = '/^\d{5}(-\d{4})?$/';

    /**
     * A Canadian Postal Code in the format L#L #L# (where L is a letter).
     * There is a restriction placed on the first letter in the Postal Code to
     * ensure that a valid province, territory, or region is specified
     *
     * @var string
     */
    const POSTALCODE_CA = '/^[ABCEGHJKLMNPRSTVXY]\d[A-Z] \d[A-Z]\d$/';

    /**
     * A Australian Postal Code contains three main body parts.
     * The first is the state or territory abbreviation:
     *    NSW (New South Wales)
     *    ACT (Australian Capital Territory)
     *    VIC (Victoria)
     *    QLD (Queensland)
     *    SA (South Australia)
     *    WA (Western Australia)
     *    TAS (Tasmania)
     *    NT (Northern Territory)
     *
     * The second is a space
     * The third 4 digits following the state or territory abbreviation code
     *
     * @var string
     */
    const POSTALCODE_AUS = '/^([N][S][W]|[A][C][T]|[V][I][C]|[Q][L][D]|[S][A]|[W][A]|[T][A][S]|[N][T])([ ])([0-9]{4})$/';

    /**
     * A UK Postal Code pattern. E.x:
     * + A9 9AA
     * + A99 9AA
     * + AA9 9AA
     * + AA99 9AA
     * + AA9A 9AA
     *
     * @var string
     */
    const POSTALCODE_UK = '/^([a-zA-Z]){1}([0-9][0-9]|[0-9]|[a-zA-Z][0-9][a-zA-Z]|[a-zA-Z][0-9][0-9]|[a-zA-Z][0-9]){1}([ ])([0-9][a-zA-z][a-zA-z]){1}$/';

    /**
     * A social security number in the format ###-##-####, where the dashes
     * are optional and the three groups can have optional spacing between them.
     *
     * @var string
     */
    const SSN = '/^\d{3}\s*-?\s*\d{2}\s*-?\s*\d{4}$/';

    /**
     * A string whose characters are UTF-8 alphabetical characters, numbers, underscores
     * and dashes only.
     *
     * @var string
     */
    const ALPHANUM_DASH_UTF8 = '/^[-\pL\pN_]++$/uD';

    /**
     * A string whose characters are ASCII alphabetical characters, numbers, underscores     *
     * and dashes only.
     *
     * @var string
     */
    const ALPHANUM_DASH_ASCII = '/^[-a-z0-9_]++$/iD';

    /**
     * String contains only letters, numbers, whitespace, dashes, periods, and underscores
     *
     * pL matches letters
     * pN matches numbers
     * pZ matches whitespace
     * pPc matches underscores
     * pPd matches dashes
     * pPo matches normal puncuation
     *
     * @var string
     */
    const SAFE_TEXT = '/^[\pL\pN\pZ\p{Pc}\p{Pd}\p{Po}]++$/uD';

    /**
     * HTML anchor tag
     *
     * @var string
     */
    const HTML_LINK = '/<a[\s]+[^>]*?href[\s]?=[\s\"\']*([^"\'\s]+)[\"\']*.*?>([^<]+|.*?)?<\/a>/';

    /**
     * HTML metatag.
     *
     * Captures :
     * - name attribute of the HTML metatag tag
     * - content attribute of the HTML metatag tag
     */
    const HTML_METATAG = '/<meta[\s]+[^>]*?name[\s]?=[\s\"\']+(.*?)[\s\"\']+content[\s]?=[\s\"\']+(.*?)[\"\']+.*?>/';

    /**
     * IP table.
     */
    const IP_METATABLE = '/^(?<Date>.+\s\d+\s\d+\:\d+\:\d+).+\:.+\:(?<Traffic>.+)\:(?<Rule>.+)\:IN\=(?<InboundInterface>.+)\sOUT\=(?<OutboundIntercace>.*?)\s(?:MAC\=(?<MacAddress>.+)\s|)SRC\=(?<Source>.+)\sDST\=(?<Destination>.+)\sLEN\=.+TOS\=.+PROTO\=(?<Protocol>.+)\sSPT\=(?<SourcePort>.+)\sDPT\=(?<DestinationPort>.+)\s.+$/';

    /**
     * PHP function.
     *
     * @see http://www.php.net/manual/en/language.functions.php
     */
    const PHPFUNC = '/[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*./';

    /**
     * PHP variable.
     *
     * @see http://www.php.net/manual/en/language.variables.php
     */
    const PHPVAR = '/[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/';

    /**
     * Regular expression that can be used to validate numeric patterns.
     *
     * <p>This is numeric only and doesn't include additional syntax.</p>
     *
     * <p><code>1,233,434.09</code> is not allowed.</p>
     * @var string
     */
    const DIGITS = "/^\d+$/";

    /**
     * Regular expression that can be used to validate a phone pattern.
     *
     * <p>Pattern is without any additional syntax, this is strictly:</p>
     *
     * <p><code>123-123-1233<code> or <code>1234567890</code></p>
     * @var string
     */
    const FAX = "/^\d{3}\-\d{3}\-\d{4}|\d{10}$/";

    /**
     * Regular expression that can be used to validate the month.
     *
     * <p><code>1-12</code> or <code>01-09, 10, 11, 12</code></p>
     * @var string
     */
    const MONTH = "/^0?[1-9]$|^1[0-2]$/";

    /**
     * Regular expression that can be used to validate U.S date patterns.
     *
     * <p><code>mm/dd/yyyy</code></p>
     * @var string
     */
    const DATE_US = "#^\d{1,2}/\d{1,2}/\d{4}$#";

    /**
     * Regular expression that can be used to validate an address pattern.
     *
     * @var string
     */
    const ADDRESS = "@^[a-zA-Z\d]+(([-',. #][a-zA-Z\d ])?[a-zA-Z\d]*[.]*)*$@";

    /**
     * Regular expression that can be used to validate the month and year.
     *
     * <p>The month being the first in the pattern.</p>
     *
     * <p><code>1-12/2006</code> or <code>01-09, 10, 11, 12/2006</code></p>
     *
     * @var string
     */
    const MONTH_YEAR = "#^(0?[1-9]|1[0-2])/\d{4}$#";

    /**
     * Regular expression that can be used to validate the year using 4 digits.
     *
     * <p><code>2006</code></p>
     * @var string
     */
    const YEAR = "#^\d{4}$#";

    /**
     * Regular expression that can be used to validate a name.
     *
     * <p>This may contain charaters such as, '-', for names like 'Wu-Tang'</p>
     *
     * @var string
     */
    const NAME_PATTERN_2 = "#^([a-zA-Z '\-]+)$#";

    /**
     * Regular expression that can be used to validate a name.
     *
     * @var string
     */
    const NAME_PATTERN_1 = "#^.*[B-Db-dF-Hf-hJ-Nj-nP-Tp-tV-Xv-xZz]{6,}.*$#";

    /**
     * Regular expression that can be used to match UTF-8 words in a string.
     *
     * @see http://vn.php.net/str_word_count
     * @see http://www.regular-expressions.info/unicode.html
     * @var string
     */
    const WORD_UTF8 = "/\p{L}[\p{L}\p{Mn}\p{Pd}'\x{2019}]*/u";
}

?>