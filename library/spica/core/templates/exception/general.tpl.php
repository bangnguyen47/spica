<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<base href="<?php echo SpicaRequest::getFullBaseUrl(); ?>" />
<title>An exception has occurred - Spica Framework</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="description" content="Spica Framework" />
<link rel="stylesheet" href="public/spica/css/global.css" type="text/css" media="all" />
</head>
<body>
<div id="wrapper">
  <div id="header" class=""> </div>
  <div id="main">
    <div id="repos">
      <div id="mainPanelError">
        <?php if (false === $showFile): /* Production mode */ ?>
        <div class="userInfo">
          <h1 class="pageHeader">Something is technically wrong</h1>
          <p>An exception has occurred during request handling. If you think you reached this page in error, try reloading your browser. If this does not work, please do not hesitate to contact support.</p>
        </div>

        <?php endif; ?>
        <h1 class="exception"><?php echo get_class($e); ?></h1>
        <span class="line"><strong><?php echo $e->getMessage(); ?></strong></span>
        <?php $trace = $e->getTrace(); ?>
        <?php if ($trace): ?>
          <?php if ($hint): ?><div class="hint line"><?php echo $hint ?></div><?php endif; ?>
          <div class="line"><div class="topStack">thrown at line <span><?php echo $e->getLine(); ?></span> in <span class="call"><?php echo spica_exception_trace_stringify($trace[0], $showParams) ?></span></div></div>
          <div id="stackTrace"> <strong>StackTrace :</strong>
          <?php foreach ($trace as $k => $line): ?>
          <div class="stackTraceElement code"> <?php echo ($k < count($trace) - 1) ? '<span class="call">'.spica_exception_trace_stringify($trace[$k + 1], $showParams).'</span>' : ""; ?>
            <?php if (isset($line['file'])): ?>
              <?php echo ' (' .basename($line['file']) .':'.$line['line'].')'; ?> <br />
              <?php if ($showFile): ?><span class="fileInfo">in file <?php echo $line['file']; ?></span><?php endif; ?>
            <?php endif; ?>
          </div>
          <?php endforeach; ?>
          </div>
        <?php endif; ?>
      </div>
      <div class="bottom"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div id="footer"><div class="info">Powered with Spica framework (v<?php echo Spica::getVersion(); ?>). <a class="hasIcon" href="http://phpvietnam.net/">PHPVietnam Group</a> &copy; 2009</div></div>
</body>
</html>
