<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<base href="<?php echo SpicaRequest::getFullBaseUrl(); ?>" />
<title>Access Denied!</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="description" content="Spica Framework" />
<link rel="stylesheet" href="public/spica/css/global.css" type="text/css" media="all" />
</head>
<body>
<div id="wrapper">
  <div id="header" class=""> </div>
  <div id="main">
    <div id="repos">
      <div id="mainPanelError">
        <div class="userInfo">
          <h1 class="pageHeader">Access Denied!</h1>
          <p>You have reached a page that you are not authorized to see. Usually this happens when you are not logged in. If you feel you have reached this page in error please contact the webmaster</p>
        </div>
      </div>
      <!-- #mainPanelError -->
      <div class="bottom"></div>
    </div>
    <!-- #title -->
  </div>
  <div class="clearfix"></div>
</div>
<div id="footer"><div class="info">Powered with Spica framework (v<?php echo Spica::getVersion(); ?>). <a class="hasIcon" href="http://phpvietnam.net/">PHPVietnam Group</a> &copy; 2009</div></div>
</body>
</html>
