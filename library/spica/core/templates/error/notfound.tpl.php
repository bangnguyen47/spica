<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<base href="<?php echo SpicaRequest::getFullBaseUrl(); ?>" />
<title>The requested page does not exist</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="description" content="Spica Framework" />
<link rel="stylesheet" href="public/spica/css/global.css" type="text/css" media="all" />
</head>
<body>
<div id="wrapper">
  <div id="header" class=""> </div>
  <div id="main">
    <div id="repos">
      <div id="mainPanelError">
        <div class="userInfo">
          <h1 class="pageHeader">Page Not Found</h1>
          <p>We&apos;re sorry. The requested page does not exist. To continue, please
          return to the home page or use your browser&apos;s &apos;Back&apos; button to get to the
          page that you last visited. If you think you reached this error page by
          following a broken link, we&apos;d appreciate you reporting it to us.</p>
          <p>
          Request URL:<span class="code"><?php echo SpicaContext::getFullUrl(); ?></span>
          <br />
          Details:<span class="code"><?php echo $message ?></span>
          </p>
        </div>
      </div>
      <!-- #mainPanelError -->
      <div class="bottom"></div>
    </div>
    <!-- #title -->
  </div>
  <div class="clearfix"></div>
</div>
<div id="footer"><div class="info">Powered with Spica framework (v<?php echo Spica::getVersion(); ?>). <a class="hasIcon" href="http://phpvietnam.net/">PHPVietnam Group</a> &copy; 2009</div></div>
</body>
</html>
