<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<base href="<?php echo SpicaRequest::getFullBaseUrl(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="public/spica/css/global.css" type="text/css" media="all" />
<title><?php echo $this->_options['page_title']; ?></title>
</head>
<body>
<div id="wrapper">
  <div class="main">
    <div class="error">
        <h1 class="pageHeader">An unexpected error has occurred while serving the requested page. </h1>
        <h5>The server returned an "Internal Server Error".</h5>
        <?php if (SpicaContext::isDebug()): ?>
        <p>Details: A fatal error has been occurred</p>
        <ul>
          <li>Seriousity:<span class="code"><?php echo $level; ?></span></li>
          <li>Message:<span class="code"><?php echo $message; ?></span></li>
          <li>Source:<span class="code"><?php echo spica_path_make_relative($file).':'.$line; ?></span></li>
        </ul>
        <?php else: ?>
          <p>I am sorry but something is broken.</p>
          <p>Please e-mail us at <?php echo str_replace('@', '<span>@</span>', $this->_options['contact_email']); ?>
          and let us know what you were doing when this error occurred.
          We will fix it as soon as possible. Sorry for any inconvenience caused.</p>
        <?php endif; ?>
        <p><strong>Please try the following:</strong></p>
        <ul>
          <li>you can try refreshing the page, the problem may be temporary</li>
          <li>you can go back to <a class="hasIcon" href="javascript:history.go(-1)">previous page</a> or return to <a class="hasIcon" href="<?php echo SpicaContext::getFullUrl(); ?>">home page</a></li>
          <li>we&apos;ve been notified of the problem and will do our best to make sure it doesn&apos;t happen again!</li>
        </ul>
    </div>
  </div>
  <div class="push"></div>
  <div id="footer"><div class="info">Powered with Spica framework (v<?php echo Spica::getVersion(); ?>). <a class="hasIcon" href="http://phpvietnam.net/">PHPVietnam Group</a> &copy; 2009</div></div>
</div>
</body>
</html>