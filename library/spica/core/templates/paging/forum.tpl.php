<?php if ($this->_totalPages > 1): ?>
<div class="pagination">Page:

  <?php if ($this->_currentPage > 3): ?>
    <a href="<?php echo str_replace('{#}', 1, $this->_urlTemplate) ?>">1</a>
    <?php if ($this->_currentPage != 4) ?>&hellip;<?php endif ?>

  <?php for ($i = $this->_currentPage - 2, $stop = $this->_currentPage + 3; $i < $stop; ++$i): ?>

    <?php if ($i < 1 || $i > $this->_totalPages) continue;
    if ($this->_currentPage == $i):
    ?>
      <span class="current"><?php echo $i ?></span>
    <?php else: ?>
      <a href="<?php echo str_replace('{#}', $i, $this->_urlTemplate) ?>"><?php echo $i ?></a>
    <?php endif ?>

  <?php endfor ?>

  <?php if ($this->_currentPage <= $this->_totalPages - 3): ?>
    <?php if ($this->_currentPage != $this->_totalPages - 3) echo '&hellip;' ?>
    <a href="<?php echo str_replace('{#}', $this->_totalPages, $this->_urlTemplate) ?>"><?php echo $this->_totalPages ?></a>
  <?php endif ?>

</div>
<?php endif; ?>