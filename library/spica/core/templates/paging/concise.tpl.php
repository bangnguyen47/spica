<?php if ($this->_totalPages > 1): ?>
<div class="pagination">

  <?php if ($this->_prevPage): ?>
    <a href="<?php echo str_replace('{#}', $this->_prevPage, $this->_urlTemplate) ?>">&laquo;&nbsp;Previous</a>
  <?php else: ?>
    <span class="disabled">&laquo;&nbsp;Previous</span>
  <?php endif ?>

  | Page <?php echo $this->_currentPage ?> of <?php echo $this->_totalPages ?>

  | Items <?php echo $this->_firstItem ?>&ndash;<?php echo $this->_lastItem ?> of <?php echo $this->_totalItems ?>

  | <?php if ($this->_nextPage): ?>
    <a href="<?php echo str_replace('{#}', $this->_nextPage, $this->_urlTemplate) ?>">Next&nbsp;&raquo;</a>
  <?php else: ?>
    <span class="disabled">Next&nbsp;&raquo;</span>
  <?php endif ?>

</div>
<?php endif; ?>