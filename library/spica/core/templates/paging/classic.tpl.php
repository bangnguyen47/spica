<?php if ($this->_totalPages > 1): ?>
<div class="pagination">

  <?php if ($this->_firstPage): ?>
    <a href="<?php echo str_replace('{#}', 1, $this->_urlTemplate) ?>">&lsaquo;&nbsp;First</a>
  <?php endif ?>

  <?php if ($this->_prevPage): ?>
    <a href="<?php echo str_replace('{#}', $this->_prevPage, $this->_urlTemplate) ?>">&lt;</a>
  <?php endif ?>


  <?php for ($i = 1; $i <= $this->_totalPages; $i++): ?>

    <?php if ($i == $this->_currentPage): ?>
    <span class="current"><?php echo $i ?></span>>
    <?php else: ?>
      <a href="<?php echo str_replace('{#}', $i, $this->_urlTemplate) ?>"><?php echo $i ?></a>
    <?php endif ?>

  <?php endfor ?>


  <?php if ($this->_nextPage): ?>
    <a href="<?php echo str_replace('{#}', $this->_nextPage, $this->_urlTemplate) ?>">&gt;</a>
  <?php endif ?>

  <?php if ($this->_lastPage): ?>
    <a href="<?php echo str_replace('{#}', $this->_lastPage, $this->_urlTemplate) ?>">Last&nbsp;&rsaquo;</a>
  <?php endif ?>

</div>
<?php endif; ?>