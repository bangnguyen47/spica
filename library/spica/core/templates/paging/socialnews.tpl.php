<?php if ($this->_totalPages > 1): ?>
<div class="pagination">

  <?php if ($this->_prevPage): ?>
    <a href="<?php echo str_replace('{#}', $this->_prevPage, $this->_urlTemplate) ?>">&laquo;&nbsp;Previous</a>
  <?php else: ?>
    <span class="disabled">&laquo;&nbsp;Previous</span>
  <?php endif ?>


  <?php if ($this->_totalPages < 13): /* « Previous  1 2 3 4 5 6 7 8 9 10 11 12  Next » */ ?>

    <?php for ($i = 1; $i <= $this->_totalPages; $i++): ?>
      <?php if ($i == $this->_currentPage): ?>
        <span class="current"><?php echo $i ?></span>
      <?php else: ?>
        <a href="<?php echo str_replace('{#}', $i, $this->_urlTemplate) ?>"><?php echo $i ?></a>
      <?php endif ?>
    <?php endfor ?>

  <?php elseif ($this->_currentPage < 9): /* « Previous  1 2 3 4 5 6 7 8 9 10 … 25 26  Next » */ ?>

    <?php for ($i = 1; $i <= 10; $i++): ?>
      <?php if ($i == $this->_currentPage): ?>
        <span class="current"><?php echo $i ?></span>
      <?php else: ?>
        <a href="<?php echo str_replace('{#}', $i, $this->_urlTemplate) ?>"><?php echo $i ?></a>
      <?php endif ?>
    <?php endfor ?>

    &hellip;
    <a href="<?php echo str_replace('{#}', $this->_totalPages - 1, $this->_urlTemplate) ?>"><?php echo $this->_totalPages - 1 ?></a>
    <a href="<?php echo str_replace('{#}', $this->_totalPages, $this->_urlTemplate) ?>"><?php echo $this->_totalPages ?></a>

  <?php elseif ($this->_currentPage > $this->_totalPages - 8): /* « Previous  1 2 … 17 18 19 20 21 22 23 24 25 26  Next » */ ?>

    <a href="<?php echo str_replace('{#}', 1, $this->_urlTemplate) ?>">1</a>
    <a href="<?php echo str_replace('{#}', 2, $this->_urlTemplate) ?>">2</a>
    &hellip;

    <?php for ($i = $this->_totalPages - 9; $i <= $this->_totalPages; $i++): ?>
      <?php if ($i == $this->_currentPage): ?>
        <span class="current"><?php echo $i ?></span>
      <?php else: ?>
        <a href="<?php echo str_replace('{#}', $i, $this->_urlTemplate) ?>"><?php echo $i ?></a>
      <?php endif ?>
    <?php endfor ?>

  <?php else: /* « Previous  1 2 … 5 6 7 8 9 10 11 12 13 14 … 25 26  Next » */ ?>

    <a href="<?php echo str_replace('{#}', 1, $this->_urlTemplate) ?>">1</a>
    <a href="<?php echo str_replace('{#}', 2, $this->_urlTemplate) ?>">2</a>
    &hellip;

    <?php for ($i = $this->_currentPage - 5; $i <= $this->_currentPage + 5; $i++): ?>
      <?php if ($i == $this->_currentPage): ?>
        <span class="current"><?php echo $i ?></span>
      <?php else: ?>
        <a href="<?php echo str_replace('{#}', $i, $this->_urlTemplate) ?>"><?php echo $i ?></a>
      <?php endif ?>
    <?php endfor ?>

    &hellip;
    <a href="<?php echo str_replace('{#}', $this->_totalPages - 1, $this->_urlTemplate) ?>"><?php echo $this->_totalPages - 1 ?></a>
    <a href="<?php echo str_replace('{#}', $this->_totalPages, $this->_urlTemplate) ?>"><?php echo $this->_totalPages ?></a>

  <?php endif ?>


  <?php if ($this->_nextPage): ?>
    <a href="<?php echo str_replace('{#}', $this->_nextPage, $this->_urlTemplate) ?>">Next&nbsp;&raquo;</a>
  <?php else: ?>
    <span class="disabled">Next&nbsp;&raquo;</span>
  <?php endif ?>

</div>
<?php endif; ?>