<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

include_once 'spica/core/security/Security.php';

/**
 * Represents the real world user for Spica CMS applications.
 * This encapsulates the username and the various roles the user can be in.
 *
 * This allows non-security related user information (such as email addresses,
 * telephone numbers etc) to be stored in a convenient location.
 *
 * namespace spica\cms\security\User
 *
 * @category   spica
 * @package    cms
 * @subpackage security
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 07, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CmsUser.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class CmsUser extends SpicaUser
{

}

?>