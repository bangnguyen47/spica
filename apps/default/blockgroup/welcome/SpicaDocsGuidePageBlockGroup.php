<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * A block group of a page represents the body content of Spica manual pages.
 *
 * namespace spica\docs\blockgroup\welcome\GuidePageBlockGroup
 *
 * @category   spica
 * @package    docs
 * @subpackage blockgroup\welcome
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaDocsGuidePageBlockGroup.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaDocsGuidePageBlockGroup extends SpicaPageBlockGroup
{
    /**
     * Template file for this block.
     *
     * @var string
     */
    protected $_templateFile;

    /**
     * Legal block names that can be rendered in this block group template.
     *
     * @var array
     */
    protected $_blocks = array('welcome/SpicaDocsNoteBlock');

    /**
     * Hook to perform action before SpicaPageBlock's _filter() and initialize() are called.
     *
     * It is often used by PageBlock developer to register SpicaPageBlockFilter
     * objects for used with filter chain.
     */
    public function beforeInitialization()
    {

    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/view/SpicaPageBlock#initialize()
     */
    public function initialize()
    {
        // The value of 'pname' must be validated by controller
        $this->_templateFile = SpicaContext::getLocaleName().'/docs/guide/'.SpicaRequest::get('pname');

        if (false === $this->templateExists($this->_templateFile))
        {
            $this->_templateFile = SpicaContext::getLocaleName().'/docs/guide/not_found';
        }
    }
}

?>