<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * A block group of a page represents the body content of the category creation page.
 *
 * namespace spica\cms\blockgroup\default\CategoryCreation
 *
 * @category   spica
 * @package    cms
 * @subpackage blockgroup\default
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 19, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DefaultCategoryCreationBlockGroup.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class DefaultCategoryCreationBlockGroup extends SpicaPageBlockGroup
{
    /**
     * The theme name in use
     *
     * @var string
     */
    public $themeName;

    /**
     * Form object.
     *
     * @var CmsCategoryForm
     */
    public $form;

    /**
     * Template file for this block. Defaults to "defaultCategoryCreation"
     *
     * @var string
     */
    protected $_templateFile;

    /**
     * Hook to perform action before SpicaPageBlock's _filter() and initialize() are called.
     *
     * It is often used by PageBlock developer to register SpicaPageBlockFilter
     * objects for used with filter chain.
     */
    public function beforeInitialization()
    {

    }

    /**
     * (non-PHPdoc)
     * @see library/spica/core/view/SpicaPageBlock#initialize()
     */
    public function initialize()
    {
        $this->form      = SpicaContext::getForm();
        $this->themeName = SpicaContext::getThemeName();

        // Switch template file
        if (true === $this->form->isProcessed())
        {
            $this->_templateFile = 'defaultCategoryCreationDone';
        }
    }
}

?>