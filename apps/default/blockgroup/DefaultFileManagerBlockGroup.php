<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * A block group of a page represents the body content of the account registration page.
 *
 * namespace spica\cms\blockgroup\default\FileManager
 *
 * @category   spica
 * @package    cms
 * @subpackage blockgroup\default
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 08, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DefaultFileManagerBlockGroup.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class DefaultFileManagerBlockGroup extends SpicaPageBlockGroup
{
    /**
     * The theme name in use
     *
     * @var string
     */
    public $themeName;

    /**
     * Template file for this block. Defaults to "defaultFileManager"
     *
     * @var string
     */
    protected $_templateFile;

    /**
     * List of files.
     *
     * @var SpicaFileList
     */
    public $fileList;

    /**
     * Base file path.
     *
     * @var string
     */
    public $basePath;

    /**
     * Truncated base path to files that can be used to show up.
     *
     * @var string
     */
    public $publicBasePath;

    /**
     * Base path to theme specific image path.
     *
     * @var string
     */
    public $imagePath;

    /**
     * Hook to perform action before SpicaPageBlock's _filter() and initialize() are called.
     *
     * It is often used by PageBlock developer to register SpicaPageBlockFilter
     * objects for used with filter chain.
     */
    public function beforeInitialization()
    {

    }

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/view/SpicaPageBlock#initialize()
     */
    public function initialize()
    {
        include_once 'spica/core/utils/FileUtils.php';

        $this->imagePath      = SpicaContext::getImagePath();
        $this->publicBasePath = 'public';
        $this->basePath       = SpicaContext::getBasePath().'/public';

        $path    = SpicaRequest::get('rpath');
        $dirPath = '';

        if (!empty($path))
        {
            $dirPath = str_replace('..', '', spica_url_decode($path));
        }

        $this->fileList = new SpicaFileList($this->basePath.'/'.$dirPath);

        if (true === $this->fileList->isReadable())
        {
            foreach ($this->fileList as $index => $file)
            {
                $file->lastModified = date('Y-m-d H:i:s', $file->lastModified);

                if (false === $file->isDot)
                {
                    $relativePath = spica_url_encode(str_replace($this->basePath.'/', '', $file->fullPath));
                    $file->url    = 'filemanager/index/index/rpath/'.$relativePath;
                    $file->size   = SpicaFileUtils::byteCountToDisplaySize($file->size);
                    continue;
                }

                if (SpicaStringUtils::endsWith($file->fileName, '..'))
                {
                    $path = rtrim(dirname($dirPath), '.');
                }
                else
                {
                    $path = $dirPath; // Is dot "."
                }

                $file->url = 'filemanager/index/index/rpath/'.spica_url_encode($path);
                $this->fileList[$index] = $file;
            }
        }
    }
}

?>