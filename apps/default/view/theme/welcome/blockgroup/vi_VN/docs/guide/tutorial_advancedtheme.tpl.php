    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Hướng dẫn cơ bản (Tutorial) - Theme nâng cao</h2>
    <p>Trong bài <a href="docs/guide/page/pname/tutorial_simpletheme" class="hasIcon">Viết ứng dụng đơn giản có dùng layout và theme (căn bản)</a>,
    quý vị đã làm quen với cách thức chúng ta sử dụng theme, layout và thậm chí là
    block và block group. Tuy nhiên bài này sẽ giúp chúng ta có cái nhìn chi tiết hơn về
    các thành phần quan trọng này trong Spica.</p>
    <p>Kiến trúc Spica hỗ trợ <b>decorator</b> giúp định nghĩa một layout template chung chứa các
    vùng đặt chỗ cho các thành phần nội dung bao gồm nhưng không hạn chế
    với <code>title</code>, <code>head</code>, <code>block</code>, <code>block group</code>
    và <code>body</code>.</p>

    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>