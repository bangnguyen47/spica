    <h1 class="pageHeader">Spica Framework - Manual - Lỗi</h1>

    <h2 class="header"><?php echo $this->message->summary; ?></h2>
    <p>Một lỗi đã xuất hiện trong quá trình xử lý trang quý vị đang tìm kiếm.</p>
    <?php if (null !== $this->message->details): ?>
    <p><?php echo $this->message->details; ?></p>
    <?php endif; ?>
    <p>Xin vui lòng kiểm tra lại đường dẫn trên thanh Address Bar của trình duyệt nếu như
    trang được thông báo là không tìm thấy hay tham số không hợp lệ.
    Các trường hợp khác có thể là do lỗi ứng dụng. Chúng tôi đã được thông báo và sẽ khắc phục
    trong thời gian sớm nhất.</p>
    <p><a href="javascript:history.back();">Trở lại trang trước</a>.</p>