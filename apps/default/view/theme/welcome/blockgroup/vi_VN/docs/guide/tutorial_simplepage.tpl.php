    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Hướng dẫn cơ bản (Tutorial) - Simple Page</h2>
    <p>Trong bài hướng dẫn này, chúng tôi sẽ cùng quý vị thực hiện các bước để tạo ra một
    trang web đơn giản gần giống như trong thực tế. Để thực hiện, quý vị sẽ sử
    dụng <code>SpicaTemplate</code> và <code>SpicaContext::getTemplate()</code>.
    </p>
    <p>Trước khi bắt đầu, chúng tôi giả định rằng quý vị đã làm quen với
    ví dụ <a href="docs/guide/page/pname/tutorial_helloworld">Hello World</a>.
    Vì vậy, chúng tôi sẽ bỏ qua các chi tiết liên quan đến thiết kế URL và cách thức
    <code>Spica</code> ánh xạ URL vào các lớp do lập trình viên định nghĩa. Tuy nhiên,
    chúng ta sẽ nhắc lại những thiết lập có được từ hướng dẫn trước.</p>
    <ul>
      <li>Thư mục gốc chứa web content là <code class="bordered">C:\webroot</code> trên Windows
      hay <code>/home/webserver/webroot</code> trên Linux.</li>
      <li>Trên máy tính phát triển của người lập trình, thư mục gốc nói trên có thể
      truy cập bằng cách gõ <code class="bordered">http://localhost</code></li>
      <li>Chúng ta giả định người lập trình sẽ giải nén code của Spica Framework vào thư mục con mang tên <code>spica</code>
      nằm trong thư mục gốc chứa web content. Do vậy, đường dẫn đầy đủ đến code Spica là <code>C:\webroot\spica</code>.</li>
      <li>Trong thư mục <code class="bordered">C:\webroot\spica\apps\default\controller\welcome\sample</code>, chúng ta
      đã có file <code class="bordered">Welcome_SamplePrint.php</code> với nội dung

<pre class="brush: php">
&lt;?php

/**
 * &quot;Hello World&quot; program controller.
 */
 class Welcome_SamplePrint
 {
     public function helloworldAction()
     {
         SpicaResponse::setBody('Hello World');
     }
 }

?&gt;

</pre>

      </li>
      <li>Chúng ta có thể chạy lớp này thông qua URL: <code>http://localhost/spica/</code><code class="o">sample/print/helloworld</code></li>
    </ul>

    <h3>Căn bản về cấu trúc ứng dụng trong Spica</h3>

    <p>Tuy nhiên để làm việc với template, tức là View trong kiến trúc MVC của Spica, quý vị cần nắm được một
    số thông tin về cách Spica cấu hình và vận hành hệ thống: </p>
    <ul>
      <li>Một trang web lập trình trên Spica đều thuộc về một package cụ thể. Danh sách các package
      được khai báo trong file <code class="bordered">apps/default/config/config.php</code> trong một
      mảng có tên là <code class="bordered">$GLOBALS['__profiles__']</code>. Mảng này gồm nhiều chỉ mục trong đó
      khóa của chỉ mục như <code>&quot;app0&quot;</code>, <code>&quot;app1&quot;</code> ...
      chính là tên của package.
      <p>Spica chỉ cho phép dùng một package tại một thời điểm. Package được sử dụng
      phải được gán vào giá trị của <code class="bordered">$GLOBALS['__state__']['profile']</code>.
      Ví dụ:</p>
<pre class="brush: php; highlight: 3">
$GLOBALS['__state__'] = array(
// USER: The name of the context in use.E.x: app1. You can change it
  'profile'          =&gt; 'app0',
// USER: Final exception should be handled by default settings or not
</pre>
     <p>Khi quý vị đọc hướng dẫn này thì cũng là lúc package có tên là <code>app0</code> được cấu hình giống
như những gì mà chúng ta nhìn thấy ở trên. Package <code>&quot;app0&quot;</code> là package mặc định của Spica Framework có các thông số
sau đây:</p>
<pre class="brush: php; highlight: 10;">
$GLOBALS['__profiles__'] = array(
// Context 'app0' - default Spica framework context
  'app0' =&gt; array(
     'function_namespace' =&gt; 'spica',
     'default_package'    =&gt; 'welcome',
     'default_module'     =&gt; 'home',
     'default_controller' =&gt; 'index',
     'default_action'     =&gt; 'index',
     'backup_route'       =&gt; array(),
     'theme'              =&gt; 'welcome',
     'image_base_path'    =&gt; null, // null means using the default path (theme specific)
     'js_base_path'       =&gt; null, // null means using the default path (theme specific)
     'css_base_path'      =&gt; null, // null means using the default path (theme specific)
     'flash_base_path'    =&gt; null, // null means using the default path (theme specific)
     'debug'              =&gt; false,
     'locale_string'      =&gt; 'vi_VN',
     'locale_charset'     =&gt; 'UTF8',
     'error'              =&gt; 'e1'),
</pre>
      <p>Xin chú ý đến dòng thứ 10 (dòng tô đậm). Đó là dòng khai báo thông tin về View tức là các
      vấn đề có liên quan đến template mà chúng ta đang quan tâm. Dòng này chỉ ra rằng với package <code>&quot;app0&quot;</code> thì theme
      đang được sử dụng có tên là <code>welcome</code>. Theme trong Spica là một thư mục chứa một tập hợp các file và thư mục con
      xác định một phong cách trình bày trang nhất quán dùng cho toàn bộ site. Chúng ta sẽ tìm hiểu sâu hơn về theme
      trong các bài hướng dẫn sau.</p>
      <p>Toàn bộ các file gói trong theme có tên là <code>welcome</code> theo quy định của Spica
      sẽ được tìm thấy ở <code class="bordered">apps/default/view/theme/welcome</code></p>
      </li>
      <li>Trang HTML template mà chúng ta sẽ tạo ra sẽ nằm trong thư
      mục <code class="bordered">apps/default/view/theme/welcome/page</code>. </li>
    </ul>
    <h3>Thực hiện viết code</h3>
    <p>Bây giờ chúng ta sẽ sửa đổi phương thức <code class="bordered">helloworldAction()</code> để tạo ra một trang web đơn giản sử dụng template. Kết quả cuối
    cùng trông như sau: </p>
<pre class="brush: php;highlight: 11">
&lt;?php

/**
 * &quot;Hello World&quot; program controller.
 */
 class Welcome_SamplePrint
 {
     public function helloworldAction()
     {
         $template = SpicaContext::getTemplate();
         $template-&gt;pageTitle = 'Simple Page';
         SpicaResponse::setBody($template-&gt;fetch('index'));
     }
 }

?&gt;
</pre>
    <p>Phương thức <code>SpicaContext::getTemplate()</code> giúp tạo ra một đối tượng <code>SpicaTemplate</code>.
    Dòng <code>$template-&gt;pageTitle = 'Simple Page';</code> giúp gán một giá trị vào cho thuộc tính <code>pageTitle</code>
    của đối tượng này. Mã <code>$template-&gt;fetch('index')</code> sẽ đọc nội dung
    của file template có tên là <code>index.tpl.php</code>, đánh giá các mã PHP có
    trong file đó và trả lại mã HTML. Hàm <code>SpicaResponse::setBody()</code> sẽ
    giúp gửi mã HTML về cho trình duyệt.</p>
    <p>Tuy nhiên, trước khi kiểm tra mã của chúng ta có chạy được hay không bằng cách duyệt
    địa chỉ <code>http://localhost/spica/</code><code class="o">sample/print/helloworld</code>, chúng ta
    cần thực hiện một số thao tác sau đây.</p>
    <ul>
      <li>Vào thư mục <code class="bordered">apps/default/view/theme/welcome/page</code> mà chúng ta đã nhắc đến ở trên
      và tạo ra một file template có tên là <code>index.tpl.php</code> có nội dung

<pre class="brush:php; highlight: 4">
&lt;!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"&gt;
&lt;html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"&gt;
&lt;head&gt;
&lt;title&gt;&lt;?php echo $this-&gt;pageTitle; ?&gt;&lt;/title&gt;
&lt;meta http-equiv="Content-Type" content="text/html; charset=utf-8" /&gt;
&lt;link rel="stylesheet" href="public/spica/css/global.css" type="text/css" media="all" /&gt;
&lt;/head&gt;
&lt;body&gt;

&lt;h1 class="pageHeader"&gt;Welcome to Spica Framework!&lt;/h1&gt;

&lt;p&gt;Spica is an easy to learn and an ultra light PHP5 Framework.&lt;/p&gt;

&lt;/body&gt;
&lt;/html&gt;
</pre></li>
     <li>Bây giờ duyệt đến địa chỉ <code>http://localhost/spica/</code><code class="o">sample/print/helloworld</code>, quý vị
     sẽ thấy một trang web với page title là Simple Page hiện lên. Hẳn là quý vị đã chú ý
     đến dòng mã thứ 4 ở trên đã được in đậm. Đó chính là chỗ giá trị của pageTitle (mà
     lúc trước chúng ta đã gán) được in ra.</li>
    </ul>
    <h3>Kết luận</h3>
    <p>Như vậy quý vị đã làm quen với cách tạo trang và gán biến vào template file. Tuy nhiên,
    template là cơ chế đơn giản nhất mà Spica hỗ trợ. Trong <a href="docs/guide/page/pname/tutorial_simpletheme">bài sau</a>,
    quý vị sẽ làm quen với Spica theme, một cơ chế cho phép nhóm các template
    file có cùng phong cách hay tổ chức giao diện thành một thực thể. Đây là cách
    thiết kế và trình bày site rất phổ biến khi làm việc trên các ứng dụng web có
    độ phức tạp trung bình đến khó mà chúng ta thường thấy ở Drupal,
    Joomla hay các PHP CMS nổi tiếng thế giới khác.</p>

    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>
