    <h1 class="pageHeader">Spica Framework - Manual - Basic guide</h1>

    <h2 class="header">Hướng dẫn cơ bản (Tutorial)</h2>
    <ul>
      <li><a href="docs/guide/helloworld" class="hasIcon">Viết ứng dụng đơn giản với Hello World</a></li>
      <li><a href="docs/guide/simplepage" class="hasIcon">Viết ứng dụng đơn giản có 1 trang template</a></li>
      <li><a href="docs/guide/simpletheme" class="hasIcon">Viết ứng dụng đơn giản có dùng layout và theme</a></li>
      <li><a href="docs/guide/configfile" class="hasIcon">Tìm hiểu về file cấu hình</a></li>
    </ul>
