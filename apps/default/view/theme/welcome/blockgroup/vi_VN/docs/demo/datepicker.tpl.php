    <h1 class="pageHeader">Spica Framework - Manual - Demo - Date Picker</h1>

    <?php echo $this->fetchWidget('spica.DatePickerWidget'); ?>
    <?php echo $this->fetchWidget('DatePickerWidget', array(), SpicaContext::getBasePath().'/library/spica/core/view/ui/DatePickerWidget.tpl.php'); ?>

    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>