    <h1 class="pageHeader">Spica Framework Introduction</h1>
    <p>Spica là một PHP web framework nguồn mở được xây dựng trên mô hình MVC, sử dụng kĩ thuật lập trình
    hướng đối tượng (OO) và hàm (functional). Yêu cầu tối thiểu để sử dụng Spica là PHP 5.2.6, tốt hơn là
    PHP 5.3 và các bản cao hơn.</p>
    <p>Spica mang theo một triết lý riêng về tổ chức ứng dụng web từ mô hình HMVC (MVC-pull) cho đến kiến trúc
    event-oriented, từ khái niệm multiple profile application cho đến kiến trúc provider-based. Vì
    thế, Spica mang theo nhiều điểm khác biệt so với các PHP framework đương đại. </p>
    <p>Các tính năng nổi bật của Spica gồm có:</p>
    <div class="box em bordered">
      <ul>
        <li>Tốc độ xử lý rất nhanh so với các PHP framework đương đại nhờ kiến trúc thông minh và code được tối ưu cao.</li>
        <li>Dễ sử dụng</li>
        <li>Kiến trúc HMVC hiện đại: mô hình dữ liệu (Model), điều khiển ứng dụng (Controller), chức năng hiển thị (View) đồng thời cho
        phép View có quyền chủ động hơn nhằm tăng khả năng sử dụng lại View logic.</li>
        <li>Kiến trúc plugin - giúp mở rộng tính năng hay thay đổi cách hoạt động mặc định của chu kỳ ứng dụng</li>
        <li>Kiến trúc View mạnh mẽ hỗ trợ khả năng theming, từng theme có thể có nhiều layout</li>
        <li>Hệ thống Database Access Layer mạnh mẽ, nhanh dựa theo tổ chức của mô hình JDBC</li>
        <li>Hỗ trợ tạo nhiều database connection đến các database server khác nhau trên cùng một request và một hệ thống API</li>
        <li>Kiến trúc Spica có thể được test thông qua Unit Testing một cách dễ dàng</li>
        <li>Mô hình validation đủ mạnh để xử lý nhiều tình huống phức tạp</li>
        <li>Mô hình authentication và authorization linh hoạt, hỗ trợ nhiều back-ends</li>
        <li>Thư viện hỗ trợ caching phong phú: file, memcached, xcache ...</li>
        <li>Mô hình xử lý lỗi thống nhất (PHP notice, warning, fatal eror, exception ...)</li>
        <li>Có khả năng tổ chức ứng dụng thành các stage: development, testing, production</li>
      </ul>
    </div>
    <h2 class="header">Yêu cầu hệ thống</h2>
    <p>Để hệ thống vận hành được, yêu cầu tối thiểu sau cần được đáp ứng</p>
    <ul>
      <li>Web server: Apache với module mod_rewrite được bật, Nginx, Lighttpd</li>
      <li>PHP 5.2.6 trở lên</li>
    </ul>
    <p>Khuyến cáo</p>
    <ul>
      <li>Web server
        <ul>
          <li>Apache 2.2.11 kết hợp với PHP như là một module</li>
          <li>Nginx chạy ở chế độ FastCGI</li>
          <li>Lighttpd chạy ở chế độ FastCGI</li>
        </ul>
      </li>
      <li>PHP 5.2.6 trở lên có bật APC</li>
    </ul>
    <h2 class="header">Cài đặt Spica User Manual</h2>
    <p>Để ứng dụng Spica User Manual hoạt động bình thường, quý vị thực hiện một số thao tác sau:</p>
    <ul>
      <li>Vào <code>tools/</code> và chạy <code>vendor_unzip.bat</code> nếu quý vị
      dùng Windows hoặc <code>vendor_unzip.sh</code> nếu quý vị dùng Unix/Linux.</li>
      <li>Để có thể sử dụng được tính năng add comment cho từng bài hướng dẫn, quý vị hãy vào <code>docs/sql/spicadocs.sql</code> vào tạo một database có
    tên là <code>spicadocs</code> với các bảng như mô tả bên trong. Sau đó, quý vị
    mở file <code>app/config/config.php</code> và tìm đến

<pre class="brush: php;highlight: [4, 5, 6, 7]">
  'spicadocs' =&gt; array(
     'type'       =&gt; 'db',
     'dbhost'     =&gt; 'localhost',
     'db'         =&gt; 'spicadocs',
     'dbuser'     =&gt; 'root',
     'dbpassword' =&gt; '123456',
     'dbvendor'   =&gt; 'mysql',
     'dbcharset'  =&gt; 'utf8',
     'dbport'     =&gt; '3306',
     'dbsocket'   =&gt; null,
     'dbflag'     =&gt; null,
     'dbssl'      =&gt; null,
     'dbreport'   =&gt; 'all'),
);

</pre>
     Hãy chỉnh lại các thông số cần thiết để truy cập database server mà bạn vừa thiết lập.

      </li>
    </ul>
