    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Khái niệm Controller</h2>
    <p>Controller đóng vai trò chữ C trong mô hình MVC. Về lý thuyết, Controller
    là nơi diễn ra các logic điều khiển luồng ứng dụng. Một số hoạt động thường 
    thấy của Controller là </p>
    <ul>
      <li>Tạo form, gửi message đến form để yêu cầu validate dữ liệu</li>
      <li>Tạo các service liên quan đến nghiệp vụ ứng dụng, yêu cầu service class tương tác với nguồn dữ liệu để
    trả về hay thay đổi trạng thái dữ liệu: thực hiện các thao tác chuyển đổi dữ liệu, 
    kiểm tra quyền truy cập trên một hoạt động cụ thể, tương tác với database, tương tác với
    các web services</li>
      <li>Tạo view object, gán các nguồn dữ liệu lấy được từ service object vào cho view.</li>
    </ul>

    <p>Trong bài <a href="docs/guide/page/pname/tutorial_appstructure" class="hasIcon">Các khái
    niệm về cấu trúc ứng dụng và thư mục</a>, chúng ta đã biết rằng trong Spica <code>action</code>
    chính là nơi trang được phục vụ chính. Theo lẽ tự nhiên, đó cũng là nơi mà
    chức năng Controller được thể hiện. </p>

    <p>Đây chính là Controller mẫu để sinh ra dòng "Hello World" mà chúng ta đã cùng nhau
    xây dựng trong bài <a href="docs/guide/page/pname/tutorial_helloworld" class="hasIcon">Viết ứng dụng đơn giản với Hello World</a></p>
<pre class="brush: php">
&lt;?php

/**
 * &quot;Hello World&quot; program controller.
 */
 class Welcome_SamplePrint
 {
     public function helloworldAction()
     {
         SpicaResponse::setBody('Hello World');
     }
 }

?&gt;

</pre>
    <p>Trong Spica, Controller bao hàm việc tổ chức các action có liên quan thành một nhóm 
    theo cách phân loại của chính developer. Đối với ví dụ trên, <code>Welcome_SamplePrint</code> 
    là một Controller class có tên là <code>Print</code> và namespace của nó là 
    <code>Welcome_Sample</code>. (Xem bài <a href="docs/guide/page/pname/tutorial_appstructure" class="hasIcon">Khái quát về cấu trúc ứng dụng và thư mục</a>)
    </p>

    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>