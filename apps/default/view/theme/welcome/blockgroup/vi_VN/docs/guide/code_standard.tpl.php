    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Các chuẩn và quy tắc - Quy tắc viết mã</h2>
    <p>Tuân thủ theo quy tắc viết code chung là cách dễ nhất để mọi người có thể hiểu được
    code của nhau. </p>
    <h3>Quy tắc đặt tên</h3>
    <p>Tuyệt đối không dùng Hungarian notation để đặt tên cho lớp, biến. Ví dụ</p>
<pre class="brush: php">
$strUserAddress; // incorrect
$intCounter; // incorrect
$objString(); // incorrect

class clsMyClass // incorrect
{
}
</pre>
    <h4>Tên namespace</h4>
    <p>Tên namespace luôn viết thường, có thể dùng underscore (<code>_</code>) nhưng hạn chế.</p>
    <h4>Tên hàm</h4>
    <p>Tên hàm viết thường, bắt đầu bằng tiền tố <code>spica_</code>. Sau đó có thể là
    một danh từ chỉ package của function đó. Ví dụ:</p>
<pre class="brush: php">
spica_html_quote(); // package html
</pre>
    <h4>Tên lớp</h4>
    <p>Tên lớp viết hoa chữ cái đầu tiên và tuân thủ nguyên tắc <code>CamelCase</code>. Tên lớp bắt đầu
    bằng tiền tố <code>Spica</code> (không khuyến cáo sau khi PHP 5.3 trở nên phổ biến). Ví dụ:</p>
<pre class="brush: php">
SpicaSession;
SpicaPageLayout;
</pre>
    <h4>Tên phương thức</h4>
    <p>Tên phương thức tuần thủ theo nguyên tắc <code>camelCase</code>. Tên phương thức bắt đầu
    bằng chữ viết thường. Các từ ghép liền lại với nhau và viết chữ cái đầu tiên trong các
    từ là chữ hoa. Phương thức protected và private cần có một kí hiệu underscore đằng trước. Ví dụ:</p>
<pre class="brush: php">
&lt;php

class SpicaHtml
{
    public function createNewInstance() // correct
    {

    }

    public function cleanAll() // correct
    {

    }

    public function CleanAll() // incorrect
    {

    }

    public function clean_all() // incorrect
    {

    }

    protected function _cleanNonArrayParameter() // correct
    {

    }

    protected function cleanNonArrayParameter() // incorrect
    {

    }
}

?&gt;
</pre>
    <h4>Tên thuộc tính của lớp</h4>
    <p>Tuân thủ theo nguyên tắc đặt tên của phương thức.</p>
<pre class="brush: php">

&lt;php

class SpicaHtml
{
    public $excludedTags = array(); // correct
    protected $_unsafeCharacters = array(); // correct
    public $_newobject; // incorrect
    public $_new_object; // incorrect
}

?&gt;
</pre>
    <h4>Tên biến, tham số và tên thuộc tính của lớp</h4>
    <p>Tên biến, tham số hay thuộc tính lớp tuân thủ nguyên tắc <code>camelCase</code> nghĩa
    là bắt đầu bằng chữ thường, không chứa ký hiệu underscore (<code>_</code>). Ví dụ:</p>
<pre class="brush: php">

&lt;php

$packageName = 'spica'; // correct
$pageLayout = 'spica2'; // correct
$page_layout = 'spica2'; // incorrect

function spica_html_clean($excludedTags) // correct
{

}

?&gt;
</pre>
    <h4>Tên hằng và hằng lớp</h4>
    <p>Luôn dùng chữ hoa toàn bộ để đặt tên cho các hằng. Ví dụ: <code></code></p>
<pre class="brush: php">
&lt;php

define('MY_CONSTANT', 'foo/bar');

class SpicaHtml
{
   const HTML_QUOTES = '"';
}

?&gt;
</pre>
    <h3>Quy tắc về dấu ngoặc <code>()</code> và <code>{}</code></h3>
    <p>Các dấu ngoặc nhọn <code>{</code> và <code>}</code> luôn nằm trên một hàng riêng.</p>
<pre class="brush: php">

&lt;php

if (count($_GET) &gt; 1) // correct
{
    $hasParameters = true;
}

if (count($_GET) &gt; 1) { // incorrect
    $hasParameters = true;
}

?&gt;
</pre>
    <p>Luôn dùng dấu ngoặc nhọn để phân biệt thân của cấu trúc điều khiển bất kể là
    số lượng câu lệnh nhỏ hay lớn. </p>
<pre class="brush: php">

&lt;php
if (count($_GET) &gt; 1) $hasParameters = true; // incorrect
if (count($_GET) &gt; 1)
    $hasParameters = true; // incorrect

if (count($_GET) &gt; 1) // correct
{
    $hasParameters = true;
}

?&gt;
</pre>
    <p>Để tăng cường tính dễ đọc, developer có thể khai báo một mảng trên nhiều hàng nhưng dấu ngoặc
    mở <code>(</code> phải nằm trên hàng đầu tiên có chứa biến mảng và các chỉ mục mảng nên thụt vào 2 spaces
    so với cột của biến mảng.</p>
<pre class="brush: php">

&lt;php

// correct
$seasons = array(
 'summer',
 'autumn',
 'winter'
);

// incorrect
$seasons = array
(
 'summer',
 'autumn',
 'winter'
);

// incorrect
$seasons = array(
'summer',
'autumn',
'winter'
);

?&gt;
</pre>
    <p>Không đặt khoảng trắng đằng sau dấu ngoặc mở <code>(</code> và trước dấu ngoặc đóng <code>)</code>.
    Ví dụ:</p>
<pre class="brush: php">
&lt;php

if (true === isset(spica_valid_integer($_GET[$variable])))    // correct
if ( true === isset( spica_valid_integer( $_GET[$variable] ) ) )  // incorrect

while ( true === $reset ) // incorrect

while (true === $reset) // incorrect

?&gt;
</pre>
    <h3>Quy tắc chữ hoa chữ thường cho các cấu trúc ngôn ngữ</h3>
    <p>Luôn dùng chữ thường cho các hàng xác định kiểu nội bộ của PHPs: <code>false</code>,
    <code>true</code> và <code>null</code>. Tương tự <code>array()</code>.
    </p>
    <h3>Toán tử (so sánh, bitwise ...) và phép gán</h3>
    <p>Luôn đặt đặt khoảng trắng đằng trước và sau các toán tử so sánh và phép gán <code>&gt;</code>, <code>&lt;</code>,
    <code>=</code>, <code>===</code>, <code>==</code> ...
<pre class="brush: php">
if (true === $var) // correct
if ( true===$var)  // incorrect

while (1 &gt; $var) // correct
while (1&gt;$var)   // incorrect

$wordList = array(); // correct
$wordList=array();   // incorrect
</pre>