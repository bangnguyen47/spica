    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Hướng dẫn chi tiết (How-to) - Caching trong các View component
    của ứng dụng trên nền Spica</h2>
    <p>View component trong Spica được biết đến dưới các tên:</p>
    <ul>
      <li>Block</li>
      <li>BlockGroup</li>
      <li>PageLayout</li>
      <li>Template</li>
    </ul>
    <p>Do được tổ chức theo các cấu trúc và phục vụ các mục đích khác nhau nên cơ
    chế caching cho View có một số điểm cần chú ý.</p>
    <h3>Cache trong Block</h3>
    <p>Trong Spica, Block là thành phần View ở đơn vị chức năng nhỏ nhất khi cần phục vụ
    hiện thị giao diện của một ứng dụng web. Vì là đơn vị chức năng, Block có thể
    tương tác với Model để tiếp cận dữ liệu dạng read-only nhằm đáp ứng như cầu chuyển
    đổi dữ liệu thành cấu trúc hiển thị phù hợp. Spica hỗ trợ một cơ chế cache cho phép
    cấu trúc hiển thị cuối cùng này có thể được lưu trữ lại cho các lần request tiếp
    theo mà không cần phải thực hiện từ đầu và lặp đi lặp lại các thao tác
    giống nhau và tốn kém tài nguyên.</p>

<pre class="brush: php">
&lt;?php

class DefaultHeaderBlock extends SpicaPageBlock
{
    protected $_templateFile = 'defaultHeader';

    public function initialize()
    {
      // Makes calls to Model classes
    }
}

?&gt;
</pre>

    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>