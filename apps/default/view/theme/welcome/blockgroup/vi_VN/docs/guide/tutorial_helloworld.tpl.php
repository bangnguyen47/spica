    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Hướng dẫn cơ bản (Tutorial) - Hello World</h2>
    <p>Chương trình in ra dòng chữ "Hello World" hết sức đơn giản dưới đây sẽ giúp
    người lập trình làm quen với cấu trúc của Spica framework. Mục tiêu của bài hướng dẫn này
    là: </p>
    <ul>
      <li>Hiểu cách thức ánh xạ URL vào các file chứa code</li>
      <li>Hiểu cách thức in nội dung ra trình duyệt theo kiến trúc của Spica.</li>
    </ul>
    <h3>Chuẩn bị</h3>
    <p>Để ví dụ "Hello World" thật đơn giản, chúng ta sẽ giả định một số thông tin như sau:</p>
    <ul>
      <li>Thư mục gốc chứa web content là <code>C:\webroot</code> trên Windows hay <code>/home/webserver/webroot</code> trên Linux</li>
      <li>Trên máy tính phát triển của người lập trình, thư mục gốc nói trên có thể
      truy cập bằng cách gõ <code>http://localhost</code></li>
      <li>Chúng ta giả định người lập trình sẽ giải nén code của Spica Framework vào thư mục con mang tên <code>spica</code>
      nằm trong thư mục gốc chứa web content. Do vậy, đường dẫn đầy đủ đến code Spica
      là <code>C:\webroot\spica</code> trên Windows hay <code>/home/webserver/webroot/spica</code> trên Linux.</li>
    </ul>

    <h3>Thực hiện</h3>
    <p>Việc thực hiện sẽ tiến hành theo 3 bước: </p>
    <ul>
      <li>Vào đến thư mục <code class="bordered">C:\webroot\spica\apps\default\controller\welcome\</code> trên Windows
      hay <code>/home/webserver/webroot/spica/apps/default/controller/welcome/</code> trên Linux và tạo ra thư mục con <code>sample</code></li>
      <li>Trong thư mục con <code>sample</code>, tạo ra file <code>Welcome_SamplePrint.php</code></li>
      <li>Mở file này bằng code editor của bạn và thêm mã như sau:

<pre class="brush: php">
&lt;?php

/**
 * &quot;Hello World&quot; program controller.
 */
 class Welcome_SamplePrint
 {
     public function helloworldAction()
     {
         SpicaResponse::setBody('Hello World');
     }
 }

?&gt;

</pre>

      </li>
    </ul>
    <p>Mở trình duyệt và gõ địa chỉ <code>http://localhost/spica/</code><code class="o">sample/print/helloworld</code> và sau đó quý vị sẽ thấy kết quả.</p>
    <h3>Giải thích</h3>
    <h4>... từ thiết kế URL</h4>
    <p>Một ứng dụng web bao giờ cũng bắt đầu từ URL. Đó chính là địa chỉ để giúp người xem định vị
    được tài nguyên trên mạng. Việc tạo ứng dụng trên Spica cũng bắt đầu từ việc thiết kế URL.
    Chúng ta sẽ cùng nhau phân tích URL nói trên theo quy ước của Spica </p>
    <pre><code>sample/print/helloworld</code></pre>
    <p>URL trong Spica bao gồm 3 thành phần chính:</p>
    <ul>
      <li><code>sample</code>: Tên của module</li>
      <li><code>print</code>: Tên của controller</li>
      <li><code>helloworld</code>: Tên của action</li>
    </ul>
    <p>Khi URL gồm nhiều hơn 3 thành phần này thì chúng ta có thêm các tham số. </p>
    <h4>... đến cấu trúc ứng dụng</h4>
    <p>Khi một request có địa chỉ như thế này gửi đến Spica, lớp sau đây sẽ được tìm tới.</p>
    <pre><code class="g">C:\webroot\spica\</code><code>apps\default\</code><code class="o">controller\</code><code class="bg1">welcome\</code><code>sample\Welcome_SamplePrint.php</code></pre>
    trên Windows hay
    <pre><code class="g">/home/webserver/webroot/spica/</code><code>apps/default/</code><code class="o">controller/</code><code class="bg1">welcome/</code><code>sample/Welcome_SamplePrint.php</code></pre>
    trên Linux.
    <p>Toàn bộ đường dẫn <code class="g">C:\webroot\spica\</code><code>apps\default\</code><code class="o">controller\</code><code class="bg1">welcome\</code>
    được Spica xác định theo mặc định trong đó <code class="bg1">welcome\</code> là phần có thể cấu
    hình được. Phần đường dẫn <code>sample\Welcome_SamplePrint.php</code> là phần người lập trình
    cần tạo ra để đáp ứng request chứa URL như trên. </p>
    <p>Như vậy chúng ta đã vào thư mục <code>C:\webroot\spica\apps\default\controller\welcome\</code> (Linux:
    <code>/home/webserver/webroot/spica/apps/default/controller/welcome/</code>)
    vào tạo ra một thư mục con có tên là <code>sample</code>. Quý vị có thể để ý thấy tên thư mục
    này trùng với tên của module <code>sample</code> mà chúng ta đã nói ở trên. Trong thư mục con
    này, chúng ta đã tạo ra tiếp một file có tên là <code>Welcome_SamplePrint.php</code></p>
    <p>Quý vị cũng đã để ý file có phần tên là <code>_SamplePrint.php</code> trùng hợp một cách
    có chủ ý với tên của module <code>sample</code> và tên của controller <code>print</code>. </p>
    <p>Chúng ta hãy mở file này một lần nữa để xem code mà chúng ta vừa viết:</p>

<pre class="brush: php">
&lt;?php

/**
 * &quot;Hello World&quot; program controller.
 */
class Welcome_SamplePrint
{
    public function helloworldAction()
    {
        SpicaResponse::setBody('Hello World');
    }
}

?&gt;
</pre>

    <p>Như vậy file <code>Welcome_SamplePrint.php</code> chứa một lớp trùng tên <code>Welcome_SamplePrint</code>. Trong
    lớp này, chúng ta đã định nghĩa một phương thức có tên <code>helloworldAction</code>,
    trùng hợp với tên của action <code>helloworld</code> nằm trên URL.</p>
    <p>Trong hướng dẫn này, chúng ta đã bỏ qua một số chi tiết quan trọng về tổ chức ứng dụng trong Spica.
    Để hiểu thêm các khái niệm như <code>application</code>, <code>module</code>, <code>controller</code>
    hay <code>action</code>, xin xem bài <a href="docs/guide/page/pname/tutorial_appstructure" class="hasIcon">Khái quát về cấu trúc ứng dụng và thư mục</a>.</p>
    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>