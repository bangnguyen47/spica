    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Hướng dẫn chi tiết (How-to) - Xử lý request với thành phần dispatcher</h2>
    <p>Trong Spica, Dispatcher là một module thiết yếu giúp xác định lớp sẽ xử lý request thực sự. Không có dispacher
    request từ người dùng sẽ không thể được ánh xạ đến lớp ứng dụng có chức năng xử lý nghiệp vụ thực sự
    và do vậy, sẽ không có trang web nào được sinh ra hay yêu cầu nào được xử lý theo đúng nghĩa. </p>
    <h3>Khái niệm</h3>
    <p>Dispatcher là một đối tượng được khởi tạo khi Spica context vận hành nhằm chuyển tiếp cấu trúc
    request dạng mảng $_GET do URL resolver sinh ra đến một lớp Controller. Nó có vai trò trung gian
    trong việc tiếp nhận thành phần request được chuẩn hóa từ URL resolver, phân tích đặc tả quy ước,
    tìm kiếm lớp Controller có thể xử lý request và các thao tác xử lý khác liên quan đến việc này. </p>
    <h3>Giải thích về dispatcher</h3>
    <p>SpicaDispatcher là thành phần đóng vai trò là dispatcher mặc định trong Spica. Khi Spica context vận hành, nó sẽ
    lấy đối tượng này từ <code>SpicaContext::getDispatcher()</code> ngay sau khi URL resolver phân tích URL thành
    các thành phần request và mô tả chúng trong <code>$_GET</code> dưới các khóa đặc biệt.
    Do dispatcher được khởi tạo mỗi khi có một request mới và là thành phần có thể thay thế ở thời gian thực.
    Do vậy với từng request cụ thể, lập trình viên có thể cung cấp các bản cài đặt dispatcher khác nhau cho
    hệ thống để xử lý request theo các cách tiếp cận (approach) khác nhau. Khi lập trình viên không cung cấp
    bản cài đặt dispatcher cụ thể, một đối tượng của SpicaDispatcher sẽ được sinh ra.
    Cách tiếp cận của nó là như sau:</p>
    <ul>
      <li>Tham số đầu vào của nó là <code>$_GET</code>: nó cần mảng <code>$_GET</code> này có các khóa thiết yếu là
      <code>_module</code>, <code>_controller</code> và <code>_action</code></li>
      <li>Các giá trị lấy từ các khóa <code>module</code>, <code>controller</code> và <code>action</code> trong <code>$_GET</code>
      sẽ dùng để tìm đến đường dẫn file chứa lớp Controller như sau:
        <ul>
          <li>Chúng ta sẽ giả định application có tên là <code>default</code> (xem
          bài <a href="docs/guide/page/pname/tutorial_appstructure" class="hasIcon">Khái quát về cấu trúc ứng dụng và thư mục</a>
          để có thêm thông tin về cách tổ chức ứng dụng trong Spica)</li>
          <li>Thư mục bắt đầu: <code>apps/default/controller</code></li>
          <li>Thư mục chứa package: <code>apps/default/controller/</code><code class="bg1">packageName</code></li>
          <li>Thư mục chứa module cũng là thư mục chứa các lớp Controller: <code>apps/default/controller/</code><code class="bg1">packageName/</code><code class="o">modulevalue</code></li>
          <li>File chứa lớp Controller: <code>apps/default/controller/</code><code class="bg1">packageName/</code><code>modulevalue/</code><code class="g">PackageName_ModulevalueControllervalue.php</code>.
          Quy tắc tìm kiếm file như sau: lấy <code>apps/default/controller/</code> làm đường dẫn gốc, với tên của package và module name,
          tìm tiếp <code>packageName/modulevalue/</code>, trong thư mục con này tìm đến 1 file có tên theo quy ước:
            <ul>
              <li>Bắt đầu bằng tên của package với chữ cái đầu tiên viết hoa: <code>packageName</code> thành <code>PackageName</code></li>
              <li>Tiếp theo là một gạch nối <code>_</code></li>
              <li>Tiếp theo là tên của module với chữ cái đầu tiên viết hoa</li>
              <li>Tiếp theo là tên của controller với chữ cái đầu tiên viết hoa</li>
              <li>Tiếp theo là phần mở rộng của file <code>.php</code></li>
            </ul>
          </li>
        </ul>
      </li>
      <li>Dispatcher tiếp tục tìm kiếm khai báo lớp trùng tên với tên file: <code>class PackageName_ModulevalueControllervalue { }</code> </li>
      <li>Nếu lớp Controller đó tồn tại thì lấy giá trị của khóa <code>_action</code> trong <code>$_GET</code> và xác định đó là
      tên method của lớp Controller. Thực hiện lời gọi đến phương thức này nếu có.</li>
    </ul>
    <p>Đây là cách tiếp cận mặc định của Spica. Như đã nói ở trên, đối tượng cụ thể của một cài đặt SpicaDispatcher chỉ được
     gọi ra sau khi URL resolver làm xong việc của nó là tạo ra <code>$_GET</code> theo quy ước. Lập trình viên có thể thay
     đổi đối tượng mặc định của hệ thống bằng cách khai báo một class dẫn xuất từ <code>SpicaDispatcher</code>, khởi tạo nó trong
     <code>context.php</code> và đăng ký đối tượng với Spica context bằng cách gọi <code>SpicaContext::setDispatcher($dispatcher)</code></p>

    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>