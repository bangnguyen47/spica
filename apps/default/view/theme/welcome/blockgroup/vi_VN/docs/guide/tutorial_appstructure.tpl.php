    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Hướng dẫn cơ bản (Tutorial) - Khái quát về cấu trúc ứng dụng trong Spica</h2>
    <p>Để hiểu cách viết ứng dụng Spica, developer cần hiểu cách mà các ứng dụng được tổ chức trên framework này.
    Mục đích của bài viết này là:</p>
    <ul>
      <li>Tổng quan về cách tổ chức ứng dụng trong Spica</li>
      <li>Giải thích tổng quát về hệ thống thư mục</li>
    </ul>
    <h3>Sự tương tự giữa ứng dụng PHP truyền thống và ứng dụng trên Spica</h3>
    <p>Spica không nhìn website như là một tập các trang web rời rạc, gắn kết với nhau qua URL và hệ thống thư viện mức code.</p>
    <p>Nói một cách ngắn gọn, Spica không hỗ trợ kiểu phát triển ứng dụng tự do. Để xây dựng một ứng dụng mà
    Spica hiểu và hỗ trợ, developer sẽ phải tuân thủ một số nguyên tắc mà Spica ràng buộc: </p>
    <ul>
      <li>Nếu như ở các ứng dụng PHP phổ thông, người dùng gửi request đến một trang (page) và đó chính
      là nơi xử lý request thì ở Spica, <code>action</code> là khái niệm tương đương. Cụ thể hơn, ở các ứng dụng PHP
      truyền thống thì request sẽ được gửi đến URL <code>http://adomain.com/mypage.php</code> còn ở Spica,
      URL sẽ có dạng như <code>http://adomain.com/mymodule/mycontroller/mypage</code></li>
      <li>Trong các ứng dụng web trên PHP theo cách phát triển truyền thống, code PHP được viết vào một file chính, là file
      mà user sẽ gửi yêu cầu đến, ví dụ mypage.php thì ở Spica, developer sẽ viết code xử lý đó vào một phương thức và 
      phương thức đó sẽ được gọi để chạy khi có một URL gửi đến.</li>
    </ul>  
    <p>Như vậy về mặt bản chất, Spica không làm thay đổi PHP mà dựa vào cách phát triển truyền thống với các tính năng 
    mở rộng giúp cho việc xây dựng ứng dụng và mở rộng chúng có quy tắc chặt chẽ hơn.</p>
    <h3>Tổ chức ứng dụng trong Spica</h3>
    <p>Mỗi một website trong Spica được tổ chức thành ít nhất 1 <code>application</code>. Nghĩa là
    khi user gửi gõ một URL vào thanh địa chỉ của trình duyệt thì yêu cầu sẽ được gửi đến cho 1 application 
    nhất định trong Spica. Spica đặt tên cho application mặc định là <code>default</code>. Tên của application
    sẽ phục vụ request được cấu hình trên file có tên là <code>init.php</code> nằm cạnh <code>index.php</code>. 
    Về mặt tổ chức file, application này nằm trong một thư mục con, cùng tên, của thư mục <code>apps</code>. 
    Spica hiểu mỗi thư mục con của <code>apps</code> là dấu hiệu của một application.</p>
    
    <div class="image">
      <img src="public/spica/screenshot/spica-app-structure1.png" />
      <div class="caption">Hình 1: Cấu trúc thư mục của một Spica application</div>
    </div>
        
    <p>Mỗi <code>application</code> trong Spica có liên quan đến một khái niệm trừu tượng 
    có tên là <code>profile</code>. Profile là tổ chức cấu hình có tên cho phép mỗi một application 
    thay đổi cách hoạt động của nó khi xử lý request: các cấu hình mặc định, đường dẫn .... Thông số 
    của từng profile được lưu trữ trong <code>apps/</code><code class="g">my_app</code><code>/config/config.php</code>. 
    Khi thay đổi tên gọi của profile, <code>application</code> của Spica sẽ thay đổi cách xử lý của nó. </p>
    <p>Thành phần xử lý request để sinh trang hay dữ liệu chính trong <code>application</code> là Controller, 
    chính là chữ C trong mô hình MVC, cho phép nhóm các action, nơi xử lý request cho từng trang cụ thể, có liên quan dưới 
    cùng một tên gọi. Controller là một lớp PHP khái báo bên trong một file <code>.php</code> cùng tên 
    nằm trong một tổ chức thư mục bên trong <code>apps/</code><code class="g">my_app</code><code>/controller</code>. 
    Controller được chia thành các <code>module</code>, bên trên module là <code>package</code>. </p>
    <p><code>action</code> chỉ là một kiểu method trong lớp này, nơi diễn ra các  xử lý để trả về 
    response cho một request cụ thể. Mỗi action tương ứng với một trang trong một website.</p>
    
    <div class="image">
      <img src="public/spica/screenshot/spica-controller-structure.png" />
      <div class="caption">Hình 2: Cấu trúc thư mục của Spica controller</div>
    </div>    

    <p>Từ góc độ người duyệt web, họ sẽ chỉ nhìn thấy <code>module</code>, <code>controller</code> và <code>action</code> vì đây là 
    các thành phần sẽ tham gia cấu tạo nên URL. Các thành phần như như <code>application</code> hay <code>package</code>
    là các tổ chức code ở mức khái niệm dành cho developers và solution architects. <code>package</code> được quản lý qua 
    mô hình <code>profile</code> được điều khiển bởi <code>apps/</code><code class="g">my_app</code><code>/config/config.php</code>.
    Trong khi đó, <code>application</code> được điều khiển bởi <code>init.php</code> nằm bên cạnh <code>index.php</code>.
    Điều đó có nghĩa là:
    </p>
    <ul>
      <li>Website truyền thống: trang web được phục vụ khi có request đến <code>http://mydomain.com/mypage.php</code> 
      hay <code>http://mydomain.com/subdirectory1/mypage2.php</code></li>
      <li>Website trên Spica: <code>http://mydomain.com/module1/controller1/mypage</code> hay 
      <code>http://mydomain.com/module1/controller1/mypage2</code> trong đó mỗi một 
      module như <code>module1</code> lại thuộc về một <code>package</code> và một application theo cách 
      tổ chức ứng dụng của developer. User khi nhìn từ góc độ URL chỉ nhận thấy sự tồn tại của 
      module <code>module1</code>, controller có tên <code>controller1</code> và action mang 
      tên <code>mypage</code> mà thôi.</li>
    </ul>
    
    <h3>Tổ chức thư mục</h3>
    
    <p>Mỗi <code>application</code> là một tập hợp các file và thư mục có tổ chức chặt chẽ. Nó gồm 2 phần:</p>
    <ul>
      <li>Cấu trúc thư mục chứa code xử lý phía server nằm trong thư mục <code>apps</code> (Hình 1)</li>
      <li>Cấu trúc các file tĩnh như: image, JavaScript, Flash, CSS ... nằm trong thư mục <code>public</code> (Hình 3)</li>
    </ul>
    <p>Cấu trúc thư mục chứa code bao gồm các thư mục định nghĩa sẵn như sau:</p>
    <ul>
      <li><code>apps</code>: base directory for all applications
        <ul>
          <li><code>default</code>: Application named: default
            <ul>
              <li><code>block</code>: Nơi chứa các class quản lý việc sinh ra các page block, 
              đơn vị cấp ứng dụng nhỏ nhất để tạo ra một vùng HTML trên trang</li>
              <li><code>blockgroup</code>: Nơi chứa các class quản lý một vùng HTML trên trang tương tự như 
              page block nhưng thường là nơi chứa các page block khác và tổ chức chúng thành một trật tự
              phục vụ việc trình bày trang.</li>
              <li><code>config</code>: Nơi chứa các file cấu hình</li>
              <li><code>controller</code>: Nơi chứa các class thuộc về Controller tức là code để xử lý request từ user.</li>
              <li><code>event</code>: Nơi chứa các class xử lý sự kiện</li>
              <li><code>exception</code>: Nơi chứa các Exception class</li>
              <li><code>filter</code>: Nơi chứa các class xử lý việc lọc hay chuyển đổi dữ liệu khi có sự 
              kiện tương ứng xuất hiện.</li>
              <li><code>form</code>: Nơi chứa các class quản lý việc validate HTML form.</li>
              <li><code>helper</code>: Nơi chứa các helper class</li>
              <li><code>lib</code>: Nơi chứa các thư viện dùng để xử lý riêng cho ứng dụng</li>
              <li><code>locale</code>: Nơi chứa các file hỗ trợ việc dịch text từ locale này sang locale khác.</li>
              <li><code>model</code>: Nơi chứa các domain class</li>
              <li><code>security</code>: Nơi chứa các class xử lý dịch vụ liên quan đến security</li>
              <li><code>service</code>: Nơi chứa service class.</li>
              <li><code>session</code></li>
              <li><code>view</code>: Nơi chứa các class xử lý template gồm helper và filter cũng như các template files
                <ul>
                  <li><code>view/theme</code>: Mỗi thư mục con của thư mục này là một theme, gồm tập hợp các
                  file template.</li>
                </ul>
              </li>
              <li><code>ui</code>: Nơi chứa các class xử lý hiển thị các widget. </li>
            </ul>  
          </li>       
        </ul>      
      </li>
    </ul>
    <div class="image">
      <img src="public/spica/screenshot/spica-app-structure.png" />
      <div class="caption">Hình 3: Cấu trúc thư mục của một Spica application</div>
    </div>

    <p>Chi tiết về chức năng và vai trò của từng bộ phận trong một Spica application sẽ được nói rõ hơn trong các bài sau.</p>
    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>