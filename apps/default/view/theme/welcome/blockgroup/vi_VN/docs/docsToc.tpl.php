    <h1 class="pageHeader">Spica Framework Manual</h1>
    <h2 class="header">Căn bản</h2>
    <div id="mainSections">
      <div class="section column1">
        <h2>Tổng quan</h2>
        <ul>
          <li><a href="docs/guide/page/pname/overview_features" class="hasIcon">Mô tả các tính năng</a></li>
          <li><a href="docs/guide/page/pname/overview_comparison" class="hasIcon">So sánh các framework</a></li>
          <li><a href="docs/guide/page/pname/overview_changelog" class="hasIcon">Thông tin về sự thay đổi trong framework giữa các phiên bản</a></li>
          <li><a href="docs/guide/page/pname/overview_roadmap" class="hasIcon">Lộ trình phát triển</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Cài đặt</h2>
        <ul>
          <li><a href="docs/guide/page/pname/system_requirements" class="hasIcon">Yêu cầu tối thiểu</a></li>
          <li><a href="docs/system_download" class="hasIcon">Tải về</a></li>
          <li><a href="docs/guide/page/pname/system_install" class="hasIcon">Cài đặt</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="section column1">
        <h2>Hướng dẫn cơ bản (Tutorial)</h2>
        <ul>
          <li><a href="docs/guide/page/pname/tutorial_appstructure" class="hasIcon">Khái quát về cấu trúc ứng dụng và thư mục</a></li>
          <li><a href="docs/guide/page/pname/tutorial_helloworld" class="hasIcon">Viết ứng dụng đơn giản với Hello World</a></li>
          <li><a href="docs/guide/page/pname/tutorial_simplepage" class="hasIcon">Viết ứng dụng đơn giản có 1 trang template</a></li>
          <li><a href="docs/guide/page/pname/tutorial_simpletheme" class="hasIcon">Viết ứng dụng đơn giản có dùng layout và theme (căn bản)</a></li>
          <li><a href="docs/guide/page/pname/tutorial_advancedtheme" class="hasIcon">Viết ứng dụng đơn giản có dùng layout và theme (nâng cao)</a></li>
          <li><a href="docs/guide/page/pname/tutorial_configuration" class="hasIcon">Tìm hiểu về file cấu hình</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Các khái niệm trong Spica</h2>
        <ul>
          <li><a href="docs/guide/page/pname/concept_controller" class="hasIcon">Khái niệm về Controller</a></li>
          <li><a href="docs/guide/page/pname/concept_view" class="hasIcon">Các khái niệm về View: Theme, Template, Layout, Block, BlockGroup</a></li>
          <li><a href="docs/guide/page/pname/concept_model" class="hasIcon">Các khái niệm về Model và Service</a></li>
          <li><a href="docs/guide/page/pname/concept_error" class="hasIcon">Khái niệm về Error và Error Listener</a></li>
          <li><a href="docs/guide/page/pname/concept_event" class="hasIcon">Khái niệm về Event Listener</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>

      <h2 class="header">Hướng dẫn chi tiết (How-to)</h2>
      <div class="section column1">
        <h2>Xử lý request</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_dispatcher" class="hasIcon">Làm việc với request dispatcher</a></li>
          <li><a href="docs/guide/page/pname/howto_session" class="hasIcon">Làm việc với Session</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Controller và Response</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_controller" class="hasIcon">Khai báo Controller và xử lý request trong controller</a></li>
          <li><a href="docs/guide/page/pname/howto_controller_ajax" class="hasIcon">Xử lý Ajax request và XML/JSON response</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="section column1">
        <h2>Authentication và authorization</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_auth" class="hasIcon">Authentication và login form</a></li>
          <li><a href="docs/guide/page/pname/howto_authorization" class="hasIcon">Authorization và quản lý quyền truy cập</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Template, Layout, Block và Block Group</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_template" class="hasIcon">Làm việc với single page: Template</a></li>
          <li><a href="docs/guide/page/pname/howto_block_group" class="hasIcon">Làm việc với block và block group</a></li>
          <li><a href="docs/guide/page/pname/howto_layout" class="hasIcon">Làm việc với layout và theme</a></li>
          <li><a href="docs/guide/page/pname/howto_widget" class="hasIcon">Làm việc với Widget</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="section column1">
        <h2>Truy vấn database</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_databaseservice" class="hasIcon">Viết các lớp truy vấn dữ liệu</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Form và Validation Framework</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_form" class="hasIcon">Làm việc với các form</a></li>
          <li><a href="docs/guide/page/pname/howto_validation" class="hasIcon">Kiểm tra form với validation framework</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="section column1">
        <h2>Caching</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_filecaching" class="hasIcon">File caching</a></li>
          <li><a href="docs/guide/page/pname/howto_pagecaching" class="hasIcon">Thực hiện page caching</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Filter và Helper</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_errorlogging" class="hasIcon">Sử dụng MailService</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="section column1">
        <h2>Website đa ngôn ngữ với l10n và i18n</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_localization" class="hasIcon">Localization</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Mail, SMTP</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_errorlogging" class="hasIcon">Sử dụng MailService</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="section column1">
        <h2>Xử lý lỗi và exception</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_errorlogging" class="hasIcon">Lỗi, xử lý lỗi và logging</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Unit Testing</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_errorlogging" class="hasIcon">Sử dụng MailService</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="section column1">
        <h2>Xử lý event</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_event" class="hasIcon">Xử lý các event trong hệ thống</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Mã hóa</h2>
        <ul>
          <li><a href="docs/guide/page/pname/howto_errorlogging" class="hasIcon">Sử dụng MailService</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>

      <h2 class="header">Phát triển Spica</h2>
      <div class="section column1">
        <h2>Các chuẩn và quy tắc</h2>
        <ul>
          <li><a href="docs/guide/page/pname/code_standard" class="hasIcon">Quy tắc viết mã</a></li>
          <li><a href="docs/guide/page/pname/installation" class="hasIcon">Khuyến cáo cài đặt</a></li>
          <li><a href="docs/guide/page/pname/development" class="hasIcon">Khuyến cáo tổ chức ứng dụng</a></li>
          <li><a href="docs/guide/page/pname/deploynment" class="hasIcon">Khuyến cáo quy tắc phát triển và triển khai ứng dụng</a></li>
        </ul>
      </div>
      <div class="section column2">
        <h2>Tham gia phát triển Spica</h2>
        <ul>
          <li><a href="docs/guide/page/pname/subversion" class="hasIcon">Các quy tắc thực hành trên Subversion</a></li>
          <li><a href="docs/guide/page/pname/versioning" class="hasIcon">Quy ước về đánh số phiên bản</a></li>
          <li><a href="docs/guide/page/pname/unit_test" class="hasIcon">Quy ước về viết Unit Test Cases</a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
    </div>