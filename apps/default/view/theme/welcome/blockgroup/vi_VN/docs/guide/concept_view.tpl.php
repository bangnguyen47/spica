    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Khái niệm (Concept) - View - Theme, Template, Layout, Block, BlockGroup</h2>
    <p>View đóng vai trò là chữ V trong mô hình MVC. Về mặt lý thuyết, View xử lý các công việc liên quan đến
    hiển thị trang: bao gồm nhận dữ liệu từ Controller, chuyển đổi dữ liệu về dạng View hiểu được,
    thực hiện các kiểm tra trạng thái liên quan đến việc lựa chọn cách hiển thị, đọc và sinh nội dung
    của các trang template theo yêu cầu...</p>
    <p>Trong Spica, View được chia thành nhiều thành phần nhỏ hơn nhằm đáp ứng tốt hơn
    tính chất phức tạp của nghiệp vụ và tính linh hoạt của hệ thống.</p>
    <ul>
      <li><code>Template</code>: một thành phần chỉ có chức năng hiện thị một trang web
      đơn lẻ, độc lập hay một vùng template theo chỉ định (xem <a href="docs/guide/page/pname/tutorial_simplepage" class="hasIcon">Viết ứng dụng đơn giản có 1 trang template</a>).</li>
      <li><code>Layout</code>: là một khái niệm chỉ ra rằng một số trang nhất định có thể dùng chung một kết
      cấu (xem hình 1). <code>Layout</code> cho phép developer tạo ra một số mẫu trang và dùng các
      building block như <code>Block</code> và <code>BlockGroup</code> để tạo ra các trang cụ thể
      trong từng bối cảnh. Khi tạo dựng một website, người dùng có thể chia các page thành nhiều nhóm, một nhóm page
      có chung một kiểu trình bày.</li>
      <li><code>Block</code>: một đối tượng quản lý vùng nội dung nhỏ riêng biệt trên trang web
      có thể sử dụng lại trên nhiều trang. Mỗi trang web sẽ được
      xây dựng nên từ các khối (block) nội dung có thể chứa cả thông tin tĩnh và động.
      Một khối như vậy khi được chia nhỏ đến mức không thể chứa một khối khác đều trở thành
      một <code>Block</code> trong Spica. <code>Block</code> hoàn toàn có khả năng của một <code>Template</code>
      nhưng khác biệt ở chỗ <code>Block</code> có thể điều khiển nhiều template file thay vì chỉ một. Nhờ đó nó
      có thể thay đổi linh hoạt vùng nội dung của nó trên <code>Layout</code> mà chỉ cần khai báo một lần</li>
      <li><code>BlockGroup</code>: là một đối tượng quản lý một nhóm các <code>Block</code> được bố trí theo trật tự trên một
      vùng nội dung của trang. Trong Spica, trang đôi khi cần đến các khối tạo dựng lớn
      hơn <code>Block</code>, tức là chứa nhiều <code>Block</code> nhưng tổ chức chúng theo một bố cục
      con nằm trên trang. Ví dụ vùng sidebar bên trái của trang có thể  gồm các <code>Block</code>: menu,
      sign-in box, subscriber box ... Tương tự <code>Block</code>, <code>BlockGroup</code> cũng
      là building block có thể sử dụng lại. Tuy nhiên một <code>BlockGroup</code> có thể được tạo ra
      để điều khiển chính template của nó mà trên đó không cần phải chứa <code>Block</code> nào.</li>
      <li><code>Partial</code>: là một vùng template được dùng lại. Nó có thể giúp với nội dung sinh ra từ
      một <code>Block</code> hay <code>BlockGroup</code> nhưng khác ở chỗ nó chỉ là một file tĩnh chứ
      không phải là một đối tượng quản lý các template file. Vì thế, nó không thể thay đổi nội dung
      theo từng request một cách dễ dàng.</li>
    </ul>

    <p>Ngoài là còn một khái niệm bao trùm lên là <code>Theme</code>. Nó là một khái niệm
    tổng quát hơn <code>Layout</code>. Trong khi <code>Block</code> có thể dùng cấu thành
    nên <code>BlockGroup</code> rồi đến lượt nó tạo nên <code>Layout</code> thì <code>Theme</code> cho
    phép nhóm nhiều <code>Layout</code> thành một tổng thể để trình bày cách cảm thụ về giao diện
    của toàn bộ một website dưới một tên gọi chung nào đó.</p>

    <h3>Trường hợp sử dụng</h3>
    <h4>Block</h4>
    <p>Trên thực tế, chúng ta thường hay gặp các trang web được lắp ghép nên bởi
    rất nhiều các thành phần trang khác nhau. Ví dụ, khi duyệt một website về tin tức,
    chúng ta gặp trở đi trở lại ở nhiều trang khác nhau một khung có tên
    là <strong>Bài viết mới nhất</strong> hay khung top menu và banner trên đầu trang.
    Khung kiểu như vậy trên Spica sẽ đưa vào một block. Chúng ta sẽ định nghĩa
    chúng một lần và dùng lại trên nhiều trang khác nhau mà không cần quan tâm
    nhiều bên cách làm việc bên trong.</p>
    <h4>BlockGroup</h4>
    <p>Trong một trường hợp khác, chúng ta gặp một website mà ở trang chủ chia thành 3 cột trong đó
    cột bên trái chứa nhiều khung nhỏ: khung chứa danh sách menu, khung chứa form để nhập
    email nhận thông báo về các nội dung mới nhất, khung danh sách các bài viết
    nhiều người đọc nhất. Khi chúng ta duyệt sang trang chứa danh sách các bài thuộc một category.
    Trang này chỉ chia thành hai cột nhưng cột bên trái vẫn giống hệt trang chủ. Trường hợp này
    trong Spica chúng ta đưa vào block group, cho phép chúng ta sử dụng lại cả một nhóm các
    block trên các trang khác nhau.</p>

    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>