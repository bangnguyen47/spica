    <h1 class="pageHeader">Spica Framework - Manual - Hand-on guide</h1>

    <h2 class="header">Hướng dẫn chi tiết (How-to) - Sử dụng Widget trong Spica</h2>
    <p>Widget là một thành phần ứng dụng dùng để hiển thị các điều khiển phía giao diện người dùng ví dụ như
     nút, hộp thoại, các cửa sổ pop-up, các menu thả xuống, thanh cuộn, khung cửa sổ co giãn, các thước tiến độ, form ...<p>

    <h3>Demo</h3>
    <p>Date Picker: <a href="docs/demo/page/pname/datepicker">Demo</a></p>
    <?php echo $this->fetchBlock('welcome/SpicaDocsNoteBlock'); ?>