  <div id="header" class="clearfix">
    <div id="topLogo"><a href="home">Home</a></div>
    <div id="topNav" class="nav">
      <ul class="links">
        <li class="first2"><a title="About the Spica Framework and contributors" href="about">About</a></li>
        <li><a title="Download Spica Framework" href="download">Download</a></li>
        <li><a title="API documentation, user's manual, cookbook, and SVN documentation" href="docs">Introduction</a></li>
        <li><a title="API documentation, user's manual, cookbook, and SVN documentation" href="docs/toc">User Manual</a></li>
        <li><a title="Spica community forum" href="forum">Community</a></li>
        <li><a title="Spica Developer and Community Blogs" href="blog">Blog</a></li>
      </ul>
    </div>
  </div>