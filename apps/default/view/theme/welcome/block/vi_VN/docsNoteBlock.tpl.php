    <div class="docsNotes">
    <h3>Gửi bình luận hay kinh nghiệm sử dụng</h3>
    <div id="messages"></div>
    <?php if (null !== $this->pageCheckMessage): ?>
    <div class="warning"><?php echo $this->pageCheckMessage; ?></div>
    <?php endif; ?>
    <form id="note" method="post" action="docs/note/add">
      <div>
        <label>Địa chỉ email hay tên:</label>
        <div class="right"><input type="text" name="username" id="username" size="60" maxlength="40" value="user@example.com" /></div>
        <div class="warning" id="usernameFeedback" style="display:none"></div>
      </div>
      <div>
        <label>Nội dung:</label>
        <div class="right"><textarea name="note" id="noteContent" rows="10" cols="132"></textarea>
        <div class="warning" id="noteFeedback" style="display:none"></div>
        </div>
      </div>
      <div>
        <label>&nbsp;</label>
        <div class="right">
          <input type="button" name="action" value="Preview" />
          <input type="submit" name="action" value="Add Note" />
          <input type="hidden" name="reply_to" value="" />
          <input type="hidden" name="page_id" value="<?php echo $this->pageId; ?>" />
          <div class="warning" id="pageIDFeedback" style="display:none"></div>
        </div>
      </div>
    </form>
    </div>
    <script type="text/javascript" src="public/spica/js/jquery.min.js"></script>
    <script type="text/javascript" src="public/spica/js/addnote.js"></script>
    <?php if ($this->notes->isReadable()): ?>
    <h3>Bình luận của người sử dụng</h3>
    <table class="simple" id="noteList">
    <?php foreach ($this->notes as $note): ?>
      <tr class="header">
        <th><?php echo $note->get('note_username'); ?></th>
        <th><?php echo $note->get('note_dtime'); ?></th>
      </tr>
      <tr class="even">
        <td colspan="2"><?php echo SpicaStringUtils::normalizeHtml($note->get('note_content')); ?></td>
      </tr>
    <?php endforeach; ?>
    </table>
    <?php else: ?>
    <div class="warning"><?php echo $this->notes->getMessage(); ?></div>
    <?php endif; ?>