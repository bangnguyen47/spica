<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php echo spica_view_base_tag(); ?>
<title><?php echo $this->pageTitle; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="public/spica/css/reset.css" type="text/css" media="all" />
<link rel="stylesheet" href="public/spica/css/docs.css" type="text/css" media="all" />
<link rel="stylesheet" href="public/spica/syntaxhighlighter/styles/shCore.css" type="text/css" media="all" />
<link rel="stylesheet" href="public/spica/syntaxhighlighter/styles/shThemeSpica.css" type="text/css" media="all" />
</head>
<body>
<div id="wrapper">
  <?php echo $this->fetchBlock('welcome/SpicaDocsHeaderBlock'); ?>
  <div id="main">
  <?php echo $this->fetchBody(); ?>
  </div>
  <?php echo $this->fetchBlock('welcome/SpicaDocsFooterBlock'); ?>
</div>
<script type="text/javascript" src="public/spica/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="public/spica/syntaxhighlighter/scripts/shBrushPhp.js"></script>
<script type="text/javascript" src="public/spica/syntaxhighlighter/scripts/shBrushSql.js"></script>
<script type="text/javascript" src="public/spica/syntaxhighlighter/scripts/shBrushCss.js"></script>
<script type="text/javascript" src="public/spica/syntaxhighlighter/scripts/shBrushBash.js"></script>
<script type="text/javascript" src="public/spica/syntaxhighlighter/scripts/shBrushPlain.js"></script>
<script type="text/javascript" src="public/spica/syntaxhighlighter/scripts/shBrushJScript.js"></script>
<script type="text/javascript">
  SyntaxHighlighter.config.clipboardSwf = 'public/spica/syntaxhighlighter/scripts/clipboard.swf';
  SyntaxHighlighter.all();
</script>
</body>
</html>