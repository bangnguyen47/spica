<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php echo spica_view_base_tag(); ?>
<title><?php echo $this->pageTitle; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="public/spica/css/global.css" type="text/css" media="all" />
</head>
<body>

<h1 class="pageHeader">Welcome to Spica Framework!</h1>

<p>Spica is an easy to learn and an ultra light PHP5 Framework.</p>

<p>If you would like to edit this page you'll find it located at:</p>
<div style="margin-bottom: 1em; clear: right;" class="code bordered">
<span>apps/default/view/theme/welcome/layout/index.tpl.php</span>
</div>

<p>The corresponding controller for this page is found at:</p>
<div style="margin-bottom: 1em; clear: right;" class="code bordered">
<span>apps/default/controller/welcome/home/Welcome_HomeIndex.php</span>
</div>

<p>If you are exploring Spica Framework for the very first time, you should start by reading the <a href="docs/index" title="Spica Framework Manual" class="hasIcon">User Guide</a>.</p>

<div class="revInfo">
  <p>Spica version: <?php echo Spica::getVersion(); ?></p>
</div>
</body>
</html>