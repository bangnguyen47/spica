          <div class="rightContent">
            <div class="fileBrowser">
              <div class="control">
                <ul>
                  <li>Add New Folder</li>
                  <li>Add Document</li>
                  <li>Search</li>
                  <li>Statistics</li>
                </ul>
              </div>
              <h2>Documents</h2>
              <p>From this section you can manage your files. You can also email documents without worrying about the file size.</p>
              <div class="hgap">&#160;</div>
              <p>Base location: <a href="filemanager/index/index/rpath/"><?php echo $this->publicBasePath; ?></a></p>
              <?php if (true === $this->fileList->isReadable()): ?>
              <table class="simple">
                <tr class="header">
                  <th>Type</th>
                  <th style="width: 200px;">Name</th>
                  <th>Size</th>
                  <th style="width: 200px;">Last modified</th>
                </tr>
                <?php foreach ($this->fileList as $file): ?>
                <tr>
                  <td><?php if (false === $file->isFile): ?><img src="<?php echo $this->imagePath; ?>/icon_folder.png" />
                      <?php else: ?><img src="<?php echo $this->imagePath; ?>/icon_file.png" /><?php endif; ?>
                  </td>
                  <td><a href="<?php echo $file->url ?>"><?php echo $file->fileName; ?></a></td>
                  <td><?php echo $file->size; ?></td>
                  <td><?php echo $file->lastModified; ?></td>
                </tr>
                <?php endforeach; ?>
              </table>
              <?php else: echo $this->fileList->getMessage(); endif; ?>
            </div>
          </div>