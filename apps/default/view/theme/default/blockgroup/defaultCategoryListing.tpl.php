<table width="100%" cellspacing="1" cellpadding="0" border="0" style="margin-top: 12px;" class="simple">
    <tbody>
        <tr class="header">
            <th>Name</th>
            <th>Subcategories</th>
            <th>Description</th>

            <th style="width: 200px;">Subcategories</th>
            <th>Issue Tracking</th>
            <th style="width: 110px;">Actions</th>
        </tr>
        <tr class="evenRow">
            <td style="font-weight: bold;">Category 1 Core</td>
            <td><a href="admin/category/view/id/5">Downloads</a></td>

            <td>Documentation</td>
            <td>Wiki | User Forum</td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>
            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>
        </tr>

        <tr class="oddRow">
            <td style="font-weight: bold;">Category 1 Annotations</td>
            <td><a href="admin/category/view/id/5">Downloads</a></td>
            <td>Documentation</td>
            <td>Wiki</td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>

            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>
        </tr>
        <tr class="evenRow">
            <td style="font-weight: bold;">Category 1 EntityManager</td>
            <td><a href="admin/category/view/id/5">Downloads</a></td>
            <td>Documentation</td>

            <td>Wiki | User Forum</td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>
            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>
        </tr>
        <tr class="oddRow">
            <td style="font-weight: bold;">Category 1 Shards</td>

            <td><a href="admin/category/view/id/5">Downloads</a></td>
            <td>Documentation</td>
            <td>Wiki | User Forum</td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>
            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>

        </tr>
        <tr class="evenRow">
            <td style="font-weight: bold;">Category 1 Validator</td>
            <td><a href="admin/category/view/id/5">Downloads</a></td>
            <td>Documentation</td>
            <td>Wiki | User Forum</td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>

            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>
        </tr>
        <tr class="oddRow">
            <td style="font-weight: bold;">Category 1 Search</td>
            <td><a href="admin/category/view/id/5">Downloads</a></td>
            <td>Documentation</td>

            <td>Wiki | User Forum</td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>
            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>
        </tr>
        <tr class="evenRow">
            <td style="font-weight: bold;"><a href="admin/category/view/id/5">Envers</a></td>

            <td><a href="admin/category/view/id/5">Downloads</a></td>
            <td><a href="admin/category/view/id/5">Documentation</a></td>
            <td><a href="admin/category/view/id/5">Wiki</a> | <a href="admin/category/view/id/5">User Forum</a></td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>
            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>

        </tr>
        <tr class="oddRow">
            <td style="font-weight: bold;">Category 1 Tools</td>
            <td><a href="admin/category/view/id/5">Downloads</a></td>
            <td>Documentation</td>
            <td>Wiki | User Forum</td>
            <td><a href="admin/category/view/id/5">Issue Tracking</a></td>

            <td><a href="admin/category/view/id/5">Anon SVN</a> | <a href="admin/category/view/id/5">FishEye</a></td>
        </tr>
    </tbody>
</table>
