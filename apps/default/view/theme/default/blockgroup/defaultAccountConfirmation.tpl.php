<h2>Please Complete Your Socialtext Trial Registration</h2>
<div id="content">
<h3 id="flash"></h3>

<script type="text/javascript">

function validate_form(f) {

    var focused_element = null;
    var message = "";

    VALIDATION: {

        // Require a password.
        //
        if ("" == f.password.value) {
            message = "Please enter a password.";
            focused_element = f.password;
            break VALIDATION;
        }

        // Make sure the password's long enough.
        //
        if (f.password.value.length < 6) {
            message = "Passwords must be at least 6 characters long.";
            f.password.value = "";
            f.confirm_password.value = "";
            focused_element = f.password;
            break VALIDATION;
        }

        // Require a confirmation password.
        //
        if ("" == f.confirm_password.value) {
            message = "Please re-type your password.";
            focused_element = f.confirm_password;
            break VALIDATION;
        }

        // Make sure both password values match.
        //
        if (f.password.value != f.confirm_password.value) {
            message = "Passwords do not match. Please enter the same value in both password fields.";
            f.password.value = "";
            f.confirm_password.value = "";
            focused_element = f.password;
            break VALIDATION;
        }

    } // end VALIDATION block

    // If there were any validation issues, show the message and
    // set the focus to the appropriate field, then return false
    // to signal that the validation failed.
    //
    if (message != "") {
        document.getElementById("flash").innerHTML =
            '<span class="error">' + message + '</span>';
        focused_element.focus();
        return false;
    }
    else {
        return true;
    }
}

</script>

<form method="post" action="confirm-trial.cgi" onsubmit="return validate_form(this);">
<input name="confirmation_key" value="zuH5FFy1h2IX0r8GprkeF1jwRAs" type="hidden">
<table>
    <tbody><tr>

        <td align="right">
            <strong>Email Address:</strong>
        </td>
        <td><strong>pcdinh@gmail.com</strong></td>
    </tr>

    <tr>
        <td align="right">

            <label for="password"><strong>Password:</strong></label>
        </td>

        <td align="right">
            <input id="password" name="password" size="30" value="" type="password">
        </td>
    </tr>

    <tr>

        <td align="right">
            <label for="confirm_password"><strong>Re-type Password:</strong></label>
        </td>
        <td align="right">
            <input id="confirm_password" name="confirm_password" size="30" value="" type="password">
        </td>
    </tr>

    <tr>

        <td> &nbsp; </td>
        <td align="left">
            <input value="Finish &gt;&gt;" type="submit">
        </td>
    </tr>
</tbody></table>

</form>
