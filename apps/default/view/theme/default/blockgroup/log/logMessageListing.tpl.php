          <div class="rightContent">
            <div class="fileBrowser">
              <h2>Logged error messages</h2>
              <p>From this section you can manage your error messages that have been logged. </p>
              <div class="hgap">&#160;</div>
              <table class="simple">
                <tr class="header">
                  <th>Date</th>
                  <th style="width: 200px;">URL</th>
                  <th>Referer</th>
                  <th style="width: 100px;">IP</th>
                  <th style="width: 100px;">Seriousity</th>
                  <th style="width: 100px;">Message</th>
                  <th style="width: 100px;">Path</th>
                </tr>
                <?php foreach ($this->file as $line): $data = explode(' ||| ', $line); ?>
                <tr>
                  <td><?php echo (isset($data[0]))?$data[0]:''; ?></td>
                  <td><?php echo (isset($data[1]))?$data[1]:''; ?></td>
                  <td><?php echo (isset($data[2]))?$data[2]:''; ?></td>
                  <td><?php echo (isset($data[3]))?$data[3]:''; ?></td>
                  <td><?php echo (isset($data[4]))?$data[4]:''; ?></td>
                  <td><?php echo (isset($data[5]))?$data[5]:''; ?></td>
                  <td><?php echo (isset($data[6]))?$data[6]:''; ?></td>
                </tr>
                <?php endforeach; ?>
              </table>
            </div>
          </div>