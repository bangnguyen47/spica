            <div class="rightContent">
              <form class="" method="post" target="" action="account/signin">
                <div class="formHeader">
                  <h2><?php echo $this->form->getTitle(); ?></h2>
                  <p>This area is reserved for Spica CMS administrators only.
                  Enter your account details below to login to Spica CMS.
                  <?php echo $this->form->getGuide(); ?></p>

                  <?php if ($this->form->hasNotice()): ?>
                    <h4>Error: </h4>
                    <?php echo $this->form->outputNotices(); ?>
                  <?php endif; ?>
                </div>
                <div class="formInner">
                  <div class="formRow">
                    <label>Email</label>
                    <div class="rightFormSide">
                      <input name="email" type="text" value="<?php echo $this->form->get('email'); ?>" />
                      <?php echo $this->form->feedback('email'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label>Password</label>
                    <div class="rightFormSide">
                      <input name="password" type="text" value="" />
                      <?php echo $this->form->feedback('password'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label>&nbsp;</label>
                    <div class="rightFormSide">
                      <button type="submit">Sign up</button>
                    </div>
                  </div>

                </div>
              </form>
            </div> <!-- end .rightContent -->