            <div class="rightContent">
              <form class="" method="post" target="" action="admin/category/create">
                <div class="formHeader">
                  <h2><?php echo $this->form->getTitle(); ?></h2>
                  <p>Create an category.
                  <?php echo $this->form->getGuide(); ?></p>

                  <?php if ($this->form->hasNotice()): ?>
                    <h4>Error: </h4>
                    <?php echo $this->form->outputNotices(); ?>
                  <?php endif; ?>
                </div>
                <div class="formInner">
                  <div class="formRow">
                    <div class="example">
			          <h2>Category list</h2>
			          <div id="fileTreeDemo_1" class="nodeTree"></div>
		            </div>
                  </div>

                  <div class="formRow">
                    <label>Title</label>
                    <div class="rightFormSide">
                      <?php echo spica_html_input_text('title', $this->form->get('title'), array('id' => 'cat_title')); ?>
                      <?php echo $this->form->feedback('title'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label>Description</label>
                    <div class="rightFormSide">
                      <textarea id="desc" name="desc" rows="6" cols="40"><?php echo $this->form->get('desc') ?></textarea>
                      <?php echo $this->form->feedback('desc'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label></label>
                    <div class="rightFormSide">
                      Status: Published <?php echo spica_html_radio('status', 1, 1, array('id' => 'category_status')); ?>
                      Unpublished <?php echo spica_html_radio('status', 2, 1, array('id' => 'category_status')); ?>
                      <?php echo $this->form->feedback('status'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label>&nbsp;</label>
                    <div class="rightFormSide">
                      <button type="submit">Create</button>
                    </div>
                  </div>

                </div>
              </form>
              <div id="livePreview">

              </div>
            </div> <!-- end .rightContent -->