            <div class="rightContent">
              <form class="" method="post" target="" action="account/register">
                <div class="formInner">
                  <?php if ($this->form->hasNotice()): ?>
                    <h4>Error: </h4>
                    <?php echo $this->form->outputNotices(); ?>
                  <?php endif; ?>
                  <div class="formRow">
                    <label>Full name</label>
                    <div class="rightFormSide">
                      <input name="full_name" type="text" value="<?php echo $this->form->get('full_name'); ?>" />
                      <?php echo $this->form->feedback('full_name'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label>Email</label>
                    <div class="rightFormSide">
                      <input name="email" type="text" value="<?php echo $this->form->get('email'); ?>" />
                      <?php echo $this->form->feedback('email'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label>Password</label>
                    <div class="rightFormSide">
                      <input name="password" type="password" value="" />
                      <?php echo $this->form->feedback('password'); ?>
                    </div>
                  </div>

                  <div class="formRow">
                    <label>&nbsp;</label>
                    <div class="rightFormSide">
                      <input name="submit" type="submit" />
                    </div>
                  </div>

                </div>
              </form>
            </div> <!-- end .rightContent -->