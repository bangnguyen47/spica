            <div class="rightContent">
              <p>We're sorry. Your request cannot be processed at this time. You have received this error because
              your request may have been blocked by our website spam and security filters.</p>
              <p>To solve this issue please contact <a href="mailto:webmaster@phpvietnam.net" style="color: rgb(177, 11, 43);">webmaster@phpvietnam.net</a>
              with the following information:</p>
              <ul>
                <li>Subject: Forbidden error on www.phpvietnam.net</li>
                <li>URL where you received this error</li>
                <li>Information you are posting</li>
              </ul>
            </div> <!-- end .rightContent -->