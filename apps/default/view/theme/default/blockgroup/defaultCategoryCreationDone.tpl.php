            <div class="rightContent">
              <h1>Create a category</h1>
              <div id="introductoryText">
   	            <p>A category titled <strong><?php echo $this->form->getCategoryTitle(); ?></strong> has been created. </p>
              </div>
            </div> <!-- end .rightContent -->