            <div id="aboutme">
              <div>
                <h3>About me</h3>
                <img src="<?php echo SpicaContext::getImagePath(); ?>/aboutmephoto.gif" alt="Me" />
                <p><strong>Name:</strong> My name</p>
                <p><strong>Location: </strong>Hanoi, Vietnam</p>
                <p>Thanks for visiting my blog. I hope you will it interesting. "This and That and More of the Same" strives to show striking images along with intriguing human interest stories. The "Memories" blog displays images of family while "Why Ask For the Moon When We Have the Stars" is devoted to friends.</p>
                <p class="taright"><a class="arrow" href="">View my complete profile</a></p>
              </div>
            </div>
            <!-- end aboutme-->