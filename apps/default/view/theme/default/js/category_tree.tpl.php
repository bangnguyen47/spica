<script src="<?php echo SpicaContext::getJsPath(); ?>/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo SpicaContext::getJsPath(); ?>/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="<?php echo SpicaContext::getJsPath(); ?>/jqueryFileTree.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready( function() {
	$('#fileTreeDemo_1').fileTree({ root: 'demo/', script: 'admin/category/ajaxList' }, function(file) {
		alert(file);
	});

	$('#fileTreeDemo_2').fileTree({ root: 'demo/', script: 'admin/category/ajaxList', folderEvent: 'click', expandSpeed: 750, collapseSpeed: 750, multiFolder: false }, function(file) {
		alert(file);
	});

	$('#fileTreeDemo_3').fileTree({ root: 'demo/', script: 'admin/category/ajaxList', folderEvent: 'click', expandSpeed: 750, collapseSpeed: 750, expandEasing: 'easeOutBounce', collapseEasing: 'easeOutBounce', loadMessage: 'Un momento...' }, function(file) {
		alert(file);
	});

	$('#fileTreeDemo_4').fileTree({ root: 'demo/', script: 'admin/category/ajaxList', folderEvent: 'dblclick', expandSpeed: 1, collapseSpeed: 1 }, function(file) {
		alert(file);
	});

	$('#desc').keyup(function() {
  	  $('#livePreview').html( $(this).val() );
  	});
  });
</script>
