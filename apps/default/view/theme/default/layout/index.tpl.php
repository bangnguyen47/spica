<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<?php echo spica_view_base_tag(); ?>
<?php $this->loadMeta(); ?>
<?php $this->loadCss(); ?>
<title><?php echo $this->pageTitle; ?></title>
</head>
<body>
<div class="wrapper">
  <?php echo $this->fetchBlock('DefaultHeaderBlock'); ?>
  <div class="main" id="b1">
      <div id="content">
        <div class="contentBody">
          <form id="search" method="post" action="">
            <fieldset>
              <input id="searchtext" type="text" />
              <input id="searchimage" type="image" src="<?php echo SpicaContext::getImagePath(); ?>/buttom-search.gif" value="search" />
            </fieldset>
          </form>
          <!-- end search -->
          <div id="leftCol">
            <?php echo $this->fetchBlockGroup('DefaultLeftSidebarBlockGroup'); ?>
          </div>
          <!-- end leftCol -->
          <div id="rightCol">
            <?php echo $this->fetchBody(); ?>
          </div>
          <!-- end rightCol -->
          <div class="floathelp">&nbsp;</div>
        </div> <!-- end .contentBody -->
      </div>
      <!-- end #content -->
  </div>
  <!-- end #main -->
  <?php echo $this->fetchBlock('DefaultFooterBlock'); ?>
</div>
<?php echo $this->loadScript(); ?>
</body>
</html>
