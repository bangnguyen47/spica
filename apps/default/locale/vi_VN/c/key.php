<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Localized string in Vietnamese. English is used as basis.
 *
 * @category   spica
 * @package    core
 * @subpackage locale
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.2
 * @since      March 24, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: key.php 1869 2011-01-07 18:55:25Z pcdinh $
 */

return array(
'weekday.Sunday'    => 'Chủ nhật',
'weekday.Monday'    => 'Thứ hai',
'weekday.Tuesday'   => 'Thứ ba',
'weekday.Wednesday' => 'Thứ tư',
'weekday.Thursday'  => 'Thứ năm',
'weekday.Friday'    => 'Thứ sáu',
'weekday.Saturday'  => 'Thứ bảy',

'weekday.Sun' => 'CN',
'weekday.Mon' => 'Thứ 2',
'weekday.Tue' => 'Thứ 3',
'weekday.Wed' => 'Thứ 4',
'weekday.Thu' => 'Thứ 5',
'weekday.Fri' => 'Thứ 6',
'weekday.Sat' => 'Thứ 7',

'month.January'   => 'Tháng một',
'month.February'  => 'Tháng hai',
'month.March'     => 'Tháng ba',
'month.April'     => 'Tháng tư',
'month.May'       => 'Tháng năm',
'month.June'      => 'Tháng sáu',
'month.July'      => 'Tháng bảy',
'month.August'    => 'Tháng tám',
'month.September' => 'Tháng chín',
'month.October'   => 'Tháng mười',
'month.November'  => 'Tháng mười một',
'month.December'  => 'Tháng mười hai',
);
?>