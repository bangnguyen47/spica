<?php
// http://akelos.org/wiki/lang-locale-mgmt
$locale = array();
$locale['description'] = 'United States';
$locale['charset']     = 'UTF-8';
$locale['date_time_format']     = 'Y-m-d H:i:s';
$locale['db_date_time_format']  = 'Y-m-d H:i:s';
$locale['date_format']          = 'Y-m-d';
$locale['long_date_format']     = 'Y-m-d';
$locale['time_format']          = 'H:i';
$locale['long_time_format']     = 'H:i:s';
$locale['first_day_of_week']    = 0; // 0 sunday, 1 monday
$locale['weekday_abbreviation'] = 1;

$locale['currency'] = array(
  'precision'     => 2,
  'unit'          => '$',
  'unit_position' => 'left',
  'separator'     => '.',
  'delimiter'     =>  ',',
);

$locale['code']      = 'en_US';
$locale['languages'] = 'English';
// Name and positions on add & edit forms
$locale['state designation'] = 'State';
$locale['state position']    = 'after city';
$locale['zone designation']  = 'Zip code';
$locale['zone position']     = 'after state';

$dictionary = array();
// Note that we are specifying the locale, just in case we use Canadian English or British English.
$dictionary['language'] = 'US English';
?>