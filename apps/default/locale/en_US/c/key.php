<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Localized string in English.
 *
 * @category   spica
 * @package    core
 * @subpackage locale
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.2
 * @since      July 31, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: key.php 1869 2011-01-07 18:55:25Z pcdinh $
 */

return array(
'weekday.Sunday'    => 'Sunday',
'weekday.Monday'    => 'Monday',
'weekday.Tuesday'   => 'Tuesday',
'weekday.Wednesday' => 'Wednesday',
'weekday.Thursday'  => 'Thursday',
'weekday.Friday'    => 'Friday',
'weekday.Saturday'  => 'Saturday',

'weekday.Sun' => 'Sun',
'weekday.Mon' => 'Mon',
'weekday.Tue' => 'Tue',
'weekday.Wed' => 'Wed',
'weekday.Thu' => 'Thu',
'weekday.Fri' => 'Fri',
'weekday.Sat' => 'Sat',

'month.January'   => 'January',
'month.February'  => 'February',
'month.March'     => 'March',
'month.April'     => 'March',
'month.May'       => 'May',
'month.June'      => 'June',
'month.July'      => 'July',
'month.August'    => 'August',
'month.September' => 'September',
'month.October'   => 'October',
'month.November'  => 'November',
'month.December'  => 'December',
);
?>