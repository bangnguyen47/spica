<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Configuration file.
 *
 * Most of the things below can be changed by user.
 *
 * @category   spica
 * @package    config
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.1
 * @since      March 14, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: global.php 1869 2011-01-07 18:55:25Z pcdinh $
 */

/**
 * Datasource connection or data service configuration.
 *
 * @var array
 */
$GLOBALS['__ds__'] = array(
  'db1' => array(
     'type'       => 'db',
     'dbhost'     => 'localhost',
     'db'         => 'spica',
     'dbuser'     => 'root',
     'dbpassword' => '123456',
     'dbvendor'   => 'mysql',
     'dbcharset'  => 'utf8',
     'dbport'     => '3306',
     'dbsocket'   => null,
     'dbflag'     => null,
     'dbssl'      => null,
     'dbreport'   => 'all'),

  'spicadocs' => array(
     'type'       => 'db',
     'dbhost'     => 'localhost',
     'db'         => 'spicadocs',
     'dbuser'     => 'root',
     'dbpassword' => '123456',
     'dbvendor'   => 'mysql',
     'dbcharset'  => 'utf8',
     'dbport'     => '3306',
     'dbsocket'   => null,
     'dbflag'     => null,
     'dbssl'      => null,
     'dbreport'   => 'all'),
);

/**
 * List of application contexts' configuration.
 *
 * @var array
 */
$GLOBALS['__profiles__'] = array(
// Context 'app0' - default Spica framework context
  'app0' => array(
     'default_package'    => 'welcome',
     'default_module'     => 'home',
     'default_controller' => 'index',
     'default_action'     => 'index',
     'backup_route'       => array(),
     'theme'              => 'welcome',
     'image_base_path'    => null, // null means using the default path (theme specific)
     'js_base_path'       => null, // null means using the default path (theme specific)
     'css_base_path'      => null, // null means using the default path (theme specific)
     'flash_base_path'    => null, // null means using the default path (theme specific)
     'debug'              => false,
     'locale_string'      => 'vi_VN',
     'locale_charset'     => 'UTF8',
     'error'              => 'e1'),

// Context 'app1' - Your real web application
  'app1' => array(
     'default_package'    => 'base',
     'default_module'     => 'home',
     'default_controller' => 'index',
     'default_action'     => 'index',
     'backup_route'       => array('_module' => 'error', '_controller' => 'pageNotFound', '_action' => 'index'),
     'theme'              => 'default',
     'image_base_path'    => null, // null means using the default path (theme specific)
     'js_base_path'       => null, // null means using the default path (theme specific)
     'css_base_path'      => null, // null means using the default path (theme specific)
     'flash_base_path'    => null, // null means using the default path (theme specific)
     'debug'              => false,
     'locale_string'      => 'en_US',
     'locale_charset'     => 'UTF8',
     'error'              => 'e1'),

// Context 'app2' - Your real web application
  'app2' => array(
     'default_package'    => 'project',
     'default_module'     => 'home',
     'default_controller' => 'index',
     'default_action'     => 'index',
     'backup_route'       => array('_module' => 'error', '_controller' => 'pageNotFound', '_action' => 'index'),
     'theme'              => 'project1',
     'image_base_path'    => null, // null means using the default path (theme specific)
     'js_base_path'       => null, // null means using the default path (theme specific)
     'css_base_path'      => null, // null means using the default path (theme specific)
     'flash_base_path'    => null, // null means using the default path (theme specific)
     'debug'              => false,
     'locale_string'      => 'vi_VN',
     'locale_charset'     => 'UTF8',
     'error'              => 'e1'),
);

/**
 * Mail server connection configuration.
 *
 * @var array
 */
$GLOBALS['__mail__'] = array(
  'smtp1' => array(
     'host' => 'localhost',
     'pass' => 'test1',
     'user' => 'root'),

  'smtp2' => array(
     'host' => 'localhost',
     'pass' => 'test1',
     'user' => 'root'),
);

/**
 * Error logging configuration.
 *
 * @var array
 */
$GLOBALS['__error__'] = array(
  'e1' => array(
     'method' => 'file',
     'naming' => 'plain', // date, plain
     'path'   => 'log',   // relative path (compared to bootstrap path)
     'prefix' => '',
     'suffix' => '.log',),

  'e2' => array(
     'method' => 'file',
     'naming' => 'date',  // date, plain
     'path'   => 'log',   // relative path (compared to bootstrap path)
     'prefix' => '',
     'suffix' => '.log',),
);

/**
 * Run-time application settings and parameters.
 *
 * This variable forms a state tree of the application.
 *
 * @var array
 */
$GLOBALS['__state__'] = array(
// USER: The name of the application
  'app'              => null,
// USER: The name of the context in use.E.x: app1. You can change it
  'profile'          => 'app1',
// The directory path to bootstrap file
  'bootstrap_path'   => '',
// The directory path to the active application
  'app_path'         => '',
// Request dispatcher
  'dispatcher'       => null,
// URL Resolver instance
  'url_resolver'     => null,
// Route name that URL Resolver use to resolve URL and dispatch the request
  'route_name'       => null,
// Security context object
  'security_context' => null,
// USER: Catch-all exception handler. If not set, default behavior will be applied
  'e_handler'        => null,
// The error configuration entry in use. E.x: e1
  'error'            => '',
// The error log file
  'error_log_file'   => null,
// The error handler
  'error_listener'   => null,
// The name of the package in use.E.x: www
  'active_package'   => '',
// The stack of action route path (file path) is handled
  'action_path'      => array(),
// Active action controllers. This entry can contains many routes as results
// of re-dispatching and re-directing.
  '_controller'      => array(),
// Controller's action methods that are executed in dispatched controllers
  'action_method'    => array(),
// Routes has been resolved. This entry can contains many routes as results of re-dispatching
// The final route is expected to use to resolve the request
  'resolved_routes'  => array(),
// Indicates if response content is ready (mostly used to check before dispatching process is complete)
  'response_ready'   => false,
// The stack of errors encountered in the application life cycle
  'errors'           => array(),
// Full request URL
  'full_url'         => '',
// The stack of exceptions encountered in the application life cycle
  'exceptions'       => array(),
// An active form object
  'form_object'      => null,
// An instance of the template system or engine
  'view_object'      => null,
// An instance of the layout template system or engine
  'layout_object'    => null,
// The theme name in use
  'theme_name'       => null,
// Full base URL
  'full_base_url'    => null,
// Application base URL without http://domain.com/subdir/
  'base_app_url'     => null,
// List of events that are fired when the system runs. It provides a simple
// and elegant method of delivering asynchrononous messaging to an application.
  'fired_events'     => null,
  'locale_string'    => null, // vi_VN, en_US
  'locale_charset'   => null, // UTF8
);

/**
 * Contains localized strings of the active locale .
 *
 * @var array
 */
$GLOBALS['__t__'] = array();

/**
 * Contains localized strings of multiple locales.
 *
 * @var array
 */
$GLOBALS['__lt__'] = array();

/**
 * Response payload.
 *
 * @var array
 */
$GLOBALS['__response__'] = array(
  'body'             => '',
  'headers'          => array(),
  'cookies'          => array(),
  'response_version' => '1.1',
  'status_code'      => '200',
);

/**
 * Event handlers.
 *
 * Multiple callbacks or Closures can subscribe to an event.
 *
 * @var array Array specs: event_name => array(event handlers)
 */
$GLOBALS['__event__'] = array(
// When application context gets started.
  'spica_context_start'      => '',
// When application context stops.
  'spica_context_stop'       => '',
  'spica_url_resolver_start' => '',
  'spica_url_resolver_stop'  => '',
  'spica_dispatcher_start'   => '',
  'spica_dispatcher_stop'    => '',
  'spica_response_start'     => '',
  'spica_response_stop'      => '',
// When a page is not found.
  'spica_page_not_found'       => '',
  'spica_context_get_template' => '',
  'spica_context_get_theme'    => '',
);

/**
 * Undeclared events that are fired when application is running.
 *
 * @var array Array specs: index => array('name' =>, 'backtrace' => )
 */
$GLOBALS['__undeclared_events__'] = array();

?>