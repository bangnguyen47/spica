<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of functions that are executed to change application settings.
 * As the results, it can be used to change the way Spica handles requests.
 *
 * This file is loaded after
 * + global.php is loaded
 *
 * and before
 * + event named 'spica_context_start' is fired
 *
 * @category   spica
 * @package    config
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 05, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: context_sample.php 1869 2011-01-07 18:55:25Z pcdinh $
 */

// Switch application context configuration
SpicaContext::switchProfile('app1');
date_default_timezone_set('Asia/Saigon');

include SpicaContext::getBasePath().'/library/spica/core/RegexUrlResolver.php';

$config = SpicaContext::getConfig();
$urlResolver = new SpicaRegexUrlResolver($config, $_SERVER['SCRIPT_NAME']);
$urlResolver->addRoute('news', 'dynamic', 'news/*', array('_module' => 'admin', '_controller' => 'log'));
$urlResolver->setFallbackResolver(new SpicaStandardUrlResolver($config, $_SERVER['SCRIPT_NAME']));
SpicaContext::setUrlResolver($urlResolver);

// Anonymous function in PHP 5.3
$contextStartCallback = function() {
    error_log("Context start. \n", 3, './log/events.log');
};

Spica::subscribeEvent('spica_context_start', $contextStartCallback);

$contextStopCallback = function() {
    error_log("Context stop. \n", 3, './log/events.log');
    error_log("  Application run time settings: \n", 3, './log/events.log');

    foreach ($GLOBALS['__state__'] as $key => $value)
    {
        if (is_object($value))
        {
            $value = 'object('.get_class($value).')';
        }
        elseif (is_array($value))
        {
            $temp = array();

            foreach ($value as $k => $v)
            {
                if (is_object($v))
                {
                    $v = get_class($v);
                }
                elseif (is_array($v))
                {
                    $v = 'Array';
                }

                $temp[] = $k.':'.$v;
            }

            $value = implode("\n      ", $temp);
        }

        error_log("    Key $key: $value\n", 3, './log/events.log');
    }
};

Spica::subscribeEvent('spica_context_stop', $contextStopCallback);

include_once 'service/CmsAccessControlService.php';
Spica::subscribeEvent('spica_url_resolver_stop', new CmsAccessControlService());

?>