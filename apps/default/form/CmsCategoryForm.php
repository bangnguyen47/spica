<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

// namespace spica\cms\service\default\form;

include_once 'spica/core/service/FormService.php';
include_once 'library/spica/core/validator/Common.php';

/**
 * Form service used in CMS category creation.
 *
 * @category   spica
 * @package    cms
 * @subpackage service\default\form
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 19, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CmsCategoryForm.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class CmsCategoryForm extends SpicaFormService
{
    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/SpicaForm#buildValidation()
     */
    public function buildValidation()
    {
        $this->setRule('title', new SpicaNotEmptyValidator('Please type your category name or title.'));
        $this->setRule('desc', new SpicaNotEmptyValidator('Please type your category description.'));
        $this->setRule('status', new SpicaAllowedValuesValidator(array(1, 2), 'Please specify a valid publishing status.'));
    }

    /**
     * Gets category title.
     *
     * @return string
     */
    public function getCategoryTitle()
    {
        return $_POST['title'];
    }

    /**
     * Gets category status.
     *
     * @return int
     */
    public function getCategoryStatus()
    {
        return (int) $_POST['status'];
    }

    /**
     * Gets category description.
     *
     * @return string
     */
    public function getCategoryDescription()
    {
        return $_POST['desc'];
    }

    /**
     * Gets parent category.
     *
     * @return string
     */
    public function getCategoryParent()
    {
        return $_POST['parent'];
    }
}

?>