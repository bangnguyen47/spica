<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

// namespace spica\docs\service\form;

include_once 'spica/core/service/FormService.php';
include_once 'library/spica/core/validator/Common.php';

/**
 * Form service used in note addition on a manual page.
 *
 * @category   spica
 * @package    docs
 * @subpackage service\default\form
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 12, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaDocsNoteForm.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaDocsNoteForm extends SpicaFormService
{
    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/SpicaForm#buildValidation()
     */
    public function buildValidation()
    {
        $minLengthVtor1 = new SpicaMinLengthStringValidator(1, 0, 'The user name must be longer than 1 character.');
        $minLengthVtor2 = new SpicaMinLengthStringValidator(10, 0, 'The user note must be longer than 10 characters.');
        $integerVtor1   = new SpicaIntegerValidator('Page ID must be an integer. ', true);
        $integerVtor2   = new SpicaIntegerValidator('Reply-to ID must be an integer. ', false);

        $this->setRule('username', $minLengthVtor1);
        $this->setRule('page_id', $integerVtor1);
        $this->setRule('reply_to', $integerVtor2);
        $this->setRule('note', $minLengthVtor2);
    }
}

?>