<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

// namespace spica\cms\service\default\form;

include_once 'spica/core/service/FormService.php';
include_once 'library/spica/core/validator/Pattern.php';
include_once 'library/spica/core/validator/Common.php';

/**
 * Form service used in account sign-in context.
 *
 * @category   spica
 * @package    cms
 * @subpackage service\default\form
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 02, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DefaultSigninForm.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class DefaultSigninForm extends SpicaFormService
{
    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/SpicaForm#buildValidation()
     */
    public function buildValidation()
    {
        $this->setRule('email', new SpicaEmailPatternValidator('Please type your e-mail address in the following format: yourname@example.com'));
        $this->setRule('password', new SpicaNotEmptyValidator());
    }

    /**
     * Hooks to perform actions each time form is created.
     */
    public function initialize()
    {
        $this->_title = 'Sign in to Spica';
        $this->_guide = 'To sign in, you must have cookies enabled.
        Cookies allow us to remember who you are so that we don\'t have to ask you to sign in on every secure page.';
    }

    /**
     * Gets submitted email address.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->get('email');
    }

    /**
     * Gets submitted password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->get('password');
    }
}

?>