<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

// namespace spica\cms\service\default\form;

include_once 'spica/core/service/FormService.php';
include_once 'library/spica/core/validator/Pattern.php';
include_once 'library/spica/core/validator/Complex.php';

/**
 * Form service used in account registration context.
 *
 * @category   spica
 * @package    cms
 * @subpackage service\default\form
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 26, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: DefaultRegistrationForm.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class DefaultRegistrationForm extends SpicaFormService
{
    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/SpicaForm#buildValidation()
     */
    public function buildValidation()
    {
        $patternVtor   = new SpicaPatternValidator('#^(?=.*[\p{L}])([\p{L}\p{Pd}\p{N}\p{Pi}\p{Pf}\p{Zs}]{1,50})$#u', 'The full name contains an illegal character. Letters, numbers, single quotes and dashes are allowed only. ');
        $minLengthVtor = new SpicaMinLengthStringValidator(2, 0, 'The full name must be longer than 2 characters.');
        $compositeVtor = new SpicaAndCompositeValidator(array($patternVtor, $minLengthVtor));
        $passwordVtor  = new SpicaPatternValidator('#^(?=.*[\#$%@])(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$#',
        'Password must be at least 4 characters, no more than 12 characters,
        and must include at least one upper case letter, one lower case letter, one numeric digit and on special symbol among (# $ % @). ');

        $this->setRule('full_name', $compositeVtor);
        $this->setRule('email', new SpicaEmailPatternValidator('Email address is not valid'));
        $this->setRule('password', $passwordVtor);
    }
}

?>