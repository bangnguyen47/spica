<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * A block of a page represents the page footer.
 *
 * namespace spica\docs\block\welcome\DocsNote
 *
 * @category   spica
 * @package    docs
 * @subpackage block\welcome
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 12, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: SpicaDocsNoteBlock.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class SpicaDocsNoteBlock extends SpicaPageBlock
{
    /**
     * Template file.
     *
     * @var string
     */
    protected $_templateFile = 'vi_VN/docsNoteBlock';

    /**
     * Page ID.
     *
     * @var int
     */
    public $pageId;

    /**
     * Page ID checking message.
     *
     * @var string
     */
    public $pageCheckMessage;

    /**
     * Page notes.
     *
     * @var SpicaRowList
     */
    public $notes;

    /**
     * (non-PHPdoc)
     * @see trunk/library/spica/core/view/SpicaPageBlock#initialize()
     */
    public function initialize()
    {
        include_once 'service/SpicaDocsNoteService.php';
        $this->_templateFile = SpicaContext::getLocaleName().'/docsNoteBlock';
        $docsNoteService     = new SpicaDocsNoteService('spicadocs');

        try
        {
            $pageId       = $docsNoteService->findPageId(SpicaRequest::getController(), SpicaRequest::get('pname'));
            $this->notes  = $docsNoteService->findNotesByPageId($pageId);
            $this->pageId = $pageId;
        }
        catch (Exception $e)
        {
            $this->pageCheckMessage = $e->getMessage();
            $this->notes  = new SpicaRowList();
            $this->notes->setReadable(true);
            $this->notes->setMessage($e->getMessage());
        }
    }
}

?>