<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of actions that serves every request to "sample" module
 * and "index" controller in "base" package.
 *
 * namespace spica\cms\controller\base\sample\Index
 *
 * @category   spica
 * @package    cms
 * @subpackage controller\base\sample
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.2
 * @since      March 15, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Base_SampleIndex.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Base_SampleIndex
{
    /**
     * Renders the index page of Base_SampleIndex controller.
     *
     * This method should not print anything out intentionally because
     * those messages will be logged instead of being rendered.
     *
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        SpicaResponse::setBody('Hello World! Spica was born in "sample" module of package named "default". ');
    }

    /**
     * Tests lock mechanism in cache updates and fillings.
     *
     * This method should not print anything out intentionally because
     * those messages will be logged instead of being rendered.
     *
     * @return void This kind of method should not return anything.
     */
    public function cacheAction()
    {
        include_once  'spica/core/cache/CacheManager.php';
        $path   = SpicaContext::getBasePath().DIRECTORY_SEPARATOR.'cache/f2';
        $option = new SpicaFileCacheConfiguration($path, 5);
        $cache  = SpicaCacheManager::factory($option);

        try
        {
            $data = $cache->get('a');

            if (false === $data)
            {
                // Cache lock will be expired in 3 seconds
                $cache->acquireLock('a', 10, 100);
                echo 'Acquired. '.microtime(true);
                // Fetch data from a datasource
                $data = 'aaaaaaaa----aaaa';
                $cache->set('a', $data, 20);
                $cache->releaseLock('a');
                echo 'Release lock. '.microtime(true);
            }
        }
        catch (SpicaCacheLockException $ex)
        {
            $data = $cache->get('a');
            echo 'Lock can not be acquired. '.microtime(true);
        }
        catch (SpicaCacheNotAvailableException $ex)
        {

        }

        if (false === $data)
        {
            echo 'Unable to retrieve data. '.microtime(true);
        }
    }
}

?>