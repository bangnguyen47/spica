<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of actions that serves every request to "account" module and "register"
 * controller in "base" package.
 *
 * namespace spica\cms\controller\base\account\Register
 *
 * @category   spica
 * @package    cms
 * @subpackage controller\base\account
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 28, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Base_AccountRegister.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Base_AccountRegister
{
    /**
     * Renders the "index" page of Base_AccountRegister controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/account/register
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        include_once 'form/DefaultRegistrationForm.php';
        $form = new DefaultRegistrationForm();
        spica_import_service('CmsAccountService');

        try
        {
            if (true === $form->isPost())
            {
                $form->validate();
                $accountService = new CmsAccountService();
                $accountService->create($form);
                $form->setProcessed(true);
            }
        }
        catch (SpicaValidatorException $e)
        {
            // no-message here
        }
        catch (SpicaDatabaseException $e)
        {
            $form->addNotice($e->getNativeMessage());
        }
        catch (Exception $e)
        {
            $form->addNotice($e->getMessage());
        }

        SpicaContext::registerForm($form);
        // Theme and template
        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Account registration';
        $theme->attachCss('index');
        $theme->attachMeta('index');
        $theme->setBody('DefaultRegistrationBlockGroup');
        SpicaResponse::setBody($theme->fetch('index'));
    }
}

?>