<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of actions that serves every request to "admin" module and
 * "category" controller in "base" package.
 *
 * namespace spica\cms\controller\base\admin\Category
 *
 * @category   spica
 * @package    cms
 * @subpackage controller\base\admin
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 19, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Base_AdminCategory.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Base_AdminCategory
{
    /**
     * Renders the "index" page of Base_AdminCategory controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/admin/category
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Category Listing';
        $theme->attachCss('index');
        $theme->setBody('DefaultCategoryListingBlockGroup');
        SpicaResponse::setBody($theme->fetch('index'));
    }

    /**
     * Renders the "create" page of Base_AdminCategory controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/admin/category/create
     * @return void This kind of method should not return anything.
     */
    public function createAction()
    {
        spica_import_service('CmsCategoryService');
        $form = SpicaContext::createForm('CmsCategoryForm');

        try
        {
            if (true === $form->isPost())
            {
                $form->validate();
                $categoryService = new CmsCategoryService();
                $categoryService->create($form);
                $form->setProcessed(true);
            }
        }
        catch (SpicaValidatorException $e)
        {
            // no message here
        }
        catch (Exception $e)
        {
            $form->addNotice($e->getMessage());
        }

        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Create a category';
        $theme->attachCss('index');
        $theme->attachCss('tree');
        $theme->attachScript('category_tree');
        $theme->setBody('DefaultCategoryCreationBlockGroup');
        SpicaResponse::setBody($theme->fetch('index'));
    }

    /**
     * Responses to Ajax requests on category list by a parent.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/admin/category/ajaxList
     * @return void This kind of method should not return anything.
     */
    public function ajaxListAction()
    {
        $message  = '<ul class="jqueryFileTree" style="display: none;">';
        $message .= '<li class="directory collapsed"><a href="#" rel="1">Category 1</a></li>';
        $message .= '<li class="directory collapsed"><a href="#" rel="1">Category 2</a></li>';
        $message .= '<li class="directory collapsed"><a href="#" rel="1">Category 3</a></li>';
        $message .= "</ul>";
        SpicaResponse::setBody($message);
    }

    /**
     * Renders the "delete" page of Base_AdminCategory controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/admin/category/delete
     * @return void This kind of method should not return anything.
     */
    public function deleteAction()
    {
        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Delete a category';
        $theme->attachCss('index');
        $theme->setBody('DefaultCategoryDeletionBlockGroup');
        SpicaResponse::setBody($theme->fetch('index'));
    }
}

?>