<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of actions that serves every request to "admin" module and
 * "log" controller in "base" package.
 *
 * namespace spica\cms\controller\base\admin\Log
 *
 * @category   spica
 * @package    cms
 * @subpackage controller\base\admin
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 04, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Base_AdminLog.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Base_AdminLog
{
    /**
     * Renders the "index" page of Base_AdminLog controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/admin/log
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Log Message Listing';
        $theme->attachCss('index');
        $theme->setBody('log/DefaultLogMessageListingBlockGroup');
        SpicaResponse::setBody($theme->fetch('index'));
    }
}

?>