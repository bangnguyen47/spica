<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of actions that serves every request to "docs" module
 * and "Demo" controller in "welcome" package.
 *
 * namespace spica\docs\controller\welcome\manual\Demo
 *
 * @category   spica
 * @package    docs
 * @subpackage controller\welcome\manual
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 30, 2010
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Welcome_DocsDemo.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Welcome_DocsDemo
{
    /**
     * Page title helper.
     *
     * @var SpicaDocsPageTitle
     */
    public $pageTitle;

    /**
     * Renders the "index" page of Welcome_DocsDemo controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/docs/demo
     * @link   http://domain.com/docs/demo/index
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        try
        {
            include_once 'service/SpicaDocsNoteService.php';
            $pageService = new SpicaDocsNoteService();
            $title       = $pageService->findPageTitleByPageName(SpicaRequest::getController(), 'index');

            if ($title === $title->isReadable())
            {
                throw new Exception('There are something wrong with our database or query so the page
                can not be served at the moment. Details: '.$page->getMessage());
            }

            $theme = SpicaContext::getTheme();
            $theme->pageTitle = $title->value();
            $theme->setBody('welcome/SpicaDocsDemoPageBlockGroup');
            SpicaResponse::setBody($theme->fetch('vi_VN/guide'));
        }
        catch (Exception $e)
        {
            Spica::logException($e);
            $message = new SpicaActionMessage();
            $message->summary = $e->getMessage();
            Spica::forwardMessage($message);
            SpicaController::forward('index', 'badrequest', 'docs');
        }
    }

    /**
     * Renders manual pages.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/docs/demo
     * @link   http://domain.com/docs/demo/page/*
     * @return void This kind of method should not return anything.
     */
    public function pageAction()
    {
        include_once 'library/spica/core/validator/Common.php';

        $theme    = SpicaContext::getTheme();
        $pageName = SpicaRequest::get('pname');

        try
        {
            if (false === spica_valid_alphanum_dash($pageName, false))
            {
                include_once 'library/spica/core/Exception.php';
                throw new SpicaInvalidParameterException();
            }

            include_once 'service/SpicaDocsNoteService.php';
            $pageService = new SpicaDocsNoteService();
            $page        = $pageService->findPageTitleByPageName(SpicaRequest::getController(), $pageName);

            if (false === $page->isReadable())
            {
                $pageTitle = 'Page title cannot be found: '.$page->getMessage();
            }
            else
            {
                $pageTitle = $page->value();
            }

            $theme->pageTitle = 'Spica User Manual - '.$pageTitle;
            $theme->setBody('welcome/SpicaDocsDemoPageBlockGroup');
            SpicaResponse::setBody($theme->fetch('vi_VN/guide'));
        }
        catch (SpicaInvalidParameterException $e)
        {
            Spica::logException($e);
            $message = new SpicaActionMessage();
            $message->summary = 'The parameter "pname" that you provided is not valid. ';
            Spica::forwardMessage($message);
            SpicaController::forward('index', 'badrequest', 'docs');
        }
        catch (Exception $e)
        {
            Spica::logException($e);
            $message = new SpicaActionMessage();
            $message->summary = $e->getMessage();
            Spica::forwardMessage($message);
            SpicaController::forward('index', 'badrequest', 'docs');
        }
    }
}

?>