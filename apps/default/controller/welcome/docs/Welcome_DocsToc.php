<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of actions that serves every request to "docs" module
 * and "Toc" controller in "welcome" package.
 *
 * namespace spica\docs\controller\welcome\docs\Toc
 *
 * @category   spica
 * @package    docs
 * @subpackage controller\welcome\docs
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      June 02, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Welcome_DocsToc.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Welcome_DocsToc
{
    /**
     * Renders the "index" page of Welcome_DocsToc controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/docs/toc
     * @link   http://domain.com/docs/toc/index
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Spica Framework - User Manual - Table Of Content';
        $theme->attachCss('index');
        $theme->attachMeta('index');
        $theme->setBody('welcome/SpicaDocsTocBlockGroup');
        SpicaResponse::setBody($theme->fetch('vi_VN/docs_index'));
    }
}

?>