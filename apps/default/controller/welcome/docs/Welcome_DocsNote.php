<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of actions that serves every request to "docs" module
 * and "Note" controller in "welcome" package.
 *
 * namespace spica\docs\controller\welcome\docs\Note
 *
 * @category   spica
 * @package    docs
 * @subpackage controller\welcome\docs
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      May 11, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: Welcome_DocsNote.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class Welcome_DocsNote
{
    /**
     * Renders the "index" page of Welcome_DocsNote controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/docs/note
     * @link   http://domain.com/docs/note/index
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Spica Framework - Manual - Basic guide';
        $theme->setBody('welcome/SpicaDocsGuideIndexBlockGroup');
        SpicaResponse::setBody($theme->fetch('vi_VN/guide'));
    }

    /**
     * This action helps add a note into documentation database, based on a Ajax
     * request.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/docs/note/add
     * @return void This kind of method should not return anything.
     */
    public function addAction()
    {
        include_once 'form/SpicaDocsNoteForm.php';
        include_once 'service/SpicaDocsNoteService.php';
        sleep(2);

        // AJAX response
        $response = array('status' => '0', 'feedback' => array(), 'messages' => array());
        $form     = new SpicaDocsNoteForm();
        SpicaContext::registerForm($form);

        try
        {
            if (true === $form->isPost())
            {
                $form->validate();
                $docsNoteService = new SpicaDocsNoteService('spicadocs');
                $docsNoteService->create($form);
                $form->setProcessed(true);
            }
        }
        catch (SpicaValidatorException $e)
        {
            // Add form notice header to notify user that input is not valid
            $form->addNotice('Please correct the below fields before proceeding.');
        }
        catch (SpicaDatabaseException $e)
        {
            $form->addNotice($e->getMessage());
            Spica::logException($e);
        }
        catch (Exception $e)
        {
            $form->addNotice($e->getMessage());
            Spica::logException($e);
        }

        if (false === $form->isProcessed())
        {
            $response = array(
              'error'    => 1,
              'feedback' => array(
                'page_id'  => $form->feedback('page_id', ''),
                'reply_to' => $form->feedback('reply_to', ''),
                'username' => $form->feedback('username', ''),
                'note'     => $form->feedback('note', ''),
            ),
            );

            $response['messages'] = $form->getNotices();
        }

        SpicaResponse::setHeader("Cache-Control", "no-cache");
        SpicaResponse::setBody(json_encode($response));
    }
}

?>