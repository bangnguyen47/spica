<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
include_once 'spica/core/service/DatabaseService.php';

/**
 * AccountService provides operations for creating and/or modifying Spica CMS accounts.
 *
 * namespace spica\cms\service\core\account\Standard;
 *
 * @category   spica
 * @package    cms
 * @subpackage service\core\account
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 03, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CmsAccountService.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class CmsAccountService extends SpicaDatabaseService
{
    /**
     * Data source.
     *
     * @var SpicaDatabaseConnection
     */
    public $ds;

    /**
     * Constructs an object of <code>CmsAccountService</code> with a data source
     * alias name.
     *
     * @param string $alias Data source alias name
     */
    public function __construct($alias = 'db1')
    {
        parent::__construct($alias);
    }

    /**
     * Hooks to perform specific actions after an object of CmsAccountService
     * is created.
     */
    protected function _setup()
    {
        // no-op
    }

    /**
     * Creates an account.
     *
     * @throws SpicaDatabaseException if there is any problem with database connection or query
     * @param  DefaultRegistrationForm $form
     * @return bool
     */
    public function create($form)
    {
        include_once 'spica/core/utils/HashUtils.php';
        $email    = $this->ds->quote($form->get('email'));
        $password = SpicaPortableHashUtils::createHash($form->get('password'));

        $sql  = sprintf('INSERT INTO users (user_email, user_password)
                         VALUES (\'%s\', \'%s\')', $email, $password);

        $stmt = $this->ds->createStatement();
        $stmt->executeUpdate($sql);
        return true;
    }
}

?>