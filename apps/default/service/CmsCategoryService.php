<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
include_once 'spica/core/service/DatabaseService.php';

/**
 * This class provides services for Category.
 *
 * namespace spica\cms\service\core\category\CategoryService;
 *
 * @category   spica
 * @package    cms
 * @subpackage service\core\category
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      March 25, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CmsCategoryService.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class CmsCategoryService extends SpicaDatabaseService
{
    /**
     * Data source.
     *
     * @var Pone_Database_Connection
     */
    public $ds;

    /**
     * Constructs an object of <code>CmsCategoryService</code> with a data source
     * alias name.
     *
     * @param string $alias Data source alias name
     */
    public function __construct($alias = 'db1')
    {
        parent::__construct($alias);
    }

    /**
     * Deletes a category physically by a given ID.
     *
     * @param int $id
     */
    public function deleteById($id)
    {

    }

    /**
     * Deletes a category conceptually by a given ID.
     *
     * @param int $id
     */
    public function markDeletedById($id)
    {

    }

    /**
     * Creates a category with datqa provided in a CmsCategoryForm object.
     *
     * @param CmsCategoryForm $form
     */
    public function create(CmsCategoryForm $form)
    {
        $title  = $form->getCategoryTitle();
        $desc   = $form->getCategoryDescription();
        $status = $form->getCategoryStatus();

        $sql = "INSERT INTO node_categories (
                    category_title, category_desc, category_status, left_num, right_num, depth_level
                ) VALUES (
                    '$title', '$desc', $status, 1, 3, 1
                )";

        $stmt = $this->ds->createStatement();
        $stmt->executeUpdate($sql);
        return true;
    }
}
?>