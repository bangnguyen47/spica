<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
include_once 'spica/core/service/DatabaseService.php';
include_once 'spica/core/security/Security.php';

/**
 * Authenication service that provides validation against a sign-in action.
 *
 * namespace spica\cms\service\core\authentication\Form;
 *
 * @category   spica
 * @package    cms
 * @subpackage service\core\authentication
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 07, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CmsFormAuthenticationService.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class CmsFormAuthenticationService extends SpicaFormAuthenticationProvider
{
    /**
     * Data source.
     *
     * @var Pone_Database_Connection
     */
    public $ds;

    /**
     * Authenticated user details.
     *
     * @var CmsUser
     */
    protected $_user;

    /**
     * Constructs an object of <code>CmsFormAuthenticationService</code>
     *
     * @param string $alias Data source configuration alias name.
     */
    public function __construct($alias = 'db1')
    {
        $this->ds = SpicaDatabaseManager::getConnection($alias);
        $this->ds->setTimeout(10);
    }

    /**
     * Carries out an authentication attempt.
     *
     * @param SpicaAuthenticationRequest $req
     */
    public function authenticate($req)
    {
        include_once 'spica/core/utils/HashUtils.php';

        $email    = $this->ds->quote($req->getPrincipalName());
        $password = $req->getCredentials();

        $sql = sprintf('SELECT user_id, user_password, user_status
                        FROM users
                        WHERE user_email = \'%s\'', $email);

        $stmt = $this->ds->createStatement();
        $data = $stmt->fetchRow($sql);

        if (empty($data))
        {
            throw new SpicaIncorrectCredentialException('Email and password combination is not correct.');
        }

        $hash = $data['user_password'];

        if (false === SpicaPortableHashUtils::checkHash($password, $hash))
        {
            throw new SpicaIncorrectCredentialException('Email and password combination is not correct.');
        }

        $status = (int) $data['user_status'];
        // Check invalid status
        switch ($status)
        {
            case 2:
                throw new SpicaAccountNotActivatedException('This account has not been activated.');

            case 9:
                throw new SpicaLockedAccountException('This account has been locked.');
        }

        include_once 'security/CmsUser.php';
        $this->_user = new CmsUser($email, 0, false);
    }

    /**
     * Obtains the granted authorities for the specified user.
     *
     * @see    library/spica/core/security/SpicaFormAuthenticationProvider#getUserDetails()
     * @return CmsUser
     */
    public function getUserDetails()
    {
        return $this->_user;
    }
}

?>