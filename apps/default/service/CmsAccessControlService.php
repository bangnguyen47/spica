<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
include_once 'spica/core/service/DatabaseService.php';
include_once 'spica/core/security/Security.php';
include_once 'spica/core/utils/BaseUtils.php';

/**
 * Access control service that provides authorization against the current security context.
 *
 * namespace spica\cms\service\core\authorization\AccessControlService;
 *
 * @category   spica
 * @package    cms
 * @subpackage service\core\authorization
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 14, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CmsAccessControlService.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class CmsAccessControlService implements SpicaInvokable
{
    /**
     * Constructs an object of <code>CmsAccessControlService</code>
     */
    public function __construct()
    {

    }

    /**
     * (non-PHPdoc)
     * @see library/spica/core/SpicaInvokable#__invoke()
     */
    public function __invoke()
    {
        $aclManager    = new SpicaAuthorizationManager();
        $cmsAclService = new CmsAuthorizationService();
        $aclManager->setProviders(array($cmsAclService));

        try
        {
            $aclManager->checkPermission(SpicaContext::getSecurityContext());
        }
        catch (SpicaAuthorizationException $ex)
        {
            // do redirection here
            SpicaRequest::setModule('home');
            SpicaRequest::setController('index');
            SpicaRequest::setAction('index');
        }
        catch (Exception $ex)
        {
            // do redirection here
        }
    }
}

/**
 * CMS service that provides authorization against the current security context.
 *
 * namespace spica\cms\service\core\authorization\AuthorizationService;
 *
 * @category   spica
 * @package    cms
 * @subpackage AuthorizationService
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 14, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: CmsAccessControlService.php 1869 2011-01-07 18:55:25Z pcdinh $
 */
class CmsAuthorizationService extends SpicaRoleAuthorizationProvider
{
    /**
     * Data source.
     *
     * @var SpicaConnection
     */
    public $ds;

    /**
     * Authenticated user details.
     *
     * @var CmsUser
     */
    protected $_user;

    /**
     * Constructs an object of <code>CmsAccessControlService</code>
     *
     * @param string $alias Data source configuration alias name.
     */
    public function __construct($alias = 'db1')
    {
        $this->ds = SpicaDatabaseManager::getConnection($alias);
        $this->ds->setTimeout(10);
    }

    /**
     * Processes authorization when user is not authenticated.
     *
     * @throws SpicaAuthorizationException if authorization failed
     * @param  SpicaUser $user User to be authorized. It can be null
     * @param  SpicaSecurityContext $securityContext It can be null
     */
    public function _allowUnauthenticated($user = null, $securityContext = null)
    {
        $message = (null === $securityContext)?'initialized.':'un-initialized.';
        echo 'Denied: Unauthenticated. Security context is '.$message;
        throw new SpicaAuthorizationException();
    }

    /**
     * Processes authorization when user is authenticated as guest.
     *
     * @throws SpicaAuthorizationException if authorization failed
     * @param  SpicaUser $user
     */
    public function _allowGuest($user)
    {
        echo 'Accept: Guest. ';
    }

    /**
     * Processes authorization when user is not authenticated.
     *
     * @throws SpicaAuthorizationException if authorization failed
     * @param  int $roleId
     */
    public function _allowRole($roleId)
    {
        echo 'Accept: Role. '.$roleId;
    }
}
?>