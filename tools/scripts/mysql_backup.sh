#!/bin/bash
# Creates a cron tab at 1 a.m everyday 
# 0 1 * * * /path/to/backup.daily.cron
# For InnoDB use
# mysqldump --single-transaction --opt --host=localhost --user=USERNAME --password=PASSWORD --all-databases > /path/to/dbBackup.sql
mysqldump --opt --host=localhost --user=USERNAME --password=PASSWORD --all-databases > /home/pcdinh/backups/db/mysql/dbBackup.sql
tar -czvf /home/pcdinh/backups/db/mysql/mysql.`date '+%Y%m%d%H%M%S'`.tar.gz /home/pcdinh/backups/db/mysql/dbBackup.sql
rm -f /home/pcdinh/backups/db/mysql/dbBackup.sql
service httpd restart