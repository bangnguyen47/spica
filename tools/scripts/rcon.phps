#!/usr/local/bin/php
<?php
include('info.php');
function Status($ip,$port)
        {
                $status = fsockopen('udp://' . $ip, $port, $errno, $errstr);
                        $packet = 'SAMP';
                        $packet .= chr(strtok($ip, '.'));
                        $packet .= chr(strtok('.'));
                        $packet .= chr(strtok('.'));
                        $packet .= chr(strtok('.'));
                        $packet .= chr($port & 0xFF);
                        $packet .= chr($port >> 8 & 0xFF);
                        fwrite($status, $packet.'i');
                        stream_set_timeout($status, 1);
                        fread($status, 11);
                        $is_passworded = ord(fread($status, 1));
                        $info = stream_get_meta_data($status);
                        if ($info['timed_out']) {
                                return false;
                        }else{
                                return true;
                        fclose($status); }
}


function SendRconPacket($ip,$port,$cmd,$settings)
{
  		$pass = $settings['password'];
  			$passlen = strlen($pass);
			$cmdlen = strlen($cmd);
  			$packet = 'SAMP';
  			$packet .= chr(strtok($ip, '.')).chr(strtok('.')).chr(strtok('.')).chr(strtok('.'));
  			$packet .= chr($port & 0xFF).chr($port >> 8 & 0xFF);
  			$packet .= "x";
  			$packet .= chr($passlen & 0xFF).chr($passlen >> 8 & 0xFF).$pass;
  			$packet .= chr($cmdlen & 0xFF).chr($cmdlen >> 8 & 0xFF).$cmd;
  		$fp = fsockopen('udp://' . $ip, $port, $errno, $errstr);
  			stream_set_timeout( $fp , 1 );
			fwrite($fp, $packet);
   		while(!feof($fp)) {
    			$str=fread($fp,128);
    			$str=substr($str,13,strlen($str)-13);
    		if (!$str) break;
    			echo $str."\n";
}
			fclose($fp);
}


  $ver = "0.2";
  $whoami = shell_exec('whoami');
  	if($argv[1] == '--help' || $argv[1] == '-help') {
  echo "Linux SA-MP Rcon Console.\n";
  echo "Version: $ver.\n\n";
  echo "Type /help for a list of available commands.\n";
  //echo "Warning: this is a test version which support receive server's data!\n";
  echo "Access data for your server write in .rcon.ini (the file is hidden in your home directory), rcon command write after running the application.\n";
  echo "Application by: Torvalds (admin@torvalds.pl).\n";
} else {
	if($whoami == 'root') {
  $settings = parse_ini_file("/root/.rcon.ini");
  } else {
  $settings = parse_ini_file("/home/torvalds/.rcon.ini");
}
  $ip = $settings['ip'];
  $port = $settings['port'];
  
	if(!Status("$ip","$port")) {
  die("Server is down? Please check Your settings.\n");
}

  echo "$ver - www.torvalds.pl\n";
  echo "Remote Console: $ip:$port...\n\n";
  
ob_start();
$ip_client = shell_exec("wget http://torvalds.pl/ip.php -O - -o /dev/null");
$cmd = "echo Incomming rcon connection (Linux Rcon): $ip_client";
SendRconPacket($ip,$port,$cmd,$settings); 
ob_end_clean();
for ($i=0; ;$i++) { 		
	//$script_start = microtime(true);
	$line = readline("Rcon: ");
        readline_add_history($line);	
	$array = readline_list_history();
	$cmd = $array[$i];
		
	if(!eregi('/',$cmd) && $cmd !== 'exit'){
ob_start();	
	$cmd_log = "echo Received rcon command (Linux Rcon): $array[$i]";
	SendRconPacket($ip,$port,$cmd_log,$settings);		
ob_end_clean();	
}
	
     if($cmd == '/q') {
	die();	
}

     if($cmd == 'exit') {
	$exit = readline("Are you sure you want to shutdown your server (Y/n)? ");
} else {
	SendRconPacket($ip,$port,$cmd,$settings); 
}

     if($exit == 'Y') {     
ob_start();	
	$cmd_log = "echo Received rcon command (Linux Rcon): exit";
	SendRconPacket($ip,$port,$cmd_log,$settings);	
	SendRconPacket($ip,$port,$cmd,$settings);    
ob_end_clean();
	die();  
 }    
	
 if($cmd == '/help') {
	echo "All system commands begin with / - (example /help).\n";
	echo "List of available commands:\n";
	echo "/info - Shows info of the server, and list of players online.\n";
	echo "/q or ctrl+c - quit.\n";
	echo "'cmdlist' Shows list of available server commands.\n";
}		

 if($cmd == '/info') {
$samp = new ConnectToServer($ip, $port,"");
$info = $samp->GetInfo();
$players = $samp->GetDetailedPlayers();
$rules = $samp->GetRules();
$system_ping = exec("ping -c 3 $ip");
preg_match("/rtt min\/avg\/max\/mdev = (.*)\/(.*)\/(.*)\/(.*) ms/", $system_ping, $matches);
$output = explode(".","$matches[2]");

echo "-------------Server Info:-------------\n";
echo "Hostname: $info[Hostname].\n";
echo "Address: $ip:$port.\n";
echo "Players: $info[Players]/$info[Maxplayers].\n";
echo "Server ping: $output[0]ms.\n";
for($i=0; $i < $info[Players]; $i++) {
$ping = $ping + $players[$i]['Ping'];
}
if ($info[Players] !== 0)
{
print("Ping average: ".round($ping / $info[Players])." ms.\n");
} else {
print("Ping average: 0ms.\n");
}
echo "Mode: $info[Gamemode].\n";
echo "Map: $info[Mapname].\n";
echo "-------------Server Rules:-------------\n";
echo "Gravity: $rules[gravity].\n";
echo "Instagib: $rules[instagib].\n";
echo "Mapname: $rules[mapname].\n";
echo "Server version: $rules[version].\n";
echo "Weburl: $rules[weburl].\n";
echo "Worldtime: $rules[worldtime].\n";
echo "-------------Players list:-------------\n";
if($info[Players] == '0') { 
print ("There are no players on the server.\n");
} else {
for($i=0; $i < $info[Players]; $i++) {
	print("Name: ".$players[$i][Nick]."");	
	print("\nId: ".$players[$i][ID]."");
	print("\nPing: ".$players[$i][Ping]."");
	print("\nScore: ".$players[$i][Score]."\n--\n");
}
}
}
}
}
?>
