#!/bin/bash
#
# Shell script to perform nightly build and test
#
# Shell script intended to be run automatically by crontab to build
# and test the spica Pipeline primary command line functions.

# Jameson Rollins <jrollins@phys.columbia.edu>

######################################################################

exec 2>&1

set -e

# permit error propogation in pipelines
set -o pipefail 2>/dev/null

######################################################################

BASE="$1"

# make needed directories
mkdir -p "$BASE"/{builds,install}

# canonicalize base directory path
BASE=$(cd "$BASE" && pwd)

PREFIX="$BASE"/install

BUILDS="$BASE"/builds

######################################################################

finish() {
    date -u +"%Y-%m-%d %H:%M:%S %Z"
    stop=$(date +%s)
    bt=$(( stop - start ))
    echo "completed in $bt seconds."
}

# error handling function
failure() {
    echo
    echo "##################################################"
    echo "### nightly build FAILURE"
    echo "##################################################"

    finish
}

######################################################################

# set the trap
trap failure EXIT

######################################################################

echo
echo "##################################################"
echo "### spica Pipeline nightly build"

date -u +"%Y-%m-%d %H:%M:%S %Z"
start=$(date +%s)
printenv | sort

cd "$BASE"

echo
echo "##################################################"
echo "### lal source update..."

git clone git://ligo-vcs.phys.uwm.edu/lal.git || true
cd "$BASE"/lal
git pull

echo
echo "##################################################"
echo "### lal configure..."

LAL="$PREFIX"
./00boot
./configure --prefix="$LAL"

echo
echo "##################################################"
echo "### lal make..."

make

echo
echo "##################################################"
echo "### lal install..."

make install

echo
echo "##################################################"
echo "### spica source update..."

cd "$BASE"
svn checkout https://geco.phys.columbia.edu/spica/svn/trunk spica
cd "$BASE"/spica

echo
echo "##################################################"
echo "### make clean..."

if [ -f Makefile ] ; then
    make clean
fi

echo
echo "##################################################"
echo "### configure..."

export MATLAB
export LAL
export PREFIX
./configure

echo
echo "##################################################"
echo "### spica make source tarball..."

make NAME=spica-nightly OUTDIR="$BUILDS" tarball

echo
echo "##################################################"
echo "### spica make..."

make

echo
echo "##################################################"
echo "### spica make binary release..."

make NAME=spica-nightly OUTDIR="$BUILDS" binary-release

echo
echo "##################################################"
echo "### spica make install..."

make install

######################################################################

# unset the trap
trap - EXIT

echo
echo "##################################################"
echo "### nightly build SUCCESS!!"
echo "##################################################"

finish