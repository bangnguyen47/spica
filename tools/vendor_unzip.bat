@ECHO off
REM
REM /*
REM  * Copyright (C) 2009 Pham Cong Dinh
REM  *
REM  * This file is part of Spica.
REM  *
REM  * This is free software; you can redistribute it and/or modify it
REM  * under the terms of the GNU Lesser General Public License as
REM  * published by the Free Software Foundation; either version 3 of
REM  * the License, or (at your option) any later version.
REM  *
REM  * This software is distributed in the hope that it will be useful,
REM  * but WITHOUT ANY WARRANTY; without even the implied warranty of
REM  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
REM  * Lesser General Public License for more details.
REM  *
REM  * You should have received a copy of the GNU Lesser General Public
REM  * License along with this software; if not, write to the Free
REM  * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
REM  * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
REM  */
ECHO.
ECHO ----------------------- 
ECHO @name   Spica Framework
ECHO @author pcdinh
ECHO @since  June 02, 2009
ECHO -----------------------
ECHO.
ECHO Unzip bundled third party libraries
ECHO -----------------------------------

setlocal
REM CALL which.bat unzip.exe > which.tmp 
REM SET /p which= < which.tmp
REM DEL which.tmp

FOR /F "Tokens=2" %%I IN ('which.bat unzip.exe') DO SET which=%%I

IF NOT "%which%" == "" (
  ECHO.
  ECHO Unzip SyntaxHighLighter in JavaScript
  unzip ./vendor/syntaxhighlighter.zip -d ../public/spica/
  ECHO.
  ECHO Unzip JQuery Core
  unzip ./vendor/jquery.min.zip -d ../public/spica/js/
  ECHO.
  ECHO Unzip SwiftMailer 4.0
  unzip ./vendor/Swift.zip -d ../library/
) ELSE (
  ECHO You need to install Unzip program that can be downloaded from http://info-zip.org
)  
endlocal
PAUSE