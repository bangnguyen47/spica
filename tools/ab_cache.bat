@ECHO off
REM
ECHO.
ECHO ----------------------- 
ECHO @name   Spica Framework
ECHO @author pcdinh
ECHO @since  March 14, 2009
ECHO -----------------------
ECHO .
ECHO Test cache locking algorithm
ECHO ----------------------------
ab -n30000 -c60 http://localhost/repos/spica/trunk/sample/index/cache/
PAUSE