<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Command line tool to create a new application in Spica.
 *
 * @category   spica
 * @package    tools
 * @subpackage cmd
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      August 03, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: create-app.php 1869 2011-01-07 18:55:25Z pcdinh $
 */

$currentDir = dirname(__FILE__);

include_once $currentDir.'/_includes.php';

array_shift($argv);

if (false === isset($argv[0]))
{
    echo "You must specify application name. ".PHP_EOL;
    echo "Usage: ./create-app.php app_name ".PHP_EOL;
    echo "OR ./create-app.php app_name package_name ".PHP_EOL;
    echo "OR ./create-app.php app_name package_name theme_name";
    exit();
}

if (false === isset($argv[1]))
{
    echo "Default package name will be used: base ".PHP_EOL;
    $packageName = 'default';
}
else
{
    $packageName = trim($argv[1]);
}

if (false === isset($argv[2]))
{
    echo "Default theme name will be used: default ".PHP_EOL;
    $themeName = 'default';
}
else
{
    $themeName = trim($argv[2]);
}

$appName = trim($argv[0]);

$tool = new ApplicationTools();
$tool->setApplicationName($appName);
$tool->setThemeName($themeName);
$tool->setPackageName($packageName);

echo PHP_EOL;
echo "Create base directory for the application named '$appName'. ".PHP_EOL;
echo '-------------------------------------------------------------------'.PHP_EOL;
echo PHP_EOL;

$tool->createAppDirs();

echo "Starting to create directory structure for theme named $themeName ".PHP_EOL;
echo '-------------------------------------------------------------------'.PHP_EOL;
echo PHP_EOL;

$tool->createThemeAppDirs();

echo "Starting to create static media directory structure for theme named $themeName ".PHP_EOL;
echo '-------------------------------------------------------------------'.PHP_EOL;
echo PHP_EOL;

$tool->createThemeStaticDirs();

echo PHP_EOL;
echo PHP_EOL;
echo "\nCompleted. Please take a look at apps/$appName/view/theme/$themeName ".PHP_EOL;

?>