#!/bin/bash
# /*
#  * Copyright (C) 2009 Pham Cong Dinh
#  *
#  * This file is part of Spica.
#  *
#  * This is free software; you can redistribute it and/or modify it
#  * under the terms of the GNU Lesser General Public License as
#  * published by the Free Software Foundation; either version 3 of
#  * the License, or (at your option) any later version.
#  *
#  * This software is distributed in the hope that it will be useful,
#  * but WITHOUT ANY WARRANTY; without even the implied warranty of
#  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  * Lesser General Public License for more details.
#  *
#  * You should have received a copy of the GNU Lesser General Public
#  * License along with this software; if not, write to the Free
#  * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
#  */
echo
echo -----------------------
echo @name   Spica Framework
echo @author pcdinh
echo @since  March 14, 2009
echo ----------------------- 
echo 
echo chmod all files to 755
echo -----------------------------------
cd ..
find . -type d -exec chmod 755 {} \;
cd tools