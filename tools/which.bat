@echo off

setlocal
set FOUNDFLAG=0

:# Add the current directory to the path for duration of this program only.
set PATH=%PATH%;.

if exist "%~$PATH:1" ( 
  echo Found at %~$PATH:1  && set FOUNDFLAG=1
)

:# We want to look for each of the possible extensions that could be executed.
:# Replace . with space.
set SPEC=%PATHEXT:.= %

:# Replace ; with nothing.
set SPEC=%SPEC:;=%

:#Iterate across extensions, checking for each one on the path.
for %%a in (%SPEC%) do call :CHECK %1.%%a

goto :CLEANUP

:CHECK

if exist "%~$PATH:1" ( echo Found at %~$PATH:1 && set FOUNDFLAG=1 )

goto :EOF

:CLEANUP
REM if not %FOUNDFLAG% == 1 echo No runnable file found for %1
endlocal
goto :EOF