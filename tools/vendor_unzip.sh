#!/bin/bash
# /*
#  * Copyright (C) 2009 Pham Cong Dinh
#  *
#  * This file is part of Spica.
#  *
#  * This is free software; you can redistribute it and/or modify it
#  * under the terms of the GNU Lesser General Public License as
#  * published by the Free Software Foundation; either version 3 of
#  * the License, or (at your option) any later version.
#  *
#  * This software is distributed in the hope that it will be useful,
#  * but WITHOUT ANY WARRANTY; without even the implied warranty of
#  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  * Lesser General Public License for more details.
#  *
#  * You should have received a copy of the GNU Lesser General Public
#  * License along with this software; if not, write to the Free
#  * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
#  */
echo
echo -----------------------
echo @name   Spica Framework
echo @author pcdinh
echo @since  March 14, 2009
echo ----------------------- 
echo 
echo Unzip bundled third party libraries
echo -----------------------------------
echo 
echo Unzip SyntaxHighLighter in JavaScript
unzip ./vendor/syntaxhighlighter.zip -d ../public/spica/
echo 
echo Unzip JQuery Core 1.3.2
unzip ./vendor/jquery.min.zip -d ../public/spica/js/
echo 
echo Unzip SwiftMailer 4.0
unzip ./vendor/Swift.zip -d ../library/
echo Unzip PHPUnit 3.4
unzip ../tests/PHPUnit.zip -d ../tests/
echo "Done"