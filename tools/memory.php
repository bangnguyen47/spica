<?php

// Web server process name
$wsProc = 'httpd';
$command = "ps -ef | " . 
           "grep " . escapeshellarg($wsProc) . " | " . 
           "grep -v ^root | " . 
           "awk '{ print $2 '} | " . 
           "xargs pmap -d | " . 
           "grep ^mapped: | " . 
           "awk '{ print $4 }' ";

exec($command, $processes, $return);

if ($return != 0) {
	echo "Error executing check, status code is $return\n";
	exit($return);
}

$avg = array_sum($processes) / count($processes);
printf("Average %s process private memory allocation: %0.2f kb\n", $wsProc, $avg);

?>