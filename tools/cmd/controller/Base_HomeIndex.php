<?php

/**
 * Sets of actions that serves every request to "home" module
 * and "Index" controller in "base" package.
 *
 * @category   {application name}
 * @package    {package name}
 * @author     {author} 
 * @since      {date}
 * @copyright  {copyright information}
 * @license    {Your licence here}
 * @version    $Id: Base_HomeIndex.php 1648 2009-12-30 19:25:45Z pcdinh $
 */
class Base_HomeIndex
{
    /**
     * Renders the "index" page of Base_HomeIndex controller.
     *
     * This method should not print anything out intentionally because
     * they will be logged instead of being rendered.
     *
     * @link   http://domain.com/home
     * @return void This kind of method should not return anything.
     */
    public function indexAction()
    {
        $theme = SpicaContext::getTheme();
        $theme->pageTitle = 'Welcome page - Spica Framework';
        SpicaResponse::setBody($theme->fetch('index'));
    }
}

?>