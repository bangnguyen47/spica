<?php

/*
 * Copyright (C) 2009 - 2011 Pham Cong Dinh
 *
 * This file is part of Spica.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Sets of functions or methods that are executed to override the default settings
 * to handle final exception.
 *
 * This file is loaded in the delaration of SpicaContext::catchException($e).
 * Therefore, when SpicaContext::catchException($e) is called, $e variable is
 * available by default. It is the exception object thrown and we need to catch
 * and handle it.
 *
 * @category   spica
 * @package    config
 * @subpackage exceptionhook
 * @author     Pham Cong Dinh <pcdinh at phpvietnam dot net>
 * @since      Version 0.3
 * @since      April 03, 2009
 * @copyright  Pham Cong Dinh (http://www.phpvietnam.net)
 * @license    http://www.gnu.org/licenses/lgpl-3.0.txt
 * @version    $Id: exception.php 1869 2011-01-07 18:55:25Z pcdinh $
 */

// Set custm exception handler
// SpicaContext::setExceptionHandler(new SpicaCMSExceptionHandler());

?>