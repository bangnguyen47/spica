-- Spica table schema for Spica documentation 
-- @since  May 15, 2009
-- @author pcdinh
create database spicadocs;

create table notes( 
  note_id INT UNSIGNED NOT NULL AUTO_INCREMENT , 
  note_content text NOT NULL , 
  page_id INT NOT NULL , 
  note_dtime DATETIME NOT NULL , 
  note_username VARCHAR(60) NOT NULL , 
  note_status TINYINT NOT NULL , 
  note_reply_to INT , PRIMARY KEY (note_id));

DROP TABLE IF EXISTS manual_pages;

CREATE TABLE manual_pages( 
  page_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  page_section VARCHAR(60) NOT NULL , 
  page_name VARCHAR(60) NOT NULL , 
  page_title VARCHAR(60) NULL , 
  page_header1 VARCHAR(60) NULL ,  
  page_content VARCHAR(60) NULL ,  
  page_desc VARCHAR(360) NULL , 
  page_dtime DATETIME NOT NULL , 
  page_status TINYINT NOT NULL,
  spica_version VARCHAR(10)
);

INSERT INTO manual_pages (page_section, page_name, page_title, page_dtime, page_status, spica_version) 
VALUES ('guide', 'tutorial_helloworld', 'Tutorial - Says Hello World', CURRENT_TIMESTAMP, 1, '1.0');

INSERT INTO manual_pages (page_section, page_name, page_title, page_dtime, page_status, spica_version) 
VALUES ('guide', 'tutorial_simplepage', 'Tutorial - Creates a simple page using template', CURRENT_TIMESTAMP, 1, '1.0');

INSERT INTO manual_pages (page_section, page_name, page_title, page_dtime, page_status, spica_version) 
VALUES ('guide', 'tutorial_simpletheme', 'Tutorial - Creates a simple theme', CURRENT_TIMESTAMP, 1, '1.0');

INSERT INTO manual_pages (page_section, page_name, page_title, page_dtime, page_status, spica_version) 
VALUES ('howto', 'tutorial_advancedtheme', 'Tutorial - Details on Spica theme architecture', CURRENT_TIMESTAMP, 1, '1.0');